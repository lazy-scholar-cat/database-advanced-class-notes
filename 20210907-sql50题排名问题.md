# sql50题排名问题
# 按各科成绩进行排序，并显示排名， Score 重复时合并名次
## select CourseId,Score,DENSE_RANK()over( order by score desc) from StudentCourseScore group by CourseId,Score

# 查询学生的总成绩，并进行排名，总分重复时保留名次空缺
## select sum(Score),StudentId,rank() over( order by sum(score) desc ) from StudentCourseScore group by StudentId

# 查询学生的总成绩，并进行排名，总分重复时不保留名次空缺
## select StudentId,sum(score),ROW_NUMBER() over( order by sum(score) desc) from StudentCourseScore group by StudentId

# 查询各科成绩最高分、最低分和平均分：
## select CourseId,max(Score)最高, min(Score)最低, avg(Score)平均分 from StudentCourseScore group by CourseId