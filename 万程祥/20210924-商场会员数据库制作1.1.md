# 20210924-商场会员数据库制作1.1

```
/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     2021/9/26 11:08:43                           */
/*==============================================================*/


if exists (select 1
            from  sysobjects
           where  id = object_id('MemberActivityInfo')
            and   type = 'U')
   drop table MemberActivityInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberCard')
            and   type = 'U')
   drop table MemberCard
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberCardLossReortInfo')
            and   type = 'U')
   drop table MemberCardLossReortInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberCardTypeInfo')
            and   type = 'U')
   drop table MemberCardTypeInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberInfo')
            and   type = 'U')
   drop table MemberInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberLvelInfo')
            and   type = 'U')
   drop table MemberLvelInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberPointsExchangeInfo')
            and   type = 'U')
   drop table MemberPointsExchangeInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberPointsInfo')
            and   type = 'U')
   drop table MemberPointsInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberPointsRuleInfo')
            and   type = 'U')
   drop table MemberPointsRuleInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SMInfo')
            and   type = 'U')
   drop table SMInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ShopsInfo')
            and   type = 'U')
   drop table ShopsInfo
go

/*==============================================================*/
/* Table: MemberActivityInfo                                    */
/*==============================================================*/
create table MemberActivityInfo (
   MemberActivityId     int                  not null,
   MemberId             int                  null,
   SMId                 int                  null,
   ActivityInfo         nvarchar(10)         null,
   ActivityTime         nvarchar(20)         null,
   constraint PK_MEMBERACTIVITYINFO primary key (MemberActivityId)
)
go

/*==============================================================*/
/* Table: MemberCard                                            */
/*==============================================================*/
create table MemberCard (
   MemberCardId         int                  not null,
   MembereCardNum       nvarchar(30)         null,
   MembereCardType      nvarchar(30)         null,
   SMId                 int                  null,
   MemberId             int                  null,
   constraint PK_MEMBERCARD primary key (MemberCardId)
)
go

/*==============================================================*/
/* Table: MemberCardLossReortInfo                               */
/*==============================================================*/
create table MemberCardLossReortInfo (
   LoseId               int                  not null,
   MemberId             int                  null,
   SMId                 int                  null,
   LoseTime             datetime             null,
   constraint PK_MEMBERCARDLOSSREORTINFO primary key (LoseId)
)
go

/*==============================================================*/
/* Table: MemberCardTypeInfo                                    */
/*==============================================================*/
create table MemberCardTypeInfo (
   MemberCardTypeId     int                  not null,
   MemberCardNum        nvarchar(10)         null,
   MemberCardType       nvarchar(10)         null,
   MemberId             int                  null,
   constraint PK_MEMBERCARDTYPEINFO primary key (MemberCardTypeId)
)
go

/*==============================================================*/
/* Table: MemberInfo                                            */
/*==============================================================*/
create table MemberInfo (
   MemberId             int                  not null,
   MemberName           nvarchar(20)         null,
   Sex                  nvarchar(2)          null,
   Birthday             nvarchar(10)         null,
   Tel                  nvarchar(20)         null,
   IdCardNum            nvarchar(20)         null,
   Address              nvarchar(80)         null,
   constraint PK_MEMBERINFO primary key (MemberId)
)
go

/*==============================================================*/
/* Table: MemberLvelInfo                                        */
/*==============================================================*/
create table MemberLvelInfo (
   MemberLevelId        int                  not null,
   MemberId             int                  null,
   MemberLevel          nvarchar(10)         null,
   constraint PK_MEMBERLVELINFO primary key (MemberLevelId)
)
go

/*==============================================================*/
/* Table: MemberPointsExchangeInfo                              */
/*==============================================================*/
create table MemberPointsExchangeInfo (
   PointsExchange       int                  not null,
   MemberId             int                  null,
   SMId                 int                  null,
   ExchangeTime         datetime             null,
   constraint PK_MEMBERPOINTSEXCHANGEINFO primary key (PointsExchange)
)
go

/*==============================================================*/
/* Table: MemberPointsInfo                                      */
/*==============================================================*/
create table MemberPointsInfo (
   MemberPointsId       int                  not null,
   MemberId             int                  null,
   MemberCardNum        nvarchar(10)         null,
   SMId                 int                  null,
   constraint PK_MEMBERPOINTSINFO primary key (MemberPointsId)
)
go

/*==============================================================*/
/* Table: MemberPointsRuleInfo                                  */
/*==============================================================*/
create table MemberPointsRuleInfo (
   MemberPointsRuleId   int                  not null,
   MemberPointsRule     varchar(80)          null,
   SMId                 int                  null,
   constraint PK_MEMBERPOINTSRULEINFO primary key (MemberPointsRuleId)
)
go

/*==============================================================*/
/* Table: SMInfo                                                */
/*==============================================================*/
create table SMInfo (
   SMId                 int                  not null,
   SMName               nvarchar(10)         null,
   SMAbbreviation       nvarchar(10)         null,
   SMAddress            nvarchar(80)         null,
   SMIps                nvarchar(10)         null,
   IpsTel               nvarchar(30)         null,
   constraint PK_SMINFO primary key (SMId)
)
go

/*==============================================================*/
/* Table: ShopsInfo                                             */
/*==============================================================*/
create table ShopsInfo (
   ShopsId              int                  not null,
   ShopesName           nvarchar(30)         null,
   SMId                 int                  null,
   ShopsPosition        nvarchar(30)         null,
   ShopesOperator       nvarchar(30)         null,
   OperatorTel          nvarchar(20)         null,
   constraint PK_SHOPSINFO primary key (ShopsId)
)
go

```