# 20211015-SQL server游标的使用

```

游标总是与一条Transact-SQL SELECT语句相关联，它使用户可逐行访问由SQL Server返回的结果集。
游标包括以下两个部分：
⑴ 游标结果集（Cursor Result Set）：由定义该游标的SELECT语句返回的行的集合。
⑵ 游标位置（Cursor Position）：指向这个行集合中某一行的当前指针。
@@fetch_status是MicroSoft SQL SERVER的一个全局变量

其值有以下三种，分别表示三种不同含义：【返回类型integer】
0 FETCH 语句成功
-1 FETCH 语句失败或此行不在结果集中
-2 被提取的行不存在
@@fetch_status值的改变是通过fetch next from实现的
“FETCH NEXT FROM Cursor”


0	FETCH 语句成功。
-1	FETCH 语句失败或行不在结果集中。
-2	提取的行不存在。
-9	游标未执行提取操作。

```
``` sql


-- 定义游标
declare cur_ForStudentInfo cursor
for
select * from StudentInfo


--打开游标，准备使用
open cur_ForStudentInfo 

--定义变量，用于接收每一行的数据
declare @id int  , @studentcode nvarchar(10), @name nvarchar(80) , @birthday date
,@sex nvarchar(2),@classid int

-- 先获取一条，以使@@fetch_status状态初始化
fetch next from cur_ForStudentInfo 
into @id , @studentcode,@name ,@birthday,@sex,@classid

select @@FETCH_STATUS

-- 真正遍历记录的语句结构
while @@FETCH_STATUS=0
begin 
 select @id,@name,@birthday
 fetch next from cur_ForStudentInfo
 into @id , @studentcode,@name ,@birthday,@sex,@classid
 end

 -- 关闭游标
close cur_ForStudentInfo
-- 释放游标
deallocate cur_ForStudentInfo

```