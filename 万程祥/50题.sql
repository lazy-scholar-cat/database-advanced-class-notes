
-- ��ϰ��Ŀ��

-- 1.��ѯ"��ѧ "�γ̱�" ���� "�γ̳ɼ��ߵ�ѧ������Ϣ���γ̷���
select c.*  , a.score chinese , b.score math from StudentCourseScore a
inner join StudentCourseScore b on a.StudentId=b.StudentId
inner join StudentInfo c on b.StudentId=c.Id
where b.Score>a.Score and b.CourseId=2 and a.CourseId=1

-- 1.1 ��ѯͬʱ����" ��ѧ "�γ̺�" ���� "�γ̵����
select c.*  , a.score chinese , b.score math from StudentCourseScore a
inner join StudentCourseScore b on a.StudentId=b.StudentId
inner join StudentInfo c on b.StudentId=c.Id
where  b.CourseId=2 and a.CourseId=1

-- 1.2 ��ѯ����" ��ѧ "�γ̵����ܲ�����" ���� "�γ̵����(������ʱ��ʾΪ null )
select * from 
(select a.* from StudentCourseScore a where CourseId=2) math
left join  (select b.* from StudentCourseScore b where CourseId=1) chinese
on math.StudentId=chinese.StudentId

-- 1.3 ��ѯ������" ��ѧ "�γ̵�����" ���� "�γ̵����
select * from 
(select a.* from StudentCourseScore a where CourseId=2) math
right join  (select b.* from StudentCourseScore b where CourseId=1) chinese
on math.StudentId=chinese.StudentId

-- 2.��ѯ ƽ���ɼ����ڵ��� 60 �ֵ�ͬѧ�� ѧ����� �� ѧ������ �� ƽ���ɼ�
select a.StudentName , b.StudentId , AVG(b.score) from StudentInfo a 
inner join StudentCourseScore b on a.Id=b.StudentId group by StudentId,a.StudentName,b.StudentId
having AVG(b.score)>=60

-- 3.��ѯ�� �ɼ� �����ڳɼ���ѧ����Ϣ
select * from (select * from StudentCourseScore ) A left join
StudentInfo on A.StudentId=StudentInfo.Id

-- 4.��ѯ����ͬѧ��ѧ����š�ѧ��������ѡ�����������пγ̵��ܳɼ�(û�ɼ�����ʾΪ null )
select  *from (select StudentId ,count(CourseId) ����,sum(Score) �ܳɼ� from  StudentCourseScore  group by StudentId)   a
right join (select StudentName,id from StudentInfo ) b on a.StudentId=b.id

-- 4.1 ���гɼ���ѧ����Ϣ
select  *from (select StudentId ,count(CourseId) ����,sum(Score) �ܳɼ� from  StudentCourseScore  group by StudentId)   a
inner join (select StudentName,id from StudentInfo ) b on a.StudentId=b.id

-------------------------------------------------
select   StudentId ,count(CourseId) ����,sum(Score) �ܳɼ� from  StudentCourseScore    a
inner join StudentInfo  b on a.StudentId=b.id
group by StudentId


-- 5.��ѯ�������ʦ������
select count(*) from Teachers where  TeacherName like '��%' 

-- 6.��ѯѧ������������ʦ�ڿε�ͬѧ����Ϣ
select * from StudentInfo inner join 
(select StudentId from StudentCourseScore where CourseId = 
(select Id from  CourseInfo where TeacherId =
(select TeacherId from CourseInfo where TeacherId =  (select Id from Teachers where TeacherName = '����')))) a
on StudentInfo.Id = a.StudentId

-- 7.��ѯû��ѧȫ���пγ̵�ͬѧ����Ϣ

-- 8.��ѯ������һ�ſ���ѧ��Ϊ" 01 "��ͬѧ��ѧ��ͬ��ͬѧ����Ϣ

-- 9.��ѯ��" 01 "�ŵ�ͬѧѧϰ�Ŀγ� ��ȫ��ͬ������ͬѧ����Ϣ

-- 10.��ѯûѧ��"����"��ʦ���ڵ���һ�ſγ̵�ѧ������

-- 11.��ѯ���ż������ϲ�����γ̵�ͬѧ��ѧ�ţ���������ƽ���ɼ�

-- 12.����" ��ѧ "�γ̷���С�� 60���������������е�ѧ����Ϣ

-- 13.��ƽ���ɼ��Ӹߵ�����ʾ����ѧ�������пγ̵ĳɼ��Լ�ƽ���ɼ�

-- 14.��ѯ���Ƴɼ���߷֡���ͷֺ�ƽ���֣�

-- 15.��������ʽ��ʾ���γ� ID���γ� name����߷֣���ͷ֣�ƽ���֣������ʣ��е��ʣ������ʣ�������
/*
	����Ϊ>=60���е�Ϊ��70-80������Ϊ��80-90������Ϊ��>=90

	Ҫ������γ̺ź�ѡ����������ѯ����������������У���������ͬ�����γ̺���������

	�����Ƴɼ��������򣬲���ʾ������ Score �ظ�ʱ�������ο�ȱ
*/	
	
-- 15.1 �����Ƴɼ��������򣬲���ʾ������ Score �ظ�ʱ�ϲ�����
select * ,DENSE_RANK() over(partition by CourseId order by Score desc)
from StudentCourseScore

-- 16.��ѯѧ�����ܳɼ����������������ܷ��ظ�ʱ�������ο�ȱ
select StudentId , sum(Score)�ܷ� , RANK() over(order by sum(Score) desc) ����
from StudentCourseScore 
group by StudentId

-- 16.1 ��ѯѧ�����ܳɼ����������������ܷ��ظ�ʱ���������ο�ȱ
select StudentId , sum(Score) , Row_number() over(order by sum(Score) desc)
from StudentCourseScore
group by StudentId

-- 17.ͳ�Ƹ��Ƴɼ����������������γ̱�ţ��γ����ƣ�
-- [100-85]��[85-70]��[70-60]��[60-0] ����ռ�ٷֱ�

select CourseId , sum(a1)[100-85],sum(a2) [100-85],sum(a3)[100-85],sum(a4) [100-85],
sum(a1)+sum(a2)+sum(a3)+sum(a4)����,
convert (nvarchar(50),(convert (decimal(18,2) , sum(a1)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
convert (nvarchar(50),(convert (decimal(18,2) ,sum(a2)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
convert (nvarchar(50),(convert (decimal(18,2) , sum(a3)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
convert (nvarchar(50),(convert (decimal(18,2) , sum(a4)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%'
from
(
	select  CourseId ,
	(case when Score>=85 then 1 else 0 end) a1,
	(case when Score<85 and Score >=70  then 1 else 0 end) a2,
	(case when Score<70 and Score >=60  then 1 else 0 end) a3,
	(case when Score<60 and Score >=0  then 1 else 0 end) a4
	from StudentCourseScore
	where CourseId=1
) a
group by CourseId
union 
select CourseId , sum(a1)[100-85],sum(a2) [100-85],sum(a3)[100-85],sum(a4) [100-85],
sum(a1)+sum(a2)+sum(a3)+sum(a4)����,
convert (nvarchar(50),(convert (decimal(18,2) , sum(a1)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
convert (nvarchar(50),(convert (decimal(18,2) ,sum(a2)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
convert (nvarchar(50),(convert (decimal(18,2) , sum(a3)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
convert (nvarchar(50),(convert (decimal(18,2) , sum(a4)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%'
from
(
	select  CourseId ,
	(case when Score>=85 then 1 else 0 end) a1,
	(case when Score<85 and Score >=70  then 1 else 0 end) a2,
	(case when Score<70 and Score >=60  then 1 else 0 end) a3,
	(case when Score<60 and Score >=0  then 1 else 0 end) a4
	from StudentCourseScore
	where CourseId=2
) a
group by CourseId
union 
select CourseId , sum(a1)[100-85],sum(a2) [100-85],sum(a3)[100-85],sum(a4) [100-85],
sum(a1)+sum(a2)+sum(a3)+sum(a4)����,
convert (nvarchar(50),(convert (decimal(18,2) , sum(a1)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
convert (nvarchar(50),(convert (decimal(18,2) ,sum(a2)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
convert (nvarchar(50),(convert (decimal(18,2) , sum(a3)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
convert (nvarchar(50),(convert (decimal(18,2) , sum(a4)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%'
from
(
	select  CourseId ,
	(case when Score>=85 then 1 else 0 end) a1,
	(case when Score<85 and Score >=70  then 1 else 0 end) a2,
	(case when Score<70 and Score >=60  then 1 else 0 end) a3,
	(case when Score<60 and Score >=0  then 1 else 0 end) a4
	from StudentCourseScore
	where CourseId=3
) a
group by CourseId
union 
select CourseId , sum(a1)[100-85],sum(a2) [100-85],sum(a3)[100-85],sum(a4) [100-85],
sum(a1)+sum(a2)+sum(a3)+sum(a4)����,
convert (nvarchar(50),(convert (decimal(18,2) , sum(a1)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
convert (nvarchar(50),(convert (decimal(18,2) ,sum(a2)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
convert (nvarchar(50),(convert (decimal(18,2) , sum(a3)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
convert (nvarchar(50),(convert (decimal(18,2) , sum(a4)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%'
from
(
	select  CourseId ,
	(case when Score>=85 then 1 else 0 end) a1,
	(case when Score<85 and Score >=70  then 1 else 0 end) a2,
	(case when Score<70 and Score >=60  then 1 else 0 end) a3,
	(case when Score<60 and Score >=0  then 1 else 0 end) a4
	from StudentCourseScore
	where CourseId=4
) a
group by CourseId

-- 18.��ѯ���Ƴɼ�ǰ�����ļ�¼

-- 19.��ѯÿ�ſγ̱�ѡ�޵�ѧ����

-- 20.��ѯ��ֻѡ�����ſγ̵�ѧ��ѧ�ź�����

-- 21.��ѯ������Ů������

-- 22.��ѯ�����к��С��硹�ֵ�ѧ����Ϣ

-- 23.��ѯͬ��ͬ��ѧ����������ͳ��ͬ������

-- 24.��ѯ 1990 �������ѧ������

-- 25.��ѯÿ�ſγ̵�ƽ���ɼ��������ƽ���ɼ��������У�ƽ���ɼ���ͬʱ�����γ̱����������

-- 26.��ѯƽ���ɼ����ڵ��� 85 ������ѧ����ѧ�š�������ƽ���ɼ�

-- 27.��ѯ�γ�����Ϊ����ѧ�����ҷ������� 60 ��ѧ�������ͷ���

-- 28.��ѯ����ѧ���Ŀγ̼��������������ѧ��û�ɼ���ûѡ�ε������

-- 29.��ѯ�κ�һ�ſγ̳ɼ��� 70 �����ϵ��������γ����ƺͷ���

-- 30.��ѯ������Ŀγ�

-- 31.��ѯ�γ̱��Ϊ 01 �ҿγ̳ɼ��� 80 �����ϵ�ѧ����ѧ�ź�����

-- 32.��ÿ�ſγ̵�ѧ������

-- 33.�ɼ����ظ�����ѯѡ�ޡ���������ʦ���ڿγ̵�ѧ���У��ɼ���ߵ�ѧ����Ϣ����ɼ�

--34.�ɼ����ظ�������£���ѯѡ�ޡ���������ʦ���ڿγ̵�ѧ���У��ɼ���ߵ�ѧ����Ϣ����ɼ�
select* from StudentInfo
select* from StudentCourseScore


-- 35.��ѯ��ͬ�γ̳ɼ���ͬ��ѧ����ѧ����š��γ̱�š�ѧ���ɼ�
select * from StudentCourseScore
select * from (select StudentId,CourseId,Score from StudentCourseScore where CourseId=1) a 
inner join (select StudentId,CourseId,Score from StudentCourseScore where CourseId=2) b on a.Score=b.Score
inner join (select StudentId,CourseId,Score from StudentCourseScore where CourseId=3) c on b.Score=c.Score

-- 36.��ѯÿ�Ź��ɼ���õ�ǰ����

-- 37.ͳ��ÿ�ſγ̵�ѧ��ѡ������������ 5 �˵Ŀγ̲�ͳ�ƣ���

-- 38.��������ѡ�����ſγ̵�ѧ��ѧ��

-- 39.��ѯѡ����ȫ���γ̵�ѧ����Ϣ

-- 40.��ѯ��ѧ�������䣬ֻ���������

-- 41.���ճ����������㣬��ǰ���� < �������µ������������һ

-- 42.��ѯ���ܹ����յ�ѧ��

-- 43.��ѯ���ܹ����յ�ѧ��

-- 44.��ѯ���¹����յ�ѧ��

-- 45.��ѯ���¹����յ�ѧ��
