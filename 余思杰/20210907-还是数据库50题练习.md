# 天气：🌞

## 数据库50题第17题解法：
~~~

;with CTE_A
as
(
	select CourseId,
	(case when Score >= 85 then 1 else 0 end) a85,
	(case when Score >= 70 and Score <85 then 1 else 0 end) a70,
	(case when Score >= 60 and Score < 70 then 1 else 0 end) a60,
	(case when Score >= 0  and Score <60 then 1 else 0 end) a00
	from StudentCourseScore
),CTE_B as
(
	select CourseId 课程编号,sum(a85) a85,sum(a70) a70,sum(a60) a60,sum(a00) a00,
	sum(a85)+sum(a70)+sum(a60)+sum(a00) TotalCount from CTE_A
	group by CourseId
)select a.课程编号,b.CourseName
,CONVERT(nvarchar(80),CONVERT(decimal(18,2),a85*100.0/TotalCount))+'%' [100-85]
,CONVERT(nvarchar(80),CONVERT(decimal(18,2),a70*100.0/TotalCount))+'%' [85-70]
,CONVERT(nvarchar(80),CONVERT(decimal(18,2),a60*100.0/TotalCount))+'%' [70-60]
,CONVERT(nvarchar(80),CONVERT(decimal(18,2),a00*100.0/TotalCount))+'%' [60-0]
from CTE_B a
left join CourseInfo b on a.课程编号 = b.Id

~~~
![](./imgs/20210907-SQL50题练习/Num_17.png)

</br>

## 排名练习:
![](./imgs/20210907-SQL50题练习/Num_15.png)