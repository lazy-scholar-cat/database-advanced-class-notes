# 递归函数

~~~sql
create database ShoppingMail
go

use ShoppingMail
go

create table Category
(
	id int primary key identity(1,1),
	CategoryName nvarchar(80) not null,
	ParentId int not null default(0) 
)

go

insert into Category(CategoryName) values('服装')
insert into Category(CategoryName) values('手机')
insert into Category(CategoryName) values('家电')
insert into Category(CategoryName) values('食品')
insert into Category(CategoryName) values('电脑')

go

insert into Category(CategoryName,ParentId) values
('男装',(select top 1 id from Category where CategoryName = '服装'))

insert into Category(CategoryName,ParentId) values
('女装',(select top 1 id from Category where CategoryName = '服装'))

insert into Category(CategoryName,ParentId) values
('童装',(select top 1 id from Category where CategoryName = '服装'))

go

;with CTE_A
as
(
	select id,CategoryName,ParentId from Category where CategoryName = '服装'
	union all
	select c.id,c.CategoryName,c.ParentId from Category c,CTE_A a
	where c.ParentId = a.id
)select * from CTE_A

go

insert into Category(CategoryName,ParentId) values 
('休闲裤',(select top 1 id from Category where CategoryName = '男装'))
insert into Category(CategoryName,ParentId) values 
('运动裤',(select top 1 id from Category where CategoryName = '男装'))
insert into Category(CategoryName,ParentId) values 
('牛仔裤',(select top 1 id from Category where CategoryName = '男装'))

go

;with CTE_B
as
(
	select id,CategoryName,ParentId,1 Level from Category where CategoryName = '服装'
	union all
	select c.id,c.CategoryName,c.ParentId,b.Level+1 Level from Category c,CTE_B b
	where c.ParentId = b.id
)select * from CTE_B


go
~~~

# 多语句表值函数

## 语法
~~~sql
create function <函数名称>
(
    [
        @aaa 数据类型[=默认值],
        @bbb 数据类型[=默认值]
    ]
)
returns @res table
(
    ccc 数据类型，
    ddd 数据类型
)
as
begin 
    给表变量插入数据
    return
end
~~~

# 两者结合
~~~sql
create function fn_T_A
(@name nvarchar(80))
returns @res table(Id int,Name nvarchar(80),PId int)
as
begin
	;with CTE_C
	as
	(
		select id,CategoryName,ParentId from Category where CategoryName = @name
		union all
		select c.id,c.CategoryName,c.ParentId from Category c,CTE_C a
		where c.ParentId = a.id
	)insert into @res select * from CTE_C 

	return
end

go

select * from dbo.fn_T_A('服装')
~~~

# 小结
~~~
单语句表值函数和多语句表值函数，实际上的区别只在于后者有一个表类型的变量，在后者定义的函数内部，可以任意的对这个变量
进行增删改查的操作(可以操作其中的数据，但是不能对表变量进行删除等操作)；单语句表值函数，并没有一个表类型的变量，它最后只
是返回一个表，也可以理解为其整个的过程就是一个的select语句（并不只是说，只能有一个select语句，比如使用CTE，就不只一个select）

另外：多语句表值函数中的表变量，类似于数据库当中的临时表
~~~