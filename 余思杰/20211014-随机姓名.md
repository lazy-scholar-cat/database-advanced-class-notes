~~~sql
create database SSS
go
use SSS
go


alter proc proc_FirstName
@name nvarchar(80) output
as
begin
	declare @str nvarchar(80) = '赵钱孙李周吴郑王冯陈褚卫蒋沈韩杨朱秦尤许何吕施张孔曹严华金魏陶姜戚谢邹喻柏水窦章云苏潘葛奚范彭郎鲁韦昌马苗凤花方俞任袁柳酆鲍史唐费廉岑薛雷贺倪汤滕殷罗毕郝邬安常乐于时傅皮卞齐康伍余元卜顾孟平黄和穆萧尹姚邵湛汪祁毛禹狄米贝明臧计伏成戴谈宋茅庞熊纪舒屈项祝董梁杜阮蓝闵席季麻强贾路娄危江童颜郭梅盛林刁钟徐邱骆高夏蔡田樊胡凌霍 虞万支柯昝管卢莫经房裘缪干解应宗丁宣贲邓郁单杭洪包诸左石崔吉钮龚程嵇邢滑裴陆荣翁荀羊於惠甄曲家封芮羿储靳汲邴糜松井段富巫乌焦巴弓牧隗山谷车侯宓蓬全郗班仰秋仲伊宫宁仇栾暴甘钭厉戎祖武符刘景詹束龙叶幸司韶郜黎蓟薄印宿白怀蒲邰从鄂索咸籍赖卓蔺屠蒙池乔阴鬱胥能苍双闻莘党翟谭贡劳逄姬申扶堵冉宰郦雍郤璩桑桂濮牛寿通边扈燕冀郏浦尚农温别庄晏柴瞿阎充慕连茹习宦艾鱼容向古易慎戈廖庾终暨居衡步都耿满弘匡国文寇广禄阙东欧殳沃利蔚越夔隆师巩厍聂晁勾敖融冷訾辛阚那简饶空曾毋沙乜养鞠须丰巢关蒯相查后荆红游竺权逯盖益桓公万俟司马上官欧阳夏侯诸葛闻人东方赫连皇甫尉迟公羊澹台公冶宗政濮阳淳于单于太叔申屠公孙仲孙轩辕令狐钟离宇文长孙慕容鲜于闾丘司徒司空丌官司寇仉督子车颛孙端木巫马公西漆雕乐正壤驷公良拓跋夹谷宰父谷梁晋楚闫法汝鄢涂钦 段干百里东郭南门呼延归海羊舌微生岳帅缑亢况郈有琴梁丘左丘东门西门商牟佘佴伯赏南宫墨哈谯笪年爱阳佟第五言福百家姓终'
	declare @randIndex int = rand()*len(@str)
	set @name = substring(@str,@randIndex,1)
end
go

declare @res nvarchar(80)
exec proc_FirstName @res output
select @res

go

alter proc proc_LastName
@name nvarchar(80) output
as
begin
	declare @str nvarchar(80) = '嘉哲俊博妍乐佳涵晨宇怡泽子凡悦思奕依浩泓彤冰媛凯伊淇淳一洁茹清吉源渊和函妤宜云琪菱宣沂健信欣可洋萍荣榕含佑明雄梅芝英义淑卿乾亦芬萱昊芸天岚昕尧鸿棋琳孜娟宸林乔琦丞安毅凌泉坤晴竹娴婕恒渝菁龄弘佩勋宁元栋盈江卓春晋逸沅倩昱绮海圣承民智棠容羚峰钰涓新莉恩羽妮旭维家泰诗谚阳彬书苓汉蔚坚茵耘喆国仑良裕融致富德易虹纲筠奇平蓓真之凰桦玫强村沛汶锋彦延庭霞冠益劭钧薇亭瀚桓东滢恬瑾达群茜先洲溢楠基轩月美心茗丹森学文'
	declare @randNameLen int = convert(int,rand()*2)+1 
	declare @randindex1 int,@randindex2 int
	-- 判断名字的数量为1则取单名；否则取双名
	if @randNameLen = 1 
		begin
			set @randindex1 = rand()*len(@str)
			set @name = substring(@str,@randindex1,1)
		end
	else
		begin
			set @randindex1 = rand()*len(@str)
			set @randindex2 = rand()*len(@str)
			set @name = substring(@str,@randindex1,1)+substring(@str,@randindex2,1)
		end
end
go

declare @res nvarchar(80)
exec proc_FirstName @res output
select @res

go

alter proc proc_GetFinalName
@count int = 100
as
begin
	set nocount on
	if OBJECT_ID('StudentScoreInfo','U') is not null
		begin
			drop table StudentScoreInfo
		end

	create table StudentScoreInfo
	(
		Id int primary key identity,
		Name nvarchar(80) not null,
		Score int not null default(90)
	)
	declare @name nvarchar(80),@i int=0,@firstname nvarchar(80),@lastname nvarchar(80)
	
	while( @i < @count )
	begin
		exec proc_FirstName @firstname output
		exec proc_LastName @lastname output
		set @name = @firstname+@lastname
		insert into StudentScoreInfo values (@name,floor(rand()*100))
		set @i = @i + 1
	end
	set nocount off
end
go
exec proc_GetFinalName 1000000

select * from StudentScoreInfo 
select count(*) from StudentScoreInfo

create index idx_time
on StudentScoreInfo(Name)

drop index idx_time on StudentScoreInfo

drop database SSS
~~~