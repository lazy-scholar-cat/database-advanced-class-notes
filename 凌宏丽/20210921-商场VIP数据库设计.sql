use master

go
create database VIP
go

use VIP
go


--商场信息表
create table Shopping
(
 SID int primary key identity(1,1) not null,
 SNum varchar(20) not null,
 ShoppingName nvarchar(20) not null,
 ShoppingJianC nvarchar(20),
 ShoppingAderess nvarchar(50) not null,
 ShoppingPeople nvarchar(20) not null,
 ShoppingPhone int not null
)

--插入信息
insert into Shopping values ('SM0001','万宝购物广场','万宝','龙岩','张三',0775)

select * from Shopping
--会员信息表
create table Member
(
 MID int primary key identity(1,1) not null,
 MNum varchar(50) not null,
 MemberName nvarchar(50) not null,
 MemberSex char(2) default '男' check(MemberSex='男' or MemberSex='女') not null,
 MemberDate datetime not null,
 MemberPhone int not null,
 MemberNumber nvarchar(100) not null,
 MemberAddress nvarchar(100)
)

insert into Member values('V01','黄子韬','男','1993-5-2',1857759,'450924','青岛')

select * from Member
--会员卡等级表
create table Grade
(
 GID int primary key identity(1,1) not null,
 GNum nvarchar(10) not null,
 GName nvarchar(10) not null,
 GBeizhu nvarchar(50)
)
insert into Grade values('H01','金卡','消费一万或一万以上')

select * from Grade
--会员卡信息
create table Card
(
 CID int primary key identity(1,1) not null,
 CNumbear char(20) not null,
 CNum int references Member(MID),
 Ctime datetime ,
 Clssuing nvarchar(20),
 GNum int references Grade(GID),
)
insert into Card values('V01',2,'2021-9-9','万达',1)

select * from Member
select * from Card
--会员积分信息表
create table Intergral
(
 Iid int primary key identity(1,1) not null,
 MNum int references Member(MID),
 Jifen int not null,
)

insert into Intergral values(2,5000)

--商铺信息表

create table Shops
(
 SID int primary key identity(1,1) not null,
 SName varchar(20) not null,
 SAddress varchar(50),
)

insert into Shops values('卡地亚','万达')