# 存储过程  日期：2021.10.04

## 存储过程的命名规则
```
注释：假如存储过程以sp_ 为前缀开始命名那么会运行的稍微的缓慢，这是因为SQL Server将首先查找系统存储过程，所以我们决不推荐使用sp_作为前缀。

存储过程的命名有这个的语法：

[proc] [MainTableName] by[FieldName(optional)] [Action]

(1) 所有的存储过程必须有前缀'proc'. 所有的系统存储过程都有前缀"sp_", 推荐不使用这样的前缀因为会稍微的减慢。

(2) 表名就是存储过程访问的对象

(3)如果存储过程返回一条记录那么后缀是：Select

(4)如果存储过程插入数据那么后缀是：Insert

(5)如果存储过程更新数据那么后缀是：Update

(6)如果存储过程有插入和更新那么后缀是：Save

(7)如果存储过程删除数据那么后缀是：Delete

(8)如果存储过程更新表中的数据 (ie. drop and create) 那么后缀是：Create

(9)如果存储过程返回输出参数或0，那么后缀是：Output

```
## 有参的存储过程最多可以有2100个参数

```sql
-- 使用语句创建与执行无参数存储过程

create proc proc_SelectStudentInfo
as
begin
   select * from StudentInfo
end

exec proc_SelectStudentInfo

--使用语句命令创建与执行有参数存储过程
go
create proc proc_SelectStudentInfoWithParameter
@name nvarchar(80)
as
begin
    select * from StudentInfo
	where StudentName like @name
end
go

--有多个参数的存储过程
create proc proc_SelectStudentInfoWithSomeParameters
@name nvarchar(80),
@code nvarchar(80)
as
begin
    select * from StudentInfo
	where StudentName like @name or StudentCode like @code
end


--有默认值的存储过程
go
create proc proc_SelectStudentInfoWithAnyParameters
@code nvarchar(80)='1',
@name nvarchar(80)='%李%',
@birthday date='2021-10-04',
@sex char='f',
@classId int='1'
as
begin
    select * from StudentInfo
	where StudentCode like @code
	or StudentName like @name
	or Birthday<@birthday
	or sex=@sex
	or ClassId=@ClassId
end

```