# 2021-9-6   天气：晴



## 常用的sql查询语句

**1.在查询的时候，如果是有两个并且的条件的时候，比如：查询同时参加语文考试和英语考试的人信息，这个人要同时参加了两门学科的查询，这里不能直接使用and来做拼接，如果直接是 学科="语文"  and 学科="英语",表现出来的效果是在这一条记录中此字段要既是语文又是英语，这是不可能的，是查询不到数据的，但是并且又只有and,这里有一个小方法，就是将查询条件分开处理。第一阶段查询用  in  关键字先查询到第一个条件，然后在使用 and 查询第二个条件。比如: **

```sql
select *  from stu  where id  in (select id from  stu where 学科='语文') and 学科 =‘英语’。
```

分析：同时参加语文考试和英语考试的人信息，第一步就是将考过语文的人的id查询出来，将查询到的结果作为一张表，然后在从这张表里面查询考过英语的人的信息，最后出来的信息，就满足要求了

**2.distinct 去掉重复值**

```sql
  select *distinct* nama from user；
```

**3. limit** 

limit（n，m）——从第n条之后开始，取m行记录（n 1 ~ n m）

不指定n时，取前m条（n=0）

```sql
select * from user limit  1 ；——取第1条记录

select * from user limit  2,5； ——取第3~7条记录
```

**4. between  and （范围查询）**

between A and B  

A、B可以是数值、日期、字符串等，除数值外的其他类型，要加引号

```sql
select * from user  where name *between* ‘amada’ *and* ‘king’；

select * from user  where age *between* 10 *and* 30；
```

**5. 通配符（like）**

_   代表任一个字符

%  代表任意n个字符（n可以为0）

[  ]  在指定的范围内

[^ ]  不在指定范围内

```sql
select * from user  where name like '孙_' ; ——以孙开头，且只有两个字的名字

select  * from user  where name like '孙%' ; ——任意以孙开头的名字。
```

**6. 查询为空/非空（NULL）**

```sql
is null / is not null
```

### 子查询

子查询的几点说明：

- 外层的SELECT语句叫做外部查询，内层的SELECT语句叫做子查询
- 使用子查询主要是将查询的结果作为外部主查询的查询条件
- 子查询可以嵌套多层，但每层嵌套需要使用圆括号()括起来
- 大部分子查询是放在select语句的where子句中使用；也有在from子句中使用的子查询，即将子查询的结果集作为外层查询的查询范围
- 根据子查询返回一行或多行查询结果，可将子查询分为2类： 单行子查询、多行子查询：

   单行子查询的操作符： 、>、<、>=、<=、<>和！<>

   多行子查询的操作符：IN、ALL、ANY或函数min,max



**1. 使用比较运算符的子查询**

子查询结果为单行（一个数值）时，常配合比较运算符使用

【eg】：stu表（id，name，score）中，查询大于全班平均成绩的同学id、name：

```sql
select id，name
from stu
where score >  (select  avg(score)  from  stu );
```

**2. 使用in/not in关键字的子查询**

【eg】选课表course（stuid，coursename，stuname），查询没有选过“政治经济学”的学生

```sql
select stuid，stuname
from course
where stuid *not in* （select stuid from course where coursename='政治经济学'）；
```

**3. 使用exists关键字的子查询**

```sql
sqselect ··· from ··· 
where *exists*  (select ··· from ··· where ··· ）
```

使用exists关键字的子查询结果不返回任何结果，只返回逻辑值TRUE或FALSE；当子查询结果集非空时返回TRUE，子查询结果集为空时，返回FALSE；当子查询结果不为空时才执行外层查询