# 2021-9-7  天气：晴



## ROW_NUMBER() overd 函数

```sql
select *,ROW_NUMBER() over (partition by courseid  order by score) 
from StudentCourseScore
```

简单的说row_number()从1开始，为每一条分组记录返回一个数字 ,**字段值相同的给的排名依次下去**，如图：

![图裂了！](./imgs/row_number.png)



## RANK() overd 函数

```sql
select *,RANK() over (partition by courseid order by score) 
from StudentCourseScore
```

rank()也是从1开始，为每一条分组记录返回一个数字,**字段值相同的给的排名一样，相同时跳过下一个数**，如图：

![图裂了！](./imgs/rank.png)



## DENSE_RANK()函数

```sql
select *,DENSE_RANK() over (partition by courseid order by score) 
from StudentCourseScore
```

dense_rank()也是从1开始，为每一条分组记录返回一个数字,**字段值相同的给的排名一样，相同时不跳过下一个数**，如图：

![图裂了！](./imgs/rense_rank.png)