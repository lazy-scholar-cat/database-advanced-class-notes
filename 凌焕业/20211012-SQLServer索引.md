# SQLServer索引

## 什么是索引

    索引主要目的是提高了SQL Server系统的性能，加快数据的查询速度与减少系统的响应时间

下面举两个简单的例子：

    	图书馆的例子：一个图书馆那么多书，怎么管理呢？建立一个字母开头的目录，例如：a开头的书，在第一排，b开头的在第二排，
    这样在找什么书就好说了，这个就是一个聚集索引，可是很多人借书找某某作者的，不知道书名怎么办？图书管理员在写一个目录，
    某某作者的书分别在第几排，第几排，这就是一个非聚集索引。

## 重要内容

    1.SQLServer索引有两种，聚集索引和非聚集索引；
    
    2.聚集索引存储记录是物理上连续存在，而非聚集索引是逻辑上的连续，物理存储并不连续。
    
    3.聚集索引一个表只能有一个，而非聚集索引一个表可以存在多个。

## 如何创建索引

### 创建索引的语法
```sql
create [unique][clustered | nonclustered] index <索引的名称>
on 数据表(字段1 [asc | desc],字段2 [asc | desc])  [WITH [index_property [,....n]]

--说明
unique: 建立唯一索引。

clustered: 建立聚集索引。

nonclustered: 建立非聚集索引

Index_property: 索引属性

unique 索引既可以采用聚集索引结构，也可以采用非聚集索引的结构，如果不指明采用的索引结构，则SQL Server系统默认为采用非聚集索引结构

--删除索引语法

drop index IndexTable.non_index_col1

--说明
IndexTable: 索引所在的表名称。

non_index_col1: 要删除的索引名称

--显示索引信息

--使用系统存储过程：sp_helpindex 查看指定表的索引信息

exec sp_helpindex IndexTable

```
## SET NOCOUN

```sql
--语法
SET NOCOUNT { ON | OFF } 

--说明
当 SET NOCOUNT 为 ON 时，不返回计数。

当 SET NOCOUNT 为 OFF 时，返回计数

即使当SET NOCOUNT ON 时候，也更新@@RowCount
```



