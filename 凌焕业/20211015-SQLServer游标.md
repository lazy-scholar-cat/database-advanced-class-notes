# SQLServer游标

## 什么是游标

游标（Cursor）是处理数据的一种方法，为了查看或者处理结果集中的数据，游标提供了在结果集中一次以行或者多行前进或向后浏览数据的能力。

我们可以把游标当作一个指针，它可以指定结果中的任何位置，然后允许用户对指定位置的数据进行处理

## 游标的组成

游标包含两个部分：一个是游标结果集、一个是游标位置。

游标结果集：定义该游标得SELECT语句返回的行的集合。游标位置：指向这个结果集某一行的当前指针。

## 游标的分类

根据游标检测结果集变化的能力和消耗资源的情况不同，SQL Server支持的API服务器游标分为一下4种：

**静态游标：** 静态游标的结果集，在游标打开的时候建立在TempDB中，不论你在操作游标的时候，如何操作数据库，游标中的数据集都不会变。例如你在游标打开的时候，

对游标查询的数据表数据进行增删改，操作之后，静态游标中select的数据依旧显示的为没有操作之前的数据。如果想与操作之后的数据一致，则重新关闭打开游标即可。

**动态游标：** 这个则与静态游标相对，滚动游标时，动态游标反应结果集中的所有更改。结果集中的行数据值、顺序和成员在每次提取时都会变化。所有用户做的增删改语句通过游标均可见。

如果使用API函数或T-SQL Where Current of子句通过游标进行更新，他们将立即可见。在游标外部所做的更新直到提交时才可见。

**只进游标：** 只进游标不支持滚动，只支持从头到尾顺序提取数据，数据库执行增删改，在提取时是可见的，但由于该游标只能进不能向后滚动，所以在行提取后对行做增删改是不可见的。

**键集驱动游标：** 打开键集驱动游标时，该有表中的各个成员身份和顺序是固定的。打开游标时，结果集这些行数据被一组唯一标识符标识，被标识的列做删改时，用户滚动游标是可见的，如果没被标识的列增该，则不可见，比如insert一条数据，是不可见的，若可见，须关闭重新打开游标。

注意：静态游标在滚动时检测不到表数据变化，但消耗的资源相对很少。动态游标在滚动时能检测到所有表数据变化，但消耗的资源却较多。键集驱动游标则处于他们中间，所以根据需求建立适合自己的游标，避免资源浪费。

## 游标的使用

```sql
-- 定义游标
declare  cur_ForPersonalInfo2 cursor
for 
select * from PersonalInfo

-- 打开游标，准备使用
open cur_ForPersonalInfo2

-- 定义变量，用于接收每一行中的数据
declare @id int,@name nvarchar(80) ,@birthday date

-- 先获取一条，以使@@fetch_status状态初始化
fetch next from cur_ForPersonalInfo2
into @id,@name,@birthday

select @@FETCH_STATUS

-- 真正遍历记录的语句结构
while @@Fetch_Status = 0  --这里对游标的状态进行判断，如果为0，证明游标中有值
begin
	select @id,@name,@birthday -- 代表处理取出的每一行数据
	fetch next from cur_ForPersonalInfo2
	into @id,@name,@birthday
end

-- 关闭游标
close cur_ForPersonalInfo2
-- 释放游标
deallocate cur_ForPersonalInfo2
```
http://www.manongjc.com/detail/18-gxrilqbqmdaruvy.html

https://www.cnblogs.com/knowledgesea/p/3699851.html

https://www.cnblogs.com/94cool/archive/2010/04/20/1715951.html

