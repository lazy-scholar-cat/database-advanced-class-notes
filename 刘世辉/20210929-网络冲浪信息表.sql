use master
go

create database InternetBar
go

use InternetBar
go

create table tbl_card
(
	id char(8) primary key,
	password nchar(10),
	balance money,
	userName nchar(2)
)
go

create table tbl_computer
(
	id char(3) primary key,
	onUse int check(onUse in(0,1)),
	note text
)
go

create table tbl_record
(
	id int primary key,
	cardID char(8) references tbl_card(id),
	ComputerID char(3) references tbl_computer(id),
	beginTime datetime,
	endTime datetime,
	fee money
)
go

insert into tbl_card values('0023_ABC','555',98,'张军'),
('0025_bbd','abe',300,'朱俊'),('0036_CCD','何柳',100,'何柳'),
('0045_YGR','0045_YGR',58,'证验'),('0078_RJV','55885fg',600,'校庆'),
('0089_EDE','zhang',134,'张峻')
go

insert into tbl_computer values ('02',0,'25555'),
('03',1,'55555'),('04',0,'66666'),('05',1,'88888'),
('06',0,'688878'),('B01',0,'558558')
go

insert into tbl_record values (23,'0078_RJV','B01','2007-07-15 19:00:00','2007-07-15 21:00:00',20),
(34,'0025_bbd','02','2006-12-25 18:00:00','2006-12-25 22:00:00',23),
(45,'0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00',50),
(46,'0023_ABC','03','2006-12-22 15:26:00','2006-12-22 22:55:00',6),
(47,'0023_ABC','03','2006-12-23 15:26:00','2006-12-23 22:55:00',50),
(48,'0023_ABC','03','2007-01-06 15:26:00','2007-01-06 22:55:00',6),
(55,'0023_ABC','03','2006-07-21 15:26:00','2006-07-21 22:55:00',50),
(64,'0045_YGR','04','2006-12-24 18:00:00','2006-12-24 22:00:00',300),
(65,'0025_bbd','02','2006-12-28 18:00:00','2006-12-28 22:00:00',23),
(98,'0025_bbd','02','2006-12-26 18:00:00','2006-12-26 22:00:00',23)
go

--1. 查询出用户名为'张军'的上网卡的上网记录，要求显示卡号，用户名，机器编号、开始时间、结束时间，和消费金额，并按消费金额降序排列
select TC.id 卡号,userName 用户名,ComputerID 机器编号,beginTime 开始时间,endTime 结束时间,fee 消费金额 from tbl_record TR inner join tbl_card TC on TR.cardID = TC.id where cardID = (select id from tbl_card where userName = '张军') order by fee desc
--2. 查询出每台机器上的上网次数和消费的总金额
select ComputerID,COUNT(ComputerID) 上网次数,SUM(fee)消费总金额  from tbl_record group by ComputerID
--3. 查询出所有已经使用过的上网卡的消费总金额
select TC.id,SUM(fee)消费总金额 from tbl_card TC inner join tbl_record TR on TC.id = TR.cardID where TC.id = TR.cardID group by TC.id
--4. 查询出从未消费过的上网卡的卡号和用户名
select TC.id,userName  from tbl_card TC left join tbl_record TR on TC.id = TR.cardID where TC.id not in(select cardID from tbl_record)
--5. 将密码与用户名一样的上网卡信息查询出来
select * from tbl_card where userName = password
--6. 查询出使用次数最多的机器号和使用次数
select Top 1  ComputerID,COUNT(ComputerID)使用次数 from tbl_record group by ComputerID  order by COUNT(ComputerID) desc
--7. 查询出卡号是以'ABC'结尾的卡号，用户名，上网的机器号和消费金额
select TC.id,userName,ComputerID,fee from tbl_card TC inner join tbl_record TR on TC.id = TR.cardID where TC.id like '%ABC'
--8. 查询出是周六、周天上网的记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select cardID,userName,ComputerID,beginTime,endTime,fee from tbl_record TR inner join tbl_card TC on TR.cardID = TC.id  where DATEPART(dw,beginTime) in(1,7)
--9. 查询成一次上网时间超过12小时的的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select cardID,userName,ComputerID,beginTime,endTime,fee  from tbl_record TR inner join tbl_card TC on TR.cardID = TC.id  where DATEDIFF(hh,beginTime,endTime) > 12
--10. 查询除消费金额排列前三名(最高)的上网记录，要求显示上网的卡号，用户名，机器号，开始时间、结束时间和消费金额
select cardID,userName,ComputerID,beginTime,endTime,fee  from tbl_record TR inner join tbl_card TC on TR.cardID = TC.id
where fee not in(select top 3 fee from tbl_record order by fee desc)