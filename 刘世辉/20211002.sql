# 1. 用户名密码不能输入空格不合法符号

```
declare @UserName nvarchar(50),@Password nvarchar(50)
declare @count int,@count1 int
declare @resever nvarchar(60)
set @UserName='用户是否是空格或空'
set @Password='密码是否是空格或空'
set @count=charindex(' ',@UserName)   
set @count1=charindex(' ',@Password)    
	if(@count>0 or @count1>0)
		begin
			if(@count=1 or @count1=1)
				begin
					set @resever='输入内容存在不合法符号'
					print @resever
				end
					if(@count>1 or @count1>1)
						begin
							set @resever='输入内容存在不合法符号'
							print @resever
					end
			end
	else
		begin
			set @resever='登录成功'
			print @resever
		end
```



# 2. 一种攻击手段 SQL注入（用法示例如下）
```
declare @username nvarchar(80),@password nvarchar(80),@rowPassword nvarchar(80)
declare @isDeleted bit ,@isActived bit
declare @count int
declare @res nvarchar(80)

select @count=COUNT(*),@isDeleted=IsDeleted,@isActived=IsActived,@rowPassword=Password from Users 
where Username=@username

-- 如果根据提供的用户名，找到的记录数不为零，则表明用户名是对的，可以继续往下判断；否则直接提示用户名或密码错误
if(@count>0)
	begin
		if(@isDeleted=1) --如果删除标记为真，则表明当前用户已经被删除，作出提示；否则用户未被删除
			begin
				set @res='当前用户已经被删除，有任何问题请联系管理员@_@'
			end
		else
			begin
				if(@isActived=0) --如果当前用户未启用/激活，则提示信息用户未启用，登录失败；否则用户已经禁用
					begin
						set @res='用户已禁用，有任何问题请联系管理员'
					end
				else
					begin
						if(@password=@rowPassword) -- 如果传进来的密码和在记录中获取的密码相等，则认为登录成功，作出提示；否则登录失败，提示相应信息
							begin
								set @res='登录成功'
							end
						else
							begin
								set @res='用户名或者密码错误，请确认后重试。。。'
							end
					end

			end

	end
else
	begin
		set @res='用户名或者密码错误，请确认后重试。。。'
	end
```


# 3. 判断密码和重复密码是否一致，是则可以注册，并在相应数据表中插入一条记录，否则返回失败信息

```
declare @username nvarchar(80) --前端传进来的用户名
declare @password nvarchar(80),@cofirmPassword nvarchar(80) --前端传进来的密码
declare @rowPassword nvarchar(80) --是指通过用户名查询到记录后，这个代表用户的记录中的密码
declare @tmpTable table (id int,Username nvarchar(80),Password nvarchar(80))
declare @count int
declare @res nvarchar(80)

insert into @tmpTable
select @count=count(*),@rowPassword=Password from Users where Username=@username

if(@count>0)
	begin
	-- 做点什么 如提示消息或者设置一些数值
		set @res='当前用户名已经被注册，请确认后重试。。。。'
		
	end
else
	begin
		if(@password=@cofirmPassword)
			begin
				insert into Users (Username,Password) values (@username,@password)
				set @res='用户注册成功'
			end
		else
			begin
				set @res='两次输入的密码不一致，请确认后重试'
			end

	end
```

# 4.注册

```
 1.注册单个用户
insert into Users (Username,Password) values ('admin','113')

 2.批量注册
insert into Users (Username,Password) values ('user01','113'),('user02','113'),('user03','113'),('user04','113')
```
# 5.关于商场信息 的使用场景
```
-- 1.在会员信息管理的时候，需要选择商场

select Id,ShoppingMallName,ShortName from ShoppingMallInfo

-- 2.在会员卡类型管理的时候，需要选择商场

select Id,ShoppingMallName,ShortName from ShoppingMallInfo

-- 3. 商场信息本身的CRUD （create read update delete）