# 今天是在宿舍学习21天后线下学习的第一天，重获新生有点不习惯，感觉太热。
## 数据库的存储过程
```
use master
go
create database Student
go
use Student
go
create table StudentInfo
(
Id int primary key identity,
StudentName nvarchar(50),
StudentSex char(2),
StudentPhone nvarchar(15)
)
go
insert into StudentInfo values('小富贵','男','13526548961'),
('小幸福','女','13526548962'),
('小健康','男','13526548963'),
('小快乐','女','13526548964'),
('小帅帅','男','13526548965'),
('小勇敢','男','13526548966')
go
select * from StudentInfo
--存储过程语法
/*
create proc 存储过程名称
as
begin
sql条件语句
end
查
exec 存储过程名称
*/
--无参存储过程

--存储过程名称命名方式
/*
[1] 所有的存储过程必须有前缀“proc_”

[2] 表名就是存储过程主要访问的对象。

[3] 可选字段名就是条件子句。比如： proc_UserInfoByUserIDSelect

[4] 最后的行为动词就是存储过程要执行的任务。
*/

--无参存储过程
go
create proc proc_StudentInfoSelect
as
begin
select * from StudentInfo
end
exec proc_StudentInfoSelect

--有参存储过程
go
create proc proc_StudentInfoStudenNameSelect
@name nvarchar(50)
as
begin
select * from StudentInfo where StudentName like @name
end
exec proc_StudentInfoStudenNameSelect'%健%'

--多个参数存储过程
go
create proc proc_StudentInfoStudenNameStudentSexSelect
@name nvarchar(50),
@sex char(2)
as
begin
select * from StudentInfo where StudentName like @name and StudentSex like @sex
end
exec proc_StudentInfoStudenNameStudentSexSelect'%快%','女'

--默认参数存储过程
go
create proc proc_StudentInfoDefault
@name nvarchar(50)='%帅%',
@sex char(2)='男'
as
begin
select * from StudentInfo where StudentName like @name and StudentSex like @sex
end
exec proc_StudentInfoDefault
```