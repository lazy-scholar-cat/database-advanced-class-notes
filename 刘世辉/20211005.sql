# 返回值

## 自定义方法/函数

/*
函数分两种：
1. 标量函数 最终返回一个标量值的函数，叫标量函数

2. 表值函数 最终返回一个表结果的函数，叫表值函数
	a.单语句
	b.多语句
*/

-- 语法

create function <函数名称>
(
	[
		@xxxx 数据类型，
		@yyyy 数据类型
	]
)
returns 标题值类型--(如int nvarchar char decimal bit)

as

begin

	declare @result int
	set @result=1
	return @result

end

获得存储过程的返回值--通过查询分析器获得

   (1)不带任何参数的存储过程(存储过程语句中含有return)
    创建存储过程

 CREATE PROCEDURE  testReturn
 AS
 return 145
 GO

执行存储过程

 DECLARE @RC int
 exec @RC=testReturn
 select @RC

说明
 查询结果为145

   (2)带输入参数的存储过程(存储过程语句中含有return)
 
创建存储过程

 create procedure sp_add_table1
  @in_name varchar(100),
  @in_addr varchar(100),
  @in_tel varchar(100)
 as
  if(@in_name = '' or @in_name is null)
   return 1
  else
   begin
    insert into table1(name,addr,tel) values(@in_name,@in_addr,@in_tel)
    return 0
   end

   执行存储过程
 <1>执行下列，返回1

 declare @count int exec @count = sp_add_table1 '','中三路','123456' select @count
 
 <2>执行下列，返回0
 
 declare @count int exec @count = sp_add_table1 '','中三路','123456' select @count

说明
 查询结果不是0就是1
 
   (3)带输出参数的存储过程(存储过程中可以有return可以没有return)
 
 例子A：
创建存储过程

 create procedure sp_output
  @output int output
 as
  set @output = 121
  return 1

执行存储过程
 <1>执行下列，返回121

 declare @out int
 exec sp_output @out output
 select @out

 <2>执行下列，返回1

 declare @out int
 declare @count int
 exec @count = sp_output @out output
 select @count

 说明
 有return，只要查询输出参数，则查询结果为输出参数在存储过程中最后变成的值；只要不查询输出参数，则查询结果为return返回的值
 
 例子B：
 创建存储过程

 create procedure sp_output
  @output int output
 as
  set @output = 121

 执行存储过程
 
 <1>执行下列，返回121
 
 declare @out int
 exec sp_output @out output
 select @out
 
 <2>执行下列，返回0
 
 declare @out int
 declare @count int
 exec @count = sp_output @out output
 select @count
 
 说明
 没有return，只要查询输出参数，则查询结果为输出参数在存储过程中最后变成的值；只要不查询输出参数，则查询结果为0