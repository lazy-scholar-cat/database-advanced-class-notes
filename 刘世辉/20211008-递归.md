# 递归
```sql
create database Pdx 
go

use Pdx 
go

create table Pdxnb
(
	id int primary key identity,
	BikiniBottom nvarchar(80) not null,
	Stu_id int not null default 0
)
go

-- 建立第一等级
insert into Pdxnb(BikiniBottom) values ('蟹堡王餐厅')
insert into Pdxnb(BikiniBottom) values ('海之霸餐厅')
insert into Pdxnb(BikiniBottom) values ('菠萝屋')
insert into Pdxnb(BikiniBottom) values ('石头屋')
insert into Pdxnb(BikiniBottom) values ('玻璃罩圆顶树屋')

select * from Pdxnb

-- 建立第二等级
insert into Pdxnb(BikiniBottom,Stu_id) values ('海绵宝宝',(select top 1 id from Pdxnb where BikiniBottom = '蟹堡王餐厅'))
insert into Pdxnb(BikiniBottom,Stu_id) values ('蟹老板',(select top 1 id from Pdxnb where BikiniBottom = '蟹堡王餐厅'))
insert into Pdxnb(BikiniBottom,Stu_id) values ('章鱼哥',(select top 1 id from Pdxnb where BikiniBottom = '蟹堡王餐厅'))
insert into Pdxnb(BikiniBottom,Stu_id) values ('蟹堡王',(select top 1 id from Pdxnb where BikiniBottom = '蟹堡王餐厅'))

insert into Pdxnb(BikiniBottom,Stu_id) values ('痞老板',(select top 1 id from Pdxnb where BikiniBottom = '海之霸餐厅'))
insert into Pdxnb(BikiniBottom,Stu_id) values ('凯伦',(select top 1 id from Pdxnb where BikiniBottom = '海之霸餐厅'))

insert into Pdxnb(BikiniBottom,Stu_id) values ('海绵宝宝',(select top 1 id from Pdxnb where BikiniBottom = '菠萝屋'))
insert into Pdxnb(BikiniBottom,Stu_id) values ('小窝',(select top 1 id from Pdxnb where BikiniBottom = '菠萝屋'))

insert into Pdxnb(BikiniBottom,Stu_id) values ('派大星',(select top 1 id from Pdxnb where BikiniBottom = '石头屋'))

insert into Pdxnb(BikiniBottom,Stu_id) values ('珊迪',(select top 1 id from Pdxnb where BikiniBottom = '玻璃罩圆顶树屋'))

select * from Pdxnb
go

-- 递归  
-- 以高级找低级
with CTE_A
as
(
	select * from Pdxnb where id = 2
	union all
	select a.* from Pdxnb a,CTE_A b
	where a.Stu_id = b.id
)select * from CTE_A

go

-- 以低级找高级
with CTE_B
as
(
	select * from Pdxnb where id = 8
	union all
	select a.* from Pdxnb a,CTE_B b
	where a.id = b.Stu_id
)select * from CTE_B
```