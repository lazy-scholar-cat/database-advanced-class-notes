create database vip

use vip
go

create table marketInfo
(
marketNum int primary key identity(1,1) not null,
marketNaume varchar(20) not null
)

create table vipInfo
(
vipNum int primary key identity(1,1) not null,
vipName varchar(20) not null,
vipSex char(2) not null,
vipBor datetime not null,
vipTel int not null,
vipAddress varchar(20) not null
)

create table vipCard
(
cardNo int primary key identity(1,1) not null,
cardNume varchar(20) not null,
cardTypeNo varchar(20) not null,
vipNum int not null,
cardAddress varchar(20) not null
)

create table vipLevel
(
vipNum int primary key identity(1,1) not null,
vipIntegral int not null,
vipLev varchar(20) not null
)

create table vipCar
(
vipNum int primary key identity(1,1) not null,
vipCarNum varchar(20) not null
)

insert into  marketInfo values ('万达广场')

update marketInfo set marketNaume='万宝广场' where marketNaume='万达广场'

delete from marketInfo where marketNaume='万达广场'