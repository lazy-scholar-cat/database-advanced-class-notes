create database vip

use vip
go

create table marketInfo
(
marketNum int primary key identity(1,1) not null,
marketNaume varchar(20) not null
)

create table vipInfo
(
vipNum int primary key identity(1,1) not null,
vipName varchar(20) not null,
vipSex char(2) not null,
vipBor datetime not null,
vipTel int not null,
vipAddress varchar(20) not null
)

create table vipCard
(
cardNo int primary key identity(1,1) not null,
cardNume varchar(20) not null,
cardTypeNo varchar(20) not null,
vipNum int not null,
cardAddress varchar(20) not null
)

create table vipLevel
(
vipNum int primary key identity(1,1) not null,
vipIntegral int not null,
vipLev varchar(20) not null
)

create table vipCar
(
vipNum int primary key identity(1,1) not null,
vipCarNum varchar(20) not null
)

create table vipPoint
(
vipNum int primary key identity(1,1) not null,
vipCount nvarchar(80) not null,
vipMoney int not null
)

create table userInfo
(
userId int primary key identity(1,1) not null,
userName nvarchar(80) not null,
password nvarchar(80) not null
)

insert into  marketInfo values ('万达广场')

update marketInfo set marketNaume='万宝广场' where marketNaume='万达广场'

delete from marketInfo where marketNaume='万达广场'

-- 登录

-- 根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败

select * from userInfo
where Username='admin' and Password='123456'

-- 更进一步，就是当前登录的用户可能已经被注销、禁用等情况，如何应对



-- 注册

-- 用户层面 ，输入用户名，密码，重复密码

-- 注册单个用户
insert into userInfo (Username,Password) values ('admin','123456')


-- 批量注册

insert into userInfo (Username,Password) values ('user01','123456'),('user02','123456'),('user03','123456'),('user04','123456')

-- 注册通用逻辑（无关数据库）

-- 1. 判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走
alter table userInfo add constraint UQ_userInfo_userId unique(userId)

-- 2. 判断密码和重复密码是否一致，是则可以注册，并在相应数据表中插入一条记录，否则返回失败信息

