

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberActivityInfo')
            and   type = 'U')
   drop table MemberActivityInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberCard')
            and   type = 'U')
   drop table MemberCard
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberCardLossReortInfo')
            and   type = 'U')
   drop table MemberCardLossReortInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberCardTypeInfo')
            and   type = 'U')
   drop table MemberCardTypeInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberInfo')
            and   type = 'U')
   drop table MemberInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberLvelInfo')
            and   type = 'U')
   drop table MemberLvelInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberPointsExchangeInfo')
            and   type = 'U')
   drop table MemberPointsExchangeInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberPointsInfo')
            and   type = 'U')
   drop table MemberPointsInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberPointsRuleInfo')
            and   type = 'U')
   drop table MemberPointsRuleInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SMInfo')
            and   type = 'U')
   drop table SMInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ShopsInfo')
            and   type = 'U')
   drop table ShopsInfo
go

/*==============================================================*/
/* Table: MemberActivityInfo          会员活动信息表                           */
/*==============================================================*/
create table MemberActivityInfo (
   MemberActivityId     int                  not null,
   MemberId             int                  null,
   SMId                 int                  null,
   ActivityInfo         nvarchar(10)         null,
   ActivityTime         nvarchar(20)         null,
   constraint PK_MEMBERACTIVITYINFO primary key (MemberActivityId)
)
go

/*==============================================================*/
/* Table: MemberCard           会员卡信息表                                 */
/*==============================================================*/
create table MemberCard (
   MemberCardId         int                  not null,
   MembereCardNum       nvarchar(30)         null,
   MembereCardType      nvarchar(30)         null,
   SMId                 int                  null,
   MemberId             int                  null,
   constraint PK_MEMBERCARD primary key (MemberCardId)
)
go

/*==============================================================*/
/* Table: MemberCardLossReortInfo        会员卡挂失信息表                        */
/*==============================================================*/
create table MemberCardLossReortInfo (
   LoseId               int                  not null,
   MemberId             int                  null,
   SMId                 int                  null,
   LoseTime             datetime             null,
   constraint PK_MEMBERCARDLOSSREORTINFO primary key (LoseId)
)
go

/*==============================================================*/
/* Table: MemberCardTypeInfo            会员卡类型信息表                        */
/*==============================================================*/
create table MemberCardTypeInfo (
   MemberCardTypeId     int                  not null,
   MemberCardNum        nvarchar(10)         null,
   MemberCardType       nvarchar(10)         null,
   MemberId             int                  null,
   constraint PK_MEMBERCARDTYPEINFO primary key (MemberCardTypeId)
)
go

/*==============================================================*/
/* Table: MemberInfo              会员信息表                              */
/*==============================================================*/
create table MemberInfo (
   MemberId             int                  not null,
   MemberName           nvarchar(20)         null,
   Sex                  nvarchar(2)          null,
   Birthday             nvarchar(10)         null,
   Tel                  nvarchar(20)         null,
   IdCardNum            nvarchar(20)         null,
   Address              nvarchar(80)         null,
   constraint PK_MEMBERINFO primary key (MemberId)
)
go

/*==============================================================*/
/* Table: MemberLvelInfo               会员等级信息表                         */
/*==============================================================*/
create table MemberLvelInfo (
   MemberLevelId        int                  not null,
   MemberId             int                  null,
   MemberLevel          nvarchar(10)         null,
   constraint PK_MEMBERLVELINFO primary key (MemberLevelId)
)
go

/*==============================================================*/
/* Table: MemberPointsExchangeInfo         会员积分兑换表                     */
/*==============================================================*/
create table MemberPointsExchangeInfo (
   PointsExchange       int                  not null,
   MemberId             int                  null,
   SMId                 int                  null,
   ExchangeTime         datetime             null,
   constraint PK_MEMBERPOINTSEXCHANGEINFO primary key (PointsExchange)
)
go

/*==============================================================*/
/* Table: MemberPointsInfo             会员积分信息表                         */
/*==============================================================*/
create table MemberPointsInfo (
   MemberPointsId       int                  not null,
   MemberId             int                  null,
   MemberCardNum        nvarchar(10)         null,
   SMId                 int                  null,
   constraint PK_MEMBERPOINTSINFO primary key (MemberPointsId)
)
go

/*==============================================================*/
/* Table: MemberPointsRuleInfo        会员积分规则表                          */
/*==============================================================*/
create table MemberPointsRuleInfo (
   MemberPointsRuleId   int                  not null,
   MemberPointsRule     varchar(80)          null,
   SMId                 int                  null,
   constraint PK_MEMBERPOINTSRULEINFO primary key (MemberPointsRuleId)
)
go

/*==============================================================*/
/* Table: SMInfo                           商场信息表                     */
/*==============================================================*/
create table SMInfo (
   SMId                 int                  not null,
   SMName               nvarchar(10)         null,
   SMAbbreviation       nvarchar(10)         null,
   SMAddress            nvarchar(80)         null,
   SMIps                nvarchar(10)         null,
   IpsTel               nvarchar(30)         null,
   constraint PK_SMINFO primary key (SMId)
)
go

/*==============================================================*/
/* Table: ShopsInfo                   店铺信息表                          */
/*==============================================================*/
create table ShopsInfo (
   ShopsId              int                  not null,
   ShopesName           nvarchar(30)         null,
   SMId                 int                  null,
   ShopsPosition        nvarchar(30)         null,
   ShopesOperator       nvarchar(30)         null,
   OperatorTel          nvarchar(20)         null,
   constraint PK_SHOPSINFO primary key (ShopsId)
)
go

/*==============================================================*/
/* Table: UserInfo                   用户信息表                          */
/*==============================================================*/
create table UserInfo
(
	Id int not null identity primary key,
	Name nvarchar(10) not null unique,
	Password  nvarchar(10) not null 
)

--注册
insert into UserInfo (Name,Password)
values  ('admin','123')

--批量注册
insert into UserInfo (Name,Password)
values
('admin01','124'),('admin02','125'),('admin03','126');

--登录

--根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败
select * from UserInfo
where Name='admin' and Password='123'

--更进一步，就是当前登录的用户可能已经被注销，禁用等情况，如何应对

delete from UserInfo where Name = 'admin02' 