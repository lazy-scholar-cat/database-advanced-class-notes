# 十月十四日，星期四，雨

## 今天老胡讲了随机姓名及生成1000W条记录

```
create database DbTest
go
use DbTest
go

alter proc proc_CreatePersonalInfoAndInsert
@rowCount int =100
as
begin
	set nocount on
	if OBJECT_ID('PersonalInfo','U') is not null
		begin
			drop table PersonalInfo
		end

	create table PersonalInfo
	(
		Id int primary key identity,
		Name nvarchar(80) not null,
		Birthday date not null default (getdate())
	)
	declare @i int =0,@name nvarchar(80),@lastName nvarchar(80),@firstName nvarchar(80)
	while @i<@rowCount
		begin
			exec proc_GererateRandLastName @lastName output
			exec proc_GererateRandFirstName @firstName output
			set @name=@lastName+@firstName
			insert into PersonalInfo (Name) values (@name)
			set @i=@i+1
		end
	set nocount off
end
go


exec proc_CreatePersonalInfoAndInsert 10000000

select * from PersonalInfo where Name='王棋'
```
创建一个随机获得姓氏的存储过程或者函数
```
go
alter proc proc_GererateRandLastName
@name nvarchar(80) output
as
begin
	declare @str nvarchar(80)='赵钱孙李周吴郑王冯陈褚卫蒋沈韩杨朱秦尤许何吕施张孔曹严华金魏陶姜戚谢邹喻柏水窦章云苏潘葛奚范彭郎鲁韦昌马苗凤花方俞任袁柳酆鲍史唐费廉岑薛雷贺倪汤滕殷罗毕郝邬安常乐于时傅皮卞齐康伍余元卜顾孟平黄和穆萧尹姚邵湛汪祁毛禹狄米贝明臧计伏成戴谈宋茅庞熊纪舒屈项祝董梁杜阮蓝闵席季麻强贾路娄危江童颜郭'
	-- select len(@str)
	declare @rndIndex int=rand()*len(@str)
	
	set @name=substring(@str,@rndIndex,1)
	
end

go

declare @res nvarchar(80)
exec proc_GererateRandLastName @res output
select @res

```

```
go
alter proc proc_GererateRandFirstName
@name nvarchar(80) output
as
begin
	declare @str nvarchar(80)='东西南北中春夏秋冬梅兰菊竹上下左右宇宙洪荒宝灵天地琴棋书画晨霞彩风霜雪雨雷闪磊涯海水木林森树草花果德智体美劳礼仪信谦廉素愿薇采财旺福美寿康安平健贤思年幕曼蒂因增禄观'
	--select len(@str)
	declare @rndNameLen int = convert(int,rand()*2)+1
	declare @rndIndex1 int,@rndIndex2 int

```
判断名字的数量为1，则取单名；否则取双名
```
	if @rndNameLen=1
		begin
			set @rndIndex1=rand()*len(@str)
			set @name=SUBSTRING(@str,@rndIndex1,1)
		end
	else
		begin
			set @rndIndex1=rand()*len(@str)
			set @rndIndex2=rand()*len(@str)
			set @name=SUBSTRING(@str,@rndIndex1,1)+SUBSTRING(@str,@rndIndex2,1)
		end
end
go

declare @res nvarchar(80)
exec proc_GererateRandFirstName @res output
select @res
```