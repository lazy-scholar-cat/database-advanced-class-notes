use master
go
create database Mallmember
go
use Mallmember
go
create table Mallinformation --1.商场信息
(
	Mallnumber int primary key not null,
	Mallname nvarchar(30) not null,
	Malladdress nvarchar(100),
	legalperson nvarchar(10),
	Officetelephone nvarchar(20)
)
create table Memberinformation --2.会员信息
(
	Membernumber int primary key not null,
	Mname nvarchar(10) not null,
	Sex char(2) not null,
	Telephone nvarchar(20) not null
)
create table Membershipinformationcard --3.会员卡信息
(
	Membershipcardnumber int primary key not null,
	Mname nvarchar(10) not null,
	Telephone nvarchar(20) not null,
	CardAddress  nvarchar(20) not null
)
create table Membershipcardtypeinformation --4.会员卡类型信息
(	
	Membershipcardnumber int primary key not null,
	Mname nvarchar(10) not null,
	MType nvarchar(5),
	MState nvarchar(5)
)
create	 table Shopinformation --5.商铺信息
(
	ShopNo int primary key not null,
	Shopname nvarchar(10) not null,
	legalperson nvarchar(10) not null,
	Officetelephone nvarchar(20),
	BusinessHours time

)
create table Memberpointsruleinformation --6.会员积分规则信息
(
	ID int primary key not null,
	Integral int not null,
	Explain nvarchar(10),
	Inuse0expired1inuse binary not null,
	Expirationtime time
)
create table Memberpointsinformation --7.会员积分信息
(
	Membernumber int primary key not null,
	Cumulativeintegral int,
	Pointsused int ,
	Residualintegral int,
	Dateofregistration time
)
create table Memberlevelinformation --8.会员等级信息
(
	MembershipcardID int primary key not null,
	Mname nvarchar(10) not null,
	Membershiplevel nvarchar(10),
	Dateofregistration time,
	Status0disabled1available binary not null,
	ID nvarchar(18)
)
create table Memberactivityinformation --9.会员活动信息
(
	Activitynumber int primary key not null,
	Activityname nvarchar(10),
	Activitytime time,
	personincharge nvarchar(10),
	personinTelephone nvarchar(20) not null

)
create table Membershipcardlossreportinformation --11.会员卡挂失记录信息
(
	Lossreportcardnumber int primary key not null,
	Lname nvarchar(10) not null,
	Telephone nvarchar(20) not null,
	Lossreportingtime time,
	Registrationtime time ,
	Audittime time ,
	Approveno binary,
	Recordnumber int
)
create table Memberpointsexchange --12.会员积分兑换
(
	Membernumber int primary key not null,
	Nameofprizeitem nvarchar(20),
	Prizecashingtime time,
	Useintegral int
)

create table Userinformationtable --13用户信息表
(
	userid nvarchar(20) not null,
	password nvarchar(20) not null,
	password2 nvarchar(20) not null
)

insert into Mallinformation values(02324,'万达广场','上海市桂平路481号20号楼5层','曾茂军','021-24178888')
insert into Memberinformation values(20440102,'阿宇','男','13864519721')
insert into Membershipinformationcard values(20440102,'阿宇','男','13864519721')
insert into Membershipcardtypeinformation values (01,'古茗','暂无资料','暂无资料','暂无资料')
--insert into Shopinformation values ()
--insert into Memberpointsruleinformation values ()
--insert into Memberpointsinformation values ()

update Mallinformation set Mallnumber =10 where Mallnumber=02324
select * from Mallinformation
insert into Mallinformation values(02326,'万宝广场','上海市桂平路481号20号楼2层','无','021-24178888')
delete from Mallinformation where Mallnumber =02324

update  Memberinformation set Mname = '阿飞阿宇' where Mname = '阿宇'
select * from Memberinformation
insert into Memberinformation values(20440103,'阿飞','女','1586432121721')
delete from Memberinformation where Mname ='阿飞阿宇'



--用户登陆，根据数据库里面已有的信息来查询用户是否存在，是，则登陆；否，则需要注册用户

	select * from Userinformationtable where userid='admin' and password='123'

-- 更进一步，就是当前登录的用户可能已经被注销、禁用等情况，如何应对

-- 注册

-- 用户层面 ，输入用户名，密码，重复密码

-- 注册单个用户
insert into Userinformationtable (userid,password) values ('admin','123456')


-- 批量注册

insert into Userinformationtable (userid,password) values ('userid01','123456'),('userid02','123456'),('userid03','123456'),('userid04','123456')

-- 注册通用逻辑（无关数据库）

-- 1. 判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走
--唯一约束
alter table Userinformationtable add constraint unique1 unique(userid) 
-- 2. 判断密码和重复密码是否一致，是则可以注册，并在相应数据表中插入一条记录，否则返回失败信息
--瞎写的
select *from Userinformationtable  where password=password2 
insert into Userinformationtable (userid,password) values ('admin','123456')
