use master
go
create database Mallmember
go
use Mallmember
go
create table Mallinformation --1.商场信息
(
	Mallnumber int primary key not null,
	Mallname nvarchar(30) not null,
	Malladdress nvarchar(100),
	legalperson nvarchar(10),
	Officetelephone nvarchar(20)
)
create table Memberinformation --2.会员信息
(
	Membernumber int primary key not null,
	Mname nvarchar(10) not null,
	Sex char(2) not null,
	Telephone nvarchar(20) not null
)
create table Membershipinformationcard --3.会员卡信息
(
	Membershipcardnumber int primary key not null,
	Mname nvarchar(10) not null,
	Telephone nvarchar(20) not null,
	CardAddress  nvarchar(20) not null
)
create table Membershipcardtypeinformation --4.会员卡类型信息
(	
	Membershipcardnumber int primary key not null,
	Mname nvarchar(10) not null,
	MType nvarchar(5),
	MState nvarchar(5),
)
create	 table Shopinformation --5.商铺信息
(
	ShopNo int primary key not null,
	Shopname nvarchar(10) not null,
	legalperson nvarchar(10) not null,
	Officetelephone nvarchar(20),
	BusinessHours time

)
create table Memberpointsruleinformation --6.会员积分规则信息
(
	ID int primary key not null,
	Integral int not null,
	Explain nvarchar(10),
	Inuse0expired1inuse binary not null,
	Expirationtime time
)
create table Memberpointsinformation --7.会员积分信息
(
	Membernumber int primary key not null,
	Cumulativeintegral int,
	Pointsused int ,
	Residualintegral int,
	Dateofregistration time
)
create table Memberlevelinformation --8.会员等级信息
(
	MembershipcardID int primary key not null,
	Mname nvarchar(10) not null,
	Membershiplevel nvarchar(10),
	Dateofregistration time,
	Status0disabled1available binary not null,
	ID nvarchar(18)
)
create table Memberactivityinformation --9.会员活动信息
(
	Activitynumber int primary key not null,
	Activityname nvarchar(10),
	Activitytime time,
	personincharge nvarchar(10),
	personinTelephone nvarchar(20) not null

)
create table Membershipcardlossreportinformation --11.会员卡挂失记录信息
(
	Lossreportcardnumber int primary key not null,
	Lname nvarchar(10) not null,
	Telephone nvarchar(20) not null,
	Lossreportingtime time,
	Registrationtime time ,
	Audittime time ,
	Approveno binary,
	Recordnumber int
)
insert into Mallinformation values(02324,'万达广场','上海市桂平路481号20号楼5层','曾茂军','021-24178888')
insert into Memberinformation values(20440102,'阿宇','男','13864519721')
insert into Membershipinformationcard values(20440102,'阿宇','男','13864519721')

