-- 15.1 按各科成绩进行排序，并显示排名， Score 重复时合并名次
select CourseId,Score,DENSE_RANK()over( order by score desc) from StudentCourseScore group by CourseId,Score

-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺
select sum(Score),StudentId,rank() over( order by sum(score) desc ) from StudentCourseScore group by StudentId
-- 16.1 查询学生的总成绩，并进行排名，总分重复时不保留名次空缺
select StudentId,sum(score),ROW_NUMBER() over( order by sum(score) desc) from StudentCourseScore group by StudentId
-- 14.查询各科成绩最高分、最低分和平均分：
select CourseId,max(Score)最高, min(Score)最低, avg(Score)平均分 from StudentCourseScore group by CourseId
-- 17.统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比
select CourseId,sum(a1),sum(a2) ,sum(a3),sum(a4),sum(a1)+sum(a2)+sum(a3)+sum(a4),
(CONVERT(nvarchar(60),CONVERT(decimal(10,2),sum(a1)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
CONVERT(nvarchar(60),CONVERT(decimal(10,2),sum(a2)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4))))+'%',
CONVERT(nvarchar(60),CONVERT(decimal(10,2),sum(a3)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4))))+'%',
CONVERT(nvarchar(60),CONVERT(decimal(10,2),sum(a4)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4))))+'%'
from 
(select CourseId,
(case when Score>=85 then  1 else  0 end )a1,
(case when Score<=85 and Score >=70 then 1 else 0 end )a2,
(case when Score<=70 and Score>60 then 1 else 0 end )a3,
(case when Score<=60 and Score>=0 then 1 else 0 end )a4
from StudentCourseScore)a
where CourseId=1
group by CourseId
union
 
select CourseId,sum(a1),sum(a2) ,sum(a3),sum(a4),sum(a1)+sum(a2)+sum(a3)+sum(a4),
(CONVERT(nvarchar(60),CONVERT(decimal(10,2),sum(a1)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
CONVERT(nvarchar(60),CONVERT(decimal(10,2),sum(a2)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4))))+'%',
CONVERT(nvarchar(60),CONVERT(decimal(10,2),sum(a3)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4))))+'%',
CONVERT(nvarchar(60),CONVERT(decimal(10,2),sum(a4)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4))))+'%'
from 
(select CourseId,
(case when Score>=85 then  1 else  0 end )a1,
(case when Score<=85 and Score >=70 then 1 else 0 end )a2,
(case when Score<=70 and Score>60 then 1 else 0 end )a3,
(case when Score<=60 and Score>=0 then 1 else 0 end )a4
from StudentCourseScore)a
where CourseId=2
group by CourseId
union
select CourseId,sum(a1),sum(a2) ,sum(a3),sum(a4),sum(a1)+sum(a2)+sum(a3)+sum(a4),
(CONVERT(nvarchar(60),CONVERT(decimal(10,2),sum(a1)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
CONVERT(nvarchar(60),CONVERT(decimal(10,2),sum(a2)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4))))+'%',
CONVERT(nvarchar(60),CONVERT(decimal(10,2),sum(a3)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4))))+'%',
CONVERT(nvarchar(60),CONVERT(decimal(10,2),sum(a4)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4))))+'%'
from 
(select CourseId,
(case when Score>=85 then  1 else  0 end )a1,
(case when Score<=85 and Score >=70 then 1 else 0 end )a2,
(case when Score<=70 and Score>60 then 1 else 0 end )a3,
(case when Score<=60 and Score>=0 then 1 else 0 end )a4
from StudentCourseScore)a
where CourseId=3
group by CourseId


--方法二

; with CTE_A as 
(
select CourseId, 
(case when Score>=85 then  1 else  0 end )a1,
(case when Score<=85 and Score >=70 then 1 else 0 end )a2,
(case when Score<=70 and Score>60 then 1 else 0 end )a3,
(case when Score<=60 and Score>=0 then 1 else 0 end )a4
from StudentCourseScore
) ,CTE_B as 
(
select CourseId,sum(a1)a1,sum(a2)a2,sum(a3)a3,sum(a4)a4,sum(a1)+sum(a2)+sum(a3)+sum(a4)a5 from CTE_A group by CourseId
)select a.CourseId ,b.CourseName  ,sum(a1)a1,sum(a2)a2,sum(a3)a3,sum(a4)a4,sum(a1)+sum(a2)+sum(a3)+sum(a4)a5,
CONVERT(nvarchar(60),CONVERT(decimal(10,2),a1*100.0/a5))+'%'[100-85],
CONVERT(nvarchar(60),CONVERT(decimal(10,2),a2*100.0/a5))+'%'[85-70],
CONVERT(nvarchar(60),CONVERT(decimal(10,2),a3*100.0/a5))+'%'[70-60],
CONVERT(nvarchar(60),CONVERT(decimal(10,2),a4*100.0/a5))+'%'[60-0]
from  CTE_B a left join CourseInfo b on a.CourseId=b.Id group by CourseId,b.CourseName,a1,a2,a3,a4,a5