作业（VIP系统设计）

> 客户信息

| 客户编号 | 客户姓名 | 出生日期 | 电话号码 | VIP卡号 | VIP等级编号 |
| - | - | - | - | - | - |
| vip001 | 小一 | 2001-03-21 | 123456789 | K01 | 金 |
| vip002 | 小二 | 2000-10-21 | 123456789 | K02 | 银 |
| vip003 | 小三 | 1993-01-11 | 123456789 | K03 | 铜 |

> VIP信息

| 持卡人编号 | VIP卡号 | 开卡时间 | 积分 | 卡内余额 |
| - | - | - | - | - |
| vip001 | K01 | 08-20 | 3680 | 500 |
| vip002 | K03 | 08-10 | 1790 | 800 |
| vip003 | K04 | 03-05 | 4670 | 300 |

> VIP消费记录信息表

| 消费记录编号 | VIP卡号 | 消费场所 | 消费金额 |
| - | - | - | - |
| 01 | K02 | 饭店 | 240 |
| 02 | K01 | 饰品店 | 58 |
| 03 | K03 | KTV | 399 |