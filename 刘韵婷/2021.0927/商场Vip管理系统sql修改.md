# 2021.09.27 没听懂

## 数据库

```
use master
go
create database VIPxitong
go
use VIPxitong
go
create table SCxinxi--商场信息表
(
SCID int primary key identity(1,1),--商场编号
SCname nvarchar(50) not null,--商场名称
SCjianc nvarchar(25) not null,--商场简称
SCaddress nvarchar(50) not null,--商场注册地址
SCzcren nvarchar(50) not null,--商场注册人
SCbgdianhua nvarchar(50)--办公电话
)
go
create table VIPxinxi--会员信息
(
VIPID int primary key identity(1,1) not null,--会员编号
VIPname nvarchar(50) not null,--会员姓名
VIPsex nvarchar(10) not null,--会员性别
VIPcsr nvarchar(50) not null,--出生年月
VIPhaoma nvarchar(50) not null,--手机号码
VIPaddress nvarchar(50) not null--地址
)
GO
create table VIPKxinxi--会员卡信息
(
VIPKID int primary key identity(1,1) not null,--会员卡编号
Kzuzhi nvarchar(50) not null,--发卡组织
VIPID int references VIPxinxi(VIPID) not null--会员编号
)
GO
create table KLXxinxi--会员卡类型信息
(
KLXID int primary key identity(1,1) not null,--会员卡类型编号
KLXname nvarchar(50) not null--会员卡类型名称
)
create table SPxinxi--商铺信息
(
SPID int primary key identity(1,1) not null,--商铺编号
SPname nvarchar(50) not null,--商铺名称
SPzhuren nvarchar(50) not null,--商铺店主名称
SPsuoshuSC nvarchar(50) not null,--商铺所属商城
SPaddress nvarchar(50) not null,--商铺位置
)
go
create table VIPJFxinxi--会员积分信息
(
VIPKID int references VIPKxinxi(VIPKID),--会员卡编号
KLXID int references KLXxinxi(KLXID),--会员卡类型编号
VIPID int references KLXxinxi(KLXID),--会员编号
JFyue nvarchar(50) not null--会员积分余额
)
create table DJxinxi--会员等级信息
(
DJID int primary key identity(1,1) not null,--等级编号
DJname nvarchar(50) not null--等级名称
)
GO
create table HDxinxi--会员活动信息
(
HDID int primary key identity(1,1) not null,--活动编号
HDname nvarchar(50) not null,--活动名称
VIPID int references KLXxinxi(KLXID) not null,--会员编号
SPID int references SPxinxi(SPID) not null,--消费商铺编号
XFjine nvarchar(50) not null--消费金额
)
GO
create table CLxinxi--会员车辆信息
(
VIPID int references KLXxinxi(KLXID) not null,--会员编号
VIPchepai nvarchar(50) not null--车牌号
)
create table GSjilu--会员卡挂失记录信息
(
GSID int primary key identity(1,1) not null,--记录编号
VIPKID int references VIPKxinxi(VIPKID) not null,--会员卡编号
VIPID int references KLXxinxi(KLXID) not null,--会员编号
GStime nvarchar(50) not null,--本次挂失时间
GScishu nvarchar(50) not null--总挂失次数
)
go
create table Userxinxi --用户表
(
Username nvarchar(50) not null,--用户账号
Mima nvarchar(50) not null,--用户密码
)


insert into SCxinxi (SCname,SCjianc,SCaddress,SCzcren,SCbgdianhua) 
values ('荣光大型购物城','荣光','天街1号','小一','0583-87760214')
insert into VIPxinxi (VIPname,VIPsex,VIPcsr,VIPhaoma,VIPaddress)
values ('陈一','女','2000-04-05','12345678900','东街') 
insert into VIPKxinxi (Kzuzhi,VIPID)
values ('荣光',1)
insert into KLXxinxi (KLXname)
values ('购物')
insert into SPxinxi (SPname,SPzhuren,SPsuoshuSC,SPaddress)
values ('气球乐园','小肖','荣光','大街1号')
insert into VIPJFxinxi (KLXID,VIPID,JFyue)
values (1,1,2000)
insert into DJxinxi (DJname)
values ('铜星')
```

## 课堂
## ![课堂截图1](../img/登录.png)
## ![课堂截图2](../img/注册.png)
## ![课堂截图3](../img/注册通用逻辑.png)