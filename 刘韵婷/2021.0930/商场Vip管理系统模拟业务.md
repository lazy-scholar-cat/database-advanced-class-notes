## 2021.10.03 假期结束
>模拟业务课堂笔记总汇加理解

## 登录
根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败

-- select * from Users
where Username='admin' and Password='113'

更进一步，就是当前登录的用户可能已经被注销、禁用等情况，如何应对
-- 1.根据提供的用户名（这里假定用户名不能重复），查询出满足条件的记录

-- 2.判断查询到的记录的状态（如之前提过的IsActived、IsDeleted等等，也可以给用户增加额外的状态字段），给出已经被注销、已经被禁用、已经被删除等提示

-- 3.如果这条记录没有被注销、禁用、删除的情况，最后判断密码是不是和查询到的这条记录匹配，是则登录成功，否则提示“用户或者密码错误”

## SQL注入 一种攻击手段 
> 定义：Sql 注入攻击是通过将恶意的 Sql 查询或添加语句插入到应用的输入参数中，再在后台 Sql 服务器上解析执行进行的攻击，它目前黑客对数据库进行攻击的最常用手段之一。
### Web程序三层架构
通常意义上就是将整个业务应用划分为：
- 界面层（表示层）
- 业务逻辑层（又称领域层）
- 数据访问层（又称存储层）

### Sql 注入带来的威胁主要有如下几点：
 猜解后台数据库，这是利用最多的方式，盗取网站的敏感信息。
 绕过认证，列如绕过验证登录网站后台。
 注入可以借助数据库的存储过程进行提权等操作

declare @username nvarchar(80),@password nvarchar(80),@rowPassword nvarchar(80)
declare @isDeleted bit ,@isActived bit
declare @count int
declare @res nvarchar(80)

select @count=COUNT(*),@isDeleted=IsDeleted,@isActived=IsActived,@rowPassword=Password from Users 
where Username=@username

### 如果根据提供的用户名，找到的记录数不为零，则表明用户名是对的，可以继续往下判断；否则直接提示用户名或密码错误
>过程
```
if(@count>0)
	begin
		if(@isDeleted=1) --如果删除标记为真，则表明当前用户已经被删除，作出提示；否则用户未被删除
			begin
				set @res='当前用户已经被删除，有任何问题请联系管理员@_@'
			end
		else
			begin
				if(@isActived=0) --如果当前用户未启用/激活，则提示信息用户未启用，登录失败；否则用户已经禁用
					begin
						set @res='用户已禁用，有任何问题请联系管理员'
					end
				else
					begin
						if(@password=@rowPassword) -- 如果传进来的密码和在记录中获取的密码相等，则认为登录成功，作出提示；否则登录失败，提示相应信息
							begin
								set @res='登录成功'
							end
						else
							begin
								set @res='用户名或者密码错误，请确认后重试。。。'
							end
					end

			end

	end
else
	begin
		set @res='用户名或者密码错误，请确认后重试。。。'
	end
```
```
declare @password nvarchar(80) = \'1' or 1=1\`

select * from Users
where (Username='admin' and Password='1') or 1=1
```



## 注册
用户层面 ，输入用户名，密码，重复密码

-- 注册单个用户
insert into Users (Username,Password) values ('admin','113')

-- 批量注册

insert into Users (Username,Password) values ('user01','113'),('user02','113'),('user03','113'),('user04','113')

-- 注册通用逻辑（无关数据库）
>注册主要判断两点（1.是否是已注册过的账户 2.两次输入设置的密码是否是一致的 ）
-- 1. 判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走

```
定义用户名变量=传输过来的用户名
根据用户名变量查询用户表中有没有同名的记录

如果有，则提示不能注册，返回消息
如果没有，则可以继续往下
```

-- 2. 判断密码和重复密码是否一致，是则可以注册，并在相应数据表中插入一条记录，否则返回失败信息


```
SQL
declare @username nvarchar(80) --前端传进来的用户名
declare @password nvarchar(80),@cofirmPassword nvarchar(80) --前端传进来的密码
declare @rowPassword nvarchar(80) --是指通过用户名查询到记录后，这个代表用户的记录中的密码
declare @tmpTable table (id int,Username nvarchar(80),Password nvarchar(80))
declare @count int
declare @res nvarchar(80)

insert into @tmpTable
select @count=count(*),@rowPassword=Password from Users where Username=@username

if(@count>0)
	begin
	-- 做点什么 如提示消息或者设置一些数值
		set @res='当前用户名已经被注册，请确认后重试。。。。'
		
	end
else
	begin
		if(@password=@cofirmPassword)
			begin
				insert into Users (Username,Password) values (@username,@password)
				set @res='用户注册成功'
			end
		else
			begin
				set @res='两次输入的密码不一致，请确认后重试'
			end

	end

```



### 关于商场信息 的使用场景

-- 1.在会员信息管理的时候，需要选择商场

select Id,ShoppingMallName,ShortName from ShoppingMallInfo

-- 2.在会员卡类型管理的时候，需要选择商场

select Id,ShoppingMallName,ShortName from ShoppingMallInfo

-- 3. 商场信息本身的CRUD （create read update delete）