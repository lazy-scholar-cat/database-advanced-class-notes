## 2021.10.07  困
## 单语句的表值函数


# 课堂笔记
```
-- SQLServer自定义函数 
-- 目前主流的数据库
-- 关系型数据库
-- 商业
/*
1. Oracle
2. SqlServer
*/

-- 开源数据库
/*
1. Mysql
2. MariDb
3. PostgreSQL
*/

-- 非关系型数据库 NoSql
/*
1. Redis
2. MongoDb
*/

/*
1. 标量函数 返回值是一个标量值（基础数据类型的值）
2. 表值函数 返回值是一个表（集合）
	a. 单语句表值函数 
	b. 多语句表值函数
*/

/*
-- 标量函数定义的语法
create function <函数名称>
(
	[
		@xxx 数据类型 [=默认值],
		@yyy 数据类型 [=默认值]
	]
)
returns 数据类型
as
begin

end
*/

go

create function fn_GetANum
(
	
)
returns int
as

begin

	declare @result int =3
	return @result
end

go

select  dbo.fn_GetANum()


go

create function fn_GetADecimal()
returns decimal(18,2)
as
begin
	return 18.2
end
go

select dbo.fn_GetADecimal()
go

alter function fn_GetAChar()
returns decimal(18,4)
as
begin
	return convert(decimal(18,4), '18.2')
end
go

select dbo.fn_GetAChar()

-- 表值函数的定义和使用

	-- a.单语句表值函数定义
	/*
	create function <函数名称>
	(
		[
		@xxx 数据类型 [=默认值],
		@yyy 数据类型 [=默认值]
		]
	)
	returns table
	as
	return
	(
		select * from StudentInfo
	)
	*/

go
create function fn_StudentInfoSelect
()
returns table
as
	return (select * from StudentInfo)
go

-- 公用表表达式
;with CTE_A
as
(
	select *from StudentInfo
)select *from CTE_A

go
select * from dbo.fn_StudentInfoSelect()
go

create function fn_StudentInfoSelectById(@id int)
returns table
as
return
(
	select * from StudentInfo
	where Id=@id
)
go

select * from dbo.fn_StudentInfoSelectById(2)
```
# 寄几
## 无参标量函数
```
create function fn_GetNum
(
)
returns int
as
begin
    declare @result int=3
	return @result --返回result
end

go
select dbo.fn_GetNum()--dbo.和括号不能少
```

## 有参标量函数
```
create function fu_GetADecimal
(
 @result int
)
returns int
as
begin
   return @result
end

go
select dbo.fu_GetADecimal(3)
```

## 标量函数 返回值是一个标量值（基础数据类型的值）
> 函数体语句定义在begin-end语句内，其中包含了可以返回值的Transact-SQL命令

## 表值函数 返回值是一个表（集合）
### 单语句表值函数 
>单语句表值函数又称内联表值函数，这类型函数以表的形式返回一个值，相当于一个参数化的视图。
```
-- 创建单语句表值型函数语法
create function 名字
(@参数名称 参数类型(默认值))
returns table
with encryption
as
return （select语句）
```

### 多语句表值函数
> 多语句表值函数可以看做是标量函数和内联表值函数的结合体。
```
create function 函数名字（@参数列表）
returns 表变量名字 table
（
表变量的字段定义
）
as
begin

return
end
```
### 了解、熟悉Linux下的一些常用的命令


