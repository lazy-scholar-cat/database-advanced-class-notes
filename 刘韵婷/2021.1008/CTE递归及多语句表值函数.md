## 2021.1008

# 课堂
```sql
--delete from Category

;with CTE_A
as
(
	select Id,CategoryName,ParentId from Category where Id=42
	union all
	select c.Id,c.CategoryName,c.ParentId from Category c,CTE_A a
	where c.Id=a.ParentId

)select * from CTE_A
```

## 多语句表值函数 语法
```sql
create function <函数名称>
(
	[
		@xxx 数据类型 [=默认值],
		@yyy 数据类型 [=默认值]
	]
)
returns @res table (Id int ,Name nvarchar(80))
as
begin

	return
end

go

alter function fn_StudentInfoSelectByName(@name nvarchar(80))
returns @res table(Id int ,Name nvarchar(80))
as
begin
	declare @x nvarchar(80)
	insert into @res(Id,Name) select id,studentname from StudentInfo where studentname=@name

	select @x=name from @res

	delete from @res
	insert into @res(Id,Name) select id,studentname from StudentInfo
		
	drop @res
	alter @res()
	return
end

go

select * from dbo.fn_StudentInfoSelectByName('赵雷')

	总结：单语句表值函数和多语句表值函数，实际上的区别只在于后者有一个表类型的变量，在后者定义的函数内部，可以任意的对这个变量
	进行增删改查的操作(可以操作其中的数据，但是不能对表变量进行删除等操作)；单语句表值函数，并没有一个表类型的变量，它最后只
	是返回一个表，也可以理解为其整个的过程就是一个的select语句（并不只是说，只能有一个select语句，比如使用CTE，就不只一个select）

	另外：多语句表值函数中的表变量，类似于数据库当中的临时表

go
alter view vw_StudentInfo
as
select a.Id,b.studentname,c.coursename,a.score from studentcoursescore a
left join studentinfo b on a.studentid=b.id
left join CourseInfo c on a.courseId=c.id

select * from vw_StudentInfo
```
# 寄几
## CTE递归
> 递归调用是指自己调用自己，使用CTE实现递归查询必须满足三个条件：初始条件，递归调用表达式，终止条件

>递归查询至少包含两个子查询：
1. 第一个子查询称作定点（Anchor）子查询：定点查询只是一个返回有效表的查询，用于设置递归的初始值；
2. 第二个子查询称作递归子查询：该子查询调用CTE名称，触发递归查询，实际上是递归子查询调用递归子查询；
3. 两个子查询使用union all，求并集

## 二、多语句表值函数
> 多语句表值函数可以看做是标量函数和内联表值函数的结合体。
```
create function 函数名字（@参数列表）
returns 表变量名字 table
（
表变量的字段定义
）
as
begin

return
end
```
### 练习sql

