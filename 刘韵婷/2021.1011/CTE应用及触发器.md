## 2021.10.11

# CTE应用及触发器

## 触发器 定义：是一种特殊存储过程 它不由用户直接手动执行，而是当一些事件发生后，自动被调用
> 触发器的定义就是说某个条件成立的时候，触发器里面所定义的语句就会被自动的执行。因此触发器不需要人为的去调用，也不能调用。触发器和过程函数类似 过程函数必须要调用。
### ![触发器](../img/关于触发器.png)

## 作用 
1. 维护数据的有效性和完整性 
2. 简化逻辑

### 语法：
 create trigger <触发器名称>

 on <数据表>

 <after | instead of> <delete | insert | update>

 as

 begin
 
 end

 go
```sql
alter trigger tr_TmpRankForUpdate
on tmpRank
after
 Update 
as
begin
	declare @score int,@id int
	select @id=id, @score=Score from inserted
	if (@score>=0 and @score<=100)
		begin
			update tmpRank set Score=@score where Id=@id
		end
	else
		begin
			update tmpRank set Score=0 where Id=@id
		end
end

go


select *from tmpRank

update tmpRank set Score=-2 where Id=1
```

### 存储过程当中特有的两张临时表：
> inserted：插入数据时，新插入的数据在inserted表中；更新的时候，更新后的数据在inserted中（存放的是更新后的记录，增加会存放增加的记录）；
> deleted：更新的时候，deletd表中的数据是之前的旧的数据；在删除的时候，deleted中的数据是被删除的数据（存放的则是更新前的记录，删除则存放的删除的记录）；

> https://blog.csdn.net/iteye_10820/article/details/82607587?utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-2.no_search_link&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-2.no_search_link
