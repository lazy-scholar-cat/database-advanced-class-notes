# 模拟业务

-- 登录

-- 根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败



declare @userName nvarchar(80) 

declare @passWord nvarchar(80),@rowPassWord  nvarchar(80)

declare @result nvarchar(80)

declare @isDeleted  nvarchar(80)  --0代表未删除  1代表删除

declare @isActived  nvarchar(80)  --0代表正常    1代表禁用	

declare @count int 

select @count=COUNT(*), @userName=UserName,@rowPassWord=PassWord,@isDeleted=isDeleted,@isActived=isActived from Users

select * from Users

if(@count>0)

	begin

		if(@isDeleted=1)

			begin

				set @result='该用户已被删除！请联系管理员！'

					if(@isActived=1)

						begin

							set @result='该用户状态异常，已被禁用，请联系管理员！'

						end

					else

						begin

							set @result='登录成功！'

						end

			end

		else

			begin

				set @result='登录成功！'

			end

	end

else

	begin

		set @result='用户名或密码有误！登录失败！'

	end
