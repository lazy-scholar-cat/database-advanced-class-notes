~~~~
create database MallVip
go
use MallVip
go


--if exists(select 1 from sys.sysforeignkey where role='FK_SHOPMESS_REFERENCE_MALLMESS') then
 --   alter table ShopMessage
 --      delete foreign key FK_SHOPMESS_REFERENCE_MALLMESS
--end if;

--if exists(select 1 from sys.sysforeignkey where role='FK_VIPCARDM_REFERENCE_VIPMESSA') then
--    alter table VipCardMessage
 --      delete foreign key FK_VIPCARDM_REFERENCE_VIPMESSA
--end if;

drop table if exists MallMessage;

drop table if exists PointDuiHuan;

drop table if exists ShopMessage;

drop table if exists VipCardMessage;

drop table if exists VipMessage;

drop table if exists VipPointMessage;




/*==============================================================*/
/* Table: Users                                                 */
/*==============================================================*/

create table Users  --用户信息表
(
	id int primary key identity,
	UserName nvarchar(80) not null,
	PassWord nvarchar(80) not null
)

/*==============================================================*/
/* Table: MallMessage                                           */
/*==============================================================*/
create table MallMessage  --商城信息表
(
   Id                   nvarchar(80)                   not null,
   MallName             nvarchar(80)                   null,
   MallDress            nvarchar(80)                   null,
   MallRegisterPeople   nvarchar(80)                   null,
   MallWorkPhone        nvarchar(80)                   null,
   constraint PK_MALLMESSAGE primary key clustered (Id)
);

/*==============================================================*/
/* Table: PointDuiHuan                                          */
/*==============================================================*/
create table PointDuiHuan  --积分兑换表
(
   id                   int                              not null,
   GiftName             nvarchar(80)                   null,
   DeletePoint          decimal(18,2)                  null,
   constraint PK_POINTDUIHUAN primary key clustered (id)
);

/*==============================================================*/
/* Table: ShopMessage                                           */
/*==============================================================*/
create table ShopMessage --商铺信息表
(
   id                   nvarchar(80)                   null,
   ShopName             nvarchar(80)                   null,
   ShopDress            nvarchar(80)                   null,
   ShopManager          nvarchar(80)                   null,
   ShopPhone            nvarchar(80)                   null,
   ShopRent             decimal(18,2)              null,
   ShopEnterDate        date                           null,
   ShopEndDate          date                           null,
   Mallid               nvarchar(80)                   null
);

/*==============================================================*/
/* Table: VipCardMessage                                        */
/*==============================================================*/
create table VipCardMessage --会员卡信息表
(
   id                   nvarchar(80)                   not null,
   SendCardMall         nvarchar(80)                   null,
   VipId                int                            null,
   constraint PK_VIPCARDMESSAGE primary key clustered (id)
);

/*==============================================================*/
/* Table: VipMessage                                            */
/*==============================================================*/
create table VipMessage  --会员信息表
(
   id                   int                            not null,
   VipName              nvarchar(80)                   null,
   Gender               char(2)                        null,
   Birth                date                           null,
   Tellphone            bigint                         null,
   PeopleCard           nvarchar(80)                   null,
   VipAddress           nvarchar(80)                   null,
   OpenCardDate         date                           null,
   Vipmoney             decimal(18,2)                  null,
   constraint PK_VIPMESSAGE primary key clustered (id)
);

/*==============================================================*/
/* Table: VipPointMessage                                       */
/*==============================================================*/
create table VipPointMessage --会员积分表
(
   id                   nvarchar(80)                   not null,
   Point                decimal(18,2)                  null,
   VipId                int                            null,
   constraint PK_VIPPOINTMESSAGE primary key clustered (id)
);

alter table ShopMessage
   add constraint FK_SHOPMESS_REFERENCE_MALLMESS foreign key (Mallid)
      references MallMessage (Id)
     

alter table VipCardMessage
   add constraint FK_VIPCARDM_REFERENCE_VIPMESSA foreign key (VipId)
      references VipMessage (id)
    
insert into MallMessage values  --商城信息表
('SM001','万达广场','莆田城厢区X号街道','张三','0594-10010'),
('SM002','金鼎广场','莆田城厢区X号街道','李四','0594-10011'),
('SM003','红星美凯龙','莆田城厢区X号街道','王五','0594-10012'),
('SM004','正荣财富中心','莆田城厢区X号街道','赵六','0594-10013')

insert into ShopMessage values   --商铺信息表
('SP001','老凤祥','2F-1号','贺芸','10010','50','2001-06-15','2021-06-15','SM001'),
('SP002','周六福','3F-1号','林耀东','10011','30','2008-04-28','2023-04-28','SM002'),
('SP003','周大生','1F-5号','林耀华','10012','60','2009-06-30','2022-06-30','SM003'),
('SP004','中国黄金','1F-8号','林宗辉','10013','45','2010-01-15','2026-01-15','SM004')

insert into VipMessage values   --会员信息表
('001','高明远','男','1998-05-06','1301111222','35011112222','闽大','2001-09-11','600'),
('002','骆山河','男','1998-05-06','1301111222','35011112222','闽大','2001-09-11','800'),
('003','何勇','男','1998-05-06','1301111222','35011112222','闽大','2001-09-11','900'),
('004','孙兴','男','1998-05-06','1301111222','35011112222','闽大','2001-09-11','1200')


insert into VipCardMessage values  --会员卡信息表
('VipCard001','SM001','001'),
('VipCard002','SM002','002'),
('VipCard003','SM003','003'),
('VipCard004','SM004','004')

insert into VipPointMessage values   --会员积分信息表
('S001','2002','001'),
('S002','1500','002'),
('S003','850','003'),
('S004','720','004')

insert into PointDuiHuan values   --积分兑换表
('g001','电冰箱','2000'),
('g002','微波炉','1500'),
('g003','卫生纸','800'),
('g004','洗脸盆','700')


select * from MallMessage  --商城信息表
select * from ShopMessage	--商铺信息表	
select * from VipMessage	--会员信息表
select * from VipCardMessage --会员卡信息表
select * from VipPointMessage --会员积分信息表
select * from PointDuiHuan		--积分兑换表



-- 登录

-- 根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败

select * from Users
where Username='admin' and Password='113'

-- 更进一步，就是当前登录的用户可能已经被注销、禁用等情况，如何应对


-- 注册

-- 用户层面 ，输入用户名，密码，重复密码

-- 注册单个用户
insert into Users (Username,Password) values ('admin','113')


-- 批量注册

insert into Users (Username,Password) values ('user01','113'),('user02','113'),('user03','113'),('user04','113')


--查询老凤祥商铺所属的商城信息
select a.*,b.MallName 商城名称,b.MallDress 地址,b.MallRegisterPeople 法人,b.MallWorkPhone 办公电话 from 
(select Id 商铺编号,ShopName 商城名称,ShopDress 商铺地址,ShopManager 店长,ShopPhone 店长电话,MallId 商城编号 from ShopMessage)a 
inner join MallMessage b on a.商城编号=b.Id
where 商城名称 ='老凤祥' 


--查询高明远什么时间在哪个商城办理了会员卡  要求显示：姓名，性别,手机号  商城的详细信息也要查出来
select a.*,c.* from 
(select VipName,Gender,Tellphone,OpenCardDate,Id from VipMessage where VipName='高明远')a
inner join 
VipCardMessage b on a.Id=b.VipId
inner join 
MallMessage c on b.SendCardMall=c.Id

~~~