-- 登录

-- 根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败



declare @userName nvarchar(80) 
declare @passWord nvarchar(80),@rowPassWord  nvarchar(80)
declare @result nvarchar(80)
declare @isDeleted  nvarchar(80)  --0代表未删除  1代表删除
declare @isActived  nvarchar(80)  --0代表正常    1代表禁用	
declare @count int 

select @count=COUNT(*), @userName=UserName,@rowPassWord=PassWord,@isDeleted=isDeleted,@isActived=isActived from Users

select * from Users

if(@count>0)
	begin
		if(@isDeleted=1)
			begin
				set @result='该用户已被删除！请联系管理员！'
					if(@isActived=1)
						begin
							set @result='该用户状态异常，已被禁用，请联系管理员！'
						end
					else
						begin
							set @result='登录成功！'
						end
			end
		else
			begin
				set @result='登录成功！'
			end
	end
else
	begin
		set @result='用户名或密码有误！登录失败！'
	end



-- 更进一步，就是当前登录的用户可能已经被注销、禁用等情况，如何应对


-- 注册

-- 用户层面 ，输入用户名，密码，重复密码

-- 注册单个用户
insert into Users (Username,Password) values ('admin','113')


-- 批量注册

insert into Users (Username,Password) values ('user01','113'),('user02','113'),('user03','113'),('user04','113')

-- 注册通用逻辑（无关数据库）

-- 1. 判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走
select @count=COUNT(*), @userName=UserName from Users

-- 2.判断用户名和密码出现空格不允许注册 
----------------------------------------------
--(1).用户名中间出现空格的情况
declare @Username nvarchar(80) ='吴煌'	
select len(@Username)总长度	
declare @len int =len(@Username)
select replace(@Username,' ','')空格替换成无
select len(replace(@Username,' ',''))空格替换成无的长度
declare @center int =len(replace(@Username,' ',''))
----------------------------------
--(2).用户名左边出现空格的情况
select len(@userName)字符串长度
set @len  = len(@userName)
select ltrim(@userName)
select len(ltrim(@userName)) 去除左边空格后的长度
declare @ltrim int =len(ltrim(@userName))
----------------------------------------------------
--(3).用户名右边出现空格的情况
select replace(@userName,' ','A')
select len(replace(@userName,' ','A'))替换后字符串长度
set @len=len(replace(@userName,' ','A'))
select len(rtrim(@userName)) 去除右边空格后的长度
declare @rtrim int =len(rtrim(@userName))
--密码出现空格，同理，用以上的三种方法
------------------------------------------------------
declare @count int 
select @count=COUNT(*), @userName=UserName from Users group by UserName
if(@count>0)
	begin
		  print '已存在该用户！请重新输入！'
	end
else
	begin 
		 if(@len>@ltrim or @len>@rtrim or @len>@center)
				begin
					print '有空格！注册失败！'
				end
		 else
			begin
				insert into Users(UserName,PassWord) values(@userName,'123')
			end
	end


select * from Users
delete  Users where UserName='吴煌'
--=================================================================================






-- 1.在会员信息管理的时候，需要选择商场
select id,MallName from MallMessage
-- 2.在会员卡类型管理的时候，需要选择商场

-- 3. 商场信息本身的CRUD （create read update delete）



--查询老凤祥商铺所属的商城信息
select a.*,b.MallName 商城名称,b.MallDress 地址,b.MallRegisterPeople 法人,b.MallWorkPhone 办公电话 from 
(select Id 商铺编号,ShopName 商城名称,ShopDress 商铺地址,ShopManager 店长,ShopPhone 店长电话,MallId 商城编号 from ShopMessage)a 
inner join MallMessage b on a.商城编号=b.Id
where 商城名称 ='老凤祥' 


--查询高明远什么时间在哪个商城办理了会员卡  要求显示：姓名，性别,手机号  商城的详细信息也要查出来
select a.*,c.* from 
(select VipName,Gender,Tellphone,OpenCardDate,Id from VipMessage where VipName='高明远')a
inner join 
VipCardMessage b on a.Id=b.VipId
inner join 
MallMessage c on b.SendCardMall=c.Id



select * from MallMessage  --商城信息表
select * from ShopMessage	--商铺信息表	
select * from VipMessage	--会员信息表
select * from VipCardMessage --会员卡信息表
select * from VipPointMessage --会员积分信息表
select * from PointDuiHuan		--积分兑换表



declare @Username nvarchar(80)='吴煌'				 --此时有一个空格 也就相当于有三个字符 
select len(@userName)   								 --用len查询的字符长度是3
declare @len int =len(@userName)

select DATALENGTH(@userName)

set @Username = replace(@Username,' ','')
select len(@userName) 	
 --后有空格                                     or             前中有空格 
	2xlen(@userName) != DATALENGTH(@userName)					len(@userName) != len(@userName) 			




