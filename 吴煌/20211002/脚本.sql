create database MallVip
go
use MallVip
go


--用户信息表
create table Users
(
	Id			int		primary key identity,
	UserName	nvarchar(80)	not null,
	PassWord	nvarchar(80)	not null,
	isDeleted   nvarchar(80)	default(0)	,  	 --0代表未删除  1代表删除
	isActived   nvarchar(80)	default(0)		 --0代表正常    1代表禁用	
)
go
insert into Users values
('admin','123','0','0'),('user01','123','0','1'),('user02','123','0','0'),('user03','123','0','1')
go
select * from Users
--商城信息表
create table MallMessage
(
	Id int primary key  identity,
	MallName nvarchar(20) not null,
	MallDress nvarchar(80) not null,
	MallRegisterPeople nvarchar(30) not null,
	MallWorkPhone nvarchar(20) not null
)
go
insert into MallMessage values
('万达广场','莆田城厢区X号街道','张三','0594-10010'),
('金鼎广场','莆田城厢区X号街道','李四','0594-10011'),
('红星美凯龙','莆田城厢区X号街道','王五','0594-10012'),
('正荣财富中心','莆田城厢区X号街道','赵六','0594-10013')

select * from MallMessage

create table ShopMessage --商铺信息表
(
	Id int primary key  identity ,
	ShopName nvarchar(20) not null,
	ShopDress nvarchar(80) not null,
	ShopManager nvarchar(30) not null,
	ShopPhone nvarchar(20) not null,
	ShopRent  int not null,
	ShopEnterDate date not null,
	ShopEndDate date not null,
	MallId int foreign key references MallMessage(Id)  
)
go

insert into ShopMessage values
('老凤祥','2F-1号','贺芸','10010','50','2001-06-15','2021-06-15','1'),
('周六福','3F-1号','林耀东','10011','30','2008-04-28','2023-04-28','2'),
('周大生','1F-5号','林耀华','10012','60','2009-06-30','2022-06-30','3'),
('中国黄金','1F-8号','林宗辉','10013','45','2010-01-15','2026-01-15','4')

select * from MallMessage
select * from ShopMessage

create table VipMessage --会员信息表
(
	Id int primary key  identity,
	VipName nvarchar(20) not null,
	Gender  char(2) check(Gender='男' or Gender='女'),
	Birth   date not null,
	Tellphone bigint not null,
	PeopleCard  nvarchar(20) not null,
	VipAddress  nvarchar(20) ,
	OpenCardDate  date not null,
	Vipmoney   decimal
)
go
insert into VipMessage values
('高明远','男','1998-05-06','1301111222','35011112222','闽大','2001-09-11','600'),
('骆山河','男','1998-05-06','1301111222','35011112222','闽大','2001-09-11','800'),
('何勇','男','1998-05-06','1301111222','35011112222','闽大','2001-09-11','900'),
('孙兴','男','1998-05-06','1301111222','35011112222','闽大','2001-09-11','1200')
go


select  * from VipMessage

create table VipCardMessage --会员卡信息
(
	VipCard nvarchar(30) not null,
	SendCardMall int foreign key references MallMessage(Id),
	VipId  int not null foreign key references VipMessage(Id)
)
go
select * from VipCardMessage

insert into VipCardMessage values
('VipCard001','1','1'),
('VipCard002','2','2'),
('VipCard003','3','3'),
('VipCard004','4','4')
go
select * from VipCardMessage

create table VipPointMessage --会员积分信息表
(
	Id int primary key  identity,
	Point   bigint ,
	VipId  int    references VipMessage(Id)
)
go

insert into VipPointMessage values
('2002','1'),
('1500','2'),
('850','3'),
('720','4')
go
select * from VipPointMessage

create table PointDuiHuan
(
	Id int primary key  identity,
	GiftName nvarchar(40) not null,
	DeletePoint bigint ,
)
go

insert into PointDuiHuan values
('电冰箱','2000'),
('微波炉','1500'),
('卫生纸','800'),
('洗脸盆','700')
go

create table VipLoseMessage --会员挂失信息表
(
	Id int primary key identity,
	VipCard nvarchar(80),
	VipId  int
)

select * from MallMessage  --商城信息表
select * from ShopMessage	--商铺信息表	
select * from VipMessage	--会员信息表
select * from VipCardMessage --会员卡信息表
select * from VipPointMessage --会员积分信息表
select * from PointDuiHuan		--积分兑换表
select * from VipLoseMessage  --会员挂失信息表



--查询老凤祥商铺所属的商城信息
select a.*,b.MallName 商城名称,b.MallDress 地址,b.MallRegisterPeople 法人,b.MallWorkPhone 办公电话 from 
(select Id 商铺编号,ShopName 商城名称,ShopDress 商铺地址,ShopManager 店长,ShopPhone 店长电话,MallId 商城编号 from ShopMessage)a 
inner join MallMessage b on a.商城编号=b.Id
where 商城名称 ='老凤祥' 


--查询高明远什么时间在哪个商城办理了会员卡  要求显示：姓名，性别,手机号  商城的详细信息也要查出来
select a.*,c.* from 
(select VipName,Gender,Tellphone,OpenCardDate,Id from VipMessage where VipName='高明远')a
inner join 
VipCardMessage b on a.Id=b.VipId
inner join 
MallMessage c on b.SendCardMall=c.Id



