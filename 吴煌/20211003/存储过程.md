# 存储过程
     在工作中会经常遇到很多重复性的工作，比如今天查小明考勤，明天查小红考勤，这时候就可以把常用的SQL写好存储起来，这就是存储过程。
    这样下次遇到想查谁的考勤，直接使用存储过程就可以了，就不需要再重新写一遍SQL了，这就极大的提高了工作效率。



~~~sql
select * from StuInfo
go


-- 有参数
create proc proc_StuInfo_Parameter
@Name nvarchar(80)
as
begin
	select * from StuInfo where StuName like @Name
end 
go

exec proc_StuInfo_Parameter'刘%'
go

-- 多个参数
create proc proc_StuInfo_Multi_Parameter
@Name nvarchar(80),
@Sex nvarchar,
@Age nvarchar(80)
as
begin
	select * from StuInfo 
	where StuName like @Name 
	and StuSex=@Sex
	and StuAge=@Age
end
go
execute proc_StuInfo_Multi_Parameter '刘%','男','19'
go

-- 默认参数
create proc proc_StuInfo_Default_Parameter
@Name nvarchar(80)='刘德华',
@Sex nvarchar(80)='男',
@Age nvarchar(80)='19'
as
begin
	select * from StuInfo where StuName=@Name and StuSex=@Sex and StuAge=@Age
end

Execute proc_StuInfo_Default_Parameter





select * from StuInfo 
go
~~~
