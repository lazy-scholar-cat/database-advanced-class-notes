use StuDents
go

select * from StuInfo
go

--单个参数返回值
create proc proc_StuInfo
@Name nvarchar(80)output
as
begin
	 select @Name
	 select * from StuInfo where stuid=1
	 select @Name
end

declare @anyName nvarchar(80)='张三'
execute proc_StuInfo @anyName output
select @anyName
go

-- 修改存储过程语法
alter proc proc_StuInfo
@Name nvarchar(80)output
as
begin
	 select @Name
	 select * from StuInfo where stuid=2
end

declare @anyName nvarchar(80)='张三'
execute proc_StuInfo @anyName output
select @anyName
go

-- 多个参数返回值
create proc proc_StuInfoS
@Name nvarchar(80) output,
@Sex char(2) output
as
begin
	  select @Name,@Sex
	  select * from StuInfo where StuId=1
end
go

declare @anyName nvarchar(80)='张三',@anySex char(2)='男'
execute proc_StuInfoS @anyName output,@anySex output
select @anyName,@anySex

-- 删除存储过程语法
drop proc proc_StuInfo
go
-- 存储过程、函数、触发器或视图的最大嵌套层数(最大层数为 32)。

--自定义函数
create function fun_AAA
(
	@Code nvarchar(80)
)
returns nvarchar(10) --表示返回的是字符串类型
as
begin
	 declare @result nvarchar(80)
	 select  @result=StuName from StuInfo where StuId=@Code
	 return @result
end	
go

select  dbo.fun_AAA(1)  --根据学号查询出姓名

-- 删除自定义函数
drop function fun_AAA

select * from StuInfo