create database StuDents
go
use StuDents
go

create table StuInfo
(
	
	id int primary key identity,
	stuName nvarchar(80),
	stuSex  char(2),
	stuAge  int
)
go

insert into Stuinfo values
('张三','男',26),('李四','女',19),('王五','男',22)
insert into Stuinfo values
('张继科','男',30)
go

create table StuClassInfo
(
	
	id int primary key identity,
	ClassName  nvarchar(20),
	stuId  int foreign key references StuInfo(id)
)
go


insert into StuClassInfo values
('Sqlserver',2),('JavaScript',1),('Asp.net MVC',3),('HTML',4)
go



select * from Stuinfo
select * from StuClassInfo

