select * from Area
go

create table stuInfo
(
	id int primary key identity,
	name nvarchar(80)
)
go

insert into stuInfo(name) values ('����'),('����')
select * from stuInfo

create table coursInfo
(
	id int primary key identity,
	coursName nvarchar(80),
	stuId int foreign key references stuInfo(id)
)
go

insert into coursInfo(coursName,stuId) values ('Sqlserver',2),('JavaScript',1)
go
select * from coursInfo

-- �ݹ�
alter function fun_GuoJia(@name nvarchar(80))
returns table
as
return 
	  (	
		with CTE_A
			as
			(
				select Code,AreaName,ParentCode from Area where AreaName=@name
				union all
				select b.Code,b.AreaName,b.ParentCode from Area b,CTE_A a where b.ParentCode=a.Code
				
			)select * from CTE_A
	  )   

select * from fun_GuoJia('����')  --��һ���ؼ��У���ѯ�õؼ��е���һ��

--������ֵ����
alter function fun_Get
(
	@name nvarchar(80)
)
returns @result table(id int,name nvarchar(80)  )
as
begin
	  insert into @result(id,name) select * from stuInfo 
	  return
end


select * from dbo.fun_Get('����')

select * from stuInfo
select * from coursInfo


--��ͼ
create view vw_A
as
select a.name ѧ��,b.coursName �γ� from stuInfo a inner join coursInfo b on a.id=b.stuId 

select * from vw_A


