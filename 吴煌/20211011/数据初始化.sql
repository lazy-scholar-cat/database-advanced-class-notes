create database StuDents
go
use StuDents
go

create table StuInfo
(
	
	id int primary key identity,
	stuName nvarchar(80),
	stuSex  char(2),
	stuAge  int
)
go

insert into Stuinfo values
('张三','男',26),('李四','女',19),('王五','男',22)
insert into Stuinfo values
('张继科','男',30)
go

create table StuCoursInfo
(
	
	id int primary key identity,
	coursName  nvarchar(20),
	stuId  int foreign key references StuInfo(id)
)
go


insert into StuCoursInfo values
('Sqlserver',2),('JavaScript',1),('Asp.net MVC',3),('HTML',4)
go



create table StuScoreInfo
(
	id int primary key identity,
	stuId int foreign key references StuInfo(id),
	coursId int foreign key references StuCoursInfo(id),
	score int
)
go

insert into StuScoreInfo(stuId,coursId,score) values
(1,2,89),(2,1,100),(3,4,59),(4,3,95)
go




select * from Stuinfo
select * from StuCoursInfo
select * from StuScoreInfo


