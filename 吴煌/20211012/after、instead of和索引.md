## 触发器 after
~~~sql
create trigger trg_stuScoreForInserted
on Stuinfo
after
	 insert
as
begin
	 select * from inserted
	 select * from deleted 
end

insert into StuInfo(stuName,stuSex,stuAge)values('马龙','男',29)
~~~
![图裂了！](../img/after.png)

## 触发器 inserted of
~~~sql
alter trigger trg_stuScoreForUpdate
on stuInfo
instead of insert
as
begin
	  declare @name nvarchar(80),@sex nvarchar(80),@Age int
	  select @name=stuName,@sex=stuSex,@Age=stuAge from inserted
	  if(@Age>=0 and @Age<=150 )
		begin
			insert into StuInfo(stuName,stuSex,stuAge) values
			(@name,@sex,@Age)	
		end
	  else
		begin
			 set nocount on
             --当 SET NOCOUNT 为 ON 时，不返回计数（表示受 Transact-SQL 语句影响的行数）。
             --当 SET NOCOUNT 为 OFF 时，返回计数。

			 print '年龄超出范围，请重试'
		end
end

insert into StuInfo values('许昕','男',-1)
~~~
![图裂了！](../img/isteadof.png)

## 随机插入
~~~sql
declare @i int,@name nvarchar(80)
set @i=0
while @i<10000
begin 
	 set @name='A'+cast(cast(rand()*100 as float)as nvarchar)
	 insert into ceShi(name)values(@name)
	 set @i=@i+1
end
~~~
## 什么是索引？
~~~
　　SQL索引有两种:1.聚集索引 2,非聚集索引
	索引主要目的是提高了SQL Server系统的性能，加快数据的查询速度与减少系统的响应时间 
~~~