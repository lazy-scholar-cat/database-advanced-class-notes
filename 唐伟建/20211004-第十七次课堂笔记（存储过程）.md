# 存储过程命名规范



存储过程的命名有这个的语法：

    [proc_执行的表名/主要的表名] By [字段名] [存储过程执行的任务]

    所有的存储过程必须有前缀“proc_”，所有的系统存储过程**都有前缀“sp_”。

## 推荐行为动词后缀

    Select 返回一条或多条记录，如procClientRateSelect

    Insert 插入数据，如procEmailMergeInsert

    Update 更新数据

    Save 插入和更新数据

    Delete 删除数据

    Create 更新表数据，如删除并删除 DropAndCreate

    Output 返回输出参数或0

    Validate 验证操作

    Get 获取记录


## 其他常见行为动词后缀

    Oper 包含增删改查操作

    Join 加入

    Add 添加

    Get 获取

    Set 设置


**注意**：如果存储过程以sp_为前缀开始命名，那么会运行的稍微缓慢，这是因为SQL Server将首先查找系统存储过程，所以决不推荐使用sp_作为前缀。


## 存储过程命名
规范的命名可提高开发和维护的效率，存储过程参考以下命名规范，建议采用动词+表名来描述操作类型。

    存储过程命名包含项目

    * 操作方式

        如典型的CURD代表的增删改查操作

    * 操作对象

        以表名表示单条数据以表名+List表示多条数据以Data表示非单表数据

    * 操作条件

        使用By表示操作条件关键字若条件个数小于等于2个则使用By+条件，如GetUserInfoByUserID、GetUserListByClubID。
        若条件个数大于2个则使用BySome，如GetUserListBySome。
    * 特殊意义

        以For表示特殊意义关键字，多种条件可采用ForAForBFor...，如GetUserListBySomeForPage。

## 存储过程的MAX参数个数为2100个