#第四次笔记

##git reset

1、reset --hard：重置stage区和工作目录

2、reset --soft：保留工作目录，并把重置 HEAD 所带来的新的差异放进暂存区

3、reset 不加参数(mixed)：保留工作目录，并清空暂存区

##git log

1、git log查看当前分支的提交历史，显示的内容是默认的格式

2、git log --oneline参数则只显示提交ID的必要长度和提交说明。

3、git log ----all参数可以将所有分支的提交显示，但是所有的分支提交交叠在一起看着很不方便。

4、git log --graph参数能够将分支区分开来，这样看着更加直观清晰。

5、git log -----nx就显示最近的x个提交，数字是几就显示最近的几个提交。

6、git log ------author='提交者的名字'可以只显示该提交者的提交记录。

7、git log ------before=xxxx/xx/xx指定显示特定日期之前的提交

##order by 语句

1、ORDER BY 语句用于根据指定的列对结果集进行排序。

2、ORDER BY 语句默认按照升序对记录进行排序。

##group by 

1、GROUP BY 语句用于结合聚合函数，根据一个或多个列对结果集进行分组

##partition  by

1、partition  by关键字是分析性函数的一部分，它和聚合函数（如group by）不同的地方在于它能返回一个分组中的多条记录，而聚合函数一般只有一条反映统计值的记录，

2、partition  by用于给结果集分组，如果没有指定那么它把整个结果集作为一个分组。

3、partition by 与group by不同之处在于前者返回的是分组里的每一条数据，并且可以对分组数据进行排序操作。后者只能返回聚合之后的组的数据统计值的记录。
