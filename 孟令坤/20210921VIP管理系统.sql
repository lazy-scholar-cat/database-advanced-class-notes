create database VIP

go

use VIP

go 

create table MallInfo
(
 Mall_ID nvarchar(20) primary key not null,
 Mall_designation nvarchar(40) not null,
 Mall_abbreviation nvarchar(20) not null,
 Mall_Address nvarchar(40) not null,
 Mall_Registrant nvarchar(20) not null,
 Mall_Phone nvarchar(20) not null
)
go
insert into MallInfo values ('SM0001','�򱦹���㳡','��','���Ҵ��X��','����','597-77889900'),
('SM0002','��ﹺ��㳡','���','���Ҵ��X��','����','0597-77889900')

go

use VIP
go
create table MemberInfo
(
 Meb_number nvarchar(20) primary key not null,
 Meb_name nvarchar(20) not null,
 Meb_Sex nvarchar(5) default('��') check(Meb_Sex = '��' or Meb_Sex = 'Ů'),
 Meb_birth date,
 Meb_phone nvarchar(20) not null,
 Meb_IDnumber nvarchar(40) not null,
 Meb_address nvarchar(20)
)

go
insert into MemberInfo values ('Vip001','�����','Ů','2020-12-08','130334455566','3506465656','����'),
('Vip002','������','Ů','2020-12-08','130334455566','3506465656','����')

go
use VIP
go
create table VIPcardInfo
(
 ID nvarchar(20) not null,
 CardId nvarchar(20),
 Cardtype nvarchar(20),
 Cardorganization nvarchar(20),
 Cardnumber nvarchar(20) references Memberinfo(Meb_number) not null,
)
go
insert into VIPcardInfo values ('VipCard0001','X1001','CardType0001','���','Vip001')
go
use VIP
go

create table CardtypeInfo
(
 typeId nvarchar(20) not null,
 typeName nvarchar(20) not null,
 typeremarks nvarchar(20)
)
go
insert into CardtypeInfo values ('CardType0001','��ͨ��Ա��','һ�������ϵĻ�Ա��'),
('CardType0002','�洢��','�񿨡���������Ա��������')
go
use VIP
go

create table ShopsInfo
(
 ShopID nvarchar(20) primary key not null,
 ShopName nvarchar(20),
 Shopposition nvarchar(20) not null,
 Shopoperator nvarchar(20) not null
)
go
insert into ShopsInfo values ('Shop0001','Esprit','��¥-1C','�����'),
('Shop0002','���ı���','��¥-3C','���´�')
go
use VIP
go

create table RuleInfo
(
 RuleID nvarchar(20) primary key not null,
 RuleActivityID nvarchar(20),
 Rulea nvarchar(20)
)
go
insert into RuleInfo values ('Arule0001','Ac0001','1Ԫ��1��'),
('Arule0002','Ac0002','1Ԫ��2��')
go
use VIP
go

create table MempointsInfo
(
 IntegralID nvarchar(20) primary key not null,
 Meb_number nvarchar(20) references Memberinfo(Meb_number) not null,
 Integral nvarchar(20) not null
)
go
insert into MempointsInfo values ('Point0001','Vip001','50000'),
('Point0002','Vip002','5358')
go
use VIP
go

create table GradeInfo
(
 GradeID nvarchar(20) primary key not null,
 GradeName nvarchar(25) not null,
 Gradeabbreviation nvarchar(20) not null,
 Gradecondition nvarchar(20) not null
)
go
insert into GradeInfo values ('Level0001','һ����Ա','0��ͨ��Ա','>100'),
('Level0002','������Ա','������Ա','>10000')
go
use VIP
go

create table ActivityInfo
(
 ActID nvarchar(20) primary key not null,
 ActName nvarchar(20) not null,
 Actdate datetime,
 Actenddate datetime
)
go
insert into ActivityInfo values ('Ac0001','�ճ��','2021-09-01 00:00:00','2022-08-31'),
('Ac0002','������ͷ�','2021-09-18 00:00:00','2021-09-21')
go
use VIP
go

create table VehicleInfo
(
 VehID nvarchar(20) primary key not null,
 Meb_number nvarchar(20) references Memberinfo(Meb_number) not null,
 Vehnumber nvarchar(20) not null
)
go
insert into VehicleInfo values ('Car0001','Vip001','��D XJ803'),
('Car0002','Vip002','��A FM1007')
go
use VIP
go

create table LossInfo
(
 LossID nvarchar(20) primary key not null,
 LossCardID nvarchar(20),
 Meb_number nvarchar(20) references Memberinfo(Meb_number) not null
)
go
insert into LossInfo values ('ReportLose0001','VipCard0002','Vip001')
go
use VIP
go

create table Exchange
(
 ExcID nvarchar(20) primary key not null,
 Excgift nvarchar(20),
 Meb_number nvarchar(20) references Memberinfo(Meb_number) not null
)
go 
insert into Exchange values ('Exchange0001','Gift0001','Vip001'),
('Exchange0002','Gift0004','Vip002')


select * from MallInfo
select * from ActivityInfo
select * from CardtypeInfo
select * from Exchange
select * from GradeInfo
select * from LossInfo
select * from MemberInfo
select * from MempointsInfo
select * from RuleInfo
select * from ShopsInfo
select * from VehicleInfo
select * from VIPcardInfo