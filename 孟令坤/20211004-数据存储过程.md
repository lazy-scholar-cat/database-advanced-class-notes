# 今天收获蛮多的

## 数据库存储的命名规范
~~~~~~~~~
1、语法：[proc] [sp]

2、proc是所有存储过程必须要有的前缀，sp是系统存储过程要有的前缀

3、表的名称就是存储过程访问的对象

4、字段名是条件语句

5、exec 是执行语句

6、如果存储过程返回一条记录那么后缀是：Select

7、如果存储过程插入数据那么后缀是：Insert

8、如果存储过程更新数据那么后缀是：Update

9、如果存储过程有插入和更新那么后缀是：Save

10、如果存储过程删除数据那么后缀是：Delete

11、如果存储过程更新表中的数据 (ie. drop and create) 那么后缀是：Create

12、如果存储过程返回输出参数或0，那么后缀是：Output
~~~~~~~~~

## 存储过程的基本语法

无参数
~~~~~
create proc proc_表名（存储过程）
as
begin
  select * from 表名
  end

  exec proc_表名（执行语句）
~~~~~

有参数
~~~~~
create proc proc_表名parameter（存储过程）
参数@name（随便命名） nvarchar(45)
as
begin
select * from 表名
where 字段名称 like @name '%(名字中的其中一个字)%'
end

exec proc_表名（执行语句）
~~~~~

默认参数
~~~~
create proc proc_表名（存储过程）
参数@name = '赋值'
参数@sex = '赋值'
参数@classID = '赋值'
as
begin 
select * from 表名
where 字段名称 like @name and 字段名称 = @sex and 字段名称 = @classID
end

exec proc_表名
~~~~



## 参数上限

1、存储过程最大参数数量：2100

2、存储过程中最大的本地变量数量没有固定限制，由可用内存决定

3、存储过程最大可用内存为128M

[最大容量](https://docs.microsoft.com/zh-cn/sql/sql-server/maximum-capacity-specifications-for-sql-server?view=sql-server-ver15)

## 我的代码
~~~~
create table StuInfo
(
 ID int primary key identity(1,1) not null,
 Name nvarchar(20) not null,
 Birthday datetime not null,
 Sex nvarchar(2) default('男') not null,
 ClassID char(2) not null
)  

insert into StuInfo values(1,'张三','1998-12-25','男','2'),(2,'陈璐萍','2000-05-05','女','2')

select * from StuInfo

go
create proc proc_StuInfo --无参数
as
begin 
  select * from StuInfo
end
go
exec proc_StuInfo

go
create proc proc_StuInfoPrameter --有参数
@name nvarchar(80)
as
begin
select * from StuInfo
where Name like @name 
end 

go 

exec proc_StuInfoPrameter '%陈%'
go

create proc proc_StuInfoSomePrameter --有多个参数
@name nvarchar(80),
@classid char(2)
as
begin
select * from  StuInfo
where Name like @name or ClassID like @classid
end
go
exec proc_StuInfoSomePrameter '陈','2'

go
create proc proc_StuInfoDefaultPrameter
@id int = 2,
@name nvarchar(80) = '陈',
@birthday datetime ='2002-05-05',
@sex nvarchar(2) = '女',
@classid char(2) = '2'
as
begin
 select * from StuInfo
 where ID like @id or Name like @name
 or Birthday > @birthday or Sex = @sex or ClassID = @classid
 end
 exec proc_StuInfoDefaultPrameter      --有点问题执行出来是空表
~~~~

## 老师代码

```sql
-- 存储过程语法

/*
create proc 存储过程名称
as
begin


end
*/

select * from StudentInfo
go


create proc proc_SelectStudentInfo
as
begin
	select * from StudentInfo
end
go

exec proc_SelectStudentInfo

go

-- 有参数的存储过程

create proc proc_SelectStudentInfoWithPrameter
@name nvarchar(80)
as
begin
	select * from StudentInfo
	where StudentName like @name
end
go

exec proc_SelectStudentInfoWithPrameter '%李%'


go

-- 有多个参数的存储过程
create proc proc_SelectStudentInfoWithSomeParameters
@name nvarchar(80),
@code nvarchar(80)
as
begin
	select * from StudentInfo
	where StudentName like @name or StudentCode like @code

end
go

exec proc_SelectStudentInfoWithSomeParameters '88',''

go

-- 有默认值的存储过程

create proc proc_SelectStudentInfoWithAnyParameters
@code nvarchar(80)='01',
@name nvarchar(80)='%李%',
@birthday date='2021-10-03',
@sex char='m',
@classId int='1'
as
begin
	select * from StudentInfo
	where StudentCode like @code 
	or StudentName like @name
	or Birthday<@birthday
	or sex =@sex
	or ClassId= @classId
end

exec proc_SelectStudentInfoWithAnyParameters    




