create database ClassicDb
GO

use ClassicDb
GO

create table StudentInfo
(
    Id int PRIMARY key not null IDENTITY,
    StudentCode nvarchar(80),
    StudentName nvarchar(80),
    Birthday date not null,
    Sex nvarchar(2),
    ClassId int not null
)

GO

-- select * from StudentInfo

insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('01' , '赵雷' , '1990-01-01' , 'm',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('02' , '钱电' , '1990-12-21' , 'm',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('03' , '孙风' , '1990-12-20' , 'm',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('04' , '李云' , '1990-12-06' , 'm',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('05' , '周梅' , '1991-12-01' , 'f',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('06' , '吴兰' , '1992-01-01' , 'f',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('07' , '郑竹' , '1989-01-01' , 'f',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('09' , '张三' , '2017-12-20' , 'f',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('10' , '李四' , '2017-12-25' , 'f',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('11' , '李四' , '2012-06-06' , 'f',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('12' , '赵六' , '2013-06-13' , 'f',1)
insert into StudentInfo (StudentCode,StudentName,Birthday,Sex,ClassId) values('13' , '孙七' , '2014-06-01' , 'f',1)


GO


CREATE TABLE Teachers
(
    Id int PRIMARY key not null IDENTITY,
    TeacherName nvarchar(80)
)

go
-- select * from Teachers

insert into Teachers (TeacherName) values('张三')
insert into Teachers (TeacherName) values('李四')
insert into Teachers (TeacherName) values('王五')

GO

create table CourseInfo
(
    Id int PRIMARY key not null IDENTITY,
    CourseName NVARCHAR(80) not null,
    TeacherId int not null
)

go
-- select * from CourseInfo

insert into CourseInfo (CourseName,TeacherId) values( '语文' , 2)
insert into CourseInfo (CourseName,TeacherId) values( '数学' , 1)
insert into CourseInfo (CourseName,TeacherId) values( '英语' , 3)

GO

create table StudentCourseScore
(
    Id int PRIMARY key not null IDENTITY,
    StudentId int not null,
    CourseId int not null,
    Score int not null
)
go
-- select * from StudentCourseScore

insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='01') , 1 , 80)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='01') , 2 , 90)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='01') , 3 , 99)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='02') , 1 , 70)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='02') , 2 , 60)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='02') , 3 , 80)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='03') , 1 , 80)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='03') , 2 , 80)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='03') , 3 , 80)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='04') , 1 , 50)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='04') , 2 , 30)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='04') , 3 , 20)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='05') , 1 , 76)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='05') , 2 , 87)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='06') , 1 , 31)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='06') , 3 , 34)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='07') , 2 , 89)
insert into StudentCourseScore (StudentId,CourseId,Score) values((select Id from StudentInfo where StudentCode='07') , 3 , 98)

go

-- 练习题目：
-- 1.查询"数学 "课程比" 语文 "课程成绩高的学生的信息及课程分数
select c.学号, StudentInfo.StudentName 姓名,c.数学成绩,c.语文成绩  from
 (
   select a.StudentId 学号,a.Score 数学成绩,b.Score 语文成绩  from 
	(
		select * from StudentCourseScore 
		where CourseId = 1
	)a

	join

	(
		select * from StudentCourseScore 
		where CourseId = 2
	)b 
	on a.StudentId = b.StudentId
	where a.Score < b.Score
  )c 

  join 
   StudentInfo 
   on c.学号 = StudentInfo.Id
-- 1.1 查询同时存在" 数学 "课程和" 语文 "课程的情况
	--select * from StudentCourseScore where CourseId in(1,2) 	
   select  a.StudentId 学号,StudentInfo.StudentName 姓名  from 
   (select StudentId from StudentCourseScore where CourseId in(1,2) group by StudentId having count(StudentId) >1) a
	join 
	StudentInfo
	on a.StudentId = StudentInfo.Id


-- 1.2 查询存在" 数学 "课程但可能不存在" 语文 "课程的情况(不存在时显示为 null )
	select a.StudentId 学号,a.Score 数学成绩,b.Score 语文成绩  from 
(
    select * from StudentCourseScore 
	where CourseId = 1
)a
left join
(
	select * from StudentCourseScore 
	where CourseId = 2
)b on a.StudentId = b.StudentId

-- 1.3 查询不存在" 数学 "课程但存在" 语文 "课程的情况 
select c.StudentId 学号,StudentInfo.StudentName 姓名  from 
(
select a.StudentId  from
    (select StudentId from StudentCourseScore where CourseId != 1 group by StudentId ) a
                  left join
	              ( select StudentId from StudentCourseScore where CourseId = 2 group by StudentId ) b
                  on a.StudentId = b.StudentId   
)c 
 left join
	StudentInfo 
 on c.StudentId = StudentInfo.id


-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩
  
	select a.学号,StudentInfo.StudentName 姓名, a.平均成绩 from 
	(
	select StudentId 学号, avg(Score)平均成绩 from StudentCourseScore group by StudentId having avg(Score) >= 60
	) a
	left join 
	StudentInfo 
	on a.学号 = StudentInfo.Id

-- 3.查询在 成绩 表存在成绩的学生信息
select * from StudentInfo where Id in (select StudentId from StudentCourseScore group by StudentId)
-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的总成绩(没成绩的显示为 null )
 --select * from StudentInfo,CourseInfo,StudentCourseScore

 select StudentInfo.Id 编号,StudentInfo.StudentName 姓名,a.选课总数,a.总成绩 from 
 StudentInfo 
 left join
 (
 select StudentId 学号,count( distinct CourseId  )选课总数,sum(Score)总成绩 from StudentCourseScore group by StudentId 
 ) a
 on a.学号 = StudentInfo.Id

-- 4.1 查有成绩的学生信息
 select a.学号,StudentInfo.StudentName 姓名, StudentInfo.Birthday 生日,StudentInfo.ClassId 班级 ,StudentInfo.Sex 性别 from
 (
 select StudentId 学号,count(CourseId)选课数 from StudentCourseScore  group by StudentId  having count(CourseId) > 0
 )a
 left join 
 StudentInfo 
 on a.学号 = StudentInfo.Id

-- 5.查询「李」姓老师的数量
 select count(TeacherName)李姓老师个数 from Teachers where TeacherName like '李%' group by TeacherName 

-- 6.查询学过「张三」老师授课的同学的信息
   --1.在老师表中找出张三对应的id
   select Id from CourseInfo where TeacherId in 
   (select Id from Teachers where TeacherName = '张三')
   --3.把对应id子查询放入分数表再对学生id分组(用 in 是因为上面用了in可能会产生多个张三对应多个课程id 分组是因为一个学生一门课程可能重考)
   select StudentId from StudentCourseScore where CourseId in 
   (select Id from CourseInfo where TeacherId = 
   (select Id from Teachers where TeacherName = '张三')) 
   group by StudentId
   --4.最后连接学生
    select 学号, StudentCode 编码, StudentName 姓名, Birthday 生日, Sex 性别, ClassId 班级 from 
    (
		  select  StudentId 学号 from StudentCourseScore where CourseId in (select Id from CourseInfo where TeacherId = (select Id from Teachers where TeacherName = '张三')) group by StudentId
    ) a
    left join
   studentinfo 
   on studentinfo.Id = a.学号

-- 7.查询没有学全所有课程的同学的信息
select * from StudentCourseScore
--1.先在课程表中计算总课程数
select count(Id) from CourseInfo 
--2.在分数表中子查询学全了所有课程的学生学号
select StudentId,count(distinct CourseId) 
from StudentCourseScore 
group by StudentId 
having count(CourseId) =(select count(Id) from CourseInfo )
--3.在学生表中进行排除
select * from studentinfo where Id not in
(select StudentId from StudentCourseScore group by StudentId having count(CourseId) =(select count(Id) from CourseInfo )) 
-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息
  --与学生信息表连接完整学生信息
  select a.id,StudentCode 编号,StudentName 姓名, Birthday 生日,Sex 性别,ClassId 班级,a.和01相同的课程总数 from 
  (
	  select StudentId id,count(CourseId) 和01相同的课程总数 from StudentCourseScore where StudentId !=
	  (select Id from studentinfo where StudentCode =01) and CourseId in
	  (select distinct CourseId from StudentCourseScore where StudentId =
	  (select Id from studentinfo where StudentCode =01))  
      group by StudentId 
  )a
  left join
  studentinfo
  on 
  studentinfo.id = a.id
-- 9.查询和" 01 "号的同学学习的课程 完全相同的其他同学的信息
--因为学没学是从有没有考试记录看的，又给课程加了去重，所以只要是和01学的课程总数一样就是学的一样：直接把上面搬下来改一点点
  select a.id,StudentCode 编号,StudentName 姓名, Birthday 生日,Sex 性别,ClassId 班级,a.和01相同的课程总数 from 
  (
	  select StudentId id,count(CourseId) 和01相同的课程总数 from StudentCourseScore where StudentId !=
	  (select Id from studentinfo where StudentCode =01) and CourseId in
	  (select distinct CourseId from StudentCourseScore where StudentId =
	  (select Id from studentinfo where StudentCode =01))  
      group by StudentId  
	  having count(CourseId) = 3
  )a
  left join
  studentinfo
  on 
  studentinfo.id = a.id
-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名
   select * from StudentInfo where id not in (select StudentId 
   from StudentCourseScore where CourseId in 
   (select Id from CourseInfo where TeacherId in 
   (select Id from Teachers where TeacherName = '张三')))
-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩

--先找出不及格的学号(不能先分组)
--1.不考虑重考的情况
 select a.StudentId 学号,StudentName 姓名, a.平均成绩 from 
  (
	  select StudentId,avg(Score) 平均成绩 from StudentCourseScore where StudentId in (select StudentId from StudentCourseScore where Score < 60) group by StudentId
  )a
  left join
  studentinfo
  on 
  studentinfo.id = a.StudentId
--2.重考的情况
 --同上连接学生信息表补全信息
 select b.StudentId 学号,StudentName 姓名, b.平均成绩 from 
  (
	 select StudentId,avg(Score) 平均成绩 from StudentCourseScore where StudentId in (select a.StudentId from (select StudentId,CourseId ,avg(Score) 单科平均成绩 from StudentCourseScore  group by StudentId,CourseId  having avg(Score) < 60) a
) group by StudentId
  )b
  left join
  studentinfo
  on 
  studentinfo.id = b.StudentId

-- 12.检索" 数学 "课程分数小于 60，按分数降序排列的学生信息
	--补全信息
	 select id 学号,StudentName 姓名, b.平均成绩 from 
  (
		select StudentId,avg(Score) 平均成绩 from StudentCourseScore where StudentId in(	select  a.StudentId from  (select StudentId,CourseId from StudentCourseScore where CourseId = (select Id from CourseInfo where CourseName = '数学') group by StudentId,CourseId) a
)
  )b
  left join
  studentinfo
  on 
  studentinfo.id = b.StudentId
-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩
select c.学号,c.姓名,c.所有课程总成绩, c.平均成绩  from 
(
select b.学号,StudentName 姓名,b.所有课程总成绩, b.平均成绩 from 
  (
select StudentId 学号,sum(Score) 所有课程总成绩,avg(Score)平均成绩 from StudentCourseScore group by StudentId 
  )b
  left join
  studentinfo
  on 
  studentinfo.id = b.学号
  )c
  order by c.平均成绩 desc
-- 14.查询各科成绩最高分、最低分和平均分：
select CourseId,max(Score)最高分,min(Score)最低分,avg(Score)平均分 from StudentCourseScore group by CourseId 
-- 15.以如下形式显示：课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
/*
	及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90

	要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序排列

	按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺
*/	
	
-- 15.1 按各科成绩进行排序，并显示排名， Score 重复时合并名次
 select StudentId 学号 ,sum(Score) 语文成绩, DENSE_RANK() over(order by sum(Score) desc) as '语文排名'  
 from StudentCourseScore 
 where CourseId = 1 
 group by StudentId 

-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺
select  StudentId 学号,sum(Score) 总成绩,RANK() over(order by sum(Score) desc) as '总成绩排名' 
from StudentCourseScore 
group by StudentId  

-- 16.1 查询学生的总成绩，并进行排名，总分重复时不保留名次空缺
select StudentId  学号 ,sum(Score) 总成绩,DENSE_RANK() over(order by sum(Score) desc) as 总成绩排名 
from StudentCourseScore 
group by StudentId 
	
--终于成功了	
select  StudentId 学号,sum(Score)总成绩, '排名' = IDENTITY(int, 1, 1)   into #newStudentCourseScore from StudentCourseScore group by StudentId order by sum(Score) desc
select * from #newStudentCourseScore


-- 17.统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比
select  CourseId 课程编号,count(StudentId )人数 from StudentCourseScore group by CourseId 
--sum(StudentId )是计算分组之后所有组CourseId里面的StudentId总数
--sum(StudentId )是计算分组之后每个组CourseId里面的StudentId总数
select * from StudentCourseScore
-- 18.查询各科成绩前三名的记录
select * from StudentCourseScore where CourseId =1 and Score in 
(select top 3 a.Score 
from (select  distinct(Score) from  StudentCourseScore where CourseId = 1  ) a 
order by a.Score desc
)
-- 19.查询每门课程被选修的学生数
select CourseName 课程名称, 课程编号 ,选修学生数 from 
(
select CourseId 课程编号,count(StudentId) 选修学生数 from StudentCourseScore group by CourseId 
) a
left join 
CourseInfo
on a.课程编号 = CourseInfo.Id
-- 20.查询出只选修两门课程的学生学号和姓名
select StudentId 学号,StudentName 姓名 from 
(
select  StudentId ,count(distinct CourseId) 课程id from StudentCourseScore group by StudentId having count(distinct CourseId) =2
) a
left join 
studentinfo
on a.StudentId = studentinfo.Id
-- 21.查询男生、女生人数
select 男生人数,女生人数 from 
(
select count(Id)男生人数  from studentinfo where Sex = 'm'
) a
left join
(
select count(Id)女生人数 from studentinfo where Sex = 'f'
) b
on '张益飞很帅' ='张益飞很帅'
-- 22.查询名字中含有「风」字的学生信息
select * from studentinfo where StudentName like '%风%'
-- 23.查询同名同性学生名单，并统计同名人数
--先把建立同名同姓的学生放在一起再作为临时表
select * from 
(
select distinct (a.StudentName) from studentinfo  a join studentinfo b on a.Id<>b.Id where a.StudentName = b.StudentName
) a
 left join
 (
select count(StudentName) 同名同姓总人数 from (select a.StudentName from studentinfo  a join studentinfo b on a.Id<>b.Id where a.StudentName = b.StudentName
   )e
)f
on 1=1

-- 24.查询 1990 年出生的学生名单
select * from StudentInfo  where Birthday ='1990-01-01'

-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列
select CourseId,avg(Score)平均成绩 
from StudentCourseScore group by CourseId 
order by avg(Score) desc ,CourseId asc

-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩
select StudentId 学号 ,StudentName 姓名, 平均成绩  from 
(
select StudentId ,avg(Score) 平均成绩 from StudentCourseScore group by StudentId having avg(Score) >= 85
) a
left join studentinfo
 on a.StudentId = studentinfo.Id

-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数
select StudentName 姓名 ,Score 分数 from 
(
select StudentId,Score from StudentCourseScore where CourseId = 2 and Score < 60 
) a
left join studentinfo
 on a.StudentId = studentinfo.Id
 
-- 28.查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）
select a.StudentName 姓名, a.Id 学号,b.CourseId 课程编号,b.Score 分数 
from studentinfo a 
left join StudentCourseScore b  
on a.Id = b.StudentId
-- 29.查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数
select StudentName 姓名 from studentinfo a  left join( 
select StudentId  
from StudentCourseScore where Score > 70 
group by StudentId)b on a.Id = b.StudentId

select CourseName 课程名称 from CourseInfo a left join( 
select CourseId 
from StudentCourseScore 
where Score > 70  
group by CourseId) b on a.Id = b.CourseId


-- 30.查询不及格的课程
select CourseName 有不及格情况的课程名称 
from CourseInfo left join 
(select CourseId from StudentCourseScore  where Score < 60 group by CourseId)a 
on CourseInfo.Id = a.CourseId 


-- 31.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名

-- 32.求每门课程的学生人数

-- 33.成绩不重复，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩

--34.成绩有重复的情况下，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩

-- 35.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩

-- 36.查询每门功成绩最好的前两名

-- 37.统计每门课程的学生选修人数（超过 5 人的课程才统计）。

-- 38.检索至少选修两门课程的学生学号

-- 39.查询选修了全部课程的学生信息

-- 40.查询各学生的年龄，只按年份来算

-- 41.按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一

-- 42.查询本周过生日的学生

-- 43.查询下周过生日的学生

-- 44.查询本月过生日的学生

-- 45.查询下月过生日的学生