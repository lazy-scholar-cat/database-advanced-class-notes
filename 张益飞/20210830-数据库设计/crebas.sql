/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     2021/9/20 19:24:48                           */
/*==============================================================*/

--use master
--create database SCVIP

--drop database SCVIP


--if exists(select 1 from sys.sysforeignkey where role='FK_会员卡信息表_REFERENCE_会员信息表') then
--    alter table 会员卡信息表
--       delete foreign key FK_会员卡信息表_REFERENCE_会员信息表
--end if;

--if exists(select 1 from sys.sysforeignkey where role='FK_会员卡信息表_REFERENCE_会员类型表') then
--    alter table 会员卡信息表
--       delete foreign key FK_会员卡信息表_REFERENCE_会员类型表
--end if;

--if exists(select 1 from sys.sysforeignkey where role='FK_会员卡充值记录表_REFERENCE_会员卡信息表') then
--    alter table 会员卡充值记录表
--       delete foreign key FK_会员卡充值记录表_REFERENCE_会员卡信息表
--end if;

--if exists(select 1 from sys.sysforeignkey where role='FK_会员积分表_REFERENCE_会员信息表') then
--    alter table 会员积分表
--       delete foreign key FK_会员积分表_REFERENCE_会员信息表
--end if;

--if exists(select 1 from sys.sysforeignkey where role='FK_店铺折扣力度表_REFERENCE_店铺信息表') then
--    alter table 店铺折扣力度表
--       delete foreign key FK_店铺折扣力度表_REFERENCE_店铺信息表
--end if;

--if exists(select 1 from sys.sysforeignkey where role='FK_挂失记录表_REFERENCE_会员信息表') then
--    alter table 挂失记录表
--       delete foreign key FK_挂失记录表_REFERENCE_会员信息表
--end if;

--if exists(select 1 from sys.sysforeignkey where role='FK_消费记录表_REFERENCE_会员信息表') then
--    alter table 消费记录表
--       delete foreign key FK_消费记录表_REFERENCE_会员信息表
--end if;

--if exists(select 1 from sys.sysforeignkey where role='FK_消费记录表_REFERENCE_会员卡信息表') then
--    alter table 消费记录表
--       delete foreign key FK_消费记录表_REFERENCE_会员卡信息表
--end if;

--if exists(select 1 from sys.sysforeignkey where role='FK_消费记录表_REFERENCE_店铺信息表') then
--    alter table 消费记录表
--       delete foreign key FK_消费记录表_REFERENCE_店铺信息表
--end if;

--if exists(select 1 from sys.sysforeignkey where role='FK_积分交易表_REFERENCE_会员积分表') then
--    alter table 积分交易表
--       delete foreign key FK_积分交易表_REFERENCE_会员积分表
--end if;

--drop table if exists 会员信息表;

--drop table if exists 会员卡信息表;

--drop table if exists 会员卡充值记录表;

--drop table if exists 会员活动表;

--drop table if exists 会员积分表;

--drop table if exists 会员类型表;

--drop table if exists 商城信息表;

--drop table if exists 店铺信息表;

--drop table if exists 店铺折扣力度表;

--drop table if exists 挂失记录表;

--drop table if exists 消费记录表;

--drop table if exists 积分交易表;

/*==============================================================*/
/* Table: 会员信息表                                                 */
/*==============================================================*/


create table 会员信息表 
(
   会员编号                 nvarchar(80)                   not null,
   姓名                   nvarchar(80)                   null,
   性别                   nvarchar(80)                   null,
   身份证                  nvarchar(80)                   null,
   手机号                  nvarchar(80)                   null,
   constraint PK_会员信息表 primary key clustered (会员编号)
);
insert into 会员信息表 values('U01','老黄','f','360...','187...')
insert into 会员信息表 values('U02','老赖','m','360...','177...')
insert into 会员信息表 values('U03','老胡','f','360...','167...')



/*==============================================================*/
/* Table: 会员卡信息表                                                */
/*==============================================================*/
create table 会员卡信息表 
(
   卡编号                  nvarchar(80)                   not null,
   外键类型编号               nvarchar(80)                   null,
   会员编号                 nvarchar(80)                   null,
   开卡时间                 nvarchar(80)                   null,
   constraint PK_会员卡信息表 primary key clustered (卡编号)
);

insert into 会员卡信息表 values('K01','V01','U01','2022-13-58')
insert into 会员卡信息表 values('K02','V02','U01','2030-31-88')
insert into 会员卡信息表 values('K03','V03','U02','2078-73-58')


/*==============================================================*/
/* Table: 会员卡充值记录表                                              */
/*==============================================================*/
create table 会员卡充值记录表 
(
   序号                   nvarchar(80)                   not null,
   外键卡编号                nvarchar(80)                   null,
   充值金额                 nvarchar(80)                   null,
   充值时间                 nvarchar(80)                   null,
   余额                   nvarchar(80)                   null,
   constraint PK_会员卡充值记录表 primary key clustered (序号)
);

insert into 会员卡充值记录表 values('CM01','K01','10000','2050-09-16','10000')
insert into 会员卡充值记录表 values('CM02','K03','5050','2070-09-16','10')


/*==============================================================*/
/* Table: 会员活动表                                                 */
/*==============================================================*/
create table 会员活动表 
(
   活动编号                 nvarchar(80)                   not null,
   活动名称                 nvarchar(80)                   null,
   主办方                  nvarchar(80)                   null,
   负责人                  nvarchar(80)                   null,
   手机号                  nvarchar(80)                   null,
   时间                   nvarchar(80)                   null,
   地点                   nvarchar(80)                   null,
   活动内容                 nvarchar(80)                   null,
   constraint PK_会员活动表 primary key clustered (活动编号)
);

insert into 会员活动表 values('AT02','秒杀会员','万宝','老万','187..','2090-14-56','14栋栏杆','会员全场半价')


/*==============================================================*/
/* Table: 会员积分表                                                 */
/*==============================================================*/
create table 会员积分表 
(
   积分编号                 nvarchar(80)                   not null,
   外键会员编号               nvarchar(80)                   null,
   总积分                  nvarchar(80)                   null,
   constraint PK_会员积分表 primary key clustered (积分编号)
);


insert into 会员积分表 values('J01','U01','90分')
insert into 会员积分表 values('J02','U02','60分')

/*==============================================================*/
/* Table: 会员类型表                                                 */
/*==============================================================*/
create table 会员类型表 
(
   类型编号                 nvarchar(80)                   not null,
   类型名称                 nvarchar(80)                   null,
   价格                   nvarchar(80)                   null,
   constraint PK_会员类型表 primary key clustered (类型编号)
);

insert into 会员类型表 values('V01','VIP','666')
insert into 会员类型表 values('V02','SVIP','888')
insert into 会员类型表 values('V03','SSVIP','999')


/*==============================================================*/
/* Table: 商城信息表                                                 */
/*==============================================================*/
create table 商城信息表 
(
   商城编号                 nvarchar(80)                   not null,
   名称                   nvarchar(80)                   null,
   位置                   nvarchar(80)                   null,
   负责人                  nvarchar(80)                   null,
   手机号                  nvarchar(80)                   null,
   constraint PK_商城信息表 primary key clustered (商城编号)
);


insert into 商城信息表(商城编号,名称,位置,负责人,手机号) values('SM01','万达','莆田仙游枫亭','德尔塔','0791-120')

/*==============================================================*/
/* Table: 店铺信息表                                                 */
/*==============================================================*/
create table 店铺信息表 
(
   店铺编号                 nvarchar(80)                   not null,
   店铺名称                 nvarchar(80)                   null,
   负责人                  nvarchar(80)                   null,
   手机号                  nvarchar(80)                   null,
   好评率                  nvarchar(80)                   null,
   开铺时间                 nvarchar(80)                   null,
   坐标                   nvarchar(80)                   null,
   constraint PK_店铺信息表 primary key clustered (店铺编号)
);


insert into 店铺信息表 values('D01','喜莱客','老喜','188...','99%','2028-28-85','2F-A1')
insert into 店铺信息表 values('D02','土耳其烤肉','老头','187...','1%','2028-28-85','4F-B1')

/*==============================================================*/
/* Table: 店铺折扣力度表                                               */
/*==============================================================*/
create table 店铺折扣力度表 
(
   序号                   int                            not null,
   外键店铺编号               nvarchar(80)                   null,
   VIP                  nvarchar(80)                   null,
   SVIP                 nvarchar(80)                   null,
   SSVIP                nvarchar(80)                   null,
   constraint PK_店铺折扣力度表 primary key clustered (序号)
);

insert into 店铺折扣力度表 values(1,'D01','9折','7折','6折')
insert into 店铺折扣力度表 values(2,'D02','5折','3折','1折')

/*==============================================================*/
/* Table: 挂失记录表                                                 */
/*==============================================================*/
create table 挂失记录表 
(
   序号                   nvarchar(80)                   not null,
   外键会员编号               nvarchar(80)                   null,
   挂失时间                 nvarchar(80)                   null,
   挂失原因                 nvarchar(80)                   null,
   挂失地点                 nvarchar(80)                   null,
   constraint PK_挂失记录表 primary key clustered (序号)
);

insert into 挂失记录表 values('GS01','U01','3202-25-85','穷','万宝1F-B15')


/*==============================================================*/
/* Table: 消费记录表                                                 */
/*==============================================================*/
create table 消费记录表 
(
   序号                   nvarchar(80)                   not null,
   外键会员编号               nvarchar(80)                   null,
   外键卡编号                nvarchar(80)                   null,
   外键店铺编号               nvarchar(80)                   null,
   消费金额                 nvarchar(80)                   null,
   消费时间                 nvarchar(80)                   null,
   享受折扣                 nvarchar(80)                   null,
   获得积分                 nvarchar(80)                   null,
   constraint PK_消费记录表 primary key clustered (序号)
);

insert into 消费记录表 values('B01','U01','K01','D01','500','3021-09-16','一折','8分')
insert into 消费记录表 values('B02','U01','K02','D02','800','1021-80-16','二折','10分')


/*==============================================================*/
/* Table: 积分交易表                                                 */
/*==============================================================*/
create table 积分交易表 
(
   序号                   nvarchar(80)                   not null,
   外键积分编号               nvarchar(80)                   null,
   消费积分                 nvarchar(80)                   null,
   消费时间                 nvarchar(80)                   null,
   兑换物品                 nvarchar(80)                   null,
   constraint PK_积分交易表 primary key clustered (序号)
);

insert into 积分交易表 values('JB01','J01','10分','2080-09-16','万达广场')
insert into 积分交易表 values('JB02','J02','5分','2090-09-16','人造月球')


alter table 会员卡信息表
   add constraint FK_会员卡信息表_REFERENCE_会员信息表 foreign key (会员编号)
      references 会员信息表 (会员编号)
      --on update restrict
      --on delete restrict;

alter table 会员卡信息表
   add constraint FK_会员卡信息表_REFERENCE_会员类型表 foreign key (外键类型编号)
      references 会员类型表 (类型编号)
      --on update restrict
      --on delete restrict;

alter table 会员卡充值记录表
   add constraint FK_会员卡充值记录表_REFERENCE_会员卡信息表 foreign key (外键卡编号)
      references 会员卡信息表 (卡编号)
      --on update restrict
      --on delete restrict;

alter table 会员积分表
   add constraint FK_会员积分表_REFERENCE_会员信息表 foreign key (外键会员编号)
      references 会员信息表 (会员编号)
      --on update restrict
      --on delete restrict;

alter table 店铺折扣力度表
   add constraint FK_店铺折扣力度表_REFERENCE_店铺信息表 foreign key (外键店铺编号)
      references 店铺信息表 (店铺编号)
      --on update restrict
      --on delete restrict;

alter table 挂失记录表
   add constraint FK_挂失记录表_REFERENCE_会员信息表 foreign key (外键会员编号)
      references 会员信息表 (会员编号)
      --on update restrict
      --on delete restrict;

alter table 消费记录表
   add constraint FK_消费记录表_REFERENCE_会员信息表 foreign key (外键会员编号)
      references 会员信息表 (会员编号)
      --on update restrict
      --on delete restrict;

alter table 消费记录表
   add constraint FK_消费记录表_REFERENCE_会员卡信息表 foreign key (外键卡编号)
      references 会员卡信息表 (卡编号)
      --on update restrict
      --on delete restrict;

alter table 消费记录表
   add constraint FK_消费记录表_REFERENCE_店铺信息表 foreign key (外键店铺编号)
      references 店铺信息表 (店铺编号)
      --on update restrict
      --on delete restrict;

alter table 积分交易表
   add constraint FK_积分交易表_REFERENCE_会员积分表 foreign key (外键积分编号)
      references 会员积分表 (积分编号)
      --on update restrict
      --on delete restrict;


select * from 商城信息表
select * from 店铺信息表
select * from 店铺折扣力度表
select * from 会员活动表
select * from 会员积分表
select * from 会员卡充值记录表
select * from 会员卡信息表
select * from 会员类型表
select * from 会员信息表
select * from 积分交易表
select * from 消费记录表
select * from 挂失记录表