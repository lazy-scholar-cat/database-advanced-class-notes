create database ShoppingVIP
on 
(
name='ShoppingVIP_data',
filename='C:\app\ShoppingVip_data.mdf',
size=10mb,
maxsize=100mb,
filegrowth=10%
)
log on
(
name='ShoppingVIP_log',
filename='C:\app\ShoppingVip_log.ldf',
size=10mb,
filegrowth=100mb
)
USE ShoppingVIP
go
create table Mallinformationtable
(
Mallnumber int primary key not null,
Storename nvarchar(20) not null,
Mallisreferredtoas nvarchar(20) not null,
IP nvarchar(20) not null,
LOHR nvarchar(20) not null ,
officephone nvarchar(20)not null
)
insert Mallinformationtable(Mallnumber,Storename,Mallisreferredtoas,IP,LOHR,officephone)
select 001,'新百佳','新百','新街2号','李佳','0628-85521238'
go
create table memberinformation
(
Vnumber int primary key not null,
mName nvarchar(20) not null,
gender nvarchar(20)not null,
Birthday nvarchar(20)not null,
Tel nvarchar(20)not null,
ID nvarchar(20)not null,
addres nvarchar(20)
)
insert memberinformation(Vnumber,mName,gender,Birthday,Tel,ID,addres)
select 001,'周慧','女','1995-02-02','17525459945','？？？？？？？？？','闽大'
go
create table MembershipcardInformation
(
Membership int primary key not null,
MembershipCardNo nvarchar(20) not null,
MembershipcardType nvarchar(20)not null,
IssuingInstitution nvarchar(20)not null,
PUID nvarchar(20)not null,
)
insert MembershipcardInformation(Membership,MembershipCardNo,MembershipcardType,IssuingInstitution,PUID)
select 001,'C001','VXb001','新百佳','VIP001'
go
create table MembershipcardType
(
Membership int primary key not null,
IssuingInstitution nvarchar(20) not null,
PUID nvarchar(20)not null,
Members nvarchar(20)not null
)
insert MembershipcardType(Membership,IssuingInstitution,PUID,Members)
select 001,'新百佳','VIP001','9折'
go
create table Storeinformation
(
SShopNo int primary key not null,
Name nvarchar(20) not null,
Belongstothemall nvarchar(20)not null,
Members nvarchar(20)not null
)
insert Storeinformation(SShopNo,Name,Belongstothemall,Members)
select 001,'仓汇衣品','新百佳','9折'
go
create table Memberpointsruleinformation
(
Mallnumber int primary key not null,
SShopNo nvarchar(20) not null,
Membership nvarchar(20)not null,
integrationrule nvarchar(20)not null
)
insert Memberpointsruleinformation(Mallnumber,SShopNo,Membership,integrationrule)
select 001,'001','VXb001','消费十¥兑一积分'
go
create table JifenAccount
(
ASD int primary key not null,
PUID nvarchar(20) not null,
name nvarchar(20)not null,
credits nvarchar(20)not null
)
insert JifenAccount(ASD,PUID,name,credits)
select 002,	'C002',	'韩琳','1231'
go
create table MembershiplevelInformation
(
Mallnumber int primary key not null,
Membership nvarchar(20) not null,
PUID nvarchar(20)not null,
grade nvarchar(20)not null
)
insert MembershiplevelInformation(Mallnumber,Membership,PUID,grade)
select 001,'VXb001','C001',	'VIP3'
go
