## CTE应用及触发器 
~~~spl
  触发器对表进行插入、更新、删除的时候会自动执行的特殊存储过程。触发器一般用在check约束更加复杂的约束上面。触发器和普通的存储过程的区别是：触发器是当对某一个表进行操作。诸如：update、insert、delete这些操作的时候，系统会自动调用执行该表上对应的触发器。SQL Server 2005中触发器可以分为两类：DML触发器和DDL触发器，其中DDL触发器它们会影响多种数据定义语言语句而激发，这些语句有create、alter、drop语句。
-- 触发器 是一种特殊存储过程 它不由用户直接手动执行，而是当一些事件发生后，自动被调用

-- 作用 1、维护数据的有效性和完整性 2、简化逻辑
/*
语法：

create trigger <触发器名称>
on <数据表>
<after | instead of> <delete | insert | update>
as
begin

end

*/
~~~
![图裂了！！！](./第一个笔记图片/20211011.png)
![图裂了！！！](./第一个笔记图片/202110111.png)