## 
```sql
create database Uresa
go 
USE Uresa
go
alter proc proc_naanw
@sria int=100 
as 
begin
 if OBJECT_ID('naanw','U') is not null
     begin 
	  drop table naanw
	  end

	create table naanw
	(
	Id int primary key identity,
	Name nvarchar(80),
	Birthday date not null default (getdate())
	)
	declare @i int =0,@name nvarchar(80),@lastName nvarchar(80),@firstName nvarchar(80) 
	while @i<@sria
	begin 
	exec proc_Uname @lastName output
	exec proc_Uselect @firstName output
	set @name=@lastName+@firstName
	insert into naanw(Name) Values (@name)
	set @i=@i+1
	end

end
go
exec proc_naanw 1000
select*from naanw


go 
alter proc proc_Uname
@lastname nvarchar(80) output
as
begin
	declare @str nvarchar(80)='贾路娄危江童颜郭梅盛林刁钟徐邱骆高夏蔡田樊胡凌霍虞万支柯昝管卢莫经房裘缪干解应宗丁宣贲邓郁单杭洪包诸左石崔吉钮龚程嵇邢滑裴陆荣翁荀羊於惠甄曲家封芮羿储靳汲邴糜松井段富巫乌焦巴弓牧隗山谷车侯宓蓬全郗班仰秋仲伊宫宁仇栾暴甘钭厉戎祖武符刘景詹束龙叶幸司韶郜黎蓟薄印宿白怀蒲邰从鄂索咸籍赖卓蔺屠蒙池乔阴鬱胥能苍双闻莘党翟谭贡劳逄姬申扶堵冉宰郦雍郤璩桑桂濮牛寿通边扈燕冀郏浦尚农温别庄晏柴瞿阎充慕连茹习宦艾鱼容'
	--select len(@str)
	declare @Index int=rand()*len(@str)
	set @lastname=SUBSTRING(@str,@Index,1)
end
go
declare @res nvarchar(80) 
exec proc_Uname @res output
select @res



go
alter proc proc_Uselect
@firstName nvarchar(80) output
as
begin
declare @str nvarchar(80)='澄邈德泽海超海阳海荣海逸海昌瀚钰瀚文涵亮涵煦明宇涵衍浩皛浩波浩博浩初浩宕浩歌浩广浩邈浩气浩思浩言鸿宝鸿波鸿博鸿才鸿畅鸿畴鸿达鸿德鸿飞鸿风鸿福鸿光鸿晖鸿朗鸿文鸿轩鸿煊鸿骞鸿远鸿云鸿哲鸿祯鸿志鸿卓嘉澍光济澎湃彭泽鹏池鹏海浦和浦泽瑞渊越泽博耘德运辰宇辰皓辰钊辰铭辰锟辰阳辰韦辰良辰沛晨轩晨涛晨濡晨潍鸿振吉星铭晨起运运凡运凯运鹏运浩运诚运良运鸿运锋运盛运升运杰运珧运骏运凯运乾维运运晟运莱运华耘豪星爵星腾星睿星泽星鹏星然震轩震博康震震博振强振博振华振锐振凯振海振国振平昂然昂雄昂杰昂熙昌勋昌盛昌淼昌茂昌黎昌燎昌翰晨朗德明德昌德曜范明飞昂高旻晗日昊然昊天昊苍昊英昊宇昊嘉昊明昊伟昊硕昊磊昊东鸿晖鸿朗华晖金鹏晋鹏敬曦景明景天景浩俊晖君昊昆琦昆鹏昆纬昆宇昆锐昆卉昆峰昆颉昆谊昆皓昆鹏昆明昆杰昆雄昆纶'
--select len(@str)
declare @Index int=rand()*len(@str)+1
set @firstName=SUBSTRING(@str,@Index,2)
end
go
declare @res nvarchar(80)
exec proc_Uselect @res output
select @res
go
```