use master
go 

create database MKarketVIP_System
go

use MKarketVIP_System
go

create table Market --商场信息表
(
	ScId int primary key identity(1,1) not null,
	ScName nvarchar(50) not null,
	ScFrName nvarchar(20) not null,
	ScAdress nvarchar(100) not null,
	ScNumber nvarchar(50) not null
)
go

create table Memberinformation --会员信息表
(
	HyId int primary key identity(1,1) not null,
	HyNmae nvarchar(20) not null,
	HyNumber nvarchar(50) not null,
	HySex int not null,
	HyBirthday datetime,
)
go

create table CardInformation --会员卡信息表
(
	HyId int primary key identity(1,1) not null,
	HyNmae nvarchar(20) not null,
	HyNumber nvarchar(50) not null,
	HyAdrees nvarchar(50) not null
)
go

create table VehicleInformation --会员车辆信息表
(
	HyId int primary key identity(1,1) not null,
	HyNumber nvarchar(50) not null,
	HyCarNumber nvarchar(20) not null
)
go

 create table LossInformation --会员卡挂失记录信息
(
	HyId int primary key identity(1,1) not null,
	HyNmae nvarchar(20) not null,
	HyNumber nvarchar(50) not null,
	LossTime datetime,
)
go

create table CardLevelInformation  --会员卡等级信息
(
	HyId int primary key identity(1,1) not null,
	HyNmae nvarchar(20) not null,
	HyNumber nvarchar(50) not null,
	Leve nvarchar(10) not null,
)
go

create table ActivityInformation  --会员活动信息
(
	HdId int primary key not null,
	HdTime datetime,
	HdForPeople nvarchar(50) not null,
	HdContent nvarchar(50) not null,
	HdPerson nvarchar(20) not null,
	HdNumber nvarchar(50) not null,
)
go

create table ShopInformation --商铺信息表
(
	SpId int primary key not null,
	SpName nvarchar(50) not null,
	SpPerson nvarchar(20) not null,
	SpNumber nvarchar(50) not null,
	SpQualifications nvarchar(20) not null,
)
go

select * from Market
insert into Market values('LYWD01','龙岩万达广场','牛文强','龙岩市新罗区双龙路1号','123456')

select * from Memberinformation
insert into Memberinformation values
('TE001','小狗','13654896457','男','2019-09-10'),
('TE002','小猫','18131056645','女','2020-08-04'),
('TE003','小猪','13648512056','男','2021-07-10')

select * from ShopInformation

insert into ShopInformation values
('A01','胖哥俩','大胖','13789078976','餐饮')

--用户登陆，根据数据库里面已有的信息来查询用户是否存在，是，则登陆；否，则需要注册用户

	select * from Memberinformation
    where Username='admin' and Password='123456'

-- 更进一步，就是当前登录的用户可能已经被注销、禁用等情况，如何应对

-- 注册

-- 用户层面 ，输入用户名，密码，重复密码

-- 注册单个用户
insert into Memberinformation (userid,password) values ('admin','123456')


-- 批量注册

insert into Memberinformation (userid,password) values ('userid01','123456'),('userid02','123456'),('userid03','123456'),('userid04','123456')

-- 注册通用逻辑（无关数据库）

-- 1. 判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走
--唯一约束
alter table Userinformationtable add constraint unique1 unique(userid) 
-- 2. 判断密码和重复密码是否一致，是则可以注册，并在相应数据表中插入一条记录，否则返回失败信息

-- 注册通用逻辑（无关数据库）

-- 1. 判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走
alter table userInfo add constraint UQ_userInfo_userId unique(userId)

--declare @username nvarchar(80)
--declare @tmpTable table (id int,Username nvarchar(80),Password nvarchar(80))
--declare @count int

--insert into @tmpTable
--select @count=count(*) from userInfo where Username=@username

--if(@count>0)
--	begin
--	-- 做点什么 如提示消息或者设置一些数值
		
--	end
--else
--	begin


--	end

-- 2. 判断密码和重复密码是否一致，是则可以注册，并在相应数据表中插入一条记录，否则返回失败信息
