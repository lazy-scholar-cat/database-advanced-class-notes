create database VIP
go
use VIP
GO

create table StoreInformation  
(
	marketId int identity primary key ,
	marketName nvarchar(40),
	marketAddress nvarchar(80),
	legal nvarchar(20),
	phone bigint check(len(phone)=11)
)
create table memberInformation  
(
	memberId INT IDENTITY PRIMARY KEY,
	membername nvarchar(40),
	sex char(2) default('��') check(sex='��'or sex='Ů') ,
	Birth time ,
	phone bigint check(len(phone)=11)
)

create table MemberVehicleInformation
(
	carId nchar(30) primary key,
	license nvarchar(40) ,
	memberId int 
)

create table MembershipCardInformation
(
	cardNumber char(30) primary key,
	cardType NVARCHAR(40),
	memberId int 
 )
create table ReportTheLossOfTheTable
(
	ID INT IDENTITY,
	phone bigint check(len(phone)=11),
	loss nvarchar(1) check(loss='��'or loss='��')
)
create table tableOfIntegrals 
(
	Id int identity,
	memberId int,
	integrals int 
)

create table MembershipCardType 
(
	MemberTypeId char(70),
	MemberType char(100)
)