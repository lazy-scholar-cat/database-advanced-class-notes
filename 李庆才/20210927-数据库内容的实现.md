```
create database VIP
go
use VIP
GO

create table StoreInformation  
(
	marketId int identity primary key ,
	marketName nvarchar(40),
	marketAddress nvarchar(80),
	legal nvarchar(20),
	phone bigint check(len(phone)=11)
)
create table memberInformation  
(
	memberId INT IDENTITY PRIMARY KEY,
	membername nvarchar(40),
	sex char(2) default('男') check(sex='男'or sex='女') ,
	Birth time ,
	phone bigint check(len(phone)=11)
)

create table MemberVehicleInformation
(
	carId nchar(30) primary key,
	license nvarchar(40) ,
	memberId int 
)

create table MembershipCardInformation
(
	cardNumber char(30) primary key,
	cardType NVARCHAR(40),
	memberId int 
 )
create table ReportTheLossOfTheTable
(
	ID INT IDENTITY,
	phone bigint check(len(phone)=11),
	loss nvarchar(1) check(loss='是'or loss='否')
)
create table tableOfIntegrals 
(
	Id int identity,
	memberId int,
	integrals int 
)

create table MembershipCardType 
(
	MemberTypeId char(70),
	MemberType char(100)
)
```


-- 登录

-- 根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败
```
select * from Users
where Username='admin' and Password='113'
```
-- 更进一步，就是当前登录的用户可能已经被注销、禁用等情况，如何应对





-- 注册

-- 用户层面 ，输入用户名，密码，重复密码

-- 注册单个用户
```
insert into Users (Username,Password) values ('admin','113')
```

-- 批量注册
```
insert into Users (Username,Password) values ('user01','113'),('user02','113'),('user03','113'),('user04','113')
```
-- 注册通用逻辑（无关数据库）

-- 1. 判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走
```
alter table UserTable add  constraint UQ_UserID unique(UserID)
```

-- 2. 判断密码和重复密码是否一致，是则可以注册，并在相应数据表中插入一条记录，否则返回失败信息


```
if exists(select *from Password = Password1)
insert into Users (Username,Password) values ('admin','113')
```
