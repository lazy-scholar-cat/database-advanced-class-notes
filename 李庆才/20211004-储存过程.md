<h3>储存过程</h3>


![裂开了！](./img/a4.png)


```
最多可以有2100个参数
三种参数类型（in，out ，inout）
```
```
@i int,--输入型参数
@o int output--输出型参数
```



```
-- 存储过程语法

/*
create proc 存储过程名称
as
begin


end
*/

select * from StudentInfo
go


create proc proc_SelectStudentInfo
as
begin
	select * from StudentInfo
end
go

exec proc_SelectStudentInfo

go

-- 有参数的存储过程

create proc proc_SelectStudentInfoWithPrameter
@name nvarchar(80)
as
begin
	select * from StudentInfo
	where StudentName like @name
end
go

exec proc_SelectStudentInfoWithPrameter '%李%'


go

-- 有多个参数的存储过程
create proc proc_SelectStudentInfoWithSomeParameters
@name nvarchar(80),
@code nvarchar(80)
as
begin
	select * from StudentInfo
	where StudentName like @name or StudentCode like @code

end
go

exec proc_SelectStudentInfoWithSomeParameters '88',''

go

-- 有默认值的存储过程

create proc proc_SelectStudentInfoWithAnyParameters
@code nvarchar(80)='01',
@name nvarchar(80)='%李%',
@birthday date='2021-10-03',
@sex char='m',
@classId int='1'
as
begin
	select * from StudentInfo
	where StudentCode like @code 
	or StudentName like @name
	or Birthday<@birthday
	or sex =@sex
	or ClassId= @classId
end

exec proc_SelectStudentInfoWithAnyParameters
```