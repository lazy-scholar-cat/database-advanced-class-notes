# SQL 

##  数据定义
1.数据库的建立与删除
(1)建立数据库：数据库是一个包括了多个基本表的数据集，其语句格式为：
CREATE DATABASE <数据库名>，<数据库名>在系统中必须是唯一的，不能重复，不然将导致数据存取失误。


例：要建立项目管理数据库(xmmanage)，其语句应为：
CREATE DATABASE xmmanage

(2)数据库的删除：将数据库及其全部内容从系统中删除。
其语句格式为：DROP DATABASE <数据库名>


例：删除项目管理数据库(xmmanage)，其语句应为： DROP DATABASE xmmanage

2.基本表的定义及变更
本身独立存在的表称为基本表，在SQL语言中一个关系唯一对应一个基本表。基本表的定义指建立基本关系模式，而变更则是指对数据库中已存在的基本表进行删除与修改。

## 数据查询
SQL是一种查询功能很强的语言，只要是数据库存在的数据，总能通过适当的方法将它从数据库中查找出来。SQL中的查询语句只有一个：SELECT，它可与其它语句配合完成所有的查询功能。SELECT语句的完整语法，可以有6个子句。完整的语法如下：

　　SELECT 目标表的列名或列表达式集合

　　FROM 基本表或(和)视图集合

　　〔WHERE条件表达式〕

　　〔GROUP BY列名集合〕

　　〔HAVING组条件表达式〕〕

　　〔ORDER BY列名〔集合〕…〕

简单查询,使用TOP子句
查询结果排序order by 默认ASC升序，使用关键词DESC降序。
带条件的查询where,使用算术表达式，使用逻辑表达式，使用between和in关键字。
模糊查询like

在WHERE子句中的条件表达式F中可出现下列操作符和运算函数：

　　算术比较运算符：<，<=，>，>=，=，<>，!=。
　　逻辑运算符：AND，OR，NOT。

　　集合运算符：UNION(去重合并)，UNION ALL(不去重合并)，INTERSECT(交)，MINUS(差)。

　　集合成员资格运算符：IN，NOT IN

　　谓词：EXISTS(存在量词)，ALL，ANY，SOME，UNIQUE。

　　聚合函数：AVG(平均值)，MIN(最小值)，MAX(最大值)，SUM(和)，COUNT(计数)。