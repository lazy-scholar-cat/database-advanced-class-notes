 # 祝你今天开心
```
 use master
go

create database Shopvip
go
use Shopvip
go

create table UserTable --用户表
(
	UserID nvarchar(80) not null,
	Password nvarchar(80) not null,
)
go
create table ShopMessage  --商场信息表
(
 SID int primary key identity(1,1) not null,
 SName nvarchar(80) not null,
 SAdress nvarchar(80) not null,
 SDate datetime not null,
 SPerson nvarchar(20) not null,
 SPhone nvarchar(80) not null
)
go
create table VipMessage  --会员信息表
(
 VID int primary key identity(1,1) not null,
 VName nvarchar(80) not null,
 VSex char(4) not null,
 VDate datetime not null,
 VPhone nvarchar(80) not null,
 VIdentity nvarchar(80) not null,
 VAdress nvarchar(80) not null,
 VRegister datetime not null
)
go
create table VipCard --会员卡信息表
(
 VIID int primary key identity(1,1) not null,
 VIName nvarchar(80) not null,
 VIShop nvarchar(80) not null
)
go
create table ShopMessage1 --商铺信息表
(
 S1ID int primary key identity(1,1) not null,
 S1Name nvarchar(80) not null,
 S1Adress nvarchar(80) not null,
 S1AdressID int not null,
 S1Man nvarchar(80) not null,
 S1Business nvarchar(80) not null
)
go
create table VipLose --会员挂失表
(
 VID int primary key identity(1,1) not null,
 VIdentity nvarchar(80) not null,
 VName nvarchar(80) not null
)
go
create table JiFen --积分表
(
 JID int primary key identity(1,1) not null,
 JShop nvarchar(80) not null,
 JMoney nvarchar(80) not null,
 JJiFen nvarchar(80) not null
)
go
create table Pound --兑换表
(
 PID int primary key identity(1,1) not null,
 PName nvarchar(80) not null,
 PJiFen nvarchar(80) not null,
 PVipID int not null
)

select * from ShopMessage
insert into ShopMessage values ('001','银泰中心','成都','2020-8-10','徐江滨','0869-8566566')

select * from VipCard
insert into VipCard(VIName) values('吴煌','张飞','郑邵')

select * from ShopMessage1
insert into ShopMessage1(S1Name,S1AdressID) values('海澜之家','911','鸿星尔克','857','贵人鸟','246')

--登陆

--用户登陆，根据数据库里面已有的信息来查询用户是否存在，是，则登陆；否，则需要注册用户

	select * from UserTable where UserID='admin' and Password='666'

--更进一步，就是当登陆的用户可能已被注销、禁用等情况，如何应对
	--1.先在数据表中删除被禁用、注销的用户名
	delete from UserTable where UserID='admin'
	--2.然后重新注册用户

--用户登录时如果忘记密码，实现找回密码
	--1.可以在数据表中，查找用户名，即可找到对应的密码
	select * from UserTable where UserID='admin'

--注册

--单个注册
	
	insert into UserTable(UserID,Password) values('admin','666')

--批量注册
	
	insert into UserTable(UserID,Password) values('admin1','666'),('admin2','666'),('admin3','666'),('admin4','666')

--用户注册逻辑

--1.判断当前注册的用户名是否存在，如果存在则返回注册失败；反之，则注册成功
--可以使用unique语句来判断

	alter table UserTable add  constraint UQ_UserID unique(UserID)

--2.判断重复输入的密码是否一样，如果不一样，则重新输入；反之，注册成功，在数据表中添加一条数据
```