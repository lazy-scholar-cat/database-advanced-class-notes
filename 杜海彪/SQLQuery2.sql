use master
go

create database Shopvip1
go
use Shopvip1
go

create table ShopMessage  --商场信息表
(
 SID int primary key identity(1,1) not null,
 SName nvarchar(80) not null,
 SAdress nvarchar(80) not null,
 SDate datetime not null,
 SPerson nvarchar(20) not null,
 SPhone nvarchar(80) not null
)
go
create table VipMessage  --会员信息表
(
 VID int primary key identity(1,1) not null,
 VName nvarchar(80) not null,
 VSex char(4) not null,
 VDate datetime not null,
 VPhone nvarchar(80) not null,
 VIdentity nvarchar(80) not null,
 VAdress nvarchar(80) not null,
 VRegister datetime not null
)
go
create table VipCard --会员卡信息表
(
 VIID int primary key identity(1,1) not null,
 VIName nvarchar(80) not null,
 VIShop nvarchar(80) not null
)
go
create table ShopMessage1 --商铺信息表
(
 S1ID int primary key identity(1,1) not null,
 S1Name nvarchar(80) not null,
 S1Adress nvarchar(80) not null,
 S1AdressID int not null,
 S1Man nvarchar(80) not null,
 S1Business nvarchar(80) not null
)
go
create table VipLose --会员挂失表
(
 VID int primary key identity(1,1) not null,
 VIdentity nvarchar(80) not null,
 VName nvarchar(80) not null
)
go
create table JiFen --积分表
(
 JID int primary key identity(1,1) not null,
 JShop nvarchar(80) not null,
 JMoney nvarchar(80) not null,
 JJiFen nvarchar(80) not null
)
go
create table Pound --兑换表
(
 PID int primary key identity(1,1) not null,
 PName nvarchar(80) not null,
 PJiFen nvarchar(80) not null,
 PVipID int not null
)
