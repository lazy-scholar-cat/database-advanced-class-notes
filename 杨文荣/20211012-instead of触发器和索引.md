## inserted 和 deleted

| | insert | update | delete |
|-|-|-|-|
| inserted | 新插入的记录情况 | 更新后的记录情况 | 无 |
| deleted | 无 | 更新前的记录情况 | 删除前的记录情况 |


## instead of 触发器
~~~
instead of 是替换执行 insert update delete中的语句。换句话说，增删改在instead of存在的时候，并不会真正被执行，真正执行的其实是被触发的那个触发器
~~~

测试
~~~
alter trigger tr_AreaForInsert
on Area
instead of insert
as
	begin
	declare @Code nvarchar(80),@AreaName nvarchar(80),@AreaType int ,@ParentCode nvarchar(80)
	select @Code=Code,@AreaName=AreaName,@AreaType=AreaType,@ParentCode=ParentCode from inserted
	if(@AreaType>=6)
	begin
	print '插入数据有误'
	end
else 
	begin
	print '插入成功'
	end
	end
go

insert into Area(code,AreaName,AreaType,ParentCode)values('1','？？？',2,'0')
select * from Area

插入数据失败[触发了触发器只打印了插入成功]
~~~


## SET NOCOUNT 的用法

~~~
当 SET NOCOUNT 为 ON 时，不返回计数（表示受 Transact-SQL 语句影响的行数）。当 SET NOCOUNT 为 OFF 时，返回计数。
即使当 SET NOCOUNT 为 ON 时，也更新 @@ROWCOUNT 函数。
当 SET NOCOUNT 为 ON 时，将不给客户端发送存储过程中的每个语句的 DONE_IN_PROC 信息。当使用 Microsoft? SQL Server? 提供的实用工具（QA）执行查询时，在 Transact-SQL 语句（如 SELECT、INSERT、UPDATE 和 DELETE）结束时将不会在查询结果中显示”nn rows affected”。
如果存储过程中包含的一些语句并不返回许多实际的数据，则该设置由于大量减少了网络流量，因此可显著提高性能。
SET NOCOUNT 设置是在执行或运行时设置，而不是在分析时设置。
如果存储过程中有多个语句，则默认情况下，SQL Server在每个语句完成时给客户端应用程序发送一条消息，详细说明每个语句所影响的行数。大多数应用程序不需要这些消息。如果确信应用程序不需要他们，可以禁用这些消息，以提高慢速网络的性能。我们就可以通过SET NOCOUNT会话设置为应用程序禁用这些消息。（其实大部分应用程序都不需要这个值）


  建议不要让触发器返回任何结果。这是因为对这些返回结果的特殊处理必须写入每个允许对触发器表进行修改的应用程序中。为了阻止从触发器返回任何结果，不要在触发器内定义包含SELECT语句或变量赋值。如果必须在触发器中进行变量赋值，则应该在触发器的开头使用SET NOCOUNT ON语句以避免返回任何结果集。
~~~


  ## 索引

转到>>>[索引](./索引.sql) <<<

