
-- 登录

-- 根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败

--select * from UserTable
--where Username='admin' and Password='113'

-- 更进一步，就是当前登录的用户可能已经被注销、禁用等情况，如何应对
	
-- 1.根据提供的用户名（这里假定用户名不能重复），查询出满足条件的记录
	--declare @usernmae nvarchar(10)
	--select*from UserTable where Username=@usernmae
-- 2.判断查询到的记录的状态（如之前提过的IsActived、IsDeleted等等，也可以给用户增加额外的状态字段），给出已经被注销、已经被禁用、已经被删除等提示
	
-- 3.如果这条记录没有被注销、禁用、删除的情况，最后判断密码是不是和查询到的这条记录匹配，是则登录成功，否则提示“用户或者密码错误”

-- 一种攻击手段 SQL注入


/*
declare @password nvarchar(80) = \'1' or 1=1\`


select * from Users
where (Username='admin' and Password='1') or 1=1


*/



-- 注册

-- 用户层面 ，输入用户名，密码，重复密码

-- 注册单个用户
--insert into UserTable(username,Password) values ('admin','113')


-- 批量注册

--insert into UserTable (username,Password) values ('user01','113'),('user02','113'),('user03','113'),('user04','113')

-- 注册通用逻辑（无关数据库）

-- 1. 判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走

/*
declare @username nvarchar(80)
declare @tmpTable table (id int,Username nvarchar(80),Password nvarchar(80))
declare @count int

insert into @tmpTable
select @count=count(*) from Users where Username=@username

if(@count>0)
	begin
	-- 做点什么 如提示消息或者设置一些数值
		
	end
else
	begin


	end
*/

/*
定义用户名变量=传输过来的用户名
根据用户名变量查询用户表中有没有同名的记录

如果有，则提示不能注册，返回消息
如果没有，则可以继续往下

*/




-- 2. 判断密码和重复密码是否一致，是则可以注册，并在相应数据表中插入一条记录，否则返回失败信息
--declare @password nvarchar(80)
--declare @password2 nvarchar(80)


--if(@password=@password2)
--	begin
--	print('通过注册') 
--	end
--else
--	begin
--	print('密码不一致')
--	end
---- 关于商场信息 的使用场景

---- 1.在会员信息管理的时候，需要选择商场

--select M_ID,M_Name from Maket_Info
---- 2.在会员卡类型管理的时候，需要选择商场

--select M_ID,M_Name from Maket_Info

---- 3. 商场信息本身的CRUD （create read update delete）

---- 商场
--insert into Maket_Info(M_ID,M_Address,M_Leader,M_Name,M_Phone) values ( )
--delete from Maket_Info where 
--update  Maket_Info  set   where 
--select * from Maket_Info



----店家
--insert into Shop_Info(S_ID,S_Address,S_Address_ID,S_Manager,S_Manager_ID,S_Name) values ( )
--delete from Shop_Info where 
--update  Shop_Info  set   where 
--select * from Shop_Info

----商品

--insert into Shop_goods(SG_ID,S_ID,S_Money,S_Name,S_num,S_Manager_ID,S_Name) values ( )
--delete from Shop_goods where 
--update  Shop_goods  set   where 
--select * from Shop_goods


--注册用户名空格
declare @username nvarchar(20)
declare @password nvarchar(80),@cofirmPassword nvarchar(80)
declare @rowPassword nvarchar(80)
declare @tmpTable table (id int,Username nvarchar(80),Password nvarchar(80))
declare @res nvarchar(50)
declare @count int

insert into @tmpTable
select @count=count(*),@rowPassword=Password from Users where Username=@username
if(charindex(' ',@username)>0 or charindex(' ',@password)>0)
	begin
		set @res='用户名或密码包含非法字符，请重新命名'
	end
else
	begin
		if(@count>0)
		begin
		set @res='当前用户名已经被注册，请确认后重试。。。。'
	end
else
	begin
	if(@password=@cofirmPassword)
	begin
	insert into UserTable(Username,Password)values(@username,@password)
	set @res='注册成功'
	end
else
	begin
	set @res='两次密码不一致'
	end

	end

	end

declare @username nvarchar(80),@password nvarchar(80),@rowPassword nvarchar(80)
declare @isDeleted bit ,@isActived bit
declare @count int
declare @res nvarchar(80)

select @count=COUNT(*),@isDeleted=IsDeleted,@isActived=IsActived,@rowPassword=Password from Users 
where Username=@username

-- 如果根据提供的用户名，找到的记录数不为零，则表明用户名是对的，可以继续往下判断；否则直接提示用户名或密码错误
if(@count>0)
	begin
		if(@isDeleted=1) --如果删除标记为真，则表明当前用户已经被删除，作出提示；否则用户未被删除
			begin
				set @res='当前用户已经被删除，有任何问题请联系管理员@_@'
			end
		else
			begin
				if(@isActived=0) --如果当前用户未启用/激活，则提示信息用户未启用，登录失败；否则用户已经禁用
					begin
						set @res='用户已禁用，有任何问题请联系管理员'
					end
				else
					begin
						if(@password=@rowPassword) -- 如果传进来的密码和在记录中获取的密码相等，则认为登录成功，作出提示；否则登录失败，提示相应信息
							begin
								set @res='登录成功'
							end
						else
							begin
								set @res='用户名或者密码错误，请确认后重试。。。'
							end
					end

			end

	end
else
	begin
		set @res='用户名或者密码错误，请确认后重试。。。'
	end