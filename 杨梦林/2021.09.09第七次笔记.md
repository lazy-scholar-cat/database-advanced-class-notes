# 2021.09.09 天气：闷热闷热的

## 关于码云


## 关于笔记

## 关于数据库的设计

##  数据库设计的范式


## 第一范式
确保每一列都保持原子性，没有办法继续细化，所以符合第一范式。
第一范式是最基本的范式。如果数据库表中的所有字段值都是不可分解的原子值，就说明该数据库表满足了第一范式。

## 第二范式
在第一范式的基础上更进一层,目标是确保表中的每列都和主键相关. 而不能只与主键的某一部分相关（主要针对联合主键而言）。也就是说在一个数据库表中，一个表中只能保存一种数据，不可以把多种数据保存在同一张数据库表中。

## 第三范式
在第二范式的基础上更进一层,目标是确保每列都和主键列直接相关,而不是间接相关. 