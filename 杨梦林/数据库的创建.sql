create database SC

on

(

    name='SC',

	filename='D:\SC.mdf',

	size=6MB,

	maxsize=100MB,

	filegrowth=15%

)


log on 

(

    name='SC_log',

	filename='D:\SC_log.ldf',

	size=6MB,

	maxsize=100MB,

	filegrowth=15%

)

go

go

--商场信息表
create table shangc
(
shangcID nvarchar(20) not null,
shangName nvarchar(20) not null,
shangDZ nvarchar(20) not null,
shangZC nvarchar(20) not null,
shangDH nvarchar(20) not null 
)

--drop table shangc

insert into shangc
select 'Sm01','足浴商场','龙岩火车站','杜海彪','13708541156' union 
select 'Sm02','按摩商场','闽西职业技术学院南门旁','吴煌','15565422247'

select * from shangc

--会员信息表
create table VIP
(
vipID  nvarchar(10) not null,
vipName nvarchar(10) not null,
sex varchar(20) not null,
here nvarchar(25) not null,
dianhua nvarchar(22) not null,
sheng nvarchar(22) not null,
dizhi nvarchar(22) not null
)
--drop table VIP
select * from VIP
select * from shangc

insert into VIP
select 'v01','吴亦凡','男','昨天','0011','551456451424','北京监狱'union 
select 'v02','蔡徐坤','非男非女','他不配','0022','241545244','曹溪街道怡红院'union
select 'v03','郑少映 ','男','2000-01-02 ','110','123456789','十四栋422五号床'union
select 'v04','张益飞','女 ','2000-002-03','119','789456123','十四栋422四号床'

--会员卡信息

create table KA
(
kaID nvarchar(10) not null ,
Id nvarchar(10) not null,
kalei nvarchar(10) not null,
faka nvarchar(10) not null,
vipID  nvarchar(10) not null
)
insert into KA
select 'H01','1122','L01','足浴商场','V03'union 
select 'Y02','1133','X02','按摩商场','V04'

select * from VIP
select * from shangc
select * from KA

--会员卡类型信息表
create table kalei
( Id nvarchar(10) not null,
kaName nvarchar(10) not null,
zuzhi nvarchar(10) not null,
vipID  nvarchar(10) not null
)
select * from VIP
select * from shangc
select * from KA
select * from kalei

insert into kalei 
select 'L01','足浴会员卡','足浴商场','V03'union
select 'X02 ','按摩会员卡','按摩商场','V04'

--商铺信息表 
create table Spu
(
spID nvarchar(10) not null,
spName nvarchar(10) not null,
zuzhi nvarchar(10) not null,
Id nvarchar(10) not null,
vipID  nvarchar(10) not null
)
select * from Spu
insert into Spu
select 'SP001','脚臭也能洗','足浴商场','H01','V03'union
select 'SP002','正规按摩','按摩商场','Y02','V04'


--会员积分信息表
create table JI
(
jID nvarchar(10) not null,
jshu nvarchar(10) not null,
jtime nvarchar(10) not null,
jzhong nvarchar(10) not null,
vipID  nvarchar(10) not null
)
insert into JI
select 'J001','1000','2020.1.23','2022.1.23','V03'union
select 'J002','2000','2020.1.23','2022.1.23',' V04 '

select * from VIP
select * from shangc
select * from KA
select * from kalei
select * from Ji

--会员等级信息表
create table deng
(
Did nvarchar(10) not null,
Dlei nvarchar(10) not null,
vipID nvarchar(10) not null,
Dxiang nvarchar(10) not null,
Dtime nvarchar(10) not null
)
insert into deng
select 'D001','贵宾会员卡','V03','可在店过夜','半年'union
select 'D002','会员金卡','V04 ','赠AD钙一瓶','半月'

select * from deng

--会员活动信息表
create table Huo
(
vipID nvarchar(10) not null,
Htime nvarchar(10) not null,
Hxiang nvarchar(10) not null,
Hdizhi nvarchar(20) not null,
Hfu nvarchar(10) not null,
)
--drop table Huo
insert into Huo 
select 'V03','2021.09.16 ','泰式足浴法','龙岩火车站','老胡'union
select 'V04','2021.09.16','泰式按摩法','闽西职业技术学院南门旁','软工院长'

select * from Huo

--会员车辆信息表
create table che 
(
vipID nvarchar(10) not null,
Cid nvarchar(10) not null,
clei nvarchar(10) not null,
cName nvarchar(10) not null
)
insert into che
select 'V03','C001','奥迪R7','郑少映' union
select 'V02','C002','轮椅Q3','蔡徐坤'

select * from che


--会员卡挂失记录信息表
create table Gua
(
vipID nvarchar(10) not null,
Gname nvarchar(10) not null,
Gtime nvarchar(10) not null,
Gid nvarchar(10) not null
)
insert into Gua
select 'V03','郑少映','2021.02.02','1122'union 
select 'V02','蔡徐坤 ','2021.03.03','1133'

select * from Gua

-- 会员积分兑换信息表
create table dui
(
vipID nvarchar(10) not null,
duitime nvarchar(10) not null,
dwu nvarchar(10) not null,
jID nvarchar(20) not null,

)

insert into dui
select 'V03','2021.01.01','搓澡巾','J001'union
select 'V04','2021.03.03','洗脚盆','J002'
select * from dui 


--商品信息表
create table ping
(
Pid nvarchar(10) not null,
pname nvarchar(10) not null,
shangName nvarchar(20) not null,
vipID nvarchar(20) not null,

)
insert into ping 
select 'sp01 ','搓澡巾','足浴商场','V03'union
select 'sp02','洗脚盆','按摩商场','V04'

select * from ping 

create table UserTable
(
Uname nvarchar(10) not null ,
Upassword nvarchar(10) not null 

)
--登陆

--用户登陆，根据数据库里面已有的信息来查询用户是否存在，是，则登陆；否，则需要注册用户

	select * from UserTable where Uname='menglin' and Upassword='666'

--更进一步，就是当登陆的用户可能已被注销、禁用等情况，如何应对
	--1.先在数据表中删除被禁用、注销的用户名
	delete from UserTable where Uname='menglin'
	--2.然后重新注册用户

--用户登录时如果忘记密码，实现找回密码
	--1.可以在数据表中，查找用户名，即可找到对应的密码
	select * from UserTable where  Uname='admin'


--注册

--单个注册
	
	insert into UserTable values('menglin','666')

--批量注册
	
	insert into UserTable values('menglin','666'),('ml','45'),('sb','12'),('md','13'),('cao','23')


