# 时间：2021年9月28号  天气 晴  心情： 已经体验到被代码麻痹了的感觉

### 对上节课堂的内容的回顾修改，更进一步，就是当登陆的用户可能已被注销、禁用等情况，如何应对

-- 1.根据提供的用户名（这里假定用户名不能重复），查询出满足条件的记录

-- 2.判断查询到的记录的状态（如之前提过的IsActived、IsDeleted等等，也可以给用户增加额外的状态字段），给出已经被注销、已经被禁用、已经被删除等提示

-- 3.如果这条记录没有被注销、禁用、删除的情况，最后判断密码是不是和查询到的这条记录匹配，是则登录成功，否则提示“用户或者密码错误”#

### SQL注入攻击:1.通过构建特殊的输入作为参数传入Web应用程序，而这些输入大都是SQL语法里的一些组合，通过执行SQL语句进而执行攻击者所要的操作，它目前是黑客对数据库进行攻击的最常用手段之一。2.是利用是指利用设计上的漏洞，在目标服务器上运行Sql语句以及进行其他方式的攻击，动态生成Sql语句时没有对用户输入的数据进行验证是Sql注入攻击得逞的主要原因。

它不是利用操作系统的BUG来实现攻击，而是针对程序员编写时的疏忽，通过SQL语句，实现无账号登录，甚至篡改数据库。

## Sql 注入带来的威胁主要有如下几点

1.猜解后台数据库，这是利用最多的方式，盗取网站的敏感信息。
2.绕过认证，列如绕过验证登录网站后台。
3.注入可以借助数据库的存储过程进行提权等操作

比如："select id from users where username = '"+username +"' and password = '"  + password +"'" 
这里的username和password都是我们存取从web表单获得的数据。
下面我们来看一下一种简单的注入，如果我们在表单中username的输入框中输入' or 1=1-- ，password的表单中随便输入一些东西，
假如这里输入123.此时我们所要执行的sql语句就变成了
select id from users where username = '' or 1=1--  and password = '123'，
我们来看一下这个sql，因为1=1是true，后面 and password = '123'被注释掉了。

## 本节课的业务实现

```
--关于商场信息的使用场景

--1.选择会员信息管理的场景

select SID,ShopMallName,ShopMallAlsoName form ShopMall
-- 2.在会员卡类型管理的时候，需要选择商场

select SID,ShopMallName,ShopMallAlsoName form ShopMall

--3.商场本身的create、read、update、delete

insert into ShopMall values('万达购物广场','万达','张三','龙岩大道','0792-87451287')

update ShopMall set ShopMallAddress='龙岩'

delete from ShopMall where SID=2

```