
use master

go

create database Vip_System

go

use Vip_System
go  
create table Market
(
	MID int primary key identity(1,1) not null,
	MarkatName nvarchar(50) not null,
	MarketAddress nvarchar(200) not null,
	MarketManager nvarchar(10) not null,
	MarketPhone  nvarchar(50) not null,
)
go
create table VipCustomer
(
	VID int  primary key identity(1,1) not null,
	VName nvarchar(50) not null,
	VBirthday datetime not null,
	VPhone  nvarchar(50) not null,
	VAddress nvarchar(200) not null,
	VAge int not null,
	VCtime nvarchar(50) not null,
	Vsex nvarchar(10) not null ,
	
)
go
create table CardType
(
	TID int primary key identity(1,1) not null,
	TName nvarchar(10) not null,
	PS nvarchar(50) ,
)
go
create table Card
(
	CID int  identity(1,1) not null,
	CNum nvarchar(10) primary key not null,
	VID int references VipCustomer(VID) not null,
	CDate datetime not null,
	CAddress nvarchar(200) not null,
	CTypeNum int references CardType(TID) not null,
)
go
create table Integral
(
	IID int primary key identity(1,1) not null,
	CNum nvarchar(10) references Card(CNum) not null,
	IntergralNum varchar(100)  not null,
)
go

create table Shop 
(
	SID int primary key identity(1,1) not null,
	SName nvarchar(10) not null,
	SAddress nvarchar(200) not null,
	Ssptime nvarchar(10) not null,
	SManagerName nvarchar(10) not null,
)
go

create table Vehicle
(
	VeID int  primary key identity(1,1) not null,
	VID  int references VipCustomer(VID) not null,
	VeNum nvarchar(20) not null,
)
go
create table VLevel
(
	VLID int primary key identity(1,1) not null,
	VLReachNumber int not null,
	VLName nvarchar(10) not null,
)
go
