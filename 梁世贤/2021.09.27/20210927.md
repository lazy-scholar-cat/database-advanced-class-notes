use master go

create database sjb go

use sjb go

create table sc ( scid int primary key not null, scname varchar(10) not null, scdress varchar(50) not null, scfr varchar(8) not null, sctelephone varchar(20) not null ) go

create table hyinfo ( hynumber int primary key not null, hyname varchar(8) not null, hysex varchar(2) not null, hytelepone varchar(20) not null ) go

create table hykinfo ( hyknumber int primary key not null, cyr varchar(8) not null, fkjg int not null foreign key(fkjg) references sc(scid), hykh int not null, hynumber int foreign key(hynumber) references hyinfo(hynumber) ) go

create table hyktype ( typeid int primary key not null, typename varchar(4) not null, discounts varchar(50) not null ) go

create table spinfo ( spid int primary key not null, spname varchar(10) not null, spfr varchar(8) not null, spsc int foreign key(spsc) references sc(scid), lb varchar(10) ) go

create table hyjfinfo ( hykid int foreign key(hykid) references hykinfo(hyknumber), jf int not null ) go

create table useinfo ( useid varchar(8) primary key not null, zh varchar(15) not null, mm varchar(15) ) go

select * from sc

insert sc(scid,scname,scdress,scfr,sctelephone) select 579,'曹溪农贸','龙岩市新罗区曹西街道矽尘路253号','安安','18854693512' union select 778,'时代','西京市萧山区蓝柚街道小友路1105号','小豆腐','1384695289'

select * from hyinfo

insert hyinfo(hynumber,hyname,hysex,hytelepone) select 258,'张晓','男','183486775921 ' union select 14,'小贝','女','18355997486 '

select * from hykinfo

insert hykinfo(hyknumber,cyr,fkjg,hykh,hynumber) select 112,'张晓',778,112,258 union select 524,'小贝',579,457,14

select * from hyktype

insert hyktype(typeid,typename,discounts) select 1,'VIP','所有商品享受九折优惠' union select 2,'SVIP','所有商品享受七五折优惠'

select * from spinfo

insert spinfo(spid,spname,spfr,lb) select 1,'糖果屋','张晓云','甜品' union select 33,'大胃王','张云生','自助'

select * from hyjfinfo

insert hyjfinfo(hykid,jf) select 112,5913 union select 524,6982

select * from useinfo

insert useinfo(useid,zh,mm) select '张晓','11548161','15919681fdgd ' union select '小贝','1256461','d1f56sd1d561f'


-- 登录

-- 根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败

select * from Users where Username='admin' and Password='123'

-- 更进一步，就是当前登录的用户可能已经被注销、禁用等情况，如何应对 delete from Users where UserID='admin' -- 注册

-- 用户层面 ，输入用户名，密码，重复密码

-- 注册单个用户 insert into Users (Username,Password) values ('Admin','124124')

-- 批量注册

insert into Users (Username,Password) values ('user01','574574'),('user02','757457'),('user03','757575'),('user04','522575')
