自定义函数
-- 目前主流的数据库

-- 关系型数据库

-- 商业

/*

Oracle 它是在数据库领域一直处于领先地位的产品。系统可移植性好、使用方便、功能强，适用于各类大、中、小、微机环境。它是一种高效率、可靠性好的、适应高吞吐量的数据库方案。

SqlServer 具有使用方便可伸缩性好与相关软件集成程度高等优点，可跨越从运行Microsoft Windows 98 的膝上型电脑到运行Microsoft Windows 2012 的大型多处理器的服务器等多种平台使用。

*/

-- 开源数据库

/*

Mysql MySQL 是最流行的关系型数据库管理系统之一，在 WEB 应用方面，MySQL是最好的 RDBMS (Relational Database Management System，关系数据库管理系统) 应用软件之一。

MariDb 相对于MySQL最新的版本5.6来说，在性能、功能、管理、NoSQL扩展方面，MariaDB包含了更丰富的特性。比如微秒的支持、线程池、子查询优化、组提交、进度报告等

PostgreSQL 绝大多数OLTP场景，部分OLAP 适合目前互联网需要的一些信息，比如地理位置信息处理；     以postgresql作为底层数据库的greenplum数据仓库，是主流的MPP数据仓库；     基于postgresql的TimeScaleDB，是目前比较火的时序数据库之一；

*/

-- 非关系型数据库 NoSql

/*

1.Redis

适用场景：
    缓存     基础消息队列系统     排行榜系统     计数器使用     社交网站的点赞、粉丝、下拉刷新等应用：

2.MongoDb

适用场景：
    网站后台数据库：mongodb非常适合实话实说插入、更新与查询，并可以实时复制和高伸缩性，适合更新迭代快、需求变更多、以对象为主的网站应用；     小文件系统：对于json文件，二进制数据，适合用mongodb进行存储和查询     日志分析系统：对于数据量大的日志文件，IM会话消息记录，适合用mongodb来保存和查询；     缓存系统：mongodb数据库也会使用大量的内存，合理的设计，也可以作为缓存系统使用；不过目前缓存系统使用更多的方案是 memcached和redis。

#liuux命令

-------------开关机命令 1、shutdown –h now：立刻进行关机

2、shutdown –r now：现在重新启动计算机

3、reboot：现在重新启动计算机

4、su -：切换用户；passwd：修改用户密码

5、logout：用户注销

-------------常用快捷命令 1、tab = 补全

2、ctrl + l -：清屏，类似clear命令

3、ctrl + r -：查找历史命令（history）；ctrl+c = 终止

4、ctrl+k = 删除此处至末尾所有内容

5、ctrl+u = 删除此处至开始所有内容

------------------文件显示命令 more：显示文件内容，带分页；

less：显示文件内容带分页；

grep：在文件中查询内容，grep –n “查找内容” 文件名；

| [管道命令]：在linux和unix系统中 | 就是管道命令，把上一个命令的结果交给 | 的后面的命令进行处理。

例：grep –n “查找内容” 文件名 | more

cat：把文件串连接后输出到屏幕或加 > fileName 到另一个档案。

head：格式：head [-n|c num]fileName，说明：显示文件头部内容。没有参数时，显示最前10行。

tail：格式：tail [-n|c num]fileName，说明：显示文件尾部内容。没有参数时，显示最后10行。

cut：格式：cut -cnum1-num2 filename，说明：显示每行从开头算起第 num1 到 num2 的字符。