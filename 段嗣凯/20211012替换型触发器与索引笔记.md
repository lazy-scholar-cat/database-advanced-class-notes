# 替换形触发器与索引

|对表的操作|Inserted逻辑表|Deleted逻辑表|
|-|-|-|
|增加记录（insert）|存放增加的记录|无|
|删除记录（delete）|无|存放被删除的记录|
|修改记录（update）|存放更新后的记录|存放更新前的记录|

## 怎么分辨他们呢

增
```sql
create trigger tr_TmpRankForInsert
on tmpRank
after insert
as
begin
	select * from inserted
	select * from deleted
end
go
```

删
```sql
create trigger tr_TmpRankForDelete
on tmpRank
after delete
as
begin
	select * from inserted
	select * from deleted
end
go
```

改
```sql
create trigger tr_TmpRankForUpdate
on tmpRank
after update
as
begin
	select * from inserted
	select * from deleted
end
go
```

查
```sql
update tmpRank set Score=9999 where keId=11 and renId=1

delete from tmpRank where keId=11 and renId=1
go
```

## 怎么创建索引

1.NIQUE: 建立唯一索引。

2.CLUSTERED: 建立聚集索引。

3.NONCLUSTERED: 建立非聚集索引。

4.Index_property: 索引属性。

5.UNIQUE索引既可以采用聚集索引结构，也可以采用非聚集索引的结构，如果不指明采用的索引结构，则SQL Server系统默认为采用非聚集索引结构。