create database VIPxt

use VIPxt
go

create table market
(
	marketID int primary key identity(1,1) not null,
	marketName nvarchar(20) not null,
	marketAddess nvarchar(80) not null,
	marketRepresentative nvarchar(20) not null,
	marketPhone int not null
)

create table customer
(
	customerID int primary key identity(1,1) not null,
	customerName nvarchar(20) not null,
	customerSex nvarchar(3) not null,
	customerBirthday datetime not null,
	customerPhone int not null,
	customerSFZ int not null,
	customerAddess nvarchar(80) not null,
)

create table VIPcard
(
	VIPcardID int primary key identity(1,1) not null,
	VIPcardCustomerID int foreign key references customer(customerID) not null,
	VIPcardUseName nvarchar(20) not null,
	VIPMarket nvarchar(80) not null,
	VIPNumer int not null
)

create table Shops
(
	ShopsID int primary key identity(1,1) not null,
	ShopsName  nvarchar(20) not null,
	ShopsMarketID int foreign key references  market( marketID) not null,
	ShopsCommodity nvarchar(80) not null
)

create table VIPIntegral
(
	VIPIntegralID int foreign key references  VIPcard( VIPcardID) not null,
	VIPIntegralnum int not null,
	VIPIntegralExpirationtime datetime not null
)
