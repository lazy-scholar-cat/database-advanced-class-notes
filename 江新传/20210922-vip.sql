create database  VIP

on 

( 
	name='VIP',
	filename='D:\VIP.mdf',
	maxsize=100,
	size=5,
	filegrowth=10%

)

log on 

(

name='VIP_log',
filename='D:\VIP_log.ldf',
maxsize=100,
size=5,
filegrowth=10%
)
use VIP

create table ScTable

(
	scid int primary key identity ,

	scname nvarchar(50) not null ,

	scjc nvarchar (50) not null ,

	sczcdz nvarchar (50) not null ,

	sczcfrName nvarchar (50) not null ,

	bgdh nvarchar (50) not null 

 )

 create table VIPxx
(
	VIPid int identity primary key ,

	VIPName nvarchar (50) not null ,

	sjhm nvarchar (50) not null ,

 )

 create table Clxx

 (
	carid int identity primary key ,

	carh nvarchar (50) not null ,

	VIPid int foreign key references VIPxx(VIPid)
 
 )

 
 create table  Cartc 

 (
	tcid int identity primary key ,

	admintype nvarchar (50) not null ,

	tc nvarchar (50) not null ,
 
 )

 create table  StopCar

 (
	carid int identity primary key ,
	
	stoptime int not null ,
	
	tcid int foreign key references cartc(tcid)   ,
 
	moneys int  not null ,

	jf int ,
 
 )

 create table VIPCardType 

 (
	VIPtypeid int identity primary key ,

	VIPtype nvarchar (50)  not null ,
	
	VIPsx nvarchar (50) not null ,

 )

 create table VIPCard 

 (
	VIPCardid int identity primary key ,

	VIPCardtypeID int foreign key references VIPCardtype(VIPtypeid) ,
	
	VIPly int  foreign key references sctable(scid) ,
 
	VIPid int foreign key references VIPxx(VIPid) ,
 
	Cardjf int not null ,

	gs nvarchar (50) not null check (gs='�ѹ�ʧ' or gs = 'δ��ʧ')

 )

 create table Spxx

 (
	spid int identity primary key ,

	spName nvarchar (50) not null ,

	sptype nvarchar (50) not null ,

	spdd nvarchar (50) not null ,

	spdh nvarchar (50) not null ,

	spbossname nvarchar (50) not null 
 )

 create table VIPhd 

 ( 
	hdid int identity primary key ,

	hdName nvarchar (50) not null ,

	hddd nvarchar (50) not null ,

	hdtime nvarchar (50) not null ,

	hdnr nvarchar (50) not null 
 
 )

 create table  Hyjf

 (
	spid int identity ,

	spName nvarchar (50) ,

	jfjg nvarchar (50) 

 )

 
 create table MainType

 (
	typeid int identity primary key ,

	typexx nvarchar (50) not null 
 
 
 )


 create table Adminxx

 (
	mainid int identity primary key ,

	admins nvarchar (50) not null ,

	passwords nvarchar (50) not null ,

	maintype int  foreign key references MainType(typeid)
 
 )

insert into VIPxx values
(
	'����Ŀ�ͽ����','12345678945'
),
(
	'��Ҹ��ڵ�����','15887979416'
),
(
	'�����ҵĺ�����','18797498849'
)

select * from  VIPxx 


