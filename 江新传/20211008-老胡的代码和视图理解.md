# CTE递归及多语句表值函数

```sql



drop database ShoppingMall

create database ShoppingMall

use ShoppingMall


create table Category

(
	Id int primary key identity,
	CategoryName nvarchar(80) not null,
	ParentId int not null default 0
	
)


insert into Category (CategoryName) values ('服装')

insert into Category (CategoryName) values ('电脑')

insert into Category (CategoryName) values ('手机')

insert into Category (CategoryName) values ('食品')

insert into Category (CategoryName) values ('饰品')

insert into Category (CategoryName) values ('家电')


insert into Category (CategoryName,ParentId) values ('男装',(select top 1 Id from Category where CategoryName='服装'))

insert into Category (CategoryName,ParentId) values ('女装',(select top 1 Id from Category where CategoryName='服装'))

insert into Category (CategoryName,ParentId) values ('童装',(select top 1 Id from Category where CategoryName='服装'))

insert into Category (CategoryName,ParentId) values ('OL装',(select top 1 Id from Category where CategoryName='服装'))

insert into Category (CategoryName,ParentId) values ('老人装',(select top 1 Id from Category where CategoryName='服装'))

insert into Category (CategoryName,ParentId) values ('整机',(select top 1 Id from Category where CategoryName='电脑'))

insert into Category (CategoryName,ParentId) values ('组装机',(select top 1 Id from Category where CategoryName='电脑'))

insert into Category (CategoryName,ParentId) values ('笔记本',(select top 1 Id from Category where CategoryName='整机'))

insert into Category (CategoryName,ParentId) values ('品牌机台式机',(select top 1 Id from Category where CategoryName='整机'))


insert into Category (CategoryName,ParentId) values ('西装',(select top 1 Id from Category where CategoryName='男装'))

insert into Category (CategoryName,ParentId) values ('中山装',(select top 1 Id from Category where CategoryName='男装'))

insert into Category (CategoryName,ParentId) values ('风衣',(select top 1 Id from Category where CategoryName='男装'))

insert into Category (CategoryName,ParentId) values ('运动休闲',(select top 1 Id from Category where CategoryName='男装'))

insert into Category (CategoryName,ParentId) values ('家居服',(select top 1 Id from Category where CategoryName='男装'))

insert into Category (CategoryName,ParentId) values ('内衣',(select top 1 Id from Category where CategoryName='男装'))

select * from Category


--delete from Category

;with CTE_A
as
(
	select Id,CategoryName,ParentId from Category where Id=42
	union all
	select c.Id,c.CategoryName,c.ParentId from Category c,CTE_A a
	where c.Id=a.ParentId

)select * from CTE_A


-- 多语句表值函数 语法

/*
create function <函数名称>
(
	[
		@xxx 数据类型 [=默认值],
		@yyy 数据类型 [=默认值]
	]
)
returns @res table (Id int ,Name nvarchar(80))
as
begin

	return
end
*/

go

alter function fn_StudentInfoSelectByName(@name nvarchar(80))
returns @res table(Id int ,Name nvarchar(80))
as
begin
	declare @x nvarchar(80)
	insert into @res(Id,Name) select id,studentname from StudentInfo where studentname=@name

	select @x=name from @res

	delete from @res
	insert into @res(Id,Name) select id,studentname from StudentInfo
		
	drop @res
	alter @res()
	return
end

go

select * from dbo.fn_StudentInfoSelectByName('赵雷')


/*
	总结：单语句表值函数和多语句表值函数，实际上的区别只在于后者有一个表类型的变量，在后者定义的函数内部，可以任意的对这个变量
	进行增删改查的操作(可以操作其中的数据，但是不能对表变量进行删除等操作)；单语句表值函数，并没有一个表类型的变量，它最后只
	是返回一个表，也可以理解为其整个的过程就是一个的select语句（并不只是说，只能有一个select语句，比如使用CTE，就不只一个select）

	另外：多语句表值函数中的表变量，类似于数据库当中的临时表


*/

```
## 视图

```sql

go
alter view vw_StudentInfo
as
select a.Id,b.studentname,c.coursename,a.score from studentcoursescore a
left join studentinfo b on a.studentid=b.id
left join CourseInfo c on a.courseId=c.id



select * from vw_StudentInfo


```
### 视图作用理解

将需要的数据连接合成一个新的表，此表不可进行修改、更新、删除操作，可进行查询，
并且视图表内所用到的原表数据改变的话，视图表内容也会跟着改变。
作用与储存过程大致相同，调用时直接调用名称即可不必再打出复杂的查询语句

注：视图可以将其他的两个或多个视图相连，也可以将其他的视图和表相连





