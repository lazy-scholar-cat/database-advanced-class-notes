# 练习1：搞清楚 set nocount的用法

## set nocount 语句


![ll](./imgs/2021-10-12_110103.png)

## set nocount on 语句：



### 作用

执行该语句后会将消息栏里的“返回计数消失”，如图：


未执行前：

![ll](./imgs/2021-10-12_110201.png)

执行后：

![ll](./imgs/2021-10-12_110909.png)

### 在存储过程里使用

set nocount on 语句是可以在储存过程中使用的，图示：

![ll](./imgs/2021-10-12_110201.png)

![ll](./imgs/2021-10-12_110347.png)


![ll](./imgs/2021-10-12_110426.png)


![ll](./imgs/2021-10-12_110500.png)


![ll](./imgs/2021-10-12_110533.png)



只不过还有一个返回计数在哪、由谁返回我始终找不到，嗯~看来这是个问题

## set nocount off 

用于关闭 set nocount on 语句的语句，

不过要注意，set nocount on 在储存过程中是不需要使用该语句的，

因为他在走出储存过程时会自动结束set nocount on 语句，

也就是说存储过程里的set nocount on 语句是无法在储存过程外被使用的




# 索引

```sql
-- 索引 是为了加快查询效率

/*
索引分成两类：聚集索引和非聚集索引

聚集索引：就是数据存储的顺序和索引的顺序一致
非聚集索引：数据存储的秦贵育和索引未必一致

create [unique] [clustered | nonclustered] index <索引的名称>
on 数据表(字段1 [asc | desc],字段2 [asc | desc])
*/

```

# 练习2：创建函数，可以根据输入的参数，随机构造若干条数据，插入到指定的表

## 随机生成相应数据的函数

```sql

create table Name (

	id int identity primary key ,

	SurName nvarchar (50) not null ,

	Name nvarchar (50) not null ,

	
 
)

insert into Name values                         
                          ('江','新'),
						
						('王','传'),

						('刘','青'),

						('李','霞'),

						('张','子'),

						('章','枫'),

						('冯','沫'),

						('于','楠'),

						('余','琪'),

						('周','洋'),

						('赵','延'),

						('曹','帅'),

						('匡','文'),

						('黄','聪'),

						('陈','才'),

						('程','辉'),

						('何','龙'),

						('孙','海'),

						('胡','涛'),

						('万','晨'),

						('柯','成'),

						('凌','佳'),

						('郑','伟'),

						('甄','杰'),

						('贾','小'),

						('韦','宝'),
						
						('魏','明'),

						('司马','彬'),

						('诸葛','梦'),
						
						('上官','婷'),

						('欧阳','师')

create table admintable (

	id int identity primary key ,
	
	AdName nvarchar (80) not null ,
	
	AdAge  int not null ,

	Adsex  bit not null ,

)


go

create proc proc_dr

	@int int
	
as

begin


	declare @SurName nvarchar (50) ,@Name nvarchar (50) ,@Name2 nvarchar (50),@id int =1,@ll int

	while(@id<=@int)

	begin 

	select @SurName=SurName  from Name  where id in (select round(1+30*rand(),0))

	select @Name=Name  from Name  where id in (select round(1+30*rand(),0))

	set @ll=round(rand(),0)

	if (@ll=1)

		begin
			
				select @Name2=Name  from Name  where id in (select round(1+30*rand(),0))

			

		end

	else

		begin

				set @Name2=''

			
		end


	insert into admintable  values (@SurName+@Name+@Name2,round(1+119*rand(),0),@ll)

	 set @id=@id+1

	end

end

go

exec proc_dr 100

select * from admintable

```


# 练习3：在练习2的基础上，对指定的表创建索引，观察索引对数据查询的影响

## 创建索引

```sql

create  nonclustered index A

on adminTable (

		AdName asc , adAge desc ,Adsex asc

		)


-- 如果在创建表时设置了主键，那么就无法再给该表设置聚集索引，因为主键默认为聚集索引


```

## 删除索引

```sql
drop   index A on adminTable
```

## 变化

    感觉就是和 row_number 一样，按优先级进行排序






