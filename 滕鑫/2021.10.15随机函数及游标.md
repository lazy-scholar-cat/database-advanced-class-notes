# 随机函数
```sql
select cast( floor(rand()*N) as int )  --方法1
select cast( ceiling(rand()*N) as int ) --方法2
/*
方法1的数字范围：0至N-1之间，如cast( floor(rand()*100) as int)就会生成0至99之间任一整数

方法2的数字范围：1至N之间，如cast(ceiling(rand() * 100) as int)就会生成1至100之间任一整数

CEILING 函数返回大于或等于所给数字表达式的最小整数。

FLOOR 函数返回小于或等于所给数字表达式的最大整数。

例如，对于数字表达式 12.9273，CEILING 将返回 13，FLOOR 将返回 12。
*/
```

# 游标的定义与使用
```
1.游标的概念

游标是SQL Server的一种数据访问机制，它允许用户访问单独的数据行。用户可以对每一行进行单独的处理，从而降低系统开销和潜在的阻隔情况，用户也可以使用这些数据生成的SQL代码并立即执行或输出。

游标是一种处理数据的方法，主要用于存储过程，触发器和 T_SQL脚本中，它们使结果集的内容可用于其它T_SQL语句。在查看或处理结果集中向前或向后浏览数据的功能。类似与C语言中的指针，它可以指向结果集中的任意位置，当要对结果集进行逐条单独处理时，必须声明一个指向该结果集中的游标变量。

 SQL Server 中的数据操作结果都是面向集合的，并没有一种描述表中单一记录的表达形式，除非使用WHERE子句限定查询结果，使用游标可以提供这种功能，并且游标的使用和操作过程更加灵活、高效。

```
```sql
-- 游标 是在数据中当中为，遍历数据集的一种方式或手段，可以反游标当成一种指针
/*
第一步：定义游标
第二步：打开游标
第三步：先获取一条记录
第四步：判断获取数据是否成功，成功则处理数据；否则退出循环
第五步：关闭游标
第六步：释放游标
*/
/*
当用户积分100时，更新会员等级为一级会员
当用户积分为200-400时，更新为二级会员
当用户积分为500-800时，更新为三级会员
*/
-- 定义游标
declare  cur_ForPersonalInfo2 cursor
for 
select * from PersonalInfo

-- 打开游标，准备使用
open cur_ForPersonalInfo2

-- 定义变量，用于接收每一行中的数据
declare @id int,@name nvarchar(80) ,@birthday date

-- 先获取一条，以使@@fetch_status状态初始化
fetch next from cur_ForPersonalInfo2
into @id,@name,@birthday

select @@FETCH_STATUS

-- 真正遍历记录的语句结构
while @@Fetch_Status = 0
begin
	select @id,@name,@birthday -- 代表处理取出的每一行数据
	fetch next from cur_ForPersonalInfo2
	into @id,@name,@birthday
end

-- 关闭游标
close cur_ForPersonalInfo2
-- 释放游标
deallocate cur_ForPersonalInfo2

```
# 修改后
```sql
--@@cursor_rows

declare  cur_stu cursor
for 
select sno,sname,sbirthday from student

-- 打开游标，准备使用
open cur_stu

-- 定义变量，用于接收每一行中的数据
declare @sno int,@sname nvarchar(80) ,@sbirthday date

-- 先获取一条，以使@@fetch_status状态初始化
fetch next from cur_stu
into @sno,@sname,@sbirthday

select @@FETCH_STATUS

-- 真正遍历记录的语句结构
while @@Fetch_Status = 0
begin
	select @sno,@sname,@sbirthday -- 代表处理取出的每一行数据
	fetch next from cur_stu
	into @sno,@sname,@sbirthday
end

-- 关闭游标
close cur_stu
-- 释放游标
deallocate cur_stu


```

# 1千万记录查询时长

![没东西了](./imgs/datediff定义.png)
![没东西了](./imgs/1千万记录查询时长.png)
![没东西了](./imgs/一千万索引测试1.png)
![没东西了](./imgs/一千万索引测试2.png)
![没东西了](./imgs/一千万索引测试3.png)
![没东西了](./imgs/一千万索引测试4.png)
```sql

declare @begintime datetime=getdate()
select * from student where sname like '孔树庆'
declare @endtime datetime=getdate()
select DATEDIFF(ms,@begintime,@endtime)

drop index ind_stud on student
```