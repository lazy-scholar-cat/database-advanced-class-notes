create database Supermaket
GO
use Supermaket
create table Supinfo
(
	scid nvarchar(80) primary key not null,
	scname nvarchar(80),
	jc nvarchar(80),
	scaddress nvarchar(80),
	zcr nvarchar(80),
	phone nvarchar(80)
)
create table Vipinfo
(
	vipid nvarchar(80) primary key not null,
	vipname nvarchar(80),
	sex char(2),
	birth date,
	phone nvarchar(80),
	sfz nvarchar(80),
	vipaddress nvarchar(80)
)
create table Vipcardinfo
(
	vipcardid nvarchar(80) primary key not null,
	vipcid int,
	vipctypeid nvarchar(80),
	cardzz nvarchar(80),
	vipid nvarchar(80)
)
create table Vipctypeinfo
(
	vipctypeid nvarchar(80) primary key not null,
	typerank nvarchar(40),
	vipid nvarchar(80)
)
create table Shopinfo
(
	shopid nvarchar(80) primary key not null,
	shopname nvarchar(80),
	shopaddress nvarchar(80),
	zcr nvarchar(80),
	phone nvarchar(80),
	scid nvarchar(80)
)
create table Vipjfruleinfo
(
	shopid nvarchar(80) primary key not null,
	jftime int,
	vipmoney nvarchar(40),
	jf int
)
create table Vipjfinfo
(
	vipid nvarchar(80) primary key not null,
	jfbalance int
)
create table Vipclassinfo
(
	vipid nvarchar(80) primary key not null,
	classtype nvarchar(80),
	class nvarchar(80)
)
create table Vipeventinfo
(
	vipid nvarchar(80) primary key not null,
	eventtime date,
	expensejc int,
	expenseje int,
	shopid nvarchar(80)
)
create table Vipcarinfo
(
	vipid nvarchar(80) primary key not null,
	carid nvarchar(80),
	stoptime int,
	symoney int
)
create table Vipcarddsinfo
(
	vipcardid nvarchar(80) primary key not null,
	dstime date,
	gscs int,
	vipid nvarchar(80)
)
create table Vipjfdhinfo
(
	vipid nvarchar(80) primary key not null,
	dhtime date,
	dhcx int,
	jfye int
)
create table Goodsinfo
(
	spid nvarchar(80) primary key not null,
	sptype nvarchar(40),
	spname nvarchar(80),
	sptime int,
	shopid nvarchar(80)
)
create table Userinfo
(
	userid nvarchar(80) primary key not null ,
	username nvarchar(40),
	sex char(2),
	birth date,
	phone nvarchar(80),
	sfz nvarchar(80),
	useraddress nvarchar(80),
	sfvip nvarchar(80)
)
create table Roleinfo
(
	userid nvarchar(80) primary key not null ,
	username nvarchar(40),
	sex char(2),
	roletype nvarchar(40),
	sfvip nvarchar(80)
)
create table Userroleinfo
(
	userid nvarchar(80) primary key not null ,
	roletype nvarchar(40),
	userPower nvarchar(10),
	sfvip nvarchar(80)
)
select * from Supinfo
insert into Supinfo(scid,scname,jc,scaddress,zcr,phone)values('sc0001','永辉超市','永辉','福建福州','陈旭',122233)
insert into Supinfo(scid,scname,jc,scaddress,zcr,phone)values('sc0002','万达商场','万达','江西上饶','狗贼',122234)
select * from Vipinfo
insert into Vipinfo(vipid,vipname,sex,birth,phone,sfz,vipaddress)values('tx0001','祖师爷','男','1999-01-11',1333444,12345678,'福建厦门')
insert into Vipinfo(vipid,vipname,sex,birth,phone,sfz,vipaddress)values('tx0002','小红','女','2000-06-06',1222444,12345677,'江西南昌')
select * from Vipcardinfo
insert into Vipcardinfo(vipcardid,vipcid,vipctypeid,cardzz,vipid)values('hy0001',12355,'txc0001','永辉','tx0001')
insert into Vipcardinfo(vipcardid,vipcid,vipctypeid,cardzz,vipid)values('hy0002',12366,'txc0002','万达','tx0002')
select * from Vipctypeinfo
insert into Vipctypeinfo(vipctypeid,typerank,vipid)values('txc0001','白金','tx0001')
insert into Vipctypeinfo(vipctypeid,typerank,vipid)values('txc0002','黑金','tx0002')
select * from Shopinfo
insert into Shopinfo(shopid,shopname,shopaddress,zcr,phone,scid)values('sp0001','联想','福建泉州','张三',155997,'sc0001')
insert into Shopinfo(shopid,shopname,shopaddress,zcr,phone,scid)values('sp0002','六益','山西太原','小王',155998,'sc0002')
select * from Vipjfruleinfo
insert into Vipjfruleinfo(shopid,jftime,vipmoney,jf)values('sp0001',12,'不可折现',90)
insert into Vipjfruleinfo(shopid,jftime,vipmoney,jf)values('sp0002',6,'可折现',500)
select * from Vipjfinfo
insert into Vipjfinfo(vipid,jfbalance)values('tx0001',188)
insert into Vipjfinfo(vipid,jfbalance)values('tx0002',388)
select * from Vipclassinfo
insert into Vipclassinfo(vipid,classtype,class)values('tx0001','皇冠',99)
insert into Vipclassinfo(vipid,classtype,class)values('tx0002','小白',5)
select * from Vipeventinfo
insert into Vipeventinfo(vipid,eventtime,expensejc,expenseje,shopid)values('tx0001','2020-05-11',50,100,'sp0001')
insert into Vipeventinfo(vipid,eventtime,expensejc,expenseje,shopid)values('tx0002','2019-11-11',600,1500,'sp0002')
select * from Vipcarinfo
insert into Vipcarinfo(vipid,carid,stoptime,symoney)values('tx0001','京A111',3,88)
insert into Vipcarinfo(vipid,carid,stoptime,symoney)values('tx0002','京B222',10,488)
select * from Vipcarddsinfo
insert into Vipcarddsinfo(vipcardid,dstime,gscs,vipid)values('hy0001','2014-02-21',1,'tx0001')
insert into Vipcarddsinfo(vipcardid,dstime,gscs,vipid)values('hy0002','2008-01-15',3,'tx0002')
select * from Vipjfdhinfo
insert into Vipjfdhinfo(vipid,dhtime,dhcx,jfye)values('tx0001','2015-07-16',6,188)
insert into Vipjfdhinfo(vipid,dhtime,dhcx,jfye)values('tx0002','2021-09-17',7,388)
select * from Goodsinfo
insert into Goodsinfo(spid,sptype,spname,sptime,shopid)values('spi0001','电子产品','小霸王学习机',1830,'sp0001')
insert into Goodsinfo(spid,sptype,spname,sptime,shopid)values('spi0002','食品','炸鸡',7,'sp0002')
select * from Userinfo
insert into Userinfo(userid,username,sex,birth,phone,sfz,useraddress,sfvip)values('yh0001','祖师爷','男','1999-01-11',1333444,12345678,'福建厦门','tx0001')
insert into Userinfo(userid,username,sex,birth,phone,sfz,useraddress,sfvip)values('yh0002','小李','女','2007-11-06',1222555,12345671,'江西南昌','N')
select * from Roleinfo
insert into Roleinfo(userid,username,sex,roletype,sfvip)values('yh0001','祖师爷','男','老总','tx0001')
insert into Roleinfo(userid,username,sex,roletype,sfvip)values('yh0002','小李','女','客户','N')
select * from userroleinfo
insert into userroleinfo(userid,roletype,userPower,sfvip)values('yh0001','老总','Y','tx0001')
insert into userroleinfo(userid,roletype,userPower,sfvip)values('yh0002','客户','N','N')
--1.查询可折现的商铺信息
select * from Shopinfo where shopid in(select shopid from Vipjfruleinfo where vipmoney like '可折现')
--2.添加一条会员信息
insert into Vipinfo(vipid,vipname,sex,birth,phone,sfz,vipaddress)values('tx0003','卢本伟','男','1993-08-11',1111222,12345672,'广东广州')
select * from Vipinfo
--3.修改一条会员等级为tx0002的等级级别
update Vipclassinfo set classtype='青铜' where vipid='tx0002'
select * from Vipclassinfo
--4.删除会员信息里性卢的
delete from Vipinfo where vipid in(select vipid from Vipinfo where vipname like '卢%')
select * from Vipinfo
--实现业务的增删改查
--1.查询小霸王学习机来自哪个商场
select scname from Supinfo where scid in
(select scid from Shopinfo where shopid in
(select shopid from Goodsinfo where spname like '小霸王学习机'))
--2.在商铺SP0002中再添加一条商品信息
select * from Goodsinfo
insert into Goodsinfo(spid,sptype,spname,sptime,shopid)values('spi0003','食品','牛奶','180','sp0002')
--3.修改祖师爷的消费金额为30000
update Vipeventinfo set expenseje=30000 where vipid='tx0001'
select * from Vipeventinfo
--4.由于商场品牌关系要删除所有牛奶
delete from Goodsinfo where spname like '牛奶'
select * from Goodsinfo