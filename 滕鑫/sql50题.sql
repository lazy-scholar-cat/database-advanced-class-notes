
-- 练习题目：
select * from CourseInfo
select * from Teachers
select * from StudentInfo
select * from StudentCourseScore

-- 1.查询"数学 "课程比" 语文 "课程成绩高的学生的信息及课程分数
--备注：已知数学id为2，语文id为1 连接3个表
--方法1
select c.*,a.CourseId,a.Score,b.CourseId,b.Score from StudentCourseScore a join StudentCourseScore b  
on a.StudentId=b.StudentId join StudentInfo c on b.StudentId=c.StudentCode where a.CourseId=1 and b.CourseId=2 and a.Score<b.Score
--方法2
select c.*,b.courseid,b.score,a.courseid,a.score from (select * from StudentCourseScore where CourseId=1) A,
(select * from StudentCourseScore where CourseId=2) B,(select * from StudentInfo) C 
where A.studentid=b.studentid and C.id=A.studentid and b.score>a.score
-- 1.1 查询同时存在" 数学 "课程和" 语文 "课程的情况
--方法1
select c.*,a.CourseId,a.Score,b.CourseId,b.Score from StudentCourseScore a join StudentCourseScore b  
on a.StudentId=b.StudentId join StudentInfo c on b.StudentId=c.StudentCode where a.CourseId=1 and b.CourseId=2
--方法2
select * from (select * from StudentCourseScore where CourseId=1) A,(select * from StudentCourseScore where CourseId=2) B 
where A.studentid=b.studentid
-- 1.2 查询存在" 数学 "课程但可能不存在" 语文 "课程的情况(不存在时显示为 null )
--方法1
select * from (select * from StudentCourseScore where CourseId=2) A left join 
(select * from StudentCourseScore where CourseId=1) B on A.studentid=b.studentid
--方法2
select * from StudentCourseScore where StudentId in
(select StudentId from StudentCourseScore where StudentId not in
(select StudentId from StudentCourseScore where CourseId=1)or CourseId=2 group by StudentId having count(CourseId)>1)
-- 1.3 查询不存在" 数学 "课程但存在" 语文 "课程的情况
--方法1
select * from (select * from StudentCourseScore where CourseId=1) A left join 
(select * from StudentCourseScore where CourseId=2) B on A.studentid=b.studentid
--方法2
select * from  StudentCourseScore where StudentId in
(select StudentId from StudentCourseScore
where StudentId not in (select StudentId from StudentCourseScore
where CourseId='2'group by StudentId,CourseId) or CourseId='1' group by StudentId having  count(CourseId)>1)
-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩
select StudentCode,studentname,avg(score) 平均成绩 from StudentCourseScore t inner join StudentInfo x on t.StudentId=x.Id 
group by StudentCode,studentname having avg(score)>=60
-- 3.查询在 成绩 表存在成绩的学生信息
select distinct x.* from StudentCourseScore t join StudentInfo x on t.StudentId=x.StudentCode
-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的总成绩(没成绩的显示为 null )
select StudentCode,StudentName,count(CourseId)as 选课总数,sum(Score)as 所有课程的总成绩 
from StudentCourseScore t full join StudentInfo x 
on t.StudentId=x.StudentCode group by StudentCode,StudentName
-- 4.1 查有成绩的学生信息
select distinct x.* from StudentCourseScore t join StudentInfo x on t.StudentId=x.StudentCode
-- 5.查询「李」姓老师的数量
select count(id)as 李姓老师的数量 from Teachers where TeacherName like '李%'
-- 6.查询学过「张三」老师授课的同学的信息
select b.* from StudentCourseScore a join StudentInfo b on a.StudentId=b.StudentCode 
where CourseId in(select id from Teachers where TeacherName like '张三')
-- 7.查询没有学全所有课程的同学的信息
select b.*,count(CourseId)as 所学课程数量 from StudentCourseScore a full join StudentInfo b on a.StudentId=b.StudentCode
group by b.Id,b.StudentCode,b.StudentName,b.Birthday,b.Sex,b.ClassId having COUNT(CourseId)<3
-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息 
--StudentCode=01
--方法1
select distinct b.* from StudentCourseScore a join StudentInfo b on a.StudentId=b.StudentCode 
where StudentId!=1 and CourseId in(select CourseId from StudentCourseScore where StudentId=1 )
--方法2
select * from StudentInfo where Id in (select StudentId from  StudentCourseScore where StudentId!='1' and CourseId in 
(select CourseId from  StudentCourseScore where  StudentId='1') group by StudentId)
-- 9.查询和" 01 "号的同学学习的课程 完全相同的其他同学的信息
--方法1 通过学习数量已知01号学生学了3门课程，然后排除01号学生，最后查找数量跟01号学生相同的
select distinct b.*,count(CourseId)学习数量 from StudentCourseScore a join StudentInfo b on a.StudentId=b.StudentCode 
where StudentId!=1 and CourseId in(select CourseId from StudentCourseScore where StudentId=1 )
group by b.Id,b.StudentCode,b.StudentName,b.Birthday,b.Sex,b.ClassId having count(CourseId)=3
--方法2
select * from StudentInfo where id in
(select StudentId from
(select StudentId,CourseId from StudentCourseScore where StudentId in (
        select StudentId from StudentCourseScore where StudentId !=
        '1'
        group by StudentId having count(CourseId) > 2)) A,
		 (select CourseId from StudentCourseScore where StudentId =
    '1') B
	where A.CourseId = B.CourseId group by StudentId)
-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名
-- 已知张三教数学，数学id为2
select * from StudentInfo where Id not in (select StudentId from StudentCourseScore where CourseId 
in(select id from CourseInfo where TeacherId=(select id from Teachers where TeacherName like '张三' )))
-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩
--方法1
select StudentCode,StudentName,avg(Score)平均成绩 from StudentCourseScore a join StudentInfo b 
on a.StudentId=b.StudentCode 
where Score<60 group by StudentCode,StudentName having count(CourseId)>=2
--方法2
select SCS.StudentId,StudentName,AVG(score) 平均成绩 from StudentCourseScore SCS  join StudentInfo SI 
on SI.Id=StudentId group by SCS.StudentId,StudentName
having StudentId in (select StudentId from  StudentCourseScore where Score<60 group by StudentId having count(CourseId)>=2)
-- 12.检索" 数学 "课程分数小于 60，按分数降序排列的学生信息
--方法1
select b.* from StudentCourseScore a join StudentInfo b on a.StudentId=b.StudentCode 
where score<60 and Score in(select score from StudentCourseScore where CourseId in
(select id from CourseInfo where CourseName like '数学'))order by Score
--方法2
select * from StudentInfo
where Id=(select StudentId from StudentCourseScore
where CourseId in (select Id from CourseInfo where CourseName='数学') and Score<60)
-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩
select StudentName,score,avg(score) 平均成绩 from StudentCourseScore a join StudentInfo b on a.StudentId=b.StudentCode 
group by StudentName,score order by avg(score) desc
-- 14.查询各科成绩最高分、最低分和平均分：
--方法1
select max(a.Score)语文最高分,min(a.Score)语文最低分,avg(a.Score)语文平均分,
max(b.Score)数学最高分,min(b.Score)数学最低分,avg(b.Score)数学平均分,
max(c.Score)英语最高分,min(c.Score)英语最低分,avg(c.Score)英语平均分 
from(select Score from StudentCourseScore where CourseId=1) a,(select Score from StudentCourseScore where CourseId=2) b,(select Score
from StudentCourseScore where CourseId=3) c
--方法2
select CourseId,max(Score) 最高分,min(Score) 最低分,avg(Score) 平均分 from StudentCourseScore group by CourseId
-- 15.以如下形式显示：课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
/*
	及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90

	要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序排列

	按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺
*/	
select CourseId,CourseName as 课程,
max(Score) as 最高分,
min(Score) as 最低分,
avg(Score) as 平均分,
cast(cast(sum(case when Score>=60 then 1 else 0 end)*100/(count(StudentId)) as float) as nvarchar)+'%' as 及格率,
cast(cast(sum(case when Score >= 70 and Score >= 80 then 1 else 0 end)*100/(count(StudentId)) as float) as nvarchar)+'%' as 中等率,
cast(cast(sum(case when Score >= 80 and Score >= 90 then 1 else 0 end)*100/(count(StudentId)) as float) as nvarchar)+'%' as 优良率,
cast(cast(sum(case when Score >= 90 then 1 else 0 end)*100/(count(StudentId)) as float) as nvarchar)+'%' as 优秀率
from StudentCourseScore A inner join StudentInfo B on A.StudentId = B.StudentCode
inner join CourseInfo C on A.CourseId = C.Id group by CourseId,CourseName
-- 15.1 按各科成绩进行排序，并显示排名， Score 重复时合并名次
select *,DENSE_RANK() over(partition by courseid order by score desc) from StudentCourseScore 
-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺
select studentid,sum(Score),RANK() over(order by sum(Score) desc) from StudentCourseScore group by studentid
-- 16.1 查询学生的总成绩，并进行排名，总分重复时不保留名次空缺
select studentid,sum(Score) 总成绩,DENSE_RANK() over(order by sum(Score) desc) 排名 from StudentCourseScore group by studentid
-- 17.统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比
;with t as(select CourseId,(case when Score>=85 then 1 else 0 end)a85,
(case when Score>=70 and Score<85 then 1 else 0 end)a70,
(case when Score>=60 and Score<70 then 1 else 0 end)a60,
(case when Score>=0 and Score<70 then 1 else 0 end)a00 from StudentCourseScore),
x as (select CourseId,sum(a85) a85,sum(a70) a70,sum(a60) a60,sum(a00) a00,
 sum(a85)+sum(a70)+sum(a60)+sum(a00) tcount from t group by CourseId)
 select a.CourseId,b.CourseName,CONVERT(nvarchar(80),CONVERT(decimal(18,2),a85*100.0/tcount))+'%' [100-85]
 ,CONVERT(nvarchar(80),CONVERT(decimal(18,2),a70*100.0/tcount))+'%' [85-70]
 ,CONVERT(nvarchar(80),CONVERT(decimal(18,2),a60*100.0/tcount))+'%' [70-60]
 ,CONVERT(nvarchar(80),CONVERT(decimal(18,2),a00*100.0/tcount))+'%' [60-0]
 from x a left join CourseInfo b on a.CourseId=b.Id
-- 18.查询各科成绩前三名的记录
;with t as (select *,DENSE_RANK() over(partition by CourseId order by score desc) total from StudentCourseScore)
select * from t where total<=3
-- 19.查询每门课程被选修的学生数
select CourseId,count(*) 被选修的学生数 from StudentCourseScore group by CourseId
-- 20.查询出只选修两门课程的学生学号和姓名
;with t as (select StudentId from StudentCourseScore group by StudentId having count(CourseId)=2)
select StudentCode,StudentName from StudentInfo a join t on a.StudentCode=t.StudentId
-- 21.查询男生、女生人数
select Sex,count(*) from StudentInfo group by Sex
-- 22.查询名字中含有「风」字的学生信息
select * from StudentInfo where StudentName like '%风%'
-- 23.查询同名同性学生名单，并统计同名人数
select count(*) as 同名人数 from StudentInfo A,StudentInfo B
where A.StudentName = B.StudentName and A.Sex = B.Sex and A.StudentCode != B.StudentCode
-- 24.查询 1990 年出生的学生名单
select * from StudentInfo where Birthday like '1990%'
-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列
--方法1
select CourseId,avg(Score) 平均成绩,DENSE_RANK() over(order by avg(Score) desc) 排名 from StudentCourseScore group by CourseId
--方法2
select CourseId,avg(Score) from StudentCourseScore group by CourseId order by avg(Score) desc
-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩
--方法1
select StudentCode,StudentName,avg(Score) 平均成绩 from StudentCourseScore a join StudentInfo b 
on a.StudentId=b.StudentCode group by StudentCode,StudentName having avg(Score)>=85
--方法2
;with t as(select StudentId,avg(Score) 平均成绩 from StudentCourseScore group by StudentId having avg(Score)>=85),
x as(select StudentCode,StudentName from StudentInfo)
select x.StudentCode,x.StudentName,t.平均成绩 from t join x on t.StudentId=x.StudentCode
-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数
--方法1
select b.StudentName,a.Score from StudentCourseScore a join StudentInfo b on a.StudentId=b.StudentCode 
where CourseId in(select id from CourseInfo where CourseName like '数学') and score<60
--方法2
;with t as(select StudentId,Score from StudentCourseScore where CourseId in
(select id from CourseInfo where CourseName like '数学') and score<60),
x as(select StudentCode,StudentName from StudentInfo)select StudentName,Score from t join x on t.StudentId=x.StudentCode
-- 28.查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）
select StudentName,CourseName,Score from StudentCourseScore a 
full join StudentInfo b on a.StudentId=b.StudentCode full join CourseInfo c on a.CourseId=c.Id
-- 29.查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数
select StudentName,CourseId,Score from StudentCourseScore a join StudentInfo b on a.StudentId=b.StudentCode where Score>70
-- 30.查询不及格的课程
select distinct CourseName from StudentCourseScore A inner join CourseInfo B on A.CourseId = B.Id where Score<60
-- 31.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名
select StudentCode,StudentName from StudentCourseScore a join StudentInfo b 
on a.StudentId=b.StudentCode where CourseId=1 and Score>80
-- 32.求每门课程的学生人数
select CourseId,count(StudentId) 学生人数 from StudentCourseScore group by CourseId
-- 33.成绩不重复，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩
--方法1
select top 1 b.*,max(Score)成绩 from StudentCourseScore a join StudentInfo b on a.StudentId=b.StudentCode
where CourseId in(select id from CourseInfo where TeacherID in(select id from Teachers where TeacherName like '张三'))
group by b.Id,b.StudentCode,b.StudentName,b.Birthday,b.Sex,b.ClassId order by max(Score) desc
--方法2
;with a as (select StudentId,max(Score)成绩 from StudentCourseScore where CourseId in(select id from CourseInfo where TeacherID in
(select id from Teachers where TeacherName like '张三'))group by StudentId),
b as(select * from StudentInfo)select top 1 b.*,a.成绩 from a join b on a.StudentId=b.StudentCode 
group by b.Id,b.StudentCode,b.StudentName,b.Birthday,b.Sex,b.ClassId,a.成绩
--34.成绩有重复的情况下，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩
select b.*,max(Score)成绩 from StudentCourseScore a join StudentInfo b on a.StudentId=b.StudentCode where
CourseId in(select id from CourseInfo where TeacherID in(select id from Teachers where TeacherName like '张三'))and 
score in(select top 1 score from StudentCourseScore  where CourseId in(select id from CourseInfo 
where TeacherID in(select id from Teachers where TeacherName like '张三')) order by Score desc) group by
b.Id,b.StudentCode,b.StudentName,b.Birthday,b.Sex,b.ClassId order by max(Score) desc 
-- 35.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩
select A.StudentId as 学生编号,A.CourseId as 课程编号,A.Score as 学生成绩 from StudentCourseScore A,StudentCourseScore B
where A.CourseId != B.CourseId and A.Score = B.Score group by A.StudentId,A.CourseId,A.Score
-- 36.查询每门功成绩最好的前两名
select CourseId,Score from StudentCourseScore A where 
(select count(CourseId) from StudentCourseScore where CourseId=A.CourseId AND A.score<score)<2 
order by CourseId asc,score desc
-- 37.统计每门课程的学生选修人数（超过 5 人的课程才统计）。
select CourseId,count(CourseId) 选修人数 from StudentCourseScore group by CourseId having COUNT(CourseId)>5
-- 38.检索至少选修两门课程的学生学号
select StudentId from StudentCourseScore group by StudentId having count(CourseId)=2
-- 39.查询选修了全部课程的学生信息
select b.* from StudentCourseScore a join StudentInfo b on a.StudentId=b.StudentCode 
group by b.Id,b.Birthday,b.ClassId,b.Sex,b.StudentCode,b.StudentName having count(CourseId)=3 
-- 40.查询各学生的年龄，只按年份来算
select StudentName,datediff(year,Birthday,getdate()) as 年龄 from StudentInfo
-- 41.按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一
select StudentName,datediff(mm,Birthday,getdate()) as 出生月数 from StudentInfo
-- 42.查询本周过生日的学生
select * from StudentInfo where DATEPART(week,Birthday) = DATEPART(wk,getdate())
-- 43.查询下周过生日的学生
select * from StudentInfo where DATEPART(wk,Birthday) - DATEPART(wk,GETDATE()) = 1
-- 44.查询本月过生日的学生
select * from StudentInfo where DATEPART(mm,Birthday) - DATEPART(mm,GETDATE()) = 0
-- 45.查询下月过生日的学生
select * from StudentInfo where datepart(mm,Birthday) = (DATEPART(mm,GETDATE()) + 1)