create database ShoppingMallDatabase
go
use ShoppingMallDatabase
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ShopeInfo') and o.name = 'FK_SHOPEINFO_REFERENCE_6_USERS')
alter table ShopeInfo
   drop constraint FK_SHOPEINFO_REFERENCE_6_USERS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('UserRoles') and o.name = 'FK_USERROLES_REFERENCE_1_USERS')
alter table UserRoles
   drop constraint FK_USERROLES_REFERENCE_1_USERS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('UserRoles') and o.name = 'FK_USERROLES_REFERENCE_2_ROLES')
alter table UserRoles
   drop constraint FK_USERROLES_REFERENCE_2_ROLES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipCarInfo') and o.name = 'FK_VIPCARINFO_REFERENCE_10_VIPINFO')
alter table VipCarInfo
   drop constraint FK_VIPCARINFO_REFERENCE_10_VIPINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipCardInfo') and o.name = 'FK_VIPCARDINFO_REFERENCE_13_VIPCARDTYPE')
alter table VipCardInfo
   drop constraint FK_VIPCARDINFO_REFERENCE_13_VIPCARDTYPE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipCardReportLose') and o.name = 'FK_VIPCARDREPORTLOSE_REFERENCE_4_VIPINFO')
alter table VipCardReportLose
   drop constraint FK_VIPCARDREPORTLOSE_REFERENCE_4_VIPINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipCardType') and o.name = 'FK_VIPCARDTYPE_REFERENCE_12_SHOPPINGMALLINFO')
alter table VipCardType
   drop constraint FK_VIPCARDTYPE_REFERENCE_12_SHOPPINGMALLINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipGiftInfo') and o.name = 'FK_VIPGIFTINFO_REFERENCE_3_VIPACTIVITYINFO')
alter table VipGiftInfo
   drop constraint FK_VIPGIFTINFO_REFERENCE_3_VIPACTIVITYINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipInfo') and o.name = 'FK_VIPINFO_REFERENCE_11_SHOPPINGMALLINFO')
alter table VipInfo
   drop constraint FK_VIPINFO_REFERENCE_11_SHOPPINGMALLINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipPointExchange') and o.name = 'FK_VIPPOINTEXCHANGE_REFERENCE_14_VIPACTIVITYINFO')
alter table VipPointExchange
   drop constraint FK_VIPPOINTEXCHANGE_REFERENCE_14_VIPACTIVITYINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipPointExchange') and o.name = 'FK_VIPPOINTEXCHANGE_REFERENCE_8_VIPINFO')
alter table VipPointExchange
   drop constraint FK_VIPPOINTEXCHANGE_REFERENCE_8_VIPINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipPointExchange') and o.name = 'FK_VIPPOINTEXCHANGE_REFERENCE_9_VIPGIFTINFO')
alter table VipPointExchange
   drop constraint FK_VIPPOINTEXCHANGE_REFERENCE_9_VIPGIFTINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipPointInfo') and o.name = 'FK_VIPPOINTINFO_REFERENCE_7_VIPINFO')
alter table VipPointInfo
   drop constraint FK_VIPPOINTINFO_REFERENCE_7_VIPINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipPointRule') and o.name = 'FK_VIPPOINTRULE_REFERENCE_5_VIPACTIVITYINFO')
alter table VipPointRule
   drop constraint FK_VIPPOINTRULE_REFERENCE_5_VIPACTIVITYINFO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ProductsInfo')
            and   type = 'U')
   drop table ProductsInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Roles')
            and   type = 'U')
   drop table Roles
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ShopeInfo')
            and   type = 'U')
   drop table ShopeInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ShoppingMallInfo')
            and   type = 'U')
   drop table ShoppingMallInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('UserRoles')
            and   type = 'U')
   drop table UserRoles
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Users')
            and   type = 'U')
   drop table Users
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipActivityInfo')
            and   type = 'U')
   drop table VipActivityInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipCarInfo')
            and   type = 'U')
   drop table VipCarInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipCardInfo')
            and   type = 'U')
   drop table VipCardInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipCardReportLose')
            and   type = 'U')
   drop table VipCardReportLose
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipCardType')
            and   type = 'U')
   drop table VipCardType
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipGiftInfo')
            and   type = 'U')
   drop table VipGiftInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipInfo')
            and   type = 'U')
   drop table VipInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipLevel')
            and   type = 'U')
   drop table VipLevel
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipPointExchange')
            and   type = 'U')
   drop table VipPointExchange
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipPointInfo')
            and   type = 'U')
   drop table VipPointInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipPointRule')
            and   type = 'U')
   drop table VipPointRule
go

/*==============================================================*/
/* Table: ProductsInfo                                          */
/*==============================================================*/
create table ProductsInfo (
   Id                   int                  identity,
   ProductName          nvarchar(80)         null,
   Price                decimal(18,2)        null,
   Stock                int                  null,
   constraint PK_PRODUCTSINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: Roles                                                 */
/*==============================================================*/
create table Roles (
   Id                   int                  identity,
   RoleName             nvarchar(80)         null,
   constraint PK_ROLES primary key (Id)
)
go

/*==============================================================*/
/* Table: ShopeInfo                                             */
/*==============================================================*/
create table ShopeInfo (
   Id                   int                  identity,
   ShopeName            nvarchar(80)         null,
   ShopeAddress         nvarchar(800)        null,
   ShopeAddressId       int                  null,
   PersonName           nvarchar(80)         null,
   UserId               int                  null,
   constraint PK_SHOPEINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: ShoppingMallInfo                                      */
/*==============================================================*/
create table ShoppingMallInfo (
   Id                   int                  identity,
   ShoppingMallName     nvarchar(80)         null,
   ShortName            nvarchar(80)         null,
   Address              nvarchar(200)        null,
   LegalPerson          nvarchar(80)         null,
   OfficePhone          nvarchar(80)         null,
   constraint PK_SHOPPINGMALLINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: UserRoles                                             */
/*==============================================================*/
create table UserRoles (
   Id                   int                  identity,
   UserId               int                  null,
   RoleId               int                  null,
   constraint PK_USERROLES primary key (Id)
)
go

/*==============================================================*/
/* Table: Users                                                 */
/*==============================================================*/
create table Users (
   Id                   int                  identity,
   Username             nvarchar(80)         null,
   Password             nvarchar(80)         null,
   constraint PK_USERS primary key (Id)
)
go

/*==============================================================*/
/* Table: VipActivityInfo                                       */
/*==============================================================*/
create table VipActivityInfo (
   Id                   int                  identity,
   ActivityName         nvarchar(80)         null,
   BeginTime            datetime             null,
   EndTime              datetime             null,
   constraint PK_VIPACTIVITYINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: VipCarInfo                                            */
/*==============================================================*/
create table VipCarInfo (
   Id                   int                  identity,
   VipId                int                  null,
   CarNo                nvarchar(80)         null,
   constraint PK_VIPCARINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: VipCardInfo                                           */
/*==============================================================*/
create table VipCardInfo (
   Id                   int                  identity,
   CardName             nvarchar(80)         null,
   CardTypeId           int                  null,
   Remarks              nvarchar(800)        null,
   constraint PK_VIPCARDINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: VipCardReportLose                                     */
/*==============================================================*/
create table VipCardReportLose (
   Id                   int                  identity,
   VipCardId            int                  null,
   VipId                int                  null,
   constraint PK_VIPCARDREPORTLOSE primary key (Id)
)
go

/*==============================================================*/
/* Table: VipCardType                                           */
/*==============================================================*/
create table VipCardType (
   Id                   int                  identity,
   CardTypeName         nvarchar(80)         null,
   ShoppingMallId       int                  null,
   constraint PK_VIPCARDTYPE primary key (Id)
)
go

/*==============================================================*/
/* Table: VipGiftInfo                                           */
/*==============================================================*/
create table VipGiftInfo (
   Id                   int                  identity,
   GiftName             nvarchar(80)         null,
   NeedPoint            decimal(18,2)        null,
   ActivityId           int                  null,
   constraint PK_VIPGIFTINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: VipInfo                                               */
/*==============================================================*/
create table VipInfo (
   Id                   int                  identity,
   VipName              nvarchar(80)         null,
   Sex                  bit                  null,
   Birthday             date                 null,
   Telephone            nvarchar(80)         null,
   IDNum                nvarchar(80)         null,
   Address              nvarchar(200)        null,
   ShoppingMallId       int                  null,
   constraint PK_VIPINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: VipLevel                                              */
/*==============================================================*/
create table VipLevel (
   Id                   int                  identity,
   LevelName            nvarchar(80)         null,
   ShortName            nvarchar(80)         null,
   Condition            nvarchar(80)         null,
   constraint PK_VIPLEVEL primary key (Id)
)
go

/*==============================================================*/
/* Table: VipPointExchange                                      */
/*==============================================================*/
create table VipPointExchange (
   Id                   int                  identity,
   GiftId               int                  null,
   VipId                int                  null,
   VipActivityId        int                  null,
   constraint PK_VIPPOINTEXCHANGE primary key (Id)
)
go

/*==============================================================*/
/* Table: VipPointInfo                                          */
/*==============================================================*/
create table VipPointInfo (
   Id                   int                  identity,
   VipUserId            int                  null,
   VipPoint             numeric(18,2)        null,
   constraint PK_VIPPOINTINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: VipPointRule                                          */
/*==============================================================*/
create table VipPointRule (
   Id                   int                  identity,
   RuleName             nvarchar(80)         null,
   VipActiveId          int                  null,
   PointRule            decimal(18,4)        null,
   constraint PK_VIPPOINTRULE primary key (Id)
)
go

alter table ShopeInfo
   add constraint FK_SHOPEINFO_REFERENCE_6_USERS foreign key (Id)
      references Users (Id)
go

alter table UserRoles
   add constraint FK_USERROLES_REFERENCE_1_USERS foreign key (UserId)
      references Users (Id)
go

alter table UserRoles
   add constraint FK_USERROLES_REFERENCE_2_ROLES foreign key (RoleId)
      references Roles (Id)
go

alter table VipCarInfo
   add constraint FK_VIPCARINFO_REFERENCE_10_VIPINFO foreign key (Id)
      references VipInfo (Id)
go

alter table VipCardInfo
   add constraint FK_VIPCARDINFO_REFERENCE_13_VIPCARDTYPE foreign key (CardTypeId)
      references VipCardType (Id)
go

alter table VipCardReportLose
   add constraint FK_VIPCARDREPORTLOSE_REFERENCE_4_VIPINFO foreign key (Id)
      references VipInfo (Id)
go

alter table VipCardType
   add constraint FK_VIPCARDTYPE_REFERENCE_12_SHOPPINGMALLINFO foreign key (ShoppingMallId)
      references ShoppingMallInfo (Id)
go

alter table VipGiftInfo
   add constraint FK_VIPGIFTINFO_REFERENCE_3_VIPACTIVITYINFO foreign key (Id)
      references VipActivityInfo (Id)
go

alter table VipInfo
   add constraint FK_VIPINFO_REFERENCE_11_SHOPPINGMALLINFO foreign key (Id)
      references ShoppingMallInfo (Id)
go

alter table VipPointExchange
   add constraint FK_VIPPOINTEXCHANGE_REFERENCE_14_VIPACTIVITYINFO foreign key (VipActivityId)
      references VipActivityInfo (Id)
go

alter table VipPointExchange
   add constraint FK_VIPPOINTEXCHANGE_REFERENCE_8_VIPINFO foreign key (VipId)
      references VipInfo (Id)
go

alter table VipPointExchange
   add constraint FK_VIPPOINTEXCHANGE_REFERENCE_9_VIPGIFTINFO foreign key (GiftId)
      references VipGiftInfo (Id)
go

alter table VipPointInfo
   add constraint FK_VIPPOINTINFO_REFERENCE_7_VIPINFO foreign key (Id)
      references VipInfo (Id)
go

alter table VipPointRule
   add constraint FK_VIPPOINTRULE_REFERENCE_5_VIPACTIVITYINFO foreign key (Id)
      references VipActivityInfo (Id)
go
select * from Users
-- 登录

-- 根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败
select * from Users
where Username='admin' and Password='1223'
-- 更进一步，就是当前登录的用户可能已经被注销、禁用等情况，如何应对
-- 建一个当前登录的用户系统
create table Roleenter
(
	Id                   int                  identity,
   rolename             nvarchar(80)         null,
   rolepassword             nvarchar(80)         null,
   constraint PK_Roleenter primary key (Id)
)
select * from Roleenter
declare @rolename nvarchar(80),@username nvarchar(80),@xh int
select @rolename='admmi',@username='admin',@xh=0
while @xh<=10
	begin
		select @xh=@xh+1
		if @rolename!=@username
			begin
				print '当前登录的用户可能已经被注销、禁用等情况'
			end
		else
			begin
				print'登录成功'
			end
	end

-- 1.根据提供的用户名（这里假定用户名不能重复），查询出满足条件的记录
select * from Users where Username='user01'
-- 2.判断查询到的记录的状态（如之前提过的IsActived、IsDeleted等等，也可以给用户增加额外的状态字段），给出已经被注销、已经被禁用、已经被删除等提示

-- 3.如果这条记录没有被注销、禁用、删除的情况，最后判断密码是不是和查询到的这条记录匹配，是则登录成功，否则提示“用户或者密码错误”

-- 一种攻击手段 SQL注入
/*
declare @password nvarchar(80) = \'1' or 1=1\`

select * from Users
where (Username='admin' and Password='1') or 1=1

*/
-- 如果根据提供的用户名，找到的记录数不为零，则表明用户名是对的，可以继续往下判断；否则直接提示用户名或密码错误
select * from Users
declare @username nvarchar(50),@password nvarchar(50),@wz int,@state nvarchar(20),@count int
select @username='admin',@password='1223',@wz=(select charindex(' ',@username))
--charindex(想要找到的字符串,被查找的字符串，开始查找的位置，为空时默认从第一位开始查找。)
SELECT @count=count(*),@state=username from Users where username=@username and password=@password GROUP BY username
IF(@wz>0)
		begin
		print '账号或密码错误,请重新输入' 
		end	
else IF(@count>0)
		begin
			IF(@state='admin')
				begin
					print '登陆成功' 
				end
			IF(@state='user01')
				begin
					print '安全中心发现该账号存在异常行为,请联系客服查证解除' 
				end
			IF(@state='user02')
				begin
					print '该账号已被永久封禁' 
				end
			IF(@state='user03')
				begin
					print '您的账号已被冻结回收' 
				end
			IF(@state='user04')
				begin
					print '您的账号已注销,请重新注册' 
				end
		end

-- 注册

-- 用户层面 ，输入用户名，密码，重复密码

-- 注册单个用户
insert into Users(Username,Password)values('admin',1223)

-- 批量注册

insert into Users (Username,Password) values ('user 01','1234'),('user02','1235'),('user03','1236'),('user04','1237')

-- 注册通用逻辑（无关数据库）

-- 1. 判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走
select * from Roleenter
declare @count int,@rolename nvarchar(80),@username nvarchar(80),@rolepassword nvarchar(80)
--declare @tmpTable table (id int,Username nvarchar(80),Password nvarchar(80))
select @count=count(*) from Users where Username=@username
if(@count>0)
	begin
		if @rolename=@Username
		print'注册失败'
		end
else
	begin
		print'注册成功'
	end

/*
定义用户名变量=传输过来的用户名
根据用户名变量查询用户表中有没有同名的记录
如果有，则提示不能注册，返回消息
如果没有，则可以继续往下
*/

-- 2. 判断密码和重复密码是否一致，是则可以注册，并在相应数据表中插入一条记录，否则返回失败信息
declare @Password nvarchar(80)
select @rolepassword='123456',@Password='123456'
while @xh<=10
	begin
		select @xh=@xh+1
		if @rolepassword=@Password
			begin
				insert into Users(Username,Password)values(@Username,@Password)
				print '注册成功'
			end
		else
			begin
				print'注册失败'
			end
	end
-- 关于商场信息 的使用场景

-- 1.在会员信息管理的时候，需要选择商场
select Id,ShoppingMallName,ShortName from ShoppingMallInfo
-- 2.在会员卡类型管理的时候，需要选择商场
select Id,ShoppingMallName,ShortName from ShoppingMallInfo
-- 3. 商场信息本身的CRUD （create read update delete）
select * from ShoppingMallInfo
insert into ShoppingMallInfo(ShoppingMallName,ShortName,Address,LegalPerson,OfficePhone) values('万达商场','万达','福建福州','屿森',12233)
update ShoppingMallInfo set Address='江西南昌' where ShoppingMallName='万达商场'
delete from ShoppingMallInfo where ShoppingMallName='万达商场'
