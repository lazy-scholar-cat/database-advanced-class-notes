# 2021-10-06 天气：晴 心情：每个早课，每个一二节我都特别困...... 课程：关于标量函数和表值函数

## 回顾上节课的标量函数
```sql
    --自定义函数(标量函数)
--无参
create function fn_GetNum
(

)
returns int
as
begin
	declare @reslut int=3 --默认值
	return @reslut
end
go
select dbo.fn_GetNum()--调用

go

create function fn_GetParametersTwo()
returns decimal(18,4)
as
begin
	return convert(decimal(18,4),('18.2')) 
end
go
select dbo.fn_GetParametersTwo()
go
--有参
create function fn_GetParameter
(
	@num decimal(18,2)
)
returns decimal(18,2)
as
begin
	return @num
end
go

select dbo.fn_GetParameter('18.2')--会实现一个隐式转化从而可以执行

go

```

## 新认识表值函数

```sql
--自定义函数(表值函数)
--1.单语句表值函数 （内联表值函数）
--2.多语句表值函数（内联函数跟标量函数的结合体）

--单语句表值函数
--无参

alter function fn_T
()
returns table 
as
return
(
	select * from StudentInfo
)
go

select * from dbo.fn_T()

go

create function fn_TableStudentInfo()
returns table
as
return
(
		with CTE_A --采用多表查询,可以实现
as
(
	select * from StudentInfo
)select * from CTE_A
)
go

select * from dbo.fn_TableStudentInfo()--调用

go
--有参
create function fn_GetTable
(
	@code int
)
returns table 
as
return 
(
	select * from StudentInfo
	where @code=StudentCode
)
go
select * from dbo.fn_GetTable(03)
go

--多语句表值函数（预习）
create function fn_GetDoubleTable--名
(
	@id int--参数，可写可不写
) 
returns @StudentInfo table--返回一张表，给表一个名字
(
	StudentName nvarchar(80)--选择你需要的参数
)
as
begin
	insert into @StudentInfo(StudentName)--sql语句，先添加在查询
	select StudentName from StudentInfo
	where @id=Id
	return--返回这个表
end

go
select StudentName from dbo.fn_GetDoubleTable(2)--调用
```