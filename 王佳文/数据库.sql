use master
go

create database VIP_System
go

use vip_System
go

create table UserTable --用户表
(
	UserID nvarchar(50) not null,
	Password nvarchar(50) not null,
)
go

create table Market --商场信息表
(
	MID int primary key identity(1,1) not null, 
	MarkatName nvarchar(50) not null,
	MarkatAlsoName nvarchar(50) not null,
	MarketAddress nvarchar(200) not null,
	MarketManager nvarchar(10) not null,
	MarketPhone  nvarchar(50) not null,
)
go

create table Customer --会员信息表
(
	VID int primary key identity(1,1) not null,
	VName nvarchar(50) not null,
	VBirthday date not null,
	VPhone  nvarchar(50) not null,
	VAddress nvarchar(200) not null,
)
go


create table CardType --会员卡种类信息表
(
	TID int primary key identity(1,1) not null,
	TName nvarchar(10) not null,
	PS nvarchar(50) ,
)
go

create table Card --会员卡信息表
(
	CID int  identity(1,1) not null,
	CNum nvarchar(10) primary key not null,
	VID int references Customer(VID) not null,
	CDate date not null,
	CAddress nvarchar(200) not null,
	CTypeNum int references CardType(TID) not null,
)
go

create table Level  --会员等级信息表
(
	LID int primary key identity(1,1) not null,
	LNum nvarchar(10) not null,
	LName nvarchar(10) not null,
	PS nvarchar(50), 
)
go

create table Shop  --商铺信息表
(
	SID int primary key identity(1,1) not null,
	SName nvarchar(10) not null,
	SAddress nvarchar(200) not null,
	SNum nvarchar(10) not null,
	SManagerName nvarchar(10) not null,
	SBuinessNum nvarchar(10) not null,
)
go

create table Integral --积分信息表
(
	IID int primary key identity(1,1) not null,
	CNum nvarchar(10) references Card(CNum) not null,
	IntergralNum varchar(100)  not null,
)
go

create table VIPCustome --vip消费信息表
(
	VCID int  primary key identity(1,1) not null,
	VCNum int references Customer(VID) not null,
	VCShopID int references Shop(SID) not null,
	VCMoney money not null,
)
go

create table Vehicle --会员车辆信息表
(
	VeID int  primary key identity(1,1) not null,
	VID  int references Customer(VID) not null,
	VeNum nvarchar(20) not null,
)
go

select * from Market
insert into Market values('万宝购物广场','万宝','张三','龙岩大道','0595-55669900')

select * from Customer
insert into Customer values('张三','1982-05-15','18856568989','上海'),
('李四','1998-12-08','18756625251','北京')

update Customer set VName='赵六' where VID=1

select * from Shop

insert into Shop values('芙丽芳丝','2-1号','A01','王小虎','BN001')

update Shop set SNum='B02' where SID=1

select * from VIPCustome

insert into VIPCustome values(2,1,500.00)



--登陆

--用户登陆，根据数据库里面已有的信息来查询用户是否存在，是，则登陆；否，则需要注册用户

	select * from UserTable where UserID='admin' and Password='123'

--更进一步，就是当登陆的用户可能已被注销、禁用等情况，如何应对
	--1.先在数据表中删除被禁用、注销的用户名
	delete from UserTable where UserID='admin'
	--2.然后重新注册用户

--用户登录时如果忘记密码，实现找回密码
	--1.可以在数据表中，查找用户名，即可找到对应的密码
	select * from UserTable where UserID='admin'


--注册

--单个注册
	
	insert into UserTable values('admin','123')

--批量注册
	
	insert into UserTable values('admin','123'),('st','12345'),('admin','123'),('admin','123'),('admin','123')

--用户注册逻辑

--1.判断当前注册的用户名是否存在，如果存在则返回注册失败；反之，则注册成功
--可以使用unique语句来判断

	alter table UserTable add  constraint UQ_UserID unique(UserID)

--2.判断重复输入的密码是否一样，如果不一样，则重新输入；反之，注册成功，在数据表中添加一条数据

--关于商场信息的使用场景

--1.选择会员信息管理的场景

select MID,MarkatName,MarkatAlsoName form Market

-- 2.在会员卡类型管理的时候，需要选择商场

select MID,MarkatName,MarkatAlsoName form Market

--3.商场本身的create、read、update、delete

insert into Market values('万达购物广场','万达','李四','龙岩大道','0595-55669900')

update Market set MarketAddress='龙岩'

delete from Market where MID=2

--关于会员信息表的使用场景

--1.登记会员信息的时候

insert into Customer values('张三','1982-05-15','18856568989','上海')

--2.挂失时候，需要会员信息

select VCID from Customer

--3.会员信息的create、update、read、delete

insert into Customer values('赵七','1982-05-15','18856568989','上海')

update Customer set VName='王小虎' where VCID=1

delete from Customer where VName='赵七'

--关于会员卡的使用场景

--1.消费积分时，出示会员卡

select CID from Card








