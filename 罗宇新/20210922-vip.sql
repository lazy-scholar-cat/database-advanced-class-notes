use master
go

create table scxx
(
	scbh int primary key identity not null,
	scmc nvarchar(50) not null,
	scjj nvarchar(200) not null,
	sczcdz nvarchar(50) not null,
	sczcfr nvarchar(50) not null,
	bgdh nvarchar(50) not null
)
go
create table hyxx
(
	hybh int primary key identity not null,
	hyxm nvarchar(50) not null,
	xb nvarchar(20) not null,
	sjh nvarchar(50) not null,
	zz nvarchar(50) not null,
	blsj datetime not null,
	sfzh nvarchar(50) not null,
	csny date not null 
)
go
create table hykxx
(
	cardid int primary key identity not null,
	hykh nvarchar(50) not null,
	hyklx nvarchar(50) not null,
	bz nvarchar(100) not null
)
go
create table hykdjgz
(
	id int primary key identity not null,
	klb int foreign key (klb) references hykxx(cardid),
	xfmk nvarchar(50) not null
)
go
create table hykgsqk
(
	id int primary key identity not null,
	hybh int foreign key (hybh) references hyxx(hybh),
	cardid int foreign key (cardid) references hykxx(cardid),
	bbsj datetime not null,
	blqk nvarchar(50) not null,
	blgzyr nvarchar(50) not null
)
go
create table xfjl 
(
	id int primary key identity not null,
	xfjlbh int not null,
	spbh int not null,
	hyzk nvarchar(50) not null,
	sjfkje money not null,
	fkrq datetime not null
)
go
create table spxx
(
	spbh int primary key identity not null,
	spmc nvarchar(50) not null,
	spyj money not null,
	spyh nvarchar(50) not null,
	spsjrq datetime not null,
	spxjrq datetime not null
)
go
create table sdxxb
(
	sdbh int primary key identity not null,
	sdmc nvarchar(50) not null,
	sdfzr nvarchar(50) not null,
	fzrlxdh nvarchar(50) not null,
	sdyysj time not null,
	sdrzsj datetime not null,
	sdzldqsj datetime not null
)
go