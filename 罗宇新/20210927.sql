use master
go

create database VIP
go

use VIP
go

create table UserTable --用户表
(
	UserID nvarchar(50) not null,
	Password nvarchar(50) not null
)
go

create table ShopCenter --商场信息表
(
	ShopCID int primary key identity(1,1) not null, 
	ShopCName nvarchar(50) not null,
	ShopCAlsoName nvarchar(50) not null,
	ShopCAddress nvarchar(200) not null,
	ShopCManager nvarchar(10) not null,
	ShopCPhone  nvarchar(50) not null
)
go

create table Member --会员信息表
(
	MID int primary key identity(1,1) not null,
	MName nvarchar(50) not null,
	MBirthday date not null,
	MPhone  nvarchar(50) not null,
	MAddress nvarchar(200) not null
)
go


create table CardType --会员卡种类信息表
(
	TID int primary key identity(1,1) not null,
	TName nvarchar(10) not null,
	PS nvarchar(50)
)
go

create table Card --会员卡信息表
(
	CID int  identity(1,1) not null,
	CNum nvarchar(10) primary key not null,
	VID int references Member(MID) not null,
	CDate date not null,
	CAddress nvarchar(200) not null,
	CTypeNum int references CardType(TID) not null
)
go

create table Level  --会员等级信息表
(
	LID int primary key identity(1,1) not null,
	LNum nvarchar(10) not null,
	LName nvarchar(10) not null,
	PS nvarchar(50)
)
go

create table Shop  --商铺信息表
(
	SID int primary key identity(1,1) not null,
	SName nvarchar(10) not null,
	SAddress nvarchar(200) not null,
	SNum nvarchar(10) not null,
	SManagerName nvarchar(10) not null,
	SBuinessNum nvarchar(10) not null
)
go

create table Integral --积分信息表
(
	IID int primary key identity(1,1) not null,
	CNum nvarchar(10) references Card(CNum) not null,
	IntergralNum varchar(100)  not null
)
go

create table VIPCustome --vip消费信息表
(
	VCID int  primary key identity(1,1) not null,
	VCNum int references Member(MID) not null,
	VCShopID int references Shop(SID) not null,
	VCMoney money not null
)
go

create table Vehicle --会员车辆信息表
(
	VeID int  primary key identity(1,1) not null,
	VID  int references Member(MID) not null,
	VeNum nvarchar(20) not null,
)
go

SELECT * FROM ShopCenter
insert into ShopCenter values('万宝广场','万宝','22','龙岩大道','123456')

select * from Member
insert into Member values('陈旭','2001-01-01','12341313131','福州'),
('宁夏记者gyq','2001-01-02','12341313131','宁夏')


-- 登录

-- 根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败

select * from UserTable where UserID='admin' and Password='123'

-- 更进一步，就是当前登录的用户可能已经被注销、禁用等情况，如何应对
--1.登陆
--2.验证账号，有没有注销，禁用，用语句查询，
--3.跳转回去登陆，没有账号就继续跳转注册


-- 注册

-- 用户层面 ，输入用户名，密码，重复密码

-- 注册单个用户
insert into UserTable values('admin','123')


-- 批量注册

insert into UserTable values('admin','123'),('st','12345'),('admin','123'),('admin','123'),('admin','123')

-- 注册通用逻辑（无关数据库）

-- 1. 判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走


-- 2. 判断密码和重复密码是否一致，是则可以注册，并在相应数据表中插入一条记录，否则返回失败信息

