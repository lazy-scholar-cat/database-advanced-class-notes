create table aa
(
	name nvarchar(80),
	sex nvarchar(80),
	addres nvarchar(80),
)
go
insert into aa values ('11','22','33')
go

create proc proc_aa --创建
@name nvarchar(80) output
as
begin
	select @name
	select @name=name from aa
	select @name
end
go

alter proc proc_aa --修改
@name nvarchar(80) output
as
begin
	select @name
	select @name=name from aa
	select @name
end
go

declare @name nvarchar(80) = 'aa'
exec proc_aa @name output
select @name
go

drop proc proc_aa --删除
go

create proc proc_bb	--多返回值
@name nvarchar(80) output,
@sex nvarchar(80) output
as
begin
	select @name
	select @sex
end
go
declare @name nvarchar(80),@sex nvarchar(80)
exec @name output,@sex output
select @name,@sex
go


create function fun_aa (@name  nvarchar(80)) --自定义函数
returns nvarchar(80)
as
begin
return @name
end
go

select dbo.fun_aa('22')
--注意：1.所有的入参前都必须加@

--2.create后的返回，单词是returns，而不是return

--3.returns后面的跟的不是变量，而是返回值的类型，如：int，char等。

--4.在begin/end语句块中，是return。

go


create function fun_bb(@name nvarchar(80))
returns table
as
return (select * from aa)
go







