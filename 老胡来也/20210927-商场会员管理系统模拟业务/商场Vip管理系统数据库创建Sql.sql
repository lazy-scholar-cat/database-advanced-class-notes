create database ShoppingMallDatabase
go
use ShoppingMallDatabase
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ShopeInfo') and o.name = 'FK_SHOPEINFO_REFERENCE_6_USERS')
alter table ShopeInfo
   drop constraint FK_SHOPEINFO_REFERENCE_6_USERS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('UserRoles') and o.name = 'FK_USERROLES_REFERENCE_1_USERS')
alter table UserRoles
   drop constraint FK_USERROLES_REFERENCE_1_USERS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('UserRoles') and o.name = 'FK_USERROLES_REFERENCE_2_ROLES')
alter table UserRoles
   drop constraint FK_USERROLES_REFERENCE_2_ROLES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipCarInfo') and o.name = 'FK_VIPCARINFO_REFERENCE_10_VIPINFO')
alter table VipCarInfo
   drop constraint FK_VIPCARINFO_REFERENCE_10_VIPINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipCardInfo') and o.name = 'FK_VIPCARDINFO_REFERENCE_13_VIPCARDTYPE')
alter table VipCardInfo
   drop constraint FK_VIPCARDINFO_REFERENCE_13_VIPCARDTYPE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipCardReportLose') and o.name = 'FK_VIPCARDREPORTLOSE_REFERENCE_4_VIPINFO')
alter table VipCardReportLose
   drop constraint FK_VIPCARDREPORTLOSE_REFERENCE_4_VIPINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipCardType') and o.name = 'FK_VIPCARDTYPE_REFERENCE_12_SHOPPINGMALLINFO')
alter table VipCardType
   drop constraint FK_VIPCARDTYPE_REFERENCE_12_SHOPPINGMALLINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipGiftInfo') and o.name = 'FK_VIPGIFTINFO_REFERENCE_3_VIPACTIVITYINFO')
alter table VipGiftInfo
   drop constraint FK_VIPGIFTINFO_REFERENCE_3_VIPACTIVITYINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipInfo') and o.name = 'FK_VIPINFO_REFERENCE_11_SHOPPINGMALLINFO')
alter table VipInfo
   drop constraint FK_VIPINFO_REFERENCE_11_SHOPPINGMALLINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipPointExchange') and o.name = 'FK_VIPPOINTEXCHANGE_REFERENCE_14_VIPACTIVITYINFO')
alter table VipPointExchange
   drop constraint FK_VIPPOINTEXCHANGE_REFERENCE_14_VIPACTIVITYINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipPointExchange') and o.name = 'FK_VIPPOINTEXCHANGE_REFERENCE_8_VIPINFO')
alter table VipPointExchange
   drop constraint FK_VIPPOINTEXCHANGE_REFERENCE_8_VIPINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipPointExchange') and o.name = 'FK_VIPPOINTEXCHANGE_REFERENCE_9_VIPGIFTINFO')
alter table VipPointExchange
   drop constraint FK_VIPPOINTEXCHANGE_REFERENCE_9_VIPGIFTINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipPointInfo') and o.name = 'FK_VIPPOINTINFO_REFERENCE_7_VIPINFO')
alter table VipPointInfo
   drop constraint FK_VIPPOINTINFO_REFERENCE_7_VIPINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipPointRule') and o.name = 'FK_VIPPOINTRULE_REFERENCE_5_VIPACTIVITYINFO')
alter table VipPointRule
   drop constraint FK_VIPPOINTRULE_REFERENCE_5_VIPACTIVITYINFO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ProductsInfo')
            and   type = 'U')
   drop table ProductsInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Roles')
            and   type = 'U')
   drop table Roles
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ShopeInfo')
            and   type = 'U')
   drop table ShopeInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ShoppingMallInfo')
            and   type = 'U')
   drop table ShoppingMallInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('UserRoles')
            and   type = 'U')
   drop table UserRoles
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Users')
            and   type = 'U')
   drop table Users
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipActivityInfo')
            and   type = 'U')
   drop table VipActivityInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipCarInfo')
            and   type = 'U')
   drop table VipCarInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipCardInfo')
            and   type = 'U')
   drop table VipCardInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipCardReportLose')
            and   type = 'U')
   drop table VipCardReportLose
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipCardType')
            and   type = 'U')
   drop table VipCardType
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipGiftInfo')
            and   type = 'U')
   drop table VipGiftInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipInfo')
            and   type = 'U')
   drop table VipInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipLevel')
            and   type = 'U')
   drop table VipLevel
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipPointExchange')
            and   type = 'U')
   drop table VipPointExchange
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipPointInfo')
            and   type = 'U')
   drop table VipPointInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipPointRule')
            and   type = 'U')
   drop table VipPointRule
go

/*==============================================================*/
/* Table: ProductsInfo                                          */
/*==============================================================*/
create table ProductsInfo (
   Id                   int                  identity,
   ProductName          nvarchar(80)         null,
   Price                decimal(18,2)        null,
   Stock                int                  null,
   constraint PK_PRODUCTSINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: Roles                                                 */
/*==============================================================*/
create table Roles (
   Id                   int                  identity,
   RoleName             nvarchar(80)         null,
   constraint PK_ROLES primary key (Id)
)
go

/*==============================================================*/
/* Table: ShopeInfo                                             */
/*==============================================================*/
create table ShopeInfo (
   Id                   int                  identity,
   ShopeName            nvarchar(80)         null,
   ShopeAddress         nvarchar(800)        null,
   ShopeAddressId       int                  null,
   PersonName           nvarchar(80)         null,
   UserId               int                  null,
   constraint PK_SHOPEINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: ShoppingMallInfo                                      */
/*==============================================================*/
create table ShoppingMallInfo (
   Id                   int                  identity,
   ShoppingMallName     nvarchar(80)         null,
   ShortName            nvarchar(80)         null,
   Address              nvarchar(200)        null,
   LegalPerson          nvarchar(80)         null,
   OfficePhone          nvarchar(80)         null,
   constraint PK_SHOPPINGMALLINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: UserRoles                                             */
/*==============================================================*/
create table UserRoles (
   Id                   int                  identity,
   UserId               int                  null,
   RoleId               int                  null,
   constraint PK_USERROLES primary key (Id)
)
go

/*==============================================================*/
/* Table: Users                                                 */
/*==============================================================*/
create table Users (
   Id                   int                  identity,
   Username             nvarchar(80)         null,
   Password             nvarchar(80)         null,
   constraint PK_USERS primary key (Id)
)
go

/*==============================================================*/
/* Table: VipActivityInfo                                       */
/*==============================================================*/
create table VipActivityInfo (
   Id                   int                  identity,
   ActivityName         nvarchar(80)         null,
   BeginTime            datetime             null,
   EndTime              datetime             null,
   constraint PK_VIPACTIVITYINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: VipCarInfo                                            */
/*==============================================================*/
create table VipCarInfo (
   Id                   int                  identity,
   VipId                int                  null,
   CarNo                nvarchar(80)         null,
   constraint PK_VIPCARINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: VipCardInfo                                           */
/*==============================================================*/
create table VipCardInfo (
   Id                   int                  identity,
   CardName             nvarchar(80)         null,
   CardTypeId           int                  null,
   Remarks              nvarchar(800)        null,
   constraint PK_VIPCARDINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: VipCardReportLose                                     */
/*==============================================================*/
create table VipCardReportLose (
   Id                   int                  identity,
   VipCardId            int                  null,
   VipId                int                  null,
   constraint PK_VIPCARDREPORTLOSE primary key (Id)
)
go

/*==============================================================*/
/* Table: VipCardType                                           */
/*==============================================================*/
create table VipCardType (
   Id                   int                  identity,
   CardTypeName         nvarchar(80)         null,
   ShoppingMallId       int                  null,
   constraint PK_VIPCARDTYPE primary key (Id)
)
go

/*==============================================================*/
/* Table: VipGiftInfo                                           */
/*==============================================================*/
create table VipGiftInfo (
   Id                   int                  identity,
   GiftName             nvarchar(80)         null,
   NeedPoint            decimal(18,2)        null,
   ActivityId           int                  null,
   constraint PK_VIPGIFTINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: VipInfo                                               */
/*==============================================================*/
create table VipInfo (
   Id                   int                  identity,
   VipName              nvarchar(80)         null,
   Sex                  bit                  null,
   Birthday             date                 null,
   Telephone            nvarchar(80)         null,
   IDNum                nvarchar(80)         null,
   Address              nvarchar(200)        null,
   ShoppingMallId       int                  null,
   constraint PK_VIPINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: VipLevel                                              */
/*==============================================================*/
create table VipLevel (
   Id                   int                  identity,
   LevelName            nvarchar(80)         null,
   ShortName            nvarchar(80)         null,
   Condition            nvarchar(80)         null,
   constraint PK_VIPLEVEL primary key (Id)
)
go

/*==============================================================*/
/* Table: VipPointExchange                                      */
/*==============================================================*/
create table VipPointExchange (
   Id                   int                  identity,
   GiftId               int                  null,
   VipId                int                  null,
   VipActivityId        int                  null,
   constraint PK_VIPPOINTEXCHANGE primary key (Id)
)
go

/*==============================================================*/
/* Table: VipPointInfo                                          */
/*==============================================================*/
create table VipPointInfo (
   Id                   int                  identity,
   VipUserId            int                  null,
   VipPoint             numeric(18,2)        null,
   constraint PK_VIPPOINTINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: VipPointRule                                          */
/*==============================================================*/
create table VipPointRule (
   Id                   int                  identity,
   RuleName             nvarchar(80)         null,
   VipActiveId          int                  null,
   PointRule            decimal(18,4)        null,
   constraint PK_VIPPOINTRULE primary key (Id)
)
go

alter table ShopeInfo
   add constraint FK_SHOPEINFO_REFERENCE_6_USERS foreign key (Id)
      references Users (Id)
go

alter table UserRoles
   add constraint FK_USERROLES_REFERENCE_1_USERS foreign key (UserId)
      references Users (Id)
go

alter table UserRoles
   add constraint FK_USERROLES_REFERENCE_2_ROLES foreign key (RoleId)
      references Roles (Id)
go

alter table VipCarInfo
   add constraint FK_VIPCARINFO_REFERENCE_10_VIPINFO foreign key (Id)
      references VipInfo (Id)
go

alter table VipCardInfo
   add constraint FK_VIPCARDINFO_REFERENCE_13_VIPCARDTYPE foreign key (CardTypeId)
      references VipCardType (Id)
go

alter table VipCardReportLose
   add constraint FK_VIPCARDREPORTLOSE_REFERENCE_4_VIPINFO foreign key (Id)
      references VipInfo (Id)
go

alter table VipCardType
   add constraint FK_VIPCARDTYPE_REFERENCE_12_SHOPPINGMALLINFO foreign key (ShoppingMallId)
      references ShoppingMallInfo (Id)
go

alter table VipGiftInfo
   add constraint FK_VIPGIFTINFO_REFERENCE_3_VIPACTIVITYINFO foreign key (Id)
      references VipActivityInfo (Id)
go

alter table VipInfo
   add constraint FK_VIPINFO_REFERENCE_11_SHOPPINGMALLINFO foreign key (Id)
      references ShoppingMallInfo (Id)
go

alter table VipPointExchange
   add constraint FK_VIPPOINTEXCHANGE_REFERENCE_14_VIPACTIVITYINFO foreign key (VipActivityId)
      references VipActivityInfo (Id)
go

alter table VipPointExchange
   add constraint FK_VIPPOINTEXCHANGE_REFERENCE_8_VIPINFO foreign key (VipId)
      references VipInfo (Id)
go

alter table VipPointExchange
   add constraint FK_VIPPOINTEXCHANGE_REFERENCE_9_VIPGIFTINFO foreign key (GiftId)
      references VipGiftInfo (Id)
go

alter table VipPointInfo
   add constraint FK_VIPPOINTINFO_REFERENCE_7_VIPINFO foreign key (Id)
      references VipInfo (Id)
go

alter table VipPointRule
   add constraint FK_VIPPOINTRULE_REFERENCE_5_VIPACTIVITYINFO foreign key (Id)
      references VipActivityInfo (Id)
go
