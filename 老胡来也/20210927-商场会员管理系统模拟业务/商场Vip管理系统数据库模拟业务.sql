-- 登录

-- 根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败

select * from Users
where Username='admin' and Password='113'

-- 更进一步，就是当前登录的用户可能已经被注销、禁用等情况，如何应对





-- 注册

-- 用户层面 ，输入用户名，密码，重复密码

-- 注册单个用户
insert into Users (Username,Password) values ('admin','113')


-- 批量注册

insert into Users (Username,Password) values ('user01','113'),('user02','113'),('user03','113'),('user04','113')

-- 注册通用逻辑（无关数据库）

-- 1. 判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走


-- 2. 判断密码和重复密码是否一致，是则可以注册，并在相应数据表中插入一条记录，否则返回失败信息


