# CTE应用及触发器

```sql



;with CTE_A
as
(
	select Id,Code,AreaName,ParentCode from Area where AreaName='浙江'
	union all
	select a.Id,a.Code,a.AreaName,a.ParentCode from Area a ,CTE_A c
	where a.ParentCode=c.Code
)select * from CTE_A

go

alter function fn_AreaInfoSelectByName(@name nvarchar(80),@level int=0)
returns table
as
return 
(
	with CTE_A
	as
	(
		select Id,Code,AreaName,ParentCode,0 Level from Area where AreaName=@name
		union all
		select a.Id,a.Code,a.AreaName,a.ParentCode,c.Level+1 pp from Area a ,CTE_A c
		where a.ParentCode=c.Code
	)select * from CTE_A where Level<=@level
)

go


select * from dbo.fn_AreaInfoSelectByName('浙江',20)



-- 触发器 是一种特殊存储过程 它不由用户直接手动执行，而是当一些事件发生后，自动被调用

-- 作用 1、维护数据的有效性和完整性 2、简化逻辑

/*
语法：

create trigger <触发器名称>
on <数据表>
<after | instead of> <delete | insert | update>
as
begin

end

*/

go

alter trigger tr_TmpRankForUpdate
on tmpRank
after
 Update 
as
begin
	declare @score int,@id int
	select @id=id, @score=Score from inserted
	if (@score>=0 and @score<=100)
		begin
			update tmpRank set Score=@score where Id=@id
		end
	else
		begin
			update tmpRank set Score=0 where Id=@id
		end
end

go


select *from tmpRank

update tmpRank set Score=-2 where Id=1



/*
	存储过程当中特有的两张临时表：

	inserted:插入数据时，新插入的数据在inserted表中；更新的时候，更新后的数据在inserted中；
	deleted:更新的时候，deletd表中的数据是之前的旧的数据；在删除的时候，deleted中的数据是被删除的数据；

*/
```