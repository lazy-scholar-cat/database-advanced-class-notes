# 游标的定义与使用

```sql


-- 游标 是在数据中当中为，遍历数据集的一种方式或手段，可以反游标当成一种指针


/*
当用户积分100时，更新会员等级为一级会员
当用户积分为200-400时，更新为二级会员
当用户积分为500-800时，更新为三级会员




*/
-- 定义游标
declare  cur_ForPersonalInfo2 cursor
for 
select * from PersonalInfo

-- 打开游标，准备使用
open cur_ForPersonalInfo2

-- 定义变量，用于接收每一行中的数据
declare @id int,@name nvarchar(80) ,@birthday date

-- 先获取一条，以使@@fetch_status状态初始化
fetch next from cur_ForPersonalInfo2
into @id,@name,@birthday

select @@FETCH_STATUS

-- 真正遍历记录的语句结构
while @@Fetch_Status = 0
begin
	select @id,@name,@birthday -- 代表处理取出的每一行数据
	fetch next from cur_ForPersonalInfo2
	into @id,@name,@birthday
end

-- 关闭游标
close cur_ForPersonalInfo2
-- 释放游标
deallocate cur_ForPersonalInfo2



```