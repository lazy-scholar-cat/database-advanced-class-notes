/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     2021/9/22 19:29:48                           */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipCardInfo') and o.name = 'FK_VIPCARDI_REFERENCE_VIPINFO')
alter table VipCardInfo
   drop constraint FK_VIPCARDI_REFERENCE_VIPINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipCardTypeInfo') and o.name = 'FK_VIPCARDT_REFERENCE_VIPCARDI')
alter table VipCardTypeInfo
   drop constraint FK_VIPCARDT_REFERENCE_VIPCARDI
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MarketInfo')
            and   type = 'U')
   drop table MarketInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ShopInfo')
            and   type = 'U')
   drop table ShopInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipCardInfo')
            and   type = 'U')
   drop table VipCardInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipCardTypeInfo')
            and   type = 'U')
   drop table VipCardTypeInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipInfo')
            and   type = 'U')
   drop table VipInfo
go

/*==============================================================*/
/* Table: MarketInfo                                            */
/*==============================================================*/
create table MarketInfo (
   MarNumber            int                  not null,
   MarName              nvarchar(80)         null,
   MarSmil              nvarchar(80)         null,
   Maraddress           nvarchar(80)         null,
   MarPeople            nvarchar(80)         null,
   MarPhone             nvarchar(80)         null,
   constraint PK_MARKETINFO primary key (MarNumber)
)
go

/*==============================================================*/
/* Table: ShopInfo                                              */
/*==============================================================*/
create table ShopInfo (
   ShopNumber           int                  not null,
   ShopName             nvarchar(80)         null,
   ShopID               nvarchar(80)         null,
   ShopAdd              nvarchar(80)         null,
   SpName               nvarchar(80)         null,
   SpPhone              nvarchar(80)         null,
   constraint PK_SHOPINFO primary key (ShopNumber)
)
go

/*==============================================================*/
/* Table: VipCardInfo                                           */
/*==============================================================*/
create table VipCardInfo (
   VipCardNumber        int                  not null,
   VipCardID            int                  null,
   TypeID               int                  null,
   MarNumber            int                  null,
   VipNumber            int                  null,
   constraint PK_VIPCARDINFO primary key (VipCardNumber)
)
go

/*==============================================================*/
/* Table: VipCardTypeInfo                                       */
/*==============================================================*/
create table VipCardTypeInfo (
   VipCardNumber        int                  null,
   TypeID               char(10)             null,
   VipCardID            char(10)             null,
   VipCardType          char(10)             null,
   VipNumber            char(10)             null
)
go

/*==============================================================*/
/* Table: VipInfo                                               */
/*==============================================================*/
create table VipInfo (
   VipNumber            int                  not null,
   Name                 nvarchar(80)         null,
   Sex                  nvarchar(80)         null,
   Birthday             nvarchar(80)         null,
   telephone            nvarchar(80)         null,
   IdNumber             nvarchar(80)         null,
   Address              nvarchar(80)         null,
   constraint PK_VIPINFO primary key (VipNumber)
)
go

alter table VipCardInfo
   add constraint FK_VIPCARDI_REFERENCE_VIPINFO foreign key (VipNumber)
      references VipInfo (VipNumber)
go

alter table VipCardTypeInfo
   add constraint FK_VIPCARDT_REFERENCE_VIPCARDI foreign key (VipCardNumber)
      references VipCardInfo (VipCardNumber)
go
/*==============================================================*/
/* Table: UserInfo                   用户信息表                          */
/*==============================================================*/
create table UserInfo
(
	Id int not null identity primary key,
	Name nvarchar(10) not null unique,
	Password  nvarchar(10) not null ,
	isDeleted  nvarchar(80) not null ,
	IsActived  nvarchar(80) not null 
)
--批量注册
insert into UserInfo (Name,Password)
values
('admin01','124'),('admin02','125'),('admin03','126');

--登录

--根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败
select * from UserInfo
where Name='admin' and Password='123'

--更进一步，就是当前登录的用户可能已经被注销，禁用等情况，如何应对

delete from UserInfo where Name = 'admin02'


--判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走
declare @username nvarchar(10)
declare @tmpTable1 table (id int,Username nvarchar(80),Password nvarchar(80))
declare @count1 int 

insert into @tmpTable1
select @count1 = count(*) from UserInfo where Name=@username



declare @name nvarchar(80),@password nvarchar(80),@rowPassword nvarchar(80)

declare @isDeleted bit ,@isActived bit

declare @count int

declare @res nvarchar(80)

select @count=COUNT(*),@isDeleted=IsDeleted,@isActived=IsActived,@rowPassword=Password from UserInfo 

where name=@name 

    if(CHARINDEX(' ',@name) > 0 or CHARINDEX(' ',@Password) > 0)

begin
    set @res='用户名或密码出现空格，登陆不成功'
end
    else
            if(@count>0)
        begin
            if(@isDeleted=1)--判断数据是否被删除，0为否，1为是
                begin
                    set @res='该用户已被删除'
                end
                    else
                        begin
                            if(@isActived=1)--判断数据是否被禁用 0为否，1为是
                                begin
                                    set @res='该账户已被禁用,请联系管理员'
                                end
                                    else
                                        begin
                                            if(@Password=@rowPassword)--如果传进来的密码相同则登陆成功，反之则登陆失败
                                                begin
                                                    set  @res='登陆成功'
                                                end
                                                    else
                                                        begin
                                                            set  @res='登陆失败'
                                                        end
                                        end
                        end
        end


