/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     2021/9/22 19:29:48                           */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipCardInfo') and o.name = 'FK_VIPCARDI_REFERENCE_VIPINFO')
alter table VipCardInfo
   drop constraint FK_VIPCARDI_REFERENCE_VIPINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('VipCardTypeInfo') and o.name = 'FK_VIPCARDT_REFERENCE_VIPCARDI')
alter table VipCardTypeInfo
   drop constraint FK_VIPCARDT_REFERENCE_VIPCARDI
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MarketInfo')
            and   type = 'U')
   drop table MarketInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ShopInfo')
            and   type = 'U')
   drop table ShopInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipCardInfo')
            and   type = 'U')
   drop table VipCardInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipCardTypeInfo')
            and   type = 'U')
   drop table VipCardTypeInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('VipInfo')
            and   type = 'U')
   drop table VipInfo
go

/*==============================================================*/
/* Table: MarketInfo                                            */
/*==============================================================*/
create table MarketInfo (
   MarNumber            int                  not null,
   MarName              nvarchar(80)         null,
   MarSmil              nvarchar(80)         null,
   Maraddress           nvarchar(80)         null,
   MarPeople            nvarchar(80)         null,
   MarPhone             nvarchar(80)         null,
   constraint PK_MARKETINFO primary key (MarNumber)
)
go

/*==============================================================*/
/* Table: ShopInfo                                              */
/*==============================================================*/
create table ShopInfo (
   ShopNumber           int                  not null,
   ShopName             nvarchar(80)         null,
   ShopID               nvarchar(80)         null,
   ShopAdd              nvarchar(80)         null,
   SpName               nvarchar(80)         null,
   SpPhone              nvarchar(80)         null,
   constraint PK_SHOPINFO primary key (ShopNumber)
)
go

/*==============================================================*/
/* Table: VipCardInfo                                           */
/*==============================================================*/
create table VipCardInfo (
   VipCardNumber        int                  not null,
   VipCardID            int                  null,
   TypeID               int                  null,
   MarNumber            int                  null,
   VipNumber            int                  null,
   constraint PK_VIPCARDINFO primary key (VipCardNumber)
)
go

/*==============================================================*/
/* Table: VipCardTypeInfo                                       */
/*==============================================================*/
create table VipCardTypeInfo (
   VipCardNumber        int                  null,
   TypeID               char(10)             null,
   VipCardID            char(10)             null,
   VipCardType          char(10)             null,
   VipNumber            char(10)             null
)
go

/*==============================================================*/
/* Table: VipInfo                                               */
/*==============================================================*/
create table VipInfo (
   VipNumber            int                  not null,
   Name                 nvarchar(80)         null,
   Sex                  nvarchar(80)         null,
   Birthday             nvarchar(80)         null,
   telephone            nvarchar(80)         null,
   IdNumber             nvarchar(80)         null,
   Address              nvarchar(80)         null,
   constraint PK_VIPINFO primary key (VipNumber)
)
go

alter table VipCardInfo
   add constraint FK_VIPCARDI_REFERENCE_VIPINFO foreign key (VipNumber)
      references VipInfo (VipNumber)
go

alter table VipCardTypeInfo
   add constraint FK_VIPCARDT_REFERENCE_VIPCARDI foreign key (VipCardNumber)
      references VipCardInfo (VipCardNumber)
go

