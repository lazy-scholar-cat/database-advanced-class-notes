use master
go
create database VIP
go
use VIP
go
--1.商场信息
create table MallInfo
(
Mallnumber int,
Mallname nvarchar(20),
Mallabbreviation nvarchar(20),
Malldizhi nvarchar(20),
Mallfdr nvarchar(20),
Phone nvarchar(20)
)
--2.会员信息
create table MemberInfo
(
Membernumber int,
Membername nvarchar(20),
Membersex nvarchar(20),
DateOfBirth nvarchar(20),
Phone nvarchar(20),
IDnumber nvarchar(20),
Address nvarchar(20)
)
--3.会员卡信息
create table MCInfo
(
MCNumber int,
Membernumber int,
MCTnumber nvarchar(20),
CIO nvarchar(20),
)
--4.会员卡类型信息
create table MCTInfo
(
MCNumber int,
TOM nvarchar(20),
Industry nvarchar(20),
MembershipLevel nvarchar(20),
GongNeng nvarchar(20),
Issuer nvarchar(20),
SMT nvarchar(20),
UseAuthorization nvarchar(20)
)
--5.商铺信息
create table ShopInfo
(
Shopnumber int,
Shopname nvarchar(20),
LegalRepresentative nvarchar(20),
Name nvarchar(20),
Type nvarchar(20),
OperatingVarieties nvarchar(20)
)
--6.会员积分规则信息
create table MPRInfo
(
MCNumber int,
Content nvarchar(20)
)
--7.会员积分信息
create table MPInfo
(
MCNumber int,
ResidualIntegral nvarchar(20),
ExpirationTime nvarchar(20)
)
--8.会员等级信息
create table MLInfo
(
MCNumber int,
MembershipLevel nvarchar(20),
AcquisitionConditions nvarchar(20),
GrowthValue nvarchar(20)
)
--9.会员活动信息
create table MAInfo
(
ItemNumber int,
OriginalPrice nvarchar(20),
DiscountedPrice nvarchar(20),
RemainingTime nvarchar(20)
)
--10.会员车辆信息
create table MVInfo
(
Membernumber int,
LPNumber nvarchar(20),
VehicleType nvarchar(20),
ParkingTime nvarchar(20),
)
--11.会员卡挂失记录信息
create table MCLRInfo
(
MCNumber int,
MCName nvarchar(20),
IDnumber nvarchar(20),
Phone nvarchar(20),
ProcessingTime nvarchar(20),
Type nvarchar(20),
LRtime nvarchar(20),
Remarks nvarchar(20)
)
--12.会员积分兑换
create table MPE
(
TradeNumber int,
TradeName nvarchar(20),
ExchangeQuantity nvarchar(20),
UseIntegral nvarchar(20),
ResidualIntegral nvarchar(20)
)
--13.商品信息（商品信息出现在此，只是表明Vip系统里面因为一些业务需要商品信息，并不意味着商品信息表的设计应该在Vip系统中进行设计）
create table CommodityInfo
(
TradeNumber int,
TradeName nvarchar(20),
CommodityPrice nvarchar(20),
DOM nvarchar(20),
QGP nvarchar(20)
)
--14.用户信息
create table UserInfo
(
UserNumber int,
UserName nvarchar(20),
UserSex nvarchar(20),
DateOfBirth nvarchar(20),
Phone nvarchar(20),
IDnumber nvarchar(20),
Address nvarchar(20)
)