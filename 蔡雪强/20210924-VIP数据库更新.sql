use master
go
create database VIP
go
use VIP
go
--1.商场信息
create table MallInfo
(
Mallnumber int primary key not null,
Mallname nvarchar(20) not null,
Mallabbreviation nvarchar(20) not null,
Malldizhi nvarchar(20) not null,
Mallfdr nvarchar(20) not null,
Phone nvarchar(20) not null
)
--2.会员信息
create table MemberInfo
(
Membernumber int primary key not null,
Membername nvarchar(20) not null,
Membersex nvarchar(20) not null,
DateOfBirth nvarchar(20) not null,
Phone nvarchar(20) not null,
IDnumber nvarchar(20) not null,
Address nvarchar(20) not null
)
--3.会员卡信息
create table MCInfo
(
MCNumber int primary key not null,
Membernumber int not null,
MCTnumber nvarchar(20) not null,
CIO nvarchar(20) not null,
)
--4.会员卡类型信息
create table MCTInfo
(
MCNumber int primary key not null,
TOM nvarchar(20) not null,
Industry nvarchar(20) not null,
MembershipLevel nvarchar(20) not null,
GongNeng nvarchar(20) not null,
Issuer nvarchar(20) not null,
SMT nvarchar(20) not null,
UseAuthorization nvarchar(20) not null
)
--5.商铺信息
create table ShopInfo
(
Shopnumber int primary key not null,
Shopname nvarchar(20) not null,
LegalRepresentative nvarchar(20) not null,
Name nvarchar(20) not null,
Type nvarchar(20) not null,
OperatingVarieties nvarchar(20) not null
)
--6.会员积分规则信息
create table MPRInfo
(
MCNumber int primary key not null,
Content nvarchar(20) not null
)
--7.会员积分信息
create table MPInfo
(
MCNumber int primary key not null,
ResidualIntegral nvarchar(20) not null,
ExpirationTime nvarchar(20) not null
)
--8.会员等级信息
create table MLInfo
(
MCNumber int primary key not null,
MembershipLevel nvarchar(20) not null,
AcquisitionConditions nvarchar(20) not null,
GrowthValue nvarchar(20) not null
)
--9.会员活动信息
create table MAInfo
(
ItemNumber int primary key not null,
OriginalPrice nvarchar(20) not null,
DiscountedPrice nvarchar(20) not null,
RemainingTime nvarchar(20) not null
)
--10.会员车辆信息
create table MVInfo
(
Membernumber int primary key not null,
LPNumber nvarchar(20) not null,
VehicleType nvarchar(20) not null,
ParkingTime nvarchar(20) not null,
)
--11.会员卡挂失记录信息
create table MCLRInfo
(
MCNumber int primary key not null,
MCName nvarchar(20) not null,
IDnumber nvarchar(20) not null,
Phone nvarchar(20) not null,
ProcessingTime nvarchar(20) not null,
Type nvarchar(20) not null,
LRtime nvarchar(20) not null,
Remarks nvarchar(20) not null
)
--12.会员积分兑换
create table MPE
(
TradeNumber int primary key not null,
TradeName nvarchar(20) not null,
ExchangeQuantity nvarchar(20) not null,
UseIntegral nvarchar(20) not null,
ResidualIntegral nvarchar(20) not null
)
--13.商品信息（商品信息出现在此，只是表明Vip系统里面因为一些业务需要商品信息，并不意味着商品信息表的设计应该在Vip系统中进行设计）
create table CommodityInfo
(
TradeNumber int primary key not null,
TradeName nvarchar(20) not null,
CommodityPrice nvarchar(20) not null,
DOM nvarchar(20) not null,
QGP nvarchar(20) not null
)
--14.用户信息
create table UserInfo
(
UserNumber int primary key not null,
UserName nvarchar(20) not null,
UserSex nvarchar(20) not null,
DateOfBirth nvarchar(20) not null,
Phone nvarchar(20) not null,
IDnumber nvarchar(20) not null,
Address nvarchar(20) not null
)