use master
go
use Vip
go

--登录
--根据用户名查询是否存在该用户
select * from UserInfo
--登录
--查询该用户名存在吗
select * from UserInfo where UserName like '小李'

--如果存在则登录成功
select * from UserInfo where UserName like '张三'

--根据查询没有该用户，则注册，添加用户信息
insert into UserInfo values('Vip001','小李','男','2000-01-10','13455512341','45456200010105225','龙岩')

--用户输入一个用户名，但该用户名已经被注销和禁用该如何应对
--注销的话就让用户重新注册
delete from UserInfo where UserName like '小李'
select * from UserInfo where UserName like '小李'
insert into UserInfo values('Vip001','小李','男','2000-01-10','13455512341','45456200010105225','龙岩')

--禁用的话就让用户注销重新注册
delete from UserInfo where UserName like '小李'
insert into UserInfo values('Vip001','小李','男','2000-01-10','13455512341','45456200010105225','龙岩')

