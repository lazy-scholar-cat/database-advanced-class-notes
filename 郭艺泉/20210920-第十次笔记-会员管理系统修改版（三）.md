# 20210920-第十次笔记-会员管理系统修改版（三）
## 会员信息表
| 会员编号 | 姓名 | 性别 | 出生年月 | 联系方式 | 注册日期 |
| - | - | - | - | - | - |
| BH100001 | 刘文强 | 男 | 20011008 | 13344556677 | 20200401 |
| BH100102 | 陈旭 | 女 | 20000203 | 15522334455 | 20210503 |
| BH100201 | 李嘉城 | 男 | 20030502 | 18977889900 | 20201203 |
| BH100302 | 潘雨 | 女 | 20020503 | 15711223344 | 20200406 |
| BH100401 | 杜海彪 | 男 | 20010521 | 15844556611 | 20200309 |
| BH100502 | 吴凰 | 女 | 20001212 | 13866554422 | 20210809 |
| BH100602 | 滕馨 | 女 | 20010404 | 14755334411 | 20210410 |
| BH100701 | 罗雨欣 | 女 | 20000406 | 15622446699 | 20210723 |

## 会员卡信息
| 会员编号 | 会员等级 | 卡号 |
| - | - | - |
| BH100001 | K02 | 2044010228 |
| BH100102 | K01 | 2044010204 |
| BH100201 | K03 | 2044010219 |
| BH100302 | K02 | 2044010207 |
| BH100401 | K02 | 2044010203 |
| BH100502 | K03 | 2044010211 |
| BH100602 | K01 | 2044010233 |
| BH100701 | K03 | 2044010217 |

##  会员卡等级规则

| ID   | 卡类别 | 消费门槛￥ |
| ---- | ------ | ---------- |
| K01  | 普卡   | 1000       |
| K02  | 银卡   | 10000      |
| K03  | 金卡   | 100000     |

## 入驻商家

| 商家编号 | 商家名称 | 运营主体/法人            | 联系电话     |
| - | - | - | - |
| SJ01     | 周大福   | 郭艺泉                   | 18995167168  |
| SJ02     | 美丽足疗 | 黄杰耶                   | 13877788999  |
| SJ03     | NIKE     | 滔搏体育龙岩公司         | 0597-3443828 |
| SJ04     | 华润万家 | 华润万家福建分公司       | 0591-3243823 |
| SJ05     | 肯德基   | 肯德基（中国）福建分公司 | 0591-3143843 |
| SJ06     | 万达电影 | 大连万达集团福建分公司   | 0591-3443865 |
| SJ07     | 联想电脑 | 福建闽大电子科技有限公司 | 0597-3234323 |
| SJ08     | Armani   | 阿玛尼（中国）           | 021-5678765  |

##  会员卡消费信息

| 卡号       | 卡类型 | 累计消费积分 | 最近消费日期 | 最近消费金额¥ | 最近消费积分 | 最近消费商家 |
| ---------- | ------ | ------------ | ------------ | ------------- | ------------ | :----------- |
| 2044010228 | K02    | 2045         | 20210910     | 10000         | 1000         | SJ01         |
| 2044010204 | K01    | 101          | 20210705     | 100           | 10           | SJ02         |
| 2044010219 | K03    | 10031        | 20211221     | 600           | 60           | SJ03         |
| 2044010207 | K02    | 1040         | 20210808     | 320           | 32           | SJ04         |
| 2044010203 | K02    | 3120         | 20210502     | 82            | 8            | SJ05         |
| 2044010211 | K03    | 1112         | 20210910     | 600           | 60           | SJ06         |
| 2044010233 | K01    | 542          | 20210910     | 8499          | 850          | SJ07         |
| 2044010217 | K03    | 15334        | 20210907     | 5000          | 500          | SJ08         |

##　商家位置信息

| 商家编号 | 商家位置编号 |
| -------- | ------------ |
| SJ01     | Ａ１         |
| SJ02     | Ａ１         |
| SJ03     | Ａ２         |
| SJ04     | Ａ２         |
| SJ05     | Ｂ１         |
| SJ06     | Ｂ１         |
| SJ07     | Ｂ２         |
| SJ08     | Ｂ２         |

##  位置信息

| 位置编号 | 商场位置 |
| -------- | -------- |
| A1       | 一层A区  |
| A2       | 一层B区  |
| B1       | 二层A区  |
| B2       | 二层B区  |

##　会员车辆信息

．．．．．．
