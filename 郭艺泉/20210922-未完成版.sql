create database SystemForVip

GO

use SystemForVip

GO

create table CustomerInfo
(
  ID nvarchar(80) not null,
  Name nvarchar(80) not null, 
  Sex nvarchar(80) not null,
  Birthday datetime not null,
  PhoneNumber nvarchar(80) not null,
)
GO
select * from CustomerInfo

create table CardInfo
(
 ID nvarchar(80) not null,
 CardLevel nvarchar(80) not null,
 CardNumber int not null,
)
GO

select * from CardInfo

create table CardRules
(
 ID nvarchar(80) not null,
 CateGory nvarchar(80) not null,
 Threshold int not null,
)
GO

select * from CardRules

create table StoreInfo
(
 StoreId nvarchar(80) not null,
 StoreName nvarchar(80) not null,
 MainPart nvarchar(100) not null,
 StoreNumber int not null,
)
GO

select * from StoreInfo

create table ConsumeInfo
(
 CardNumber int not null,
 CardId nvarchar(80) not null,
 TotalCardPoint int not null,
 LastDate datetime,
 LastAmount int,
 LastCardPoint int,
 LastStore nvarchar(80),
)
go

select * from ConsumeInfo

create table StoreLocation
(
 StoreId nvarchar(80) not null,
 LocationID nvarchar(80),
)
go

create table LocationInfo
(
 LOcationId nvarchar(80) not null,
 location nvarchar(80),
)
GO

select * from CustomerInfo
insert into CustomerInfo values('BH100001','����ǿ','��','2001-10-08','13344556677')
insert into CustomerInfo values('BH100102','����','Ů','20000203','15522334455')
insert into CustomerInfo values('BH100201','��γ�','��','20030502','18977889900')
insert into CustomerInfo values('BH100302','����','Ů','20020503','15711223344')
insert into CustomerInfo values('BH100401','�ź���','��','20010521','15844556611')
insert into CustomerInfo values('BH100402','���','Ů','20001212','13866554422')
insert into CustomerInfo values('BH100402','��ܰ','Ů','20010404','14755334411')
insert into CustomerInfo values('BH100402','������','Ů','20000406','15622446699')
                