create database SystemForVip

GO

use SystemForVip

GO

create table CustomerInfo
(
  ID nvarchar(80) not null,
  Name nvarchar(80) not null, 
  Sex nvarchar(80) not null,
  Birthday datetime not null,
  PhoneNumber nvarchar(80) not null,
)
GO
select * from CustomerInfo

create table CardInfo
(
 ID nvarchar(80) not null,
 CardLevel nvarchar(80) not null,
 CardNumber int not null,
)
GO

select * from CardInfo

create table CardRules
(
 ID nvarchar(80) not null,
 CateGory nvarchar(80) not null,
 Threshold int not null,
)
GO

select * from CardRules

create table StoreInfo
(
 StoreId nvarchar(80) not null,
 StoreName nvarchar(80) not null,
 MainPart nvarchar(100) not null,
 StoreNumber nvarchar(80) not null,
)
GO

select * from StoreInfo

create table ConsumeInfo
(
 CardNumber int not null,
 CardId nvarchar(80) not null,
 TotalCardPoint int not null,
 LastDate datetime,
 LastAmount int,
 LastCardPoint int,
 LastStore nvarchar(80),
)
go

select * from ConsumeInfo

create table StoreLocation
(
 StoreId nvarchar(80) not null,
 LocationID nvarchar(80),
)
go

create table LocationInfo
(
 LOcationId nvarchar(80) not null,
 location nvarchar(80),
)
GO

select * from CustomerInfo
insert into CustomerInfo values('BH100001','刘文强','男','2001-10-08','13344556677')
insert into CustomerInfo values('BH100102','陈旭','女','20000203','15522334455')
insert into CustomerInfo values('BH100201','李嘉城','男','20030502','18977889900')
insert into CustomerInfo values('BH100302','潘雨','女','20020503','15711223344')
insert into CustomerInfo values('BH100401','杜海彪','男','20010521','15844556611')
insert into CustomerInfo values('BH100502','吴凰','女','20001212','13866554422')
insert into CustomerInfo values('BH100602','滕馨','女','20010404','14755334411')
insert into CustomerInfo values('BH100701','罗雨欣','女','20000406','15622446699')


  
  
select * from CardInfo
insert into CardInfo values('BH100001','K02','2044010228')
insert into CardInfo values('BH100102','K01','2044010204')   
insert into CardInfo values('BH100102','K01','2044010204')
insert into CardInfo values('BH100302','K02','2044010207')
insert into CardInfo values('BH100401','K02','2044010207')
insert into CardInfo values('BH100502','K03','2044010211')
insert into CardInfo values('BH100602','K01','2044010233')
insert into CardInfo values('BH100701','K03','2044010217')      


select * from CardRules
insert into CardRules values('K01','普卡','1000')
insert into CardRules values('K02','银卡','10000')
insert into CardRules values('K03','金卡','100000')  


select * from StoreInfo 
insert into StoreInfo values('SJ01','周大福','郭艺泉','18995167168')
insert into StoreInfo values('SJ02','美丽足疗','黄杰耶','13877788999')  
insert into StoreInfo values('SJ03','NIKE','滔搏体育龙岩公司','0597-3443828')  
insert into StoreInfo values('SJ04','华润万家','华润万家福建分公司','0591-3243823')  
insert into StoreInfo values('SJ05','肯德基','肯德基（中国）福建分公司','0591-3143843')  
insert into StoreInfo values('SJ06','万达电影','大连万达集团福建分公司','0591-3443865')  
insert into StoreInfo values('SJ07','联想电脑','福建闽大电子科技有限公司','0597-3234323')  
insert into StoreInfo values('SJ08','Armani','阿玛尼（中国）','021-5678765')   


select * from ConsumeInfo
insert into ConsumeInfo values('2044010228','K02','2045','20210910','10000','1000','SJ01') 
insert into ConsumeInfo values('2044010204','K01','101','20210705','100','10','SJ02') 
insert into ConsumeInfo values('2044010219','K03','10031','20211221','600','60','SJ03') 
insert into ConsumeInfo values('2044010207','K02','1040','20210808','320','32','SJ04') 
insert into ConsumeInfo values('2044010203','K02','3120','20210502','82','8','SJ05') 
insert into ConsumeInfo values('2044010211','K03','1112','20210910','600','60','SJ06') 
insert into ConsumeInfo values('2044010233','K01','542','20210910','8499','850','SJ07') 
insert into ConsumeInfo values('2044010217','K03','15334','20210907','5000','500','SJ08')   


select * from StoreLocation
insert into StoreLocation values('SJ01','A1')
insert into StoreLocation values('SJ02','A1')
insert into StoreLocation values('SJ03','A2')
insert into StoreLocation values('SJ04','A2')
insert into StoreLocation values('SJ05','B1')
insert into StoreLocation values('SJ06','B1')
insert into StoreLocation values('SJ07','B2')
insert into StoreLocation values('SJ08','B2')

select * from LocationInfo   
insert into LocationInfo values('A1','一层A区') 
insert into LocationInfo values('A2','一层B区') 
insert into LocationInfo values('B1','二层A区') 
insert into LocationInfo values('B2','二层B区')  




select * from CustomerInfo
select * from CardInfo
select * from CardRules
select * from StoreInfo
select * from ConsumeInfo
select * from StoreLocation
select * from LocationInfo