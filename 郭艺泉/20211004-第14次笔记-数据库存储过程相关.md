# 20211004-第14次笔记-还是学不会

---

### 一、数据库存储过程相关

#### 1.什么是存储过程

存储过程（Stored Procedure）是在大型数据库系统中，一组为了完成特定功能的SQL 语句集，存储在数据库中，经过第一次编译后调用不需要再次编译，用户通过指定存储过程的名字并给出参数（如果该存储过程带有参数）来执行它。存储过程是数据库中的一个重要对象。

简而言之，存储过程就是SQL Server为了实现特定任务，而将一些需要多次调用的固定操作语句编写成程序段，这些程序段存储在服务器上，有数据库服务器通过程序来调用

#### 2.存储过程的优点

1.存储过程只在创造时进行编译，以后每次执行存储过程都不需再重新编译，而一般SQL语句每执行一次就编译一次,所以使用存储过程可提高数据库执行速度。
2.当对数据库进行复杂操作时(如对多个表进行Update,Insert,Query,Delete时)，可将此复杂操作用存储过程封装起来与数据库提供的事务处理结合一起使用。
3.存储过程可以重复使用,可减少数据库开发人员的工作量
4.安全性高,可设定只有某些用户才具有对指定存储过程的使用权

#### 3.分类

1.系统存储过程

2.用户定义存储过程

3.扩展存储过程

#### 4.补充

存储过程可以被命名为以下任意一种，这取决于命名约定的使用。

- ​		LatestTasks
- ​		latest_tasks
- ​		uspLatestTasks
- ​		usp_latest_tasks
- ​		selectLatestTasks
- ​		select_LatestTasks
- ​		select_latest_tasks
- ​		getLatestTasks
- ​		get_latest_tasks

### 二、实例

```
/*
create proc 存储过程名称
as
begin

end
*/
```
1.带参数的存储过程

```
create proc proc_SelectStudentInfoWithParameter
@name nvarchar(80)
as
begin
	select * from UserInfo
	where StudentName like @name
end
go

exec proc_SelectStudentInfoWithParameter '%郭%'
go
```
2.带多个参数的存储过程
```
create proc proc_SelectStudentInfoWithSomeParameter
@name nvarchar(80),
@code nvarchar(80)
as
begin
	select * from StudentInfo
	where StudentCode like @code and StudentName like @name
end
go
exec proc_SelectStudentInfoWithSomeParameter '%罗%','01'
go
```
3.有默认值的存储过程
```
create proc proc_SelectStudentInfowithAnyParameters
@code nvarchar(80)='01',
@name nvarchar(80)='%郭%',
@birthday date='2021-10-04',
@sex char='m',
@classid int='1'
as
begin
	select * from StudentInfo
	where StudentCode like @code
	or StudentName like @name
	or Birthday <@birthday
	or sex=@sex
	or ClassId= @classId
end
exec proc_SelectStudentInfowithAnyParameters
```