# 20211015-第21次作业-游标相关

### 一、什么是游标


​        *游标是SQL Server的一种数据访问机制，它允许用户访问单独的数据行。用户可以对每一行进行单独的处理，从而降低系统开销和潜在的阻隔情况，用户也可以使用这些数据生成的SQL代码并立即执行或输出。*

​       *游标是一种处理数据的方法，主要用于存储过程，触发器和 T_SQL脚本中，它们使结果集的内容可用于其它T_SQL语句。在查看或处理结果集中向前或向后浏览数据的功能。类似与C语言中的指针，它可以指向结果集中的任意位置，当要对结果集进行逐条单独处理时，必须声明一个指向该结果集中的游标变量。*

    *SQL Server 中的数据操作结果都是面向集合的，并没有一种描述表中单一记录的表达形式，除非使用WHERE子句限定查询结果，使用游标可以提供这种功能，并且游标的使用和操作过程更加灵活、高效。*

### 二、作用

1. 定位到结果集中的某一行。
2. 对当前位置的数据进行读写。
3. 可以对结果集中的数据单独操作，而不是整行执行相同的操作。
4. 是面向集合的数据库管理系统和面向行的程序设计之间的桥梁

### 三、语法

```
declare  first_cur   cursor           --创建游标
for select * from fn_rands

open first_cur                        --打开游标

fetch next from first_cur into  @n,@m --读取游标

close first_cur                      --关闭游标，清除游标内缓存数据

deallocate first_cur                  --删除游标 
```

### 四、示例

```
- 定义游标
declare cur_ForStudentInfo cursor
for
select * from StudentInfo


--打开游标，准备使用
open cur_ForStudentInfo 

--定义变量，用于接收每一行的数据
declare @id int  , @studentcode nvarchar(10), @name nvarchar(80) , @birthday date
,@sex nvarchar(2),@classid int

-- 先获取一条，以使@@fetch_status状态初始化
fetch next from cur_ForStudentInfo 
into @id , @studentcode,@name ,@birthday,@sex,@classid

select @@FETCH_STATUS

-- 真正遍历记录的语句结构
while @@FETCH_STATUS=0
begin 
 select @id,@name,@birthday
 fetch next from cur_ForStudentInfo
 into @id , @studentcode,@name ,@birthday,@sex,@classid
 end

 -- 关闭游标
close cur_ForStudentInfo
-- 释放游标
deallocate cur_ForStudentInfo
```
