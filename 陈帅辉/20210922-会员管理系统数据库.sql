use VIP
go
 create table ShoopInfo--商场信息表
 (
 ShoopId int primary key  not null, 
 ShoopName nvarchar(50)  not null,
 ShoopAddress nvarchar(50) ,
 ShoopPreson nvarchar(10),
 OfficePhone char(11)
 
 )
 insert into ShoopInfo Values(1,'万宝购物广场','龙岩大道X号','张三','05977788990')



 create table MemberInfo--会员信息
 (
 MemberId int primary key not null, 
 MemberName nvarchar(50)  not null,
 Sex nvarchar(1) ,
 Birthday time ,
 MemberPhone char(11)
 
 )
  insert into MemberInfo values(1,'李四','男','20020805','12457181421') 
--会员卡信息

 create table MemberCar--会员卡信息
 (
 MemberCarId int primary key not null, 
 MemberCarName nvarchar(50)  not null,
 MemberCarType nvarchar(50) ,
 CarOrganization nvarchar(10),
 MemberId int
 )

 insert into MemberCar values(001,'V001','Type001','万达',1)
--会员卡类型信息


 create table CarTypeInfo--会员卡类型信息
 (
 CarTypeId int primary key not null, 
 CarTypeName nvarchar(50)  not null,
 Remark nvarchar(100)
 
 )
  insert into CarTypeInfo values(1,'普通VIP','可以优惠')

 create table ShopInfo--商铺信息
 (
 ShopId int primary key not null, 
 ShopName nvarchar(50)  not null,
 ShopAddress nvarchar(50) ,
 Operator nvarchar(10) ,
 OperatorPhone char(11)
 
 )
   insert into ShopInfo values(1,'海澜之家','二楼3B','马超','12242404041')


 create table MemberTotal--会员积分规则
 (
 MemberTotalId int primary key identity not null, 
 MemberActiveId int  not null,
 Reguation nvarchar(100)

 )
 insert into MemberTotal values(1,'一元积一分')


 create table MemberTotalInfo--会员积分信息
 (
 MemberTotalId int primary key identity not null, 
 MemberId int  not null,
 Tatal int
 )
  insert into MemberTotalInfo values('1','500')
--会员等级信息

 create table MemberGrade--会员等级信息
 (
 GradeId int primary key identity not null, 
 GradeName nvarchar(50)  not null,
 Condition nvarchar(100)
 )
  insert into MemberGrade values('白银会员','积分大于500')
--会员活动信息

 create table MemberActiveInfo--会员活动信息
 (
 ActiveId int primary key identity not null, 
 ActiveName nvarchar(50)  not null,
 ActionTime time ,
 EndTime time
 
 )
  insert into MemberActiveInfo values('中秋活动','20200806','20200901')

 create table Veichle--会员车辆信息
 (
 VeichleId int primary key identity not null, 
 MemberId nvarchar(50)  not null,
 BusNumber char 
 )
   insert into Veichle values('1','闽D XJ803')

 create table MemberLoseInfo--会员挂失信息
 (
 LoseInfoId int primary key identity not null, 
 LoseMemberCarId nvarchar(50)  not null,
 MemberInfoId nvarchar(50) 
 
 )
 insert into MemberLoseInfo values('暂无','暂无')

 create table MemberTotalConvert--会员积分兑换
 (
 ConvertId int primary key identity not null, 
 ConvertGiftId int  not null,
 MenberId int 
 
 )
  insert into MemberTotalConvert values(2,1)
--商品信息

 create table CommidityInfo--商品信息
 (
 CommidityId int primary key identity not null, 
 CommidityName nvarchar(50)  not null,
 CommidityPrice money ,
 CommidityStock int
 
 )
 insert into CommidityInfo values('苹果','4110',100)
--用户信息

 create table UserInfo--用户信息
 (
 UserId int primary key identity not null, 
 UserName nvarchar(50)  not null,
 UserPassWord char 
 
 )
 insert into UserInfo values('admin','123456')
--角色信息

 create table RoleInfo--角色信息
 (
 RoleId int primary key identity not null, 
 RoleNpc nvarchar(50)  not null

 )
  insert into RoleInfo values('超级管理员')
--用户角色信息

 create table UserRoleInfo--用户角色信息
 (
 UserRoleId int primary key identity not null, 
 UserId int  not null,
 RoleId int not null
 
 )
   insert into UserRoleInfo values(1,1)