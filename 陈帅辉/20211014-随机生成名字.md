```sql
use Js
go

alter proc proc_createInfoinsert
	@rowcount int =100
	as
begin

	if OBJECT_ID('startname','U')is not null
	begin
		drop table startname
	end
	create table Startname
	(
		id int primary key identity ,
		name nvarchar(50)
	
	) 
	declare @num int =0, @name nvarchar(20),@startname nvarchar(20),@lastname nvarchar(20)
	while(@num<@rowcount)
	begin
		exec proc_firstname @startname output
		exec proc_lastname @lastname output
		set @name=@startname+@lastname
		insert into Startname(name) values(@name)
		set @num=@num+1
	end
end
exec proc_createInfoinsert
select * from Startname 


go
 alter proc proc_firstname--随机生成姓的存储过程
 @name nvarchar(50) output
 as
  begin
	declare @firstname nvarchar(50)='赵钱孙李周吴郑王冯陈褚卫蒋沈韩杨朱秦尤许何吕施张孔曹严华金魏陶姜戚谢邹喻柏水窦章云苏潘葛奚范彭郎鲁韦昌马苗凤花方俞任袁柳酆鲍史唐费廉岑薛雷'
	declare @index int =rand()*len(@firstname)+1
	set @name=SUBSTRING(@firstname,@index,1)   --SUBSTRING(name,5,3) 截取name这个字段 从第五个字符开始 只截取之后的3个字符
  end	
declare @res nvarchar(20)
exec proc_firstname @res output
select @res
go


alter proc proc_lastname--随机生成名的存储过程
@name nvarchar(50) output
as
begin
	declare @lastname nvarchar(80)='侯邵孟龙万段曹钱汤尹黎易常武乔贺赖龚文牛葛严祁荆焦单衣仲包颜麻阮梅温齐游党苗左俞索安肖尚聂覃习翁闫关路殷舒岳滕伊向陶韦姬银蒲甘荣谈燕池皮穆樊洪卜蓝庄班毕晏苑盛鲁苍倪纪莫付童翟雷奚郜尤司成匡农闵巫臧戚应费詹祝刁屈邰符邢贡宿缪占权申井柳边栾冼景佟原强谷施库麦章花嵇蔚房时凌双伍宁沃裴及鲍茅裘禹饶蔺娄宗管庞巩甄靳艾涂和贝门柯骆曲弓伏邬乌祖解米甯浦都辛乐从薄季植秋喻虞云邝寇宫褚项印卓劳师席桂荀商隆封邴楼刑蒯盖那冷富郁慕敖冀丛牟卫明木茹沙芮郎狄满鞠竺闻哈靖逄雍鄂蓬历扈衡宓东滑融汲通扶广牧糜山家怀空羿能璩宰看宦钭贲湛松凤咸七堵归度殴竞钦鄢琴有后况亢缑阙勾海法晁楚厍夔越殳欧阚禄弘耿居籍蓟韶束戎福言阳爱暨终厉暴仇晋庾戈慎古连充仰全车瞿郏督郤仉桓巴於羊郦冉桓益红杭昝支霍危莘相巢谯墨须计元赏卞岑胥伯佴酆佘昌养乜蒙窦毋简柏华'
	declare @namelen int=convert(int,rand()*2)+1
	declare @index int,@index1 int
	if @namelen=1
		begin
			set @index=rand()*len(@lastname)
			set @name=SUBSTRING(@lastname,@index,1)
		end
	else
		begin
			set @index=rand()*len(@lastname)
			set @index1=rand()*len(@lastname)
			set @name=SUBSTRING(@lastname,@index,1)+substring(@lastname,@index1,1)

		end
end
declare @res nvarchar(20)
exec proc_lastname @res output
select @res
go
--查询时间
declare @time datetime
set @time =getdate()
select * from Startname
select [语句执行花费时间(毫秒)]=datediff(ms,@time,getdate)

```
```
语法：object_id('objectname')或object('objectname','type')

作用：该函数会返回指定对象的ID值，可以在sysobjects表中进行验证。

U = Table (user-defined)


```
## 地址：https://www.cnblogs.com/nimorl/p/4867422.html

## select GETDATE() 返回当前系统日期
## DATEADD(日期部分,常数,日期) 返回将日期的指定日期部分加常数后的结果返回
```
SELECT DATEADD(YY,1,GETDATE())
--等同于
SELECT DATEADD(YEAR,1,GETDATE())
```
