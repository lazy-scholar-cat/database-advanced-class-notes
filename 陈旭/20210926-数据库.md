# 数据的存储其实可以引申出数据的持久化
1.数据的持久化，有多种方法，并不是只有数据库存储一种，还有比如文件存储（文本文件、excel、word文件）

2.数据的持久化其实就是相对于数据的易失性来说的，比如说在内存当中，只要掉电，数据即消失

3.只有保存在硬盘上的数据，才不会随便丢失，硬盘上保存数据，就是把数据存储在文件当中

5.因为不同的需要，所以，操作系统中有了存储不同数据的文件，一些文件通常使用扩展名来区分保存的数据类型

6.缓存 redis 内存数据库 靠近处理中心（如CPU）





## 商城信息表
| 商城编号| 商场名称 | 位置 | 负责人 | 电话 |
| - | - | - | - | - |
| SM01 | 万达 | 万达广场 | 老铁 | 123456 |

## 会员类型表

| 类型编号 | 类型 | 消费金额 |
| - | - | - |
| V00 | 普通会员| 0 |
| V01 | 黄金会员| 999 |
| V02 | 钻石会员| 3888 |


## 会员信息表
| 会员编号 | 会员姓名 | 性别  | 出生年月 | 手机号 |  身份证号 | 住址 | 开卡日期 | 会员类型 |
| - | - | - | - | - |- | - | - | - |
| uid1 | 老子 | 男 | 2222-12-12 | 12345678901 | 350---- | 福州 | 2230-08-11 | V01 |

## 会员卡信息表

| 卡编号 | 卡号 | 会员信息 | 余额 | 积分|
| - | - | - | - |- |
| k01 | 1111 | uid1 | 1000 | 20 |

积分=付款金额/10  
如因活动付款的金额不计

## 商铺信息表
| 编号 | 商铺名称 | 商铺地址 | 
| - | - | - |
| s01 | 好家伙 | 商场2层 |

## 消费记录表

| 消费编号 | 卡编号 | 消费店铺编号 | 消费时间 | 享受优惠 | 消费金额 | 获得积分 |
| - | - | - | - | - | - | - |
| xx001 | k01  | s01 | 2230-09-11 | 赠送小吃一份 | 20 | 2 |

## 积分兑换表
| 序号 |  卡编号 | 消费时间 | 兑换物品 | 消费积分 |
| - | - | - | - | - |
| F01 | k01 | 2280-13-32 | 别墅一栋| 999 |

## 会员车辆信息表

| 编号 | 会员编号  | 车牌号 | 每日免费停车时长 |每日停车时长|
| - | - | - | - | - |
| C001 | uid1 | 闽E14111 | 80h | 0h |

## 会员挂失信息

| 挂失信息编号 | 挂失的会员编号 | 挂失时间|
| - | - | - |
| G001 | uid1 | 2030-9-11 9：30 |



![a](./img/0923表.png)