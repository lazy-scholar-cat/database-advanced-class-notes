# 游标的定义和使用

## 1.游标的组成

### 游标包含两个部分：一个是游标结果集、一个是游标位置。

### 游标结果集：定义该游标得SELECT语句返回的行的集合。游标位置：指向这个结果集某一行的当前指针。

## 2.游标的生命周期

### 游标的生命周期包含有五个阶段：声明游标、打开游标、读取游标数据、关闭游标、释放游标。

### 声明游标是为游标指定获取数据时所使用的Select语句，声明游标并不会检索任何数据，它只是为游标指明了相应的Select 语句。

### Declare 游标名称 Cursor 参数

## 3.语法
```sql
    -- 定义游标
declare  cur_ForPersonalInfo2 cursor
for 
select * from PersonalInfo

-- 打开游标，准备使用
open cur_ForPersonalInfo2

-- 定义变量，用于接收每一行中的数据
declare @id int,@name nvarchar(80) ,@birthday date

-- 先获取一条，以使@@fetch_status状态初始化
fetch next from cur_ForPersonalInfo2
into @id,@name,@birthday

select @@FETCH_STATUS

-- 真正遍历记录的语句结构
while @@Fetch_Status = 0
begin
	select @id,@name,@birthday -- 代表处理取出的每一行数据
	fetch next from cur_ForPersonalInfo2
	into @id,@name,@birthday
end

-- 关闭游标
close cur_ForPersonalInfo2
-- 释放游标
deallocate cur_ForPersonalInfo2


```
 