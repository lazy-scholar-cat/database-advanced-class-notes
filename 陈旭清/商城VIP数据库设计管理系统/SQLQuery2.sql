create database ShoppingMalDatabase
go 
use ShoppingMalDatabase 
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('UserRoles') and o.name = 'FK_USERROLE_REFERENCE_USERS')
alter table UserRoles
   drop constraint FK_USERROLE_REFERENCE_USERS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('UserRoles') and o.name = 'FK_USERROLE_REFERENCE_ROLES')
alter table UserRoles
   drop constraint FK_USERROLE_REFERENCE_ROLES
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Roles')
            and   type = 'U')
   drop table Roles
go

if exists (select 1
            from  sysobjects
           where  id = object_id('UserRoles')
            and   type = 'U')
   drop table UserRoles
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Users')
            and   type = 'U')
   drop table Users
go

/*==============================================================*/
/* Table: Roles                                                 */
/*==============================================================*/
create table Roles (
   Id                   int                  identity,
   RoleName             nvarchar(80)         null,
   constraint PK_ROLES primary key (Id)
)
go

/*==============================================================*/
/* Table: UserRoles                                             */
/*==============================================================*/
create table UserRoles (
   Id                   int                  identity,
   UserID               nvarchar(80)         null,
   RoleID               nvarchar(80)         null,
   constraint PK_USERROLES primary key (Id)
)
go

/*==============================================================*/
/* Table: Users                                                 */
/*==============================================================*/
create table Users (
   Id                   int                  identity,
   UserName             nvarchar(80)         null,
   PassWord             nvarchar(80)         null,
   constraint PK_USERS primary key (Id)
)
go




