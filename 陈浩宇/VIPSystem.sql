use master
go
create database VIPSystem
go
 use VIPSystem
 go
create table Market
(
    MID int primary key identity(1,1) not null,
	MarkatName nvarchar(50) not null,
	MarkatAlsoName nvarchar(50) not null,
	MarketManager nvarchar(10) not null,
	MarketPhone  nvarchar(50) not null,
	)
go
create table VIPCustomer
(
    VID int primary key identity(1,1) not null,
	VIPName nvarchar(50) not null,
	VIPSex char(1) not null,
	VIPBirthday datetime not null,
	VIPPhone  nvarchar(50) not null,
	VIPPersonalCard nvarchar(50) not null,
	VIPAddress nvarchar(200) not null,
)
go
create table VIPCard
(
 VIPCard int primary key identity(1,1) not null,
 VIPCardType int not null,
 CardMade nvarchar(20) not null,
 VID int references VIPCustomer(VID) not null
)
go
create table VIPCardType
(
 VIPCardType int primary key identity(1,1) not null,
  CardMade nvarchar(10) not null,
)
go
create table Shop 
(
	SID int primary key identity(1,1) not null,
	SAddress nvarchar(200) not null,
	SName nvarchar(10) not null,
	SManagerName nvarchar(10) not null,
	SPhone nvarchar(11) not null,
	STime nvarchar(10) not null,
)
go
create table VIPLevel
(
 LID int primary key identity(1,1) not null,
 VIPNum int not null
)
go
create table CommodityMessage
(
CID  int  primary key identity(1,1) not null,
CommodityName nvarchar(50) not null,
ShopID int references Market(MID) not null,
CommodityPrice money not null,
CommodityNum int not null
)
go


select * from Market
insert into Market values('�򱦹���㳡','��','����','0599-123456')

--use master
--drop database VIPSystem

update Market set MarketManager='李四' where MID=1

declare @username nvarchar(80),@password nvarchar(80),@rowPassword nvarchar(80)
declare @isDeleted bit ,@isActived bit
declare @count int
declare @res nvarchar(80)

select @count=COUNT(*),@isDeleted=IsDeleted,@isActived=IsActived,@rowPassword=Password from Users 
where Username=@username

-- 如果根据提供的用户名，找到的记录数不为零，则表明用户名是对的，可以继续往下判断；否则直接提示用户名或密码错误
if(@count>0)
	begin
		if(@isDeleted=1) --如果删除标记为真，则表明当前用户已经被删除，作出提示；否则用户未被删除
			begin
				set @res='当前用户已经被删除'
			end
		else
			begin
				if(@isActived=0) --如果当前用户未启用/激活，则提示信息用户未启用，登录失败；否则用户已经禁用
					begin
						set @res='用户已禁用'
					end
				else
					begin
						if(@password=@rowPassword) -- 如果传进来的密码和在记录中获取的密码相等，则认为登录成功，作出提示；否则登录失败，提示相应信息
							begin
								set @res='登录成功'
							end
						else
							begin
								set @res='用户名或者密码错误，请确认后重试...'
							end
					end

			end

	end
else
	begin
		set @res='用户名或者密码错误，请确认后重试...'
	end





declare @username nvarchar(80) --前端传进来的用户名
declare @password nvarchar(80),@cofirmPassword nvarchar(80) --前端传进来的密码
declare @rowPassword nvarchar(80) --是指通过用户名查询到记录后，这个代表用户的记录中的密码
declare @tmpTable table (id int,Username nvarchar(80),Password nvarchar(80))
declare @count int
declare @res nvarchar(80)

insert into @tmpTable
select @count=count(*),@rowPassword=Password from Users where Username=@username group by Password

if(@count>0)
	begin

		set @res='当前用户名已经被注册，请确认后重试。。。。'
		
	end
else
	begin
		if(@password=@cofirmPassword)
			begin
				insert into Users (Username,Password) values (@username,@password)
				set @res='用户注册成功'
			end
		else
			begin
				set @res='两次输入的密码不一致，请确认后重试'
			end

	end