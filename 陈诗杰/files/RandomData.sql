-- Drop the database 'RandomData'
-- Connect to the 'master' database to run this snippet
USE master
GO
-- Uncomment the ALTER DATABASE statement below to set the database to SINGLE_USER mode if the drop database command fails because the database is in use.
-- ALTER DATABASE RandomData SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
-- Drop the database if it exists
IF EXISTS (
  SELECT name
   FROM sys.databases
   WHERE name = N'RandomData'
)
DROP DATABASE RandomData
GO

CREATE DATABASE RandomData
GO

USE RandomData
GO

-- Create a new table called 'UserInfo' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.test') IS NOT NULL
DROP Table dbo.test
GO
-- Create the table in the specified schema
CREATE Table dbo.test
(
    Id INT NOT NULL PRIMARY KEY identity, -- primary key column
    Name NVARCHAR(50) not null,
    Birthday DATE not null,
    AGE SMALLINT,
	Mark varchar(1024)
)
GO

-- Create a new view called 'VIew_Rand' in schema 'dbo'
-- Drop the view if it already exists
IF EXISTS (
SELECT *
    FROM sys.views
    JOIN sys.schemas
    ON sys.views.schema_id = sys.schemas.schema_id
    WHERE sys.schemas.name = N'dbo'
    AND sys.views.name = N'VIew_Rand'
)
DROP VIEW dbo.VIew_Rand
GO
-- Create the view in the specified schema
CREATE VIEW dbo.VIew_Rand
AS
    -- body of the view
    SELECT rand() frand;
GO

IF OBJECT_ID('Random') is not null
	drop function Random
GO

CREATE FUNCTION Random(@n INT) --生成一个0-@n之间的随机数
RETURNS INT
AS BEGIN
    SELECT @n=@n * frand FROM dbo.View_Rand
    RETURN @n
END
GO

if OBJECT_ID('fn_RandomDateOutPut') is not null
    drop function fn_RandomDateOutPut
GO

Create function fn_RandomDateOutPut() --随机生成一个100-18年前的日期并返回
returns date AS
BEGIN
    declare @date date = GETDATE()
    while DATEDIFF(YEAR,@date,GETDATE()) NOT BETWEEN 18 and 100
	BEGIN
		set @date = GETDATE() - dbo.Random(36500)
	END

	Return @date
END
GO

if OBJECT_ID('fn_RandomChar') is not null
    drop function fn_RandomChar
GO

CREATE FUNCTION fn_RandomChar(@n int)--n为生成的字符串的长度，最大为8
RETURNS varchar(1024) as
BEGIN
	if @n > 8
		set @n = 8

    declare @res varchar(1024) = ''
    declare @char char(62) = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKKLMNOPQRSTUVWXYZ0123456789'
    declare @i int = 0
    while @i < @n
    BEGIN
        set @i = @i +1
        set @res = @res + SUBSTRING(@char,floor(dbo.Random(62)),1)
    END
    RETURN @res
END
GO

if OBJECT_ID('fn_FirstNameOutPut') is not null
	drop function fn_FirstNameOutPut
GO

create	function fn_FirstNameOutPut()
returns varchar(6) AS
BEGIN
	declare @res varchar(6)
	declare @names varchar(1024) = '阳春白雪梅兰竹菊风花雪月高山流水天涯海角知音难觅不想编了就这样吧'
	declare @n int = dbo.random(2)+1
	declare @i int = 0
	while @i < @n
	BEGIN
		set @i = @i + 1
		set @res = CONCAT(SUBSTRING(@names,floor(1+dbo.Random(len(@names))),1),@res)
	END
	return @res
END
GO

if OBJECT_ID('fn_LasttNameOutPut') is not null
	drop function fn_LastNameOutPut
GO

create	function fn_LastNameOutPut()
returns varchar(3) AS
BEGIN
	declare @res varchar(6)
	declare @lastNames varchar(1024) = '赵钱孙李周吴郑王冯陈褚卫蒋沈韩杨朱秦尤许何吕施张孔曹严华金魏陶姜戚谢邹喻柏水窦章云苏潘葛奚范彭郎鲁韦昌马苗凤花方俞任袁柳酆鲍史唐费廉岑薛雷贺倪汤'

	set @res = CONCAT(SUBSTRING(@lastNames,floor(1+dbo.Random(len(@lastNames))),1),@res)

	return @res
END
GO

create trigger tr_testAgeInsert --自动插入年龄
on test after insert as
begin
	update test set age = DATEDIFF(YYYY,Birthday,GETDATE()) where id = (select id from inserted)
end
GO

if OBJECT_ID('proc_TableInsert') is not null
    drop proc proc_TableInsert
GO

CREATE proc proc_TableInsert
@table varchar(80), --要插入数据的表
@n int = 100 --要插入的记录的条数
AS
BEGIN
    set NOCOUNT on

    declare @name varchar(1024)
    declare @birthday date
    declare @mark varchar(1024)
    declare @i INT = 0

        WHILE @i < @n
        BEGIN
            set @i = @i + 1
  
			select @name = dbo.fn_LastNameOutPut() + dbo.fn_FirstNameOutPut()
			select @birthday =  dbo.fn_RandomDateOutPut()
			select @mark = dbo.fn_RandomChar(8)

            declare @sql char(80)
            set @sql = concat(
                    'insert into ',@table,'(Name,Birthday,Mark)',
                    ' values ','(','''',@name,'''',',','''',@birthday,'''',',','''',@mark,'''',')'
                )
            exec(@sql)
        END
    
    set NOCOUNT OFF
END
GO

exec proc_TableInsert 'test','10000000'
GO