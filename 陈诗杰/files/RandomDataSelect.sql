Use RandomData
GO

if exists(select name from sys.indexes where name = 'ix_testOnName')
    Drop index ix_testOnName on test
go

CREATE INDEX ix_testOnName on test(Name)
GO

select * from test where Name = '陈就'