-- 练习题目：
-- 1.查询"数学 "课程比" 语文 "课程成绩高的学生的信息及课程分数
with CTE_A as
(
	select StudentId,
	(case when CourseId = 1 then Score end) Chinese,
	(case when CourseId = 2 then Score end) Math,
	(case when CourseId = 3 then Score end) English
	from StudentCourseScore
), CTE_B as
(
	select StudentId,sum(Chinese) Chinese,sum(Math) Math,sum(English) English from CTE_A group by StudentId
)select SI.*,CB.Chinese,CB.Math,CB.English from CTE_B CB join StudentInfo SI on CB.StudentId = SI.StudentCode
where CB.Chinese < CB.Math 
-- 1.1 查询同时存在" 数学 "课程和" 语文 "课程的情况
with CTE_A as
(
	select StudentId,
	(case when CourseId = 1 then Score end) Chinese,
	(case when CourseId = 2 then Score end) Math,
	(case when CourseId = 3 then Score end) English
	from StudentCourseScore
), CTE_B as
(
	select StudentId,sum(Chinese) Chinese,sum(Math) Math,sum(English) English from CTE_A group by StudentId
)select SI.*,CB.Chinese,CB.Math,CB.English from CTE_B CB join StudentInfo SI on CB.StudentId = SI.StudentCode 
where Math is not null and Chinese is not null
-- 1.2 查询存在" 数学 "课程但可能不存在" 语文 "课程的情况(不存在时显示为 null )
with CTE_A as
(
	select StudentId,
	(case when CourseId = 1 then Score end) Chinese,
	(case when CourseId = 2 then Score end) Math,
	(case when CourseId = 3 then Score end) English
	from StudentCourseScore
), CTE_B as
(
	select StudentId,sum(Chinese) Chinese,sum(Math) Math,sum(English) English from CTE_A group by StudentId
)select SI.*,CB.Chinese,CB.Math,CB.English from CTE_B CB join StudentInfo SI on CB.StudentId = SI.StudentCode 
where Math is not null
-- 1.3 查询不存在" 数学 "课程但存在" 语文 "课程的情况
with CTE_A as
(
	select StudentId,
	(case when CourseId = 1 then Score end) Chinese,
	(case when CourseId = 2 then Score end) Math,
	(case when CourseId = 3 then Score end) English
	from StudentCourseScore
), CTE_B as
(
	select StudentId,sum(Chinese) Chinese,sum(Math) Math,sum(English) English from CTE_A group by StudentId
)select SI.*,CB.Chinese,CB.Math,CB.English from CTE_B CB join StudentInfo SI on CB.StudentId = SI.StudentCode 
where Math is null and Chinese is not null
-- 2.查询平均成绩大于等于 60 分的同学的学生编号和学生姓名和平均成绩
select SI.StudentCode,SI.StudentName,AVG_Score from StudentInfo SI
right join (select StudentId,avg(Score) AVG_Score from StudentCourseScore group by StudentId having avg(Score) >= 60) Id 
on ID.StudentId = SI.StudentCode 
-- 3.查询在 成绩 表存在成绩的学生信息
select * from StudentInfo where StudentCode in (select distinct StudentId from StudentCourseScore)
-- 4.查询所有同学的学生编号、学生姓名、选课总数、所有课程的总成绩(没成绩的显示为 null )
select StudentCode,StudentName,CountLessons,SumScore from StudentInfo SI
left join 
(
	select StudentId , count(*) CountLessons , sum(Score) SumScore from StudentCourseScore 
	group by StudentId
)SCS on SI.StudentCode = SCS.StudentId
-- 4.1 查有成绩的学生信息
select SI.* from StudentInfo SI join (select distinct StudentId from StudentCourseScore) SCS on SI.StudentCode = SCS.StudentId
-- 5.查询「李」姓老师的数量
select count(*) from Teachers  where TeacherName like '李%'
-- 6.查询学过「张三」老师授课的同学的信息
select * from StudentInfo SI right join 
(
	select distinct StudentId from StudentCourseScore 
	where CourseId in 
	(
		select CourseId from CourseInfo 
		where TeacherId = (select TeacherId from Teachers where TeacherName = '张三')
	)
) SCS
on SI.StudentCode = SCS.StudentId
-- 7.查询没有学全所有课程的同学的信息
select * from StudentInfo 
where StudentCode not in (select StudentId from StudentCourseScore group by StudentId having count(*) = (select count(*) from CourseInfo))
-- 8.查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息
select * from StudentInfo 
where StudentCode in
(
	select distinct StudentId from StudentCourseScore 
	where CourseId in(select CourseId from StudentCourseScore where StudentId = 01 and StudentId != 01)
)
-- 后面没做