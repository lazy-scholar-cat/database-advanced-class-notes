--接上回

-- 9.查询和" 01 "号的同学学习的课程 完全相同的其他同学的信息
select * from StudentInfo
where StudentCode in 
(
	select StudentId from StudentCourseScore
	where StudentId != 01
	group by StudentId
	having SUM(CourseId) = 
	(
		select SUM(CourseId) from StudentCourseScore 
		where StudentId = 01
	)
	and
	(
		count(*) = 
		(
			select count(*) from StudentCourseScore 
			where StudentId = 01
		)
	)
)
-- 10.查询没学过"张三"老师讲授的任一门课程的学生姓名
select * from StudentInfo 
where StudentCode not in
(
	select  distinct StudentId from StudentCourseScore
	where CourseId =
	(
		select Id from CourseInfo
		where TeacherId = 
		(
			select Id from Teachers
			where TeacherName = '张三'
		)
	)
)

-- 11.查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩
with CTE_A as
(
	select StudentId,
	(case when Score < 60 then Score end) score
	from StudentCourseScore 
), CTE_B as
(
	select StudentId, score from CTE_A
	where score is not null
)select StudentId,StudentName,avg(score) avg_count 
from CTE_B CB left join StudentInfo SI
on SI.StudentCode = CB.StudentId
group by StudentId,StudentName
having COUNT(CB.StudentId) >= 2

-- 12.检索" 数学 "课程分数小于 60，按分数降序排列的学生信息

-- 13.按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩

-- 14.查询各科成绩最高分、最低分和平均分：

-- 15.以如下形式显示：课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
/*
	及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90

	要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序排列

	按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺
*/	
	
-- 15.1 按各科成绩进行排序，并显示排名， Score 重复时合并名次

-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺

-- 16.1 查询学生的总成绩，并进行排名，总分重复时不保留名次空缺

-- 17.统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比

-- 18.查询各科成绩前三名的记录

-- 19.查询每门课程被选修的学生数

-- 20.查询出只选修两门课程的学生学号和姓名

-- 21.查询男生、女生人数

-- 22.查询名字中含有「风」字的学生信息

-- 23.查询同名同性学生名单，并统计同名人数

-- 24.查询 1990 年出生的学生名单

-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列

-- 26.查询平均成绩大于等于 85 的所有学生的学号、姓名和平均成绩

-- 27.查询课程名称为「数学」，且分数低于 60 的学生姓名和分数

-- 28.查询所有学生的课程及分数情况（存在学生没成绩，没选课的情况）

-- 29.查询任何一门课程成绩在 70 分以上的姓名、课程名称和分数

-- 30.查询不及格的课程

-- 31.查询课程编号为 01 且课程成绩在 80 分以上的学生的学号和姓名

-- 32.求每门课程的学生人数

-- 33.成绩不重复，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩

--34.成绩有重复的情况下，查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩

-- 35.查询不同课程成绩相同的学生的学生编号、课程编号、学生成绩

-- 36.查询每门功成绩最好的前两名

-- 37.统计每门课程的学生选修人数（超过 5 人的课程才统计）。

-- 38.检索至少选修两门课程的学生学号

-- 39.查询选修了全部课程的学生信息

-- 40.查询各学生的年龄，只按年份来算

-- 41.按照出生日期来算，当前月日 < 出生年月的月日则，年龄减一

-- 42.查询本周过生日的学生

-- 43.查询下周过生日的学生

-- 44.查询本月过生日的学生

-- 45.查询下月过生日的学生
