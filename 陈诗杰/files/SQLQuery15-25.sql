
-- 练习题目：

-- 1.查询"数学 "课程比" 语文 "课程成绩高的学生的信息及课程分数

-- 1.1 查询同时存在" 数学 "课程和" 语文 "课程的情况
-- 15.以如下形式显示：课程 ID，课程 name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
	select CourseId,CourseName,Score,
	Rank() over (partition by CourseId order by Score desc) Rank
	from StudentCourseScore SCS join CourseInfo CI on CI.Id = SCS.Id

/*
	及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90

	要求输出课程号和选修人数，查询结果按人数降序排列，若人数相同，按课程号升序排列

	按各科成绩进行排序，并显示排名， Score 重复时保留名次空缺
*/	

-- 15.1 按各科成绩进行排序，并显示排名， Score 重复时合并名次
select *,
Dense_Rank() over (partition by CourseId order by Score desc) Rank
from StudentCourseScore
-- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺
select StudentId,sum(Score) ScoreSum,
Rank() over (order by sum(Score) desc) Rank
from StudentCourseScore
group by StudentId
-- 16.1 查询学生的总成绩，并进行排名，总分重复时不保留名次空缺
select StudentId,sum(Score) ScoreSum,
Dense_Rank() over (order by sum(Score) desc) Rank
from StudentCourseScore
group by StudentId
go
-- 17.统计各科成绩各分数段人数：课程编号，课程名称，[100-85]，[85-70]，[70-60]，[60-0] 及所占百分比
;with CTE_A as
(
		select CourseId,
		(case when Score >= 85 and Score <= 100 then 1 else 0 end) [100-85],
		(case when Score >= 70 and Score < 85 then 1 else 0 end) [85-70],
		(case when Score >= 60 and Score < 70 then 1 else 0 end) [70-60],
		(case when Score >= 0 and Score < 60 then 1 else 0 end) [60-0]
		from StudentCourseScore
),CTE_B as
(
	select CourseId,
	count(*) total,
	sum([100-85]) [100-85],
	sum([85-70]) [85-70],
	sum([70-60]) [70-60],
	sum([60-0]) [60-0]
	from CTE_A
	group by CourseId
) 
select CourseId,CI.CourseName,
concat(convert(decimal(18,2),[100-85] * 100.00 / total),'%') [100-85%],
concat(convert(decimal(18,2),[85-70] * 100.00 / total ),'%') [85-70%],
concat(convert(decimal(18,2),[70-60] * 100.00 / total),'%') [70-60%],
concat(convert(decimal(18,2),[60-0] * 100.00 / total),'%') [60-0%]
from CTE_B join CourseInfo CI on CI.Id = CTE_B.CourseId


-- 18.查询各科成绩前三名的记录
--CTE
;with CTE_A as
(
	select *,
	Dense_Rank() over (partition by CourseId order by Score desc) Rank
	from StudentCourseScore
)select * from CTE_A where Rank <= 3
--sub query
select * from
(
	select *,
	Dense_Rank() over (partition by CourseId order by Score desc) Rank
	from StudentCourseScore
) A
where Rank <= 3

-- 19.查询每门课程被选修的学生数
select CourseId,
count(StudentId) countStudent
from StudentCourseScore
group by CourseId

-- 20.查询出只选修两门课程的学生学号和姓名
select * from StudentInfo
where StudentCode in 
(
	select StudentId from StudentCourseScore
	group by StudentId
	having count(StudentId) = 2
)

-- 21.查询男生、女生人数
select Sex,COUNT(Sex) NumOfPeople from StudentInfo
group by Sex

-- 22.查询名字中含有「风」字的学生信息
select * from StudentInfo where StudentName like '%风%'
-- 23.查询同名同性学生名单，并统计同名人数
select A.StudentName,count(A.StudentName) from StudentInfo A cross join StudentInfo B
where A.StudentName = B.StudentName and A.Sex = B.Sex and A.StudentCode != B.StudentCode
group by A.StudentName
-- 24.查询 1990 年出生的学生名单
select * from StudentInfo
where year(Birthday) = 1990
-- 25.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列
select CourseId,avg(Score) from StudentCourseScore
group by CourseId 
order by avg(Score) Desc , CourseId asc
