use master
go

if exists(select name from sys.databases where name = 'VIP')
	drop database VIP
go

create database VIP
go

use VIP
go

create table PrincipalInfo --负责人信息表
(
	Principal_Id char(20) primary key,
	Principal_Name nvarchar(18) not null,
	Principal_Phone char(11) not null,
	Principal_Email varchar(50) not null
)
go

create table MallInfo --商城信息表
(
	Mall_Id char(20) primary key,
	Mall_Name nvarchar(18) not null,
	Mall_Address nvarchar(50) not null,
	Principal_Id char(20) references PrincipalInfo(Principal_Id) not null
)
go

create table ShopInfo --商铺信息表
(
	Shop_Id char(20) primary key,
	Shop_Name nvarchar(18) not null,
	Mall_Id char(20) references MallInfo(Mall_Id) not null,
	Shop_Address nvarchar(30) not null,
	Principal_Id char(20) references PrincipalInfo(Principal_Id) not null
)
go

create table ShopStaffInfo --员工信息表
(
	Staff_Id char(20) primary key not null,
	Staff_Name nvarchar(18) not null,
	Staff_Phone char(11) not null,
	WeChat varchar(20) not null,
	Mall_Id char(20) references MallInfo(Mall_Id) not null
)
go

create table VipInfo --会员信息表
(
	Vip_Id char(20) primary key,
	Vip_Name nvarchar(18) not null,
	Vip_Phone char(11),
	Vip_Address nvarchar(50),
	Vip_Email varchar(50)
)
go

create table VipCardInfo --会员卡信息表
(
	VC_Id char(20) primary key,
	Vip_Id char(20) references VipInfo(Vip_Id),
	Mall_Id char(20) references MallInfo(Mall_Id) not null,
	VC_Level char(2) not null
)
go

create table LoseReport --挂失信息表
(
	LP_Id int identity primary key,
	VC_Id char(20) references VipCardInfo(VC_Id),
	Principal_Id char(20) references PrincipalInfo(Principal_Id),
	LP_Date datetime default(getdate())
)
go

create table VipCarInfo --会员车辆信息表
(
	VCar_Id int primary key identity,
	License_Plate char(9) not null,
	Vip_Id char(20) references VipInfo(Vip_Id)
)
go

create table UserInfo
(
	Id int primary key identity,
	UserName varchar(50) unique(UserName),
	Password varchar(100),
	IsActived bit default(1)
)
go

insert into PrincipalInfo values ('MB001','张三','11451419190','zhansan@aa.com'),
('SF001','李四','19198103510','lisi@aa.com')
go

insert into VipInfo(Vip_Id,Vip_Name) values ('V001','test')
go

insert into MallInfo values ('Ma001','测试用商城001','不存在的地址','MB001')
go

insert into VipCardInfo values ('VC001','V001','Ma001','L1'),('VC002','V001','Ma001','L0')
go

insert into LoseReport(VC_Id,Principal_Id) values ('VC001','SF001')
go

insert into UserInfo(UserName,Password) values ('test01','1145141919810')

select LP_Id '挂失ID',VC.VC_Id '会员卡号',VC_Level '会员等级',VI.Vip_Name 'VIP姓名',PrI.Principal_Name '经手人',LP_Date '挂失时间'
from LoseReport LP join VipCardInfo VC on LP.VC_Id = VC.VC_Id
join VipInfo VI on VI.Vip_Id = VC.Vip_Id
join PrincipalInfo PrI on PrI.Principal_Id = LP.Principal_Id
go

update VipCardInfo set VC_Level = 'L2' where VC_Id = 'VC001'
select * from VipCardInfo
go

delete from VipCardInfo Where VC_Id = 'VC002'
select * from VipCardInfo
go
