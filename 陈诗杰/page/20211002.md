# 登入

## 登入成功的条件
 1. 用户表中存在与用户输入信息相同的用户
 2. 该用户未被注销
## 代码
~~~ sql
    declare @userName varchar(50), @password varchar(100), @isActived bit, @count int, @result nchar(10)

    select @count = count(*), @isActived = IsActived from UserInfo 
    where UserName = @userName and Password = @password
    group by IsActived --因为选择列表中有被包含在聚合函数中的列，所以选择列表中的每一列都需要被包含在聚合函数或 group by 子句中

    if(count > 0)
        begin
            if(@isActived = 1)
                begin
                    set @result = '登入成功'
                end
            else
                begin
                    set @result = '用户已被注销'
                end
        end
    else
        begin
            set @result = '用户名或密码错误'
        end
~~~

# 注册

## 注册成功的条件
1. 用户名是合法的
2. 用户名还没被注册
3. 输入的密码与重复密码一致

## 代码
~~~ sql
    declare @count int, @userName varchar(50), @password varchar(80), @rePass varchar(80), @result nvarchar(20)

    select @count = count(*) from UserInfo 
    where UserName = @userName

    if(count > 0)
        begin
            set @result = '用户名已被注册'
        end
    else
        begin
            if(@userName != replace(@userName,' ',''))
                begin
                    if(replace(@userName,' ','') = '')
                        begin
                            set @result = '请输入用户名'--全都是空格可不就是没输入嘛
                        end
                    else
                        begin
                            set @result = '非法的字符" "'
                        end
                end
            else if(@password != @rePass)
                begin
                    set @result = '两次输入的密码不一致'
                end
            else
                begin
                    set @result = '注册成功'
                end
        end
~~~