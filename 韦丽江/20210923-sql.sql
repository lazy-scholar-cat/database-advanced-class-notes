create database MallShop
go
use MallShop 
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('Exchange') and o.name = 'FK_EXCHANGE_REFERENCE_MEMBERNU')
alter table Exchange
   drop constraint FK_EXCHANGE_REFERENCE_MEMBERNU
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('MemPointInfo') and o.name = 'FK_MEMPOINT_REFERENCE_MEMBERNU')
alter table MemPointInfo
   drop constraint FK_MEMPOINT_REFERENCE_MEMBERNU
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('MemberCarInfo') and o.name = 'FK_MEMBERCA_REFERENCE_MEMBERNU')
alter table MemberCarInfo
   drop constraint FK_MEMBERCA_REFERENCE_MEMBERNU
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('MemberCard') and o.name = 'FK_MEMBERCA_REFERENCE_MEMBERNU')
alter table MemberCard
   drop constraint FK_MEMBERCA_REFERENCE_MEMBERNU
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('MemberLostInfo') and o.name = 'FK_MEMBERLO_REFERENCE_MEMBERNU')
alter table MemberLostInfo
   drop constraint FK_MEMBERLO_REFERENCE_MEMBERNU
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('UserRoleInfo') and o.name = 'FK_USERROLE_REFERENCE_ROLEINFO')
alter table UserRoleInfo
   drop constraint FK_USERROLE_REFERENCE_ROLEINFO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('UserRoleInfo') and o.name = 'FK_USERROLE_REFERENCE_USERINFO')
alter table UserRoleInfo
   drop constraint FK_USERROLE_REFERENCE_USERINFO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('Exchange')
            and   type = 'U')
   drop table Exchange
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemCardInfo')
            and   type = 'U')
   drop table MemCardInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemLevelInfo')
            and   type = 'U')
   drop table MemLevelInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemPointInfo')
            and   type = 'U')
   drop table MemPointInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemPointRule')
            and   type = 'U')
   drop table MemPointRule
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberActive')
            and   type = 'U')
   drop table MemberActive
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberCarInfo')
            and   type = 'U')
   drop table MemberCarInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberCard')
            and   type = 'U')
   drop table MemberCard
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberLostInfo')
            and   type = 'U')
   drop table MemberLostInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberNumber')
            and   type = 'U')
   drop table MemberNumber
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ProductInfo')
            and   type = 'U')
   drop table ProductInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('RoleInfo')
            and   type = 'U')
   drop table RoleInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ShopInfo')
            and   type = 'U')
   drop table ShopInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ShopMall')
            and   type = 'U')
   drop table ShopMall
go

if exists (select 1
            from  sysobjects
           where  id = object_id('UserInfo')
            and   type = 'U')
   drop table UserInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('UserRoleInfo')
            and   type = 'U')
   drop table UserRoleInfo
go

/*==============================================================*/
/* Table: Exchange                                              */
/*==============================================================*/
create table Exchange (
   Id                   int                  null,
   ExchangeID           int                  null
)
go

/*==============================================================*/
/* Table: MemCardInfo                                           */
/*==============================================================*/
create table MemCardInfo (
   TypeId               int                  identity,
   MemCardTypeName      nvarchar(80)         null,
   remark               nvarchar(80)         null,
   constraint PK_MEMCARDINFO primary key (TypeId)
)
go

/*==============================================================*/
/* Table: MemLevelInfo                                          */
/*==============================================================*/
create table MemLevelInfo (
   LevelID              int                  identity,
   LevelName            nvarchar(80)         null,
   LevShortName         nvarchar(80)         null,
   Condition            nvarchar(80)         null,
   constraint PK_MEMLEVELINFO primary key (LevelID)
)
go

/*==============================================================*/
/* Table: MemPointInfo                                          */
/*==============================================================*/
create table MemPointInfo (
   PointID              int                  identity,
   Id                   int                  null,
   Point                nvarchar(80)         null,
   constraint PK_MEMPOINTINFO primary key (PointID)
)
go

/*==============================================================*/
/* Table: MemPointRule                                          */
/*==============================================================*/
create table MemPointRule (
   PointID              int                  identity,
   ActiveId             nvarchar(80)         null,
   "Rule"               nvarchar(80)         null,
   constraint PK_MEMPOINTRULE primary key (PointID)
)
go

/*==============================================================*/
/* Table: MemberActive                                          */
/*==============================================================*/
create table MemberActive (
   ActiveID             int                  identity,
   ActiveName           nvarchar(80)         null,
   StartTime            datetime             null,
   EndTime              datetime             null,
   constraint PK_MEMBERACTIVE primary key (ActiveID)
)
go

/*==============================================================*/
/* Table: MemberCarInfo                                         */
/*==============================================================*/
create table MemberCarInfo (
   Id                   int                  identity,
   CarID                int                  null,
   License              nvarchar(80)         null,
   constraint PK_MEMBERCARINFO primary key (Id)
)
go

/*==============================================================*/
/* Table: MemberCard                                            */
/*==============================================================*/
create table MemberCard (
   CardID               int                  identity,
   Mem_Id               int                  null,
   MemCard              nvarchar(80)         null,
   MemCardType          nvarchar(80)         null,
   IssuingOrganizat     nvarchar(80)         null,
   constraint PK_MEMBERCARD primary key (CardID)
)
go

/*==============================================================*/
/* Table: MemberLostInfo                                        */
/*==============================================================*/
create table MemberLostInfo (
   ReportID             int                  identity,
   Id                   int                  null,
   VipCar               nvarchar(80)         null,
   constraint PK_MEMBERLOSTINFO primary key (ReportID)
)
go

/*==============================================================*/
/* Table: MemberNumber                                          */
/*==============================================================*/
create table MemberNumber (
   Id                   int                  identity,
   MemName              nvarchar(80)         null,
   Sex                  nvarchar(80)         null,
   Birth                datetime             null,
   Phone                nvarchar(80)         null,
   CarID                nvarchar(80)         null,
   Address              char(10)             null,
   constraint PK_MEMBERNUMBER primary key (Id)
)
go

/*==============================================================*/
/* Table: ProductInfo                                           */
/*==============================================================*/
create table ProductInfo (
   ProductID            nvarchar(80)         null,
   ProductName          nvarchar(80)         null,
   Price                nvarchar(80)         null,
   ProMax               nvarchar(80)         null
)
go

/*==============================================================*/
/* Table: RoleInfo                                              */
/*==============================================================*/
create table RoleInfo (
   RoleID               int                  identity,
   RoleName             nvarchar(80)         null,
   constraint PK_ROLEINFO primary key (RoleID)
)
go

/*==============================================================*/
/* Table: ShopInfo                                              */
/*==============================================================*/
create table ShopInfo (
   ShopID               int                  identity,
   ShopName             nvarchar(80)         null,
   LocatShopMall        nvarchar(80)         null,
   LocationNo           nvarchar(80)         null,
   Operator             nvarchar(80)         null,
   SerialNum            nvarchar(80)         null,
   Column_7             char(10)             null,
   constraint PK_SHOPINFO primary key (ShopID)
)
go

/*==============================================================*/
/* Table: ShopMall                                              */
/*==============================================================*/
create table ShopMall (
   ID                   int                  identity,
   MallName             nvarchar(80)         null,
   ShortName            nvarchar(80)         null,
   Adress               nvarchar(80)         null,
   LegalPerson          nvarchar(80)         null,
   Phone                nvarchar(80)         null,
   constraint PK_SHOPMALL primary key (ID)
)
go

/*==============================================================*/
/* Table: UserInfo                                              */
/*==============================================================*/
create table UserInfo (
   UserID               int                  identity,
   UserName             nvarchar(80)         null,
   PassWord             nvarchar(80)         null,
   constraint PK_USERINFO primary key (UserID)
)
go

/*==============================================================*/
/* Table: UserRoleInfo                                          */
/*==============================================================*/
create table UserRoleInfo (
   ID               numeric              identity,
   RoleID               int                  null,
   UserID               int                  null,
   constraint PK_USERROLEINFO primary key (ID)
)
go

alter table Exchange
   add constraint FK_EXCHANGE_REFERENCE_MEMBERNU foreign key (Id)
      references MemberNumber (Id)
go

alter table MemPointInfo
   add constraint FK_MEMPOINT_REFERENCE_MEMBERNU foreign key (Id)
      references MemberNumber (Id)
go

alter table MemberCarInfo
   add constraint FK_MEMBERCA_REFERENCE_MEMBERNU foreign key (Id)
      references MemberNumber (Id)
go

alter table MemberCard
   add constraint FK_MEMBERCA_REFERENCE_MEMBERNU foreign key (Mem_Id)
      references MemberNumber (Id)
go

alter table MemberLostInfo
   add constraint FK_MEMBERLO_REFERENCE_MEMBERNU foreign key (Id)
      references MemberNumber (Id)
go

alter table UserRoleInfo
   add constraint FK_USERROLE_REFERENCE_ROLEINFO foreign key (RoleID)
      references RoleInfo (RoleID)
go

alter table UserRoleInfo
   add constraint FK_USERROLE_REFERENCE_USERINFO foreign key (UserID)
      references UserInfo (UserID)
go
