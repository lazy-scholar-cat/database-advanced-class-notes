## 天气：阴转大雨     虽下雨 ， 但并不影响码农人的一天  
##           公用表（CTE），递归？
```
 指定临时命名的结果集，这些结果集称为公用表表达式 (CTE)。
公用表表达式 可 以包括对自身的引用。
这种表达式称为递归公用表表达式。

对于递归公用表达式来说，实现原理也是相同的，同样需要在语句中定义两部分：

   1.基本语句
   2.递归语句
   在SQL这两部分通过UNION ALL连接结果集进行返回：

  ;with CTE_A
as(

	select Id,GoodsName,ParentId from Goods where Id =8
	union all
	select a.Id ,c.GoodsName ,a.ParentId  from Goods  c,CTE_A a

)
select * from Goods 

   使用CTE准则

创建CTE时，需要注意的一些准则，列出的使用准则：

定义CTE时最好加前缀”;”
CTE内部定义的列字段要保持一致
CTE with之后第一句必须使用CTE的select。即CTE的生命周期只是在第一次使用之后就消亡。
sp中只能使用一次with语句。
定义多个CTE时，只声明一个with关键字就行，比如
with test1

as

(

select * …………

)，

test2 as

(

select * …………

)
```
```
通用表表达式(CTE)是SQL Server的一项新功能。本质上CTE是一个临时结果集，它仅仅存在于它发生的语句中。可以在SELECT、INSERT、DELETE或CREATE VIEW语句中建立一个CTE。

--   如果下面的select查询的当前结果集与原结果集的匹配对象有匹配值不为null，则让当前结果集与原结果集合并，同时用合并的结果更新更新最终的结果集比较对象；

--   否则停止执行。最终结果集就是上一次匹配值不为null的结果集。 

----------------------------------------------------------------------------------------------------------------------------------------

use ClassicDb 
go

create table Goods
(
	Id int primary key identity,
	GoodsName nvarchar(100) not null,
	ParentId int not null default 0
	
)

insert into Goods (GoodsName )VALUES('电脑'),('手机'),('食品'),('服装'),('家电'),('首饰')

insert into Goods (GoodsName ,ParentId )values
('戴尔',(select top 1 Id from Goods where GoodsName ='电脑')),
('联想',(select top 1 Id from Goods where GoodsName ='电脑')),
('华为',(select top 1 Id from Goods where GoodsName ='电脑')),
('外星人',(select top 1 Id from Goods where GoodsName ='电脑'))

insert into Goods (GoodsName ,ParentId)values
('女装',(select top 1 Id from Goods where GoodsName ='服装')),
('男装',(select top 1 Id from Goods where GoodsName ='服装')),
('运动休闲装',(select top 1 Id from Goods where GoodsName ='服装'))

insert into Goods (GoodsName ,ParentId)values
('礼服装',(select top 1 Id from Goods where GoodsName ='女装')),
('裙装',(select top 1 Id from Goods where GoodsName ='女装')),
('运动休闲装',(select top 1 Id from Goods where GoodsName ='女装'))


select * from Goods 
---------------------------------------------------------------------------------------------------------------

--公用表表达式，递归


;with CTE_A
as(

	select Id,GoodsName,ParentId from Goods where Id =8
	union all
	select a.Id ,c.GoodsName ,a.ParentId  from Goods  c,CTE_A a

)
select * from Goods 


-- 多语句表值函数 语法
--------------------------------------------------------------------------------------------------------------
/*
create function <函数名称>
(
	[
		@xxx 数据类型 [=默认值],
		@yyy 数据类型 [=默认值]
	]
)
returns @res table (Id int ,Name nvarchar(80))
as
begin

	return
end
*/
go
---------------------------------------------------------------------------------------------------------
create function fn_StudentInfoByName(@name nvarchar(100)) 
returns @res table (Id int ,Name nvarchar(100))
as
begin

	return
	end



go
alter function fn_StudentInfoByName(@name nvarchar(100)) 
returns @res table (Id int ,Name nvarchar(100))
as
begin
	declare @a nvarchar(100)
	insert into @res (Id ,Name )select id,studentname from StudentInfo where StudentName=@name

	select @a=name from @res 

	insert into @res (Id ,Name )select id,studentname from StudentInfo  
	return
	end
	go
	select * from dbo.fn_StudentInfoByName ('赵')
	----------------------------------------------------------------------------------------------------------------------------------

	/*不可删掉变量表  总之就是 单语句表值函数和多语句表值函数，实际上的区别只在于后者有一个表类型的变量，在后者定义的函数内部，可以任意的对这个变量
	进行增删改查的操作(可以操作其中的数据，但是不能对表变量进行删除等操作)；单语句表值函数，并没有一个表类型的变量，它最后只
	是返回一个表，也可以理解为其整个的过程就是一个的select语句（并不只是说，只能有一个select语句，比如使用CTE，就不只一个select）
	*/

	--错误表达  (通俗来说就是@res这个表变量不可进行删除等操作)
	
	-- drop from @res   ---临时表

```
```
	总结：

	
1.	--多语句表值函数也称为多声明表值型函数，可以看作标量型和内联表值型函数的结合体。
	--它的返回值是一个表，但它和标量型函数一样有一个用BEGIN-END 语句括起来的函数体，返回值的表中的数据是由函数体中的语句插入的。
	--可以进行多次查询，对数据进行多次筛选与合并弥补了内嵌表值型函数的不足。

2.	--	多语句表值函数
	--Begin...End 限定了函数体,Returns指定Table作为返回的数据类型
	--Create Function[own)name.]function_name([{@参数名[as]数据类型[=默认值]},[,...n]])
	--Returns @返回变量 Table<表定义>[With<函数选项>][As] Begin 函数体 Return End

	--调用函数 Select * from own_name.function_name[(参数值[,...n])][where <条件>]



总结：
1.
	递归就是一个函数体内部，  根据上一次函数返回的结果与当前函数体的新结果集比较 ，  来判断是否 循环调用自身的 函数体再次执行相同的操作。
2.union all 后的  select 语句中展现的字段必须是 与上一次结果集比较的结果集中的字段！否则就是自己与自己比较自己的结果集，毫无对照可比性不会出现null，就会死循环达到 最大递归层数是999.

``