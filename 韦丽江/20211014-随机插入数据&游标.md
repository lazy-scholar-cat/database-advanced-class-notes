```
create database per
go
use per 
go

create proc proc_person
@row int =10000
as
begin
	if OBJECT_ID ('person','U') is not null
		begin 
			drop table person
		end
	create table person
	(
		id int primary key identity not null,
		name nvarchar(100) not null,
		birthday date not null default (getdate())
	)
	
	
		declare @i int =0,@Name nvarchar(100),@lastName nvarchar(100),@FisrtName nvarchar(100)
	
	while @i<@row 
		begin
		--exec
		--exec
		set @Name =@lastName +@FisrtName 
	
	
		set @i =@i+1
	

end
	exec proc_person 10000

	select * from person 


	go

	create proc proc_lastName
	@name nvarchar(100) output
	as
	begin
		declare @str nvarchar(100)='赵钱孙李周吴郑王冯陈褚卫蒋沈韩杨朱秦尤许何吕施张孔曹严华金魏陶姜戚谢邹喻柏水窦章云苏潘葛奚范彭郎鲁韦昌马苗凤花方俞任袁柳酆鲍史唐费廉岑薛'
			declare @index int =rand()*len(@str)
			set @name =SUBSTRING (@str,@index ,1)
	end
	go

	declare @result nvarchar(100)
	exec proc_lastName @result output
	select @result 

	go

	 create proc proc_FirtName
	 @name nvarchar(100) output--创建内部函数
	 as
	 begin
		declare @str nvarchar(100)='春夏秋冬梅兰菊竹上下左右宇宙洪荒宝灵天地琴棋书画晨霞彩风霜雪雨雷闪磊涯海水'
		declare @namelen int =convert(int,rand()*2)+1
		declare @indext1 int ,@indext2 int 

	-- 判断名字的数量为1，则取单名；否则取双名
	if @namelen=1
		begin 
			set @indext1=rand()*len(@str)
			set @name =SUBSTRING (@str,@indext1 ,1)
		end
	else
		begin
			set @indext1=rand()*len(@str)
			set @indext2 =rand()*len(@str)
			set @name=SUBSTRING(@str,@indext1 ,1)+SUBSTRING(@str,@indext2 ,1)--substring 截取字符@str
		end

	end
	go

	declare @result nvarchar(100)
	exec  proc_FirtName @result output
	select @result 

```
```
	--第二种方法随机
	go
	alter proc proc_person
	@name nvarchar(100) output
	as
	begin
	    declare @str nvarchar(100)='赵钱孙李周吴郑王冯陈褚卫蒋沈韩杨朱秦尤许何吕施张孔曹严华金魏陶姜戚谢邹喻柏水窦章云苏潘葛奚范彭郎鲁韦昌马凤花方俞任袁柳酆鲍史唐费廉岑薛'
		declare @i int =rand()*len(@str)--随机出姓氏

		set @name =SUBSTRING(@str ,@i,1)
	end
	exec proc_person  1


	declare @result nvarchar(100),@result2 nvarchar(100)
	exec proc_person @result output 
	exec proc_person @result2 output
	select @result +@result2 


```
```
    2.游标的优点

SELECT 语句返回的是一个结果集，但有时候应用程序并不总是能对整个结果集进行有效地处理，游标便提供了这样一种机制，它能从包括多条记录的结果集中每次提取一条记录，游标总是与一跳SQL选择语句相关联，由结果集和指向特定记录的游标位置组成。使用游标具有一下优点：

(1).允许程序对由SELECT查询语句返回的行集中的每一次执行相同或不同的操作，而不是对整个集合执行同一个操作。

(2).提供对基于游标位置中的行进行删除和更新的能力。
 
(3).游标作为数据库管理系统和应用程序设计之间的桥梁，将两种处理方式连接起来。

```

```
## 约束、视图
订阅专栏
数据完整性：
是指存储在数据库中的所有数据值均处于正确状态。如果数据库中保存有不正确的数据值，则该数据库的数据完整性已被破坏。引入数据库完整性的概念是为了防止数据库中存在不符合语义规定的数据

非空约束：
not null

唯一约束：
unique

主键约束：
建表后添加约束：alter table [表名] add constraint [主键名] primary key( [字段名] );

primary key

外键约束：
constraint [外键的名字，任意取] foreign key [当前表字段名] references [表名] [该表名的字段名]  

检查约束：
删除约束：

alter table [表名]  drop constraint [约束名]

视图
视图是从数据表（或者其他视图）中提取数据而成的一种虚拟表。这里的“提取”实际上就是查询操作——视图中定义包含有一条select语句，该语句查询现有数据表并将其结果返回作为本视图的内容。和表结构没有什么不同，因此视图一经创建可以当做表来使用。但是和前面的子查询创建表有所不同，视图本身并不咋物理上保存数据，在视图上进行的查询或更新操作实际上都是针对基表来完成的。

创建视图
SQL>  create view myview1(姓名,编号,职位,工资)
  2   as select empno,ename,job,sal from emp
  3   where  deptno = 20;
 
视图已创建。
 
SQL> desc myview1;
 名称                                      是否为空? 类型
 ----------------------------------------- -------- ----------------------------
 姓名                                      NOT NULL NUMBER(4)
 编号                                               VARCHAR2(10)
 职位                                               VARCHAR2(9)
 工资                                               NUMBER(7,2)
删除视图
 
SQL> drop view myview1;
 
视图已删除。
 
SQL> desc myview1;
ERROR:
ORA-04043: 对象 myview1 不存在
重新编译视图
如果视图的基表结构发生变化，比如添加或删除了字段，则视图状态会被标记为无效，此后当再有用户访问该视图，Orcale会自动重新编译该视图，以避免可能的运行错误。此外，用户也可以执行alter view语句手工编译视图。

语句：alter view [视图名] compile;

在视图上进行更新操作
由于视图是一种虚拟的数据表，其本身并不存储数据，因此在视图上执行更新操作时实际更新的是其基表中的数据。并不是所有的视图都可以执行更新操作。因当遵循如下原则：

1.用户要有对视图基表进行操作的权限；

2.视图定义的子查询中不能使用分组函数、group by子句、distinct关键字、rownum伪列，其查询字段不能为表达式；

3.由两个以上基表中导出的视图不能进行delete操作；

4.基表中非空的字段在视图定义中未包括、该字段也未曾设过缺省值，则不可以在视图上进行insert操作。

添加数据：

在创建视图的时候加上with check option;可以限制在视图上添加的数据。

注意：上述 with check option 选项相当于在新定义的视图上定义了check类型的约束，和数据表的情况不同，视图上不允许定义其他类型的约束，也不允许以其他方式定义的check约束（比如在视图定义的字段名列表中定义）。

修改数据：

同上差不多

删除数据：

delete [视图名]  where [字段名]=[条件]

 
```