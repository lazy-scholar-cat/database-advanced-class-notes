```
  ##  round()&rand()的区别

 declare @a float =rand()*10
select @a
select round (@a,0)

go

declare @b float =rand()*10
select @b
select ceiling(@b)----ceiling 在原来的整数中前进了一位
declare @c  float=rand()* 10
select @ c
select floor(@c)





     游标 是在数据中当中为，遍历数据集的一种方式或手段，可以反游标当成一种指针


   ##   处理游标的四个步骤PLSQL中显式游标使用的4个步骤  创建-打开-提取-关闭
         游标处理的4个过程中涉及到的加锁,一致性读的问题.
显式游标处理需四个 PL/SQL步骤:
1 定义/声明游标：就是定义一个游标名，以及与其相对应的SELECT 语句。
格式：
CURSOR cursor_name
IS  
    select_statement;  
  
 
2 打开游标：就是执行游标所对应的SELECT 语句，将其查询结果放入工作区，并且指针指向工作区的首部，标识游标结果集合。如果游标查询语句中带有FOR UPDATE选项，OPEN 语句还将锁定数据库表中游标结果集合对应的数据行。
格式: 
OPEN cursor_name[([parameter =>] value[, [parameter =>] value]…)];
在向游标传递参数时，可以使用与函数参数相同的传值方法，即位置表示法和名称表示法。PL/SQL 程序不能用OPEN 语句重复打开一个游标。
 
3 提取游标数据：就是检索结果集合中的数据行，放入指定的输出变量中。 
 
FETCH cursor_name INTO {variable_list | record_variable };
执行FETCH语句时，每次返回一个数据行，然后自动将游标移动指向下一个数据行。当检索到最后一行数据时，如果再次执行FETCH语句，将操作失败，并将游标属性%NOTFOUND置为TRUE。所以每次执行完FETCH语句后，检查游标属性%NOTFOUND就可以判断FETCH语句是否执行成功并返回一个数据行，以便确定是否给对应的变量赋了值。
 
4关闭游标：当提取和处理完游标结果集合数据后，应及时关闭游标，以释放该游标所占用的系统资源，并使该游标的工作区变成无效，不能再使用FETCH 语句取其中数据。关闭后的游标可以使用OPEN 语句重新打开。
格式: 
CLOSE cursor_name;


```
use ClassicDb 
go
select * from StudentInfo 
--定义游标
declare cur_studentInfo cursor
for
select * from StudentInfo 


-- 打开游标，准备使用
open cur_studentInfo


---定义变量  用于接收每一行中的数据
declare @id int ,@studentname nvarchar(100),@birthday date

---先获取一条，一使@@feth_status状态初始化
fetch next from cur_studentInfo
into @id,@studentname,@birthday

select @@FETCH_STATUS 

--真正遍历记录的语句结构

while @@Fetch_Status = 0
begin
	select @id,@studentname ,@birthday -- 代表处理取出的每一行数据
	fetch next from cur_studentInfo
	into @id,@studentname,@birthday
end

-- 关闭游标
close cur_studentInfo



-- 释放游标
deallocate cur_studentInfo
```

```



## 随机
```
# 随机姓名及生成1000W条记录
create database DbTest
go
use DbTest
go

alter proc proc_CreatePersonalInfoAndInsert
@rowCount int =100
as
begin
	set nocount on
	if OBJECT_ID('PersonalInfo','U') is not null
		begin
			drop table PersonalInfo
		end

	create table PersonalInfo
	(
		Id int primary key identity,
		Name nvarchar(80) not null,
		Birthday date not null default (getdate())
	)
	declare @i int =0,@name nvarchar(80),@lastName nvarchar(80),@firstName nvarchar(80)
	while @i<@rowCount
		begin
			exec proc_GererateRandLastName @lastName output
			exec proc_GererateRandFirstName @firstName output
			set @name=@lastName+@firstName
			insert into PersonalInfo (Name) values (@name)
			set @i=@i+1
		end
	set nocount off
end
go


exec proc_CreatePersonalInfoAndInsert 10000000  ---查询插入10000000条数据

select * from PersonalInfo where Name='赵棋'---查询

-- 创建一个随机获得姓氏的存储过程或者函数  
go
alter proc proc_GererateRandLastName
@name nvarchar(80) output
as
begin
	declare @str nvarchar(80)='赵钱孙李周吴郑王冯陈褚卫蒋沈韩杨朱秦尤许何吕施张孔曹严华金魏陶姜戚谢邹喻柏水窦章云苏潘葛奚范彭郎鲁韦昌马苗凤花方俞任袁柳酆鲍史唐费廉岑薛雷贺倪汤滕殷罗毕郝邬安常乐于时傅皮卞齐康伍余元卜顾孟平黄和穆萧尹姚邵湛汪祁毛禹狄米贝明臧计伏成戴谈宋茅庞熊纪舒屈项祝董梁杜阮蓝闵席季麻强贾路娄危江童颜郭梅盛林刁钟徐邱骆高夏蔡田樊胡凌霍虞万支柯昝管卢莫经房裘缪干解应宗丁宣贲邓郁单杭洪包诸左石崔吉钮龚程嵇邢滑裴陆荣翁荀羊於惠甄曲家封芮羿储靳汲邴糜松井段富巫乌焦巴弓牧隗山谷车侯宓蓬全郗班仰秋仲伊宫宁仇栾暴甘钭厉戎祖武符刘景詹束龙叶幸司韶郜黎蓟薄印宿白怀蒲邰从鄂索咸籍赖卓蔺屠蒙池乔阴鬱胥能苍双闻莘党翟谭贡劳逄姬申扶堵冉宰郦雍郤璩桑桂濮牛寿通边扈燕冀郏浦尚农温别庄晏柴瞿阎充慕连茹习宦艾鱼容向古易慎戈廖庾终暨居衡步都耿满弘匡国文寇广禄阙东欧殳沃利蔚越夔隆师巩厍聂晁勾敖融冷訾辛阚那简饶空曾毋沙乜养鞠须丰巢关蒯相查后荆红游竺权逯盖益桓公万俟司马上官欧阳夏侯诸葛闻人东方赫连皇甫尉迟公羊澹台公冶宗政濮阳淳于单于太叔申屠公孙仲孙轩辕令狐钟离宇文长孙慕容鲜于闾丘司徒司空丌官司寇仉督子车颛孙端木巫马公西漆雕乐正壤驷公良拓跋夹谷宰父谷梁晋楚闫法汝鄢涂钦段干百里东郭南门呼延归海羊舌微生岳帅缑亢况郈有琴梁丘左丘东门西门商牟佘佴伯赏南宫墨哈谯笪年爱阳佟第五言福百家姓终'
	-- select len(@str)
	declare @rndIndex int=rand()*len(@str)
	
	set @name=substring(@str,@rndIndex,1)
	
end

go

declare @res nvarchar(80)
exec proc_GererateRandLastName @res output
select @res


go
alter proc proc_GererateRandFirstName
@name nvarchar(80) output
as
begin
	declare @str nvarchar(80)='东西南北中春夏秋冬梅兰菊竹上下左右宇宙洪荒宝灵天地琴棋书画晨霞彩风霜雪雨雷闪磊涯海水木林森树草花果德智体美劳礼仪信谦廉素愿薇采财旺福美寿康安平健贤思年幕曼蒂因增禄观伟刚勇毅俊峰强军平保东文辉力明永健世广志义兴良海山仁波宁贵福生龙元全国胜学祥才发武新利清飞彬富顺信子杰涛昌成康星光天达安岩中茂进林有坚和彪博诚先敬震振壮会思群豪心邦承乐绍功松善厚庆磊民友裕河哲江超浩亮政谦亨奇固之轮翰朗伯宏言若鸣朋斌梁栋维启克伦翔旭鹏泽晨辰士以建家致树炎德行时泰盛雄琛钧晨轩清睿宝涛华国亮新凯志明伟嘉东洪建文子云杰兴友才振辰航达鹏宇衡佳强宁丰波森学民永翔鸿海飞义生凡连良乐勇辉龙川宏谦锋双霆玉智增名进德聚军兵忠廷先江昌政君泽超信腾恒礼元磊阳月士洋欣升恩迅科富函业胜震福瀚瑞朔津韵荣为诚斌广庆成峰可健英功冬锦立正禾平旭同全豪源安顺帆向雄材利希风林奇 易来咏岩启坤昊朋和纪艺昭映威奎帅星春营章高伦庭蔚益城牧钊刚洲家晗迎罡浩景珂策皓栋起棠登越盛语钧亿基理采备纶献维瑜齐凤毅谊贤逸卫万臻儒钢洁霖隆远聪耀誉继珑哲岚舜钦琛金彰亭泓蒙祥意鑫朗晟晓晔融谋宪励璟骏颜焘垒尚镇济雨蕾韬选议曦奕彦虹宣蓝冠谱泰泊跃韦怡骁俊沣骅歌畅与圣铭溓滔溪巩影锐展笑祖时略敖堂崊绍崇悦邦望尧珺然涵博淼琪群驰照传诗靖会力大山之中方仁世梓竹至充亦丞州言佚序宜秀娟英华慧巧美娜静淑惠珠翠雅芝玉萍红娥玲芬芳燕彩春菊兰凤洁梅琳素云莲真环雪荣爱妹霞香月莺媛艳瑞凡佳嘉琼勤珍贞莉桂娣叶璧璐娅琦晶妍茜秋珊莎锦黛青倩婷姣婉娴瑾颖露瑶怡婵雁蓓纨仪荷丹蓉眉君琴蕊薇菁梦岚苑筠柔竹霭凝晓欢霄枫芸菲寒欣滢伊亚宜可姬舒影荔枝思丽秀飘育馥琦晶妍茜秋珊莎锦黛青倩婷宁蓓纨苑婕馨瑗琰韵融园艺咏卿聪澜纯毓悦昭冰爽琬茗羽希婷倩睿瑜嘉君盈男萱雨乐欣悦雯晨珺月雪秀晓然冰新淑玟萌凝文展露静智丹宁颖平佳玲彤芸莉璐云聆芝娟超香英菲涓洁萍蓉潞笑迪敏靓菁慧涵韵琳燕依妙美宜尚诗钰娜仪娇谊语彩清好睻曼蔓茜沁韶舒盛越琪霞艺函迎虹爽瑞珏桐筱苹莹名晗甜晴亭吉玉晶妍凤蒙霖希宣昕丽心可旻阳真蓝畅荣岚乔育芷姿妹姗瑾奕兰航蕾艳怡青珊才小子允加巧冉北朵多羽如帆伶采西贝其春易咏亚明秋泓伦哲益轩容玹津启婧晟婉常浩景茗尧雅杰媛诒翔为捷钧毓意琸靖渺熙微祺梦赫菡纶铭齐华菏毅瑶品梓国卿振卫叶亿娆漫兴蓓融嫒锦科润霏灿忆聪怿蕊谨丰丛璇议馨瀚潇莺珑俪骄骁灵忻昭金昊志辰安凡禾竹愉丫珂洺苒若偌珮棋淇群会维影逸娴赏霄辉莲优瑷朦涛'


		--select len(@str)            ---随机取出名字的长度
	declare @rndNameLen int = convert(int,rand()*2)+1  --创建出随机出名字   convert吧int类型转换成string类型
	declare @rndIndex1 int,@rndIndex2 int--定义

	-- 判断名字的数量为1则取单名；否则取双名
	if @rndNameLen=1
		begin
			set @rndIndex1=rand()*len(@str)
			set @name=SUBSTRING(@str,@rndIndex1,1)
		end
	else
		begin
			set @rndIndex1=rand()*len(@str)
			set @rndIndex2=rand()*len(@str)
			set @name=SUBSTRING(@str,@rndIndex1,1)+SUBSTRING(@str,@rndIndex2,1)
		end
end
go

declare @res nvarchar(80)
exec proc_GererateRandFirstName @res output---随机出名字
select @res
```

##  索引的优点：

1、大大加快数据的检索速度;

2、创建唯一性索引，保证数据库表中每一行数据的唯一性;

3、加速表和表之间的连接;

4、在使用分组和排序子句进行数据检索时，可以显著减少查询中分组和排序的时间。

缺点：

1、索引需要占物理空间。

2、当对表中的数据进行增加、删除和修改的时候，索引也要动态的维护，降低了数据的维护速度。

