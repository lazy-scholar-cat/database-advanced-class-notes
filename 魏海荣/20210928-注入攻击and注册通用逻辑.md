# 2021-09-28 又是懵逼的一天

## Sql注入攻击
> 什么是注入攻击？

> 所谓SQL注入式攻击，就是攻击者把SQL命令插入到Web表单的输入域或页面请求的查询字符串，欺骗服务器执行恶意的SQL命令。在某些表单中，用户输入的内容直接用来构造（或者影响）动态SQL命令，或作为存储过程的输入参数，这类表单特别容易受到SQL注入式攻击。
常见的SQL注入式攻击过程类如：
⑴ 某个ASP.NET Web应用有一个登录页面，这个登录页面控制着用户是否有权访问应用，它要求用户输入一个名称和密码。
⑵ 登录页面中输入的内容将直接用来构造动态的SQL命令，或者直接用作存储过程的参数。下面是ASP.NET应用构造查询的一个例子：
System.Text.StringBuilder query = new System.Text.StringBuilder（"SELECT * from Users WHERE login = '"）。Append（txtLogin.Text）。Append（"' AND password='"）。Append（txtPassword.Text）。Append（"'"）；
⑶ 攻击者在用户名字和密码输入框中输入"'或'1'='1"之类的内容。
⑷ 用户输入的内容提交给服务器之后，服务器运行上面的ASP.NET代码构造出查询用户的SQL命令，但由于攻击者输入的内容非常特殊，所以最后得到的SQL命令变成：SELECT * from Users WHERE login = '' or '1'='1' AND password = '' or '1'='1'.
⑸ 服务器执行查询或存储过程，将用户输入的身份信息和服务器中保存的身份信息进行对比。
⑹ 由于SQL命令实际上已被注入式攻击修改，已经不能真正验证用户身份，所以系统会错误地授权给攻击者。
如果攻击者知道应用会将表单中输入的内容直接用于验证身份的查询，他就会尝试输入某些特殊的SQL字符串篡改查询改变其原来的功能，欺骗系统授予访问权限。
系统环境不同，攻击者可能造成的损害也不同，这主要由应用访问数据库的安全权限决定。如果用户的帐户具有管理员或其他比较高级的权限，攻击者就可能对数据库的表执行各种他想要做的操作，包括添加、删除或更新数据，甚至可能直接删除表。
~~~
/*
declare @password nvarchar(80) = \'1' or 1=1\`


select * from Users
where (Username='admin' and Password='1') or 1=1


*/
~~~

## 注册通用逻辑（无关数据库）
> 1. 判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走

~~~
/*
declare @username nvarchar(80)
declare @tmpTable table (id int,Username nvarchar(80),Password nvarchar(80))
declare @count int

insert into @tmpTable
select @count=count(*) from Users where Username=@username

if(@count>0)
	begin
	-- 做点什么 如提示消息或者设置一些数值
		
	end
else
	begin


	end
*/

/*
定义用户名变量=传输过来的用户名
根据用户名变量查询用户表中有没有同名的记录

如果有，则提示不能注册，返回消息
如果没有，则可以继续往下

*/
~~~

>2. 判断密码和重复密码是否一致，是则可以注册，并在相应数据表中插入一条记录，否则返回失败信息

## 商场信息的使用场景

~~~
use VIPsystem
go

--关于商场信息的使用场景

--1.选择会员信息管理的场景

select a.商场编号,a.商场名称 from 商场信息 a

-- 2.在会员卡类型管理的时候，需要选择商场

select a.商场编号,a.商场名称 from 商场信息 a

--3.商场本身的CRUD create、read、update、delete

insert into 商场信息 values('WD003','万达广场(厦门集美店)','厦门市集美区银江路',' 0592-3602111')

update 商场信息 set 地址='厦门市集美区银江路137号'

delete from 商场信息 where 商场编号='WD003'
~~~