# 数据库存储

## 1.存储语句


![图示!](./imgs/1/2021-10-04_162304.png)

## 2.无参和有参存储过程


![图示!](./imgs/1/2021-10-04_172208.png)

## 3.多参和有默认值的储过程


![图示!](./imgs/1/2021-10-04_172641.png)

## 4.最多可有参数2100个


# 1. 输出
## 1.语法如下图


![图示!](./imgs/1/2021-10-05_103933.png)

```sql
-- 语法 

create proc proc_StudentInfo
@code nvarchar(80) output
as
begin
	select @code
	select @code=StudentCode from StudentInfo where Id=1
	select @code
end

go

declare @anyCode nvarchar(80)='我和我的祖国'
exec proc_StudentInfo @anyCode
select @anyCode

go

-- 修改存储过程语法
alter proc proc_StudentInfo
@code nvarchar(80) output
as
begin
	select @code
	select @code=StudentCode from StudentInfo where Id=1
	select @code
end

go
-- 删除命令
drop proc proc_StudentInfo

go


-- 多参返回值
create proc proc_StudentInfo
@code nvarchar(80) output,
@name nvarchar(80) output
as
begin
	select @code,@name
	select @code=StudentCode,@name=StudentName from StudentInfo where Id=1
	select @code,@name
end


go

declare @anyCode nvarchar(80),@anyName nvarchar(80)
exec proc_StudentInfo @anyCode output,@anyName output
select @anyCode,@anyName
go 
```


# 2. 自定义函数之标量函数

```sql
create function <方法名称>
(
	[
		@XXXX 数据类型 [=默认值],
		@YYYY 数据类型 [=默认值]
	]
)
returns int -- 表明返回会值是一个int的值
as
begin
	
	declare @result int

	set @result=1

	return @result

end
go

create function fn_GetInt
(

)
returns int -- 表明返回会值是一个int的值
as
begin
	
	declare @result int

	set @result=1

	return @result

end

go
```
```
-- 如何执行一个标题函数

-- 下面这个对不对？
select fn_GetInt() ,（错误）
--正确书写方式 
select dbo.fn_GetInt() 

因为在调用  SQL 标量值函数时，应该在函数前面加上 "dbo."，否则会报 “不是可以识别的内置函数名称”错误。
```