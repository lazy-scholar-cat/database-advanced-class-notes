```
-- 触发器 是一种特殊存储过程 它不由用户直接手动执行，而是当一些事件发生后，自动被调用

-- 作用 
一、触发器是一个具有记忆功能的，具有两个稳定状态的信息存储器件，是构成多种时序电路的最基本逻辑单元，也是数字逻辑电路中一种重要的单元电路。

在数字系统和计算机中有着广泛的应用。触发器具有两个稳定状态，即“0”和“1”，在一定的外界信号作用下，可以从一个稳定状态翻转到另一个稳定状态。

二、触发器的作用

1、可在写入数据表前，强制检验或转换数据。

2、触发器发生错误时，异动的结果会被撤销。

3、部分数据库管理系统可以针对数据定义语言（DDL）使用触发器，称为DDL触发器。

4、可依照特定的情况，替换异动的指令 (INSTEAD OF)。
5、维护数据的有效性和完整性 ，简化逻辑
```
/*
语法：
```sql
create trigger <触发器名称>
on <数据表>
<after | instead of> <delete | insert | update>
as
begin

end

*/

go

alter trigger tr_TmpRankForUpdate
on tmpRank
after
 Update 
as
begin
	declare @score int,@id int
	select @id=id, @score=Score from inserted
	if (@score>=0 and @score<=100)
		begin
			update tmpRank set Score=@score where Id=@id
		end
	else
		begin
			update tmpRank set Score=0 where Id=@id
		end
end

go


select *from tmpRank

update tmpRank set Score=-2 where Id=1

```


	存储过程当中特有的两张临时表：

	inserted:插入数据时，新插入的数据在inserted表中；更新的时候，更新后的数据在inserted中；
	deleted:更新的时候，deletd表中的数据是之前的旧的数据；在删除的时候，deleted中的数据是被删除的数据；
  ```sql 

after 和 instead of 
1.
create  trigger AA
on aa
after 
update 
as
begin
declare @score int,@id int
select @id=id, @score=Score from inserted
select * from inserted--插入或更新数据后的表
select * from deleted--插入或更新数据前的表

end

go
update aa set Score=-2 where Id=1

2.
alter trigger AA
on student
instead of insert
as 
begin 
		declare @name nvarchar(20),@sex nvarchar(20),@score nvarchar(20)
		select @name=Name,@sex=Sex , @score=Score  from inserted
if @score>=0 and @score<=100
		begin 
		insert into Student(name,sex,score)values(@name,@sex,@score)
end
else
		begin
		print '成绩输入有误'
end


end
go
insert into Student(name,sex,score)values ('小天','男',110)


  ```
## inserted 和 delete区别
```
			insert		              update		           delete
inserted   新插入的记录情况        更新后的记录情况                无


deleted    无                     更新前的记录情况               删除前的记录情况
```