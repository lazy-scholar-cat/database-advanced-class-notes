2021.10.03 
  ```
	--登陆

	--用户登陆，根据数据库里面已有的信息来查询用户是否存在，是，则登陆；否，则需要注册用户

	select * from userInfo where cardID='NO0001' and customerName='逍遥散人'

	---更进一步，就是当登陆的用户可能已被注销、禁用等情况，如何应对
	--1.先在数据表中删除被禁用、注销的用户名
	delete from userInfo where cardID='NO0001'
	--2.然后重新注册用户
	insert into userInfo values('NO0001','逍遥散人','1990-02-16','天津市','15896316990','365412698813369715','男')
	
```
    ## sql注入，一种攻击手段
```
	/*
	declare @username nvarchar(80),@password nvarchar(80),@rowpassword nvarchar(80)
	declare @isDeleted nvarchar(80),@isActuved bit
	declare @count int
	declare @res nvarchar(80)
	select @count=COUNT(*),@isDeleted=IsDeleted,@isActuved=IsActuved,@rowpassword=Password from Users
	where Username=@username--如果根据提供的用户名，找到的记录数不为零，则表明用户名是对的，可以继续往下判断；否则直接提示用户名或密码错误
 if(@count>0)
	begin
		if(@isDeleted=1) --如果删除标记为真，则表明当前用户名已经被删除，做出提示；否则用户未被删除
			begin
				set @res='该用户已经被删除！请联系管理员！'
					end
				else
				begin
					if(@isActived=0)  --如果当前用户未启用/激活，则提示信息用户未启用，登陆失败；否则用户已经禁用
						begin
							set @res='该用户已被禁用！请联系管理员！'
						end
					else
						begin
							set @result='登录成功！'
						end
					else
						begin
							if (@password=@rowpassword) --如果传进来的密码和在记录中获取的密码相等，则登陆成功；反之，则登录失败
								begin				
									set @res='登录成功！'
								end
							else
								begin
									set @res='用户名或密码有误！请确认后重试'
								end
							else
								begin
						
						end
						
					end
					
				end
			
	end
						*/
```
```
	/*
	declare @password nvarchar(80) = \'1' or 1=1\`

	select * from Users
	where (Username='admin' and Password='1') or 1=1

	*/

```
	## 注册
```
	--单个注册
	
	insert into userInfo values('NO0001','逍遥散人','1990-02-16','天津市','15896316990','365412698813369715','男')

	--批量注册
	
	insert into userInfo values('NO0001','逍遥散人','1990-02-16','天津市','15896316990','365412698813369715','男'),('NO0002','王老菊  ','1994-10-16','上海市','13604569897','398622998553698821','男')
```
	--注册通用逻辑(无关数据库)

	-- 1.判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息，否则可以继续往下走
 ```   
	/*
	declare @customerName nvarchar(50)
	declare @userInfo table (cardID  nvarchar(50) ,customerName nvarchar(50))
	declare @count int

	insert into @userInfo
	
	select @count=count(*) from @userInfo where customerName=customerName

	if(@count>0)
	begin
	-- 做点什么 如提示消息或者设置一些数值
		
	end
	else
	begin
	end
	*/
 ```  
	--2.判断密码和重真密码是否一致，是可以注册，并在相应数据表中插入一条记录，否则返回失败信息
 
	/*
	delare @username nvarchar(80) --前端传进来的用户名
	delare @password nvarchar(80),@cofirmPassword nvarchar(80) --前端传进来的密码
	delare @rowPassword nvarchar(80) --是指通过用户名查询到记录后，这个代表用户的记录中的密码
	delare @tmpTable table (id int,Username nvarchar(80),Password nvarchar(80)) -
	delare @count int 
	delare @res nvarchar() 

	insert into @tmpTable
	
	select @count=count(*) from @rowPassword=Password from Users where Username=@username

	if(@count>0)
	begin
	-- 做点什么 如提示消息或者设置一些数值
		set @res='当前用户名已经被注册，请确认后重试'

		end
	else
		begin
			insert into Users(Username,Password) values(@username,@password)
			set @res='用户注册成功！'
		end
	else
		begin
			set @res='两次输入的密码不一致，请确认后重试！'
		end


	*/
     ```
		--关于如何不让输入的用户名和密码是空格


	declare @username nvarchar(80),@password nvarchar(80),@rowpassword nvarchar(80)
	declare @isDeleted nvarchar(80),@isActuved bit
	declare @count int
	declare @res nvarchar(80)
	
	select @count=COUNT(*),@isDeleted=IsDeleted,@isActuved=IsActuved,@rowpassword=Password from Users
	where Username=@username
	if(charindex(' ',@username) > 0 or charindex(' ',@Password) > 0)
	begin
    set @res='用户名或密码出现空格，登陆不成功'
    end



--	关于商场信息的使用场景;
```
--1.在会员信息管理的时候，需要选择商场

select shopsID,shopsName,shopsPhone,shopsAddress from Shop
--2.在会员卡类型管理的时候，需要选择商场

select shopsID,shopsName,shopsPhone,shopsAddress from Shop
--3.商场信息本身的CRIUD (create read update delete)
