## 参数
存储过程通过其参数与调用程序通讯。当程序执行存储过程时，可通过存储过程的参数向该存储过程传递值。这些值可作为 Transact-SQL 编程语言中的标准变量使用。存储过程也可通过 OUTPUT 参数将值返回至调用程序。一个存储过程可有多达 2100 个参数，每个参数都有名称、数据类型、方向和默认值


## 存储过程命名规范
句法：

存储过程的命名有这个的语法：
①[proc]  ②[MainTableName] By ③[FieldName(optional)] ④[Action]

[①] 所有的存储过程必须有前缀“proc_”，所有的系统存储过程都有前缀“sp_”。

注释：假如存储过程以sp_ 为前缀开始命名那么会运行的稍微的缓慢，这是因为SQL Server将首先查找系统存储过程。

[②] 表名就是存储过程主要访问的对象。

[③]] 可选字段名就是条件子句。比如： proc_UserInfoByUserIDSelect

[④] 最后的行为动词就是存储过程要执行的任务。

如果存储过程返回一条记录那么后缀是：Select,
如果存储过程插入数据那么后缀是：Insert,
如果存储过程更新数据那么后缀是：Update,
如果存储过程有插入和更新那么后缀是：Save,
如果存储过程删除数据那么后缀是：Delete,
如果存储过程更新表中的数据 (ie. drop and create) 那么后缀是：Create,
如果存储过程返回输出参数或0，那么后缀是：Output.

## 存储语法

一、使用T-SQL创建储存过程，其语法如下。
```
create proc<存储过程名>
{@参数1 数据类型}[=默认值][output],...
{@参数n 数据类型}[=默认值][output]
as
[begin]
T-SQL语句
[end]
```
使用T-SQL调用储存过程，语法如下。
```
exec <存储过程名称>
[参数表]
```
[例1]
```
create proc proc_StudentInfoSelect
as
begin
	select * from StudentInfo
end
go

exec proc_StudentInfoSelect

go
```
[例2]
使用T-SQL创建与执行带有输入参数的存储过程
```
create proc proc_StudentInfoByStudentNameSelect
@name nvarchar(80)
as
begin
select * from StudentInfo
where StudentName like @name
end
go

exec proc_SelectStudentInfoWithPrameter '%李%'


go
```
[例3]
使用T-SQL创建与执行带有输入多个参数的存储过程

```

create proc proc_StudentInfoBySelect
@name nvarchar(80),
@code nvarchar(80)
as
begin
select * from StudentInfo
where StudentName like @name or StudentCode like @code

end
go

exec proc_SelectStudentInfoWithSomeParameters '张三',''

go
```

[例4]
使用T-SQL创建与执行带有默认值的的存储过程
```
create proc proc_SelectStudentInfoWithAnyParameters
@code nvarchar(80)='01',
@name nvarchar(80)='%李%',
@birthday date='2021-10-03',
@sex char='m',
@classId int='1'
as
begin
select * from StudentInfo
where StudentCode like @code 
or StudentName like @name
or Birthday<@birthday
or sex =@sex
or ClassId= @classId

end
go

exec proc_SelectStudentInfoWithAnyParameters

go
```

