# 替换型触发器和索引

## 替换型触发器

插入时：
```
alter trigger tr_StudentInfoForInsert
on StudentCourseScore
after 
	insert 
as
begin 
	select * from inserted 
	select * from deleted
end
go

insert into  StudentCourseScore(StudentId,CourseId,Score) values (26,2,56)

go

```

更新时

```
create trigger tr_StudentInfoForUpdate 
on StudentCourseScore
after
	update
as
begin
	select * from inserted
	select * from deleted

end
go
update StudentCourseScore set Score=56 where CourseId=2 and studentId=27

```
删除时

```

go
create trigger tr_StudentForDelete
on StudentCourseScore
after delete
as
begin
	select * from inserted
	select * from deleted
end
go

delete from StudentCourseScore where CourseId=22 and studentId=2

```

/*
			insert		              update		           delete
inserted   新插入的记录情况       更新后的记录情况               无


deleted    无                     更新前的记录情况             删除前的记录情况

*/

## 触发器之instead of

 instead of 是替换执行 insert update delete中的语句。换句话说，增删改在instead of存在的时候，并不会真正被执行，真正执行的其实是被触发的那个触发器

```
create trigger tr_TmpRankForInsert
on StudentCourseScore
instead of
	insert 
as
begin
	declare @score int ,@studentId int,@courseId int 
	select @studentId=StudentId,@courseId =CourseId,@score=Score  from inserted 
	if @score>=0 and @score <=100
	begin
		insert into StudentCourseScore(StudentId,CourseId ,Score ) values (@studentId ,@courseId ,@score )
	end
else 
	begin 
		print'成绩不在0到100的范围内，请确认后重试'
	end

	end

	go
	
	insert into  StudentCourseScore(StudentId,CourseId ,Score ) values (22 ,1 ,100 )
 ```
   

   ##  SET NOCOUNT { ON | OFF } 
   （一长串我没看懂就对了...）

   在存储过程中，经常用到SET NOCOUNT ON；

作用：阻止在结果集中返回显示受T-SQL语句或则usp影响的行计数信息。
当SET ONCOUNT ON时候，不返回计数，当SET NOCOUNT OFF时候，返回计数；

即使当SET NOCOUNT ON 时候，也更新@@RowCount；

当SET NOCOUNT on时候，将不向客户端发送存储过程每个语句的DONE_IN_proc消息，如果存储过程中包含一些并不返回实际数据的语句，网络通信流量便会大量减少，可以显著提高应用程序性能；

SET NOCOUNT 指定的设置时在执行或运行时候生效，分析时候不生效。

练习2：创建函数，可以根据输入的参数，随机构造若干条数据，插入到指定的表
```
alter procedure dbo.RandSelect
(
 @number int --输入参数(要生成的记录条数)
)
as
 declare @tableString varchar(20)
 
 create table #1(Id int ,StudentId int,CourseId int,Score varchar(50)) --存放随机生成的记录(临时表)
 create table #2(rand_num int) --存入已放入#1的记录的StudentId(实现不生成相同的两条记录)

 declare  @id int ,@studentId int , @score varchar(50),@courseid int , @randnum int , @rowcount int , @i int 
 
 select @rowcount = (select count(*) from StudentCourseScore)
 if @number<@rowcount
  begin
   set @i = @number   --要求随机生成的记录条数(输入参数)
  end
 else
  begin
   set @i = @rowcount
  end

 declare c cursor scroll  for select * from StudentCourseScore
 open c
 
 while(@i>0) 
 begin

  declare @tempoid int--记录生成生的随机数是否重复
  set @randnum = rand()*@rowcount+1
  set @tempoid = (select count(*) from #2 where rand_num = @randnum)
  while( @tempoid <>0 )
   begin
    set @randnum = rand()*@rowcount+1
    set @tempoid = (select count(*) from #2 where rand_num = @randnum)
   end
  
  fetch absolute @randnum from c into @id ,@studentId, @score ,@courseid 
  insert into #1 values(@id ,@studentId, @score ,@courseid )
  insert into #2 values(@randnum)
  set @i = @i-1
 end

 close c
 deallocate c

 select * from #1
 drop table #1
 drop table #2
GO

exec RandSelect @number=150
```
## 索引及其语法

　　索引是一种快速访问数据的途径，可提高数据库性能。索引使数据库程序无须对整个表进行扫描，就可以在其中找到所需的数据，就像书的目录，可以快速查找所需的信息，无须阅读整本书。
	索引 是为了加快查询效率

	索引分成两类：聚集索引和非聚集索引

	聚集索引：就是数据存储的顺序和索引的顺序一致
	非聚集索引：数据存储的顺序和索引未必一致


    索引的缺点：

       1、索引占用表空间，创建太多索引可能会造成索引冗余。

       2、索引会影响INSERT、UPDATE、DELETE语句的性能。

　　使用索引的原则：　　　　

　　1、装载数据后再建立索引。

　　2、频繁搜索的列可以作为索引。

　　3、在联接属性上建立索引（主外键）。

　　4、经常排序分组的列。

 	5、删除不经常使用的索引。

　　6、指定索引块的参数，如果将来会在表上执行大量的insert操作，建立索引时设定较大的ptcfree。

　　7、指定索引所在的表空间，将表和索引放在不同的表空间上可以提高性能。

　　8、对大型索引，考试使用NOLOGGING子句创建大型索引。

　　不要在下面的列创建索引：　　　

　　1、仅包含几个不同值得列。

　　2、表中仅包含几条记录。
语法：
```
create [unique] 
[clustered | nonclustered] index <索引的名称>
on 数据表(字段1 [asc | desc],字段2 [asc | desc])

```

