# SQL的查询思想
接
## 一、连接查询

### 1、内部连接查询

####   内部连接基本语法：
            在from的子句中使用inner（可省略） join来实现语法格式如下：

        select +需要显示的字段(可以自行备注“列名”) +from 表1  inner join +表2 on +条件表达式

        （例如：将StudentInfo表和Stu-Score表连接起来，需要在on后面加上
        StudentInfo.ID=Stu-Score.ID）
####   带条件的内部连接
        表连接好后使用 where+条件即可
        例如：select * from StudentInfo join Stu-Score on  StudentInfo.ID=Stu-Score.ID where Stu-Score.Score>=75
### 2、外部连接查询
#### 定义：
        在内部连接中，参与连接的表的地位是平等的。而在外部连接中，参与连接的表有主从之分，以主表的每行数据去匹配从表数据列，符合连接条件的数据将直接返回到结果集中，不符合连接条件的列将被填补上Null值后再返回到结果集中。
####  分类：
        外部连接分为左外部连接和右外部连接
####  外部连接基本语法：
        select +字段列表 +from +表1 （left\right）join 表2 on 条件表达式