## 5.1数据查询概述
        查询分为 两大类：一类是用于数据检索的选择查询。一类是用于数据更新的行为查询。
        SQL查询包括选择列表、from子句、where子句，它们分别说明所查询列、查询的表或视图和
        搜索条件。
        基本语法：select  *  from  表名   where  条件表达式
## 5.2使用字段列表指定输出字段
        1、选取全部字段：在select子句中使用一个 * 号就可以了
        2、选取部分字段：在select子句中输入需要的字段，每个字段之间用逗号分隔，顺序自定
        3、设置字段别名：
            * 原字段名 AS 字段别名
            * 字段别名=原字段名
            * 原字段名 字段别名
        4、字段的计算：为了满足实际应用的需要。例如客户需要类似“性别-学员姓名”的格式查看数据。此时可以用到字段的计算。
       即： select stuSex + '-' + stuName from stuInfo
## 5.3使用选择关键字限制记录行数
        1、使用all关键字返回全部记录：默认使用all关键字
        2、使用distinct关键字过滤重复记录：在字段列表前加关键字distinct
           select distinct 字段名 from 表名

           **技巧：可以配合子查询使用。
        3、使用TOP关键字仅显示前面若干条记录
          *  在字段列表前加TOP n 显示前n条记录
          *  在字段列表前加TOP n percent显示前n%的记录条数
## 5.4对查询记录的选择与处理

 ### 5.4.1 对查询结果筛选  
            1、使用where子句：用户筛选记录时，需要在select语句中加入条件，这时会用到where子句。where子句可包括以下几种条件运算符。
            * 比较运算符：大小比较，包括(>、>=、=、<、<=、<>、!>、!<)    
            例子：select *from stuInfo where stuSex='男'
            或：  select *from stuInfo where stuAge>=18
            
            2、范围运算符：
            **  between XX and XX  ()
            例子：查询学号在2~4的同学的信息
            select  *from stuInfo where stuID between 2 and  4

            **  not between XX and XX  ()
            例子：查询学号不在2~4的同学的信息
            select  *from stuInfo where stuID  not between 2 and  4
            
            3、列表运算符：
            **  in（项1，项2……）
            例子：查询学号为1、3、5的学员信息
            select *from stuInfo where stuID in(1,3,5……)

            **   not in（项1，项2……）
            例子：查询学号不为1、3、5的学员信息
            select *from stuInfo where stuID in(1,3,5……)

            4、空值判断符（判断表达式是否为空）：