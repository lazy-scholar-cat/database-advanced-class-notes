USE [master]
GO
/****** Object:  Database [Mall VIP management system]    Script Date: 2021/9/22 21:00:25 ******/
CREATE DATABASE [Mall VIP management system]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Mall VIP management system', FILENAME = N'E:\数据库高级\Mall VIP management system\Mall VIP management system.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Mall VIP management system_log', FILENAME = N'E:\数据库高级\Mall VIP management system\Mall VIP management system_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Mall VIP management system] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Mall VIP management system].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Mall VIP management system] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Mall VIP management system] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Mall VIP management system] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Mall VIP management system] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Mall VIP management system] SET ARITHABORT OFF 
GO
ALTER DATABASE [Mall VIP management system] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Mall VIP management system] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Mall VIP management system] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Mall VIP management system] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Mall VIP management system] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Mall VIP management system] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Mall VIP management system] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Mall VIP management system] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Mall VIP management system] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Mall VIP management system] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Mall VIP management system] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Mall VIP management system] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Mall VIP management system] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Mall VIP management system] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Mall VIP management system] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Mall VIP management system] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Mall VIP management system] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Mall VIP management system] SET RECOVERY FULL 
GO
ALTER DATABASE [Mall VIP management system] SET  MULTI_USER 
GO
ALTER DATABASE [Mall VIP management system] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Mall VIP management system] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Mall VIP management system] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Mall VIP management system] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Mall VIP management system] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Mall VIP management system', N'ON'
GO
USE [Mall VIP management system]
GO
/****** Object:  Table [dbo].[Carinfo]    Script Date: 2021/9/22 21:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Carinfo](
	[carId] [nchar](10) NOT NULL,
	[Vip _Cid] [nchar](10) NOT NULL,
	[会员编号] [nchar](10) NOT NULL,
	[车辆牌号] [nchar](10) NULL,
	[免费停放时长] [nchar](10) NULL,
 CONSTRAINT [PK_Carinfo] PRIMARY KEY CLUSTERED 
(
	[carId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[grade Info]    Script Date: 2021/9/22 21:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[grade Info](
	[gradeId] [nchar](10) NOT NULL,
	[Vip_Cid] [nchar](10) NOT NULL,
	[grade] [int] NOT NULL,
 CONSTRAINT [PK_grade Info] PRIMARY KEY CLUSTERED 
(
	[gradeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Loseinfo]    Script Date: 2021/9/22 21:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Loseinfo](
	[loseID] [nchar](10) NOT NULL,
	[Vip_Cid] [nchar](10) NOT NULL,
	[会员编号] [nchar](10) NOT NULL,
	[挂失时间] [datetime] NOT NULL,
	[目前状态] [nchar](10) NOT NULL,
 CONSTRAINT [PK_Loseinfo] PRIMARY KEY CLUSTERED 
(
	[loseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Mallinfo]    Script Date: 2021/9/22 21:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mallinfo](
	[Mallid] [nchar](10) NOT NULL,
	[MallName] [nchar](10) NOT NULL,
	[short_name] [nchar](10) NULL,
	[注册地址] [nvarchar](50) NOT NULL,
	[注册法人] [nchar](10) NOT NULL,
	[办公电话] [nvarchar](11) NOT NULL,
 CONSTRAINT [PK_Mallinfo] PRIMARY KEY CLUSTERED 
(
	[Mallid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Shopinfo]    Script Date: 2021/9/22 21:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shopinfo](
	[Shopid] [nchar](10) NOT NULL,
	[Mallids] [nchar](10) NOT NULL,
	[ShopName] [nchar](10) NOT NULL,
	[Shoptype] [nchar](10) NULL,
	[管理人] [nchar](10) NOT NULL,
	[管理人电话] [nvarchar](11) NOT NULL,
	[经营人] [nchar](10) NOT NULL,
	[经营人电话] [nvarchar](11) NOT NULL,
	[开店日期] [datetime] NULL,
 CONSTRAINT [PK_Shopinfo] PRIMARY KEY CLUSTERED 
(
	[Shopid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[typeInfo]    Script Date: 2021/9/22 21:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[typeInfo](
	[TypeID] [nchar](10) NOT NULL,
	[TypeName] [nchar](10) NOT NULL,
	[专享优惠] [nvarchar](50) NULL,
	[Vip_Cid] [nchar](10) NOT NULL,
 CONSTRAINT [PK_typeInfo] PRIMARY KEY CLUSTERED 
(
	[TypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Vip_C_info]    Script Date: 2021/9/22 21:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vip_C_info](
	[Vip_Cid] [nchar](10) NOT NULL,
	[会员编号] [nchar](10) NOT NULL,
 CONSTRAINT [PK_Vip_C_info] PRIMARY KEY CLUSTERED 
(
	[Vip_Cid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VIPinfo]    Script Date: 2021/9/22 21:00:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VIPinfo](
	[会员编号] [nchar](10) NOT NULL,
	[会员姓名] [nchar](10) NOT NULL,
	[性别] [nchar](10) NOT NULL,
	[年龄] [nchar](10) NOT NULL,
	[联系方式] [nvarchar](11) NOT NULL,
	[住址] [nvarchar](50) NOT NULL,
	[总消费金额] [money] NULL,
 CONSTRAINT [PK_VIPinfo] PRIMARY KEY CLUSTERED 
(
	[会员编号] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Carinfo]    Script Date: 2021/9/22 21:00:25 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Carinfo] ON [dbo].[Carinfo]
(
	[carId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_Shopinfo]    Script Date: 2021/9/22 21:00:25 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Shopinfo] ON [dbo].[Shopinfo]
(
	[Shopid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Carinfo]  WITH CHECK ADD  CONSTRAINT [FK_Carinfo_Vip_C_info] FOREIGN KEY([Vip _Cid])
REFERENCES [dbo].[Vip_C_info] ([Vip_Cid])
GO
ALTER TABLE [dbo].[Carinfo] CHECK CONSTRAINT [FK_Carinfo_Vip_C_info]
GO
ALTER TABLE [dbo].[Carinfo]  WITH CHECK ADD  CONSTRAINT [FK_Carinfo_VIPinfo] FOREIGN KEY([会员编号])
REFERENCES [dbo].[VIPinfo] ([会员编号])
GO
ALTER TABLE [dbo].[Carinfo] CHECK CONSTRAINT [FK_Carinfo_VIPinfo]
GO
ALTER TABLE [dbo].[grade Info]  WITH CHECK ADD  CONSTRAINT [FK_grade Info_Vip_C_info] FOREIGN KEY([Vip_Cid])
REFERENCES [dbo].[Vip_C_info] ([Vip_Cid])
GO
ALTER TABLE [dbo].[grade Info] CHECK CONSTRAINT [FK_grade Info_Vip_C_info]
GO
ALTER TABLE [dbo].[Loseinfo]  WITH CHECK ADD  CONSTRAINT [FK_Loseinfo_Vip_C_info] FOREIGN KEY([Vip_Cid])
REFERENCES [dbo].[Vip_C_info] ([Vip_Cid])
GO
ALTER TABLE [dbo].[Loseinfo] CHECK CONSTRAINT [FK_Loseinfo_Vip_C_info]
GO
ALTER TABLE [dbo].[Loseinfo]  WITH CHECK ADD  CONSTRAINT [FK_Loseinfo_VIPinfo] FOREIGN KEY([会员编号])
REFERENCES [dbo].[VIPinfo] ([会员编号])
GO
ALTER TABLE [dbo].[Loseinfo] CHECK CONSTRAINT [FK_Loseinfo_VIPinfo]
GO
ALTER TABLE [dbo].[Shopinfo]  WITH CHECK ADD  CONSTRAINT [FK_Shopinfo_Mallinfo] FOREIGN KEY([Mallids])
REFERENCES [dbo].[Mallinfo] ([Mallid])
GO
ALTER TABLE [dbo].[Shopinfo] CHECK CONSTRAINT [FK_Shopinfo_Mallinfo]
GO
ALTER TABLE [dbo].[typeInfo]  WITH CHECK ADD  CONSTRAINT [FK_typeInfo_Vip_C_info] FOREIGN KEY([Vip_Cid])
REFERENCES [dbo].[Vip_C_info] ([Vip_Cid])
GO
ALTER TABLE [dbo].[typeInfo] CHECK CONSTRAINT [FK_typeInfo_Vip_C_info]
GO
ALTER TABLE [dbo].[Vip_C_info]  WITH CHECK ADD  CONSTRAINT [FK_Vip_C_info_VIPinfo] FOREIGN KEY([会员编号])
REFERENCES [dbo].[VIPinfo] ([会员编号])
GO
ALTER TABLE [dbo].[Vip_C_info] CHECK CONSTRAINT [FK_Vip_C_info_VIPinfo]
GO
USE [master]
GO
ALTER DATABASE [Mall VIP management system] SET  READ_WRITE 
GO
