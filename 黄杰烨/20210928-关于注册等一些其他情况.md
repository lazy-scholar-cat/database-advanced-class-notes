## 2021年9月28日 天气:晴
```
--登录

--根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败
select * from UserInfo
where Name='admin' and Password='123'
```
```
--更进一步，就是当前登录的用户可能已经被注销，禁用等情况，如何应对

delete from UserInfo where Name = 'admin02' 

--判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走
declare @username nvarchar(10)
declare @tmpTable1 table (id int,Username nvarchar(80),Password nvarchar(80))
declare @count1 int 

insert into @tmpTable1
select @count1 = count(*) from UserInfo where Name=@username

--if(@count>0)
--	begin
--	-- 做点什么 如提示消息或者设置一些数值
		
--	end
--else
--	begin
```
```
- 2. 判断密码和重复密码是否一致，是则可以注册，并在相应数据表中插入一条记录，否则返回失败信息

-- 关于商场信息 的使用场景

-- 1.在会员信息管理的时候，需要选择商场

select Id,ShoppingMallName,ShortName from ShoppingMallInfo

-- 2.在会员卡类型管理的时候，需要选择商场

select Id,ShoppingMallName,ShortName from ShoppingMallInfo

-- 3. 商场信息本身的CRUD （create read update delete） insert into Market values('万达广场','万达','李四','0599-125746')

update Market set MarketManager='阿萨'

delete from Market where MID=2
```