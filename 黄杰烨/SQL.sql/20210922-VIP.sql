create database VIPsystem
use VIPsystem
go
create table Mallinformation--商场信息
(
  MallNum int primary key not null,
  MallName nvarchar(20) not null,
  Malladdress nvarchar(80),
  LegalPerson nvarchar(10),
  Officetelephone nvarchar(20)
)

create table  VIPinformation--会员信息
(
 VipNum int primary key not null,
 VipName nvarchar(10) not null,
 VipSex char(2) not null,
 VipBirth nvarchar(20) not null,
 VipTel nvarchar (20) not null,
 VipAddress nvarchar(80) not null
)

create table  VIPCardinformation--会员卡信息
(
 VipCardNum int primary key not null,
 VipName nvarchar(10) not null,
 VipRegisterTel nvarchar (20) not null,
 CardAddress nvarchar(80) not null,
 RegisterDate nvarchar(80) not null,
 CardTypeNum nvarchar(10) not null
)

create table  CardTypeinformation--会员卡类型信息
(
 VipCardNum int primary key not null,
 TypeName nvarchar(10) not null
)

create table  Shopsinformation--商铺信息
(
  ShopsNum int primary key not null,
  ShopsName nvarchar(10) not null,
  ShopsAddress nvarchar(20) not null
)

create table Vipintegralinformation--会员积分信息
(
  VipintegralNum int primary key not null,
  ID int foreign key references VIPCardinformation(VipCardNum) not null,
)

create table VipConsumptioninformation--会员消费信息
(
 ConsumptionNum int primary key not null,
 ID int foreign key references VIPCardinformation(VipCardNum) not null,
 ShopsID int foreign key references Shopsinformation(ShopsNum) not null,
)

create table VipCarinformation--会员车辆信息
(
  CarNum int primary key not null,
  ID int foreign key references VIPCardinformation(VipCardNum) not null,
  LicensePlateNumber nvarchar(20) not null,
)

create table VipActivityinformation--会员活动信息
(
  ActivityNum nvarchar(10) primary key not null,
  ActivityName nvarchar(20) not null,
  StartTime nvarchar(50) not null,
  OvertTime nvarchar(50) not null,
)

create table VipReportLoseinformation--会员挂失信息
(
 ReportLoseNum nvarchar(10) primary key not null,
 ReportLoseID int foreign key references VIPCardinformation(VipCardNum) not null,
 ReportLoseName int foreign key references VIPinformation(VipNum) not null,
)
