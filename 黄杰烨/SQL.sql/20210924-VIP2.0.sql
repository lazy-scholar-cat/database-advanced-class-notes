create database VIPsystem
use VIPsystem
go
create table Mallinformation--商场信息
(
  MallID int primary key not null,
  MallName nvarchar(20) not null,
  Malladdress nvarchar(80),
  LegalPerson nvarchar(10),
  Officetelephone nvarchar(20)
)

create table  VIPinformation--会员信息
(
 VipID nvarchar(10) primary key not null,
 VipName nvarchar(10) not null,
 VipSex char(2) not null,
 VipBirth nvarchar(20) not null,
 VipTel nvarchar (20) not null,
 VipAddress nvarchar(80) not null
)

create table  VIPCardinformation--会员卡信息
(
 VipCardID int primary key not null,
 VipName nvarchar(10) not null,
 VipRegisterTel nvarchar (20) not null,
 CardAddress nvarchar(80) not null,
 RegisterDate nvarchar(80) not null,
 CardTypeNum nvarchar(10) not null
)

create table  CardTypeinformation--会员卡类型信息
(
 VipCardID int primary key not null,
 TypeName nvarchar(10) not null
)

create table  Shopsinformation--商铺信息
(
  ShopsID int primary key not null,
  ShopsName nvarchar(10) not null,
  ShopsAddress nvarchar(20) not null
)

create table Vipintegralinformation--会员积分信息
(
  VipintegralID int primary key not null,
  ID int foreign key references VIPCardinformation(VipCardID) not null,
)

create table VipConsumptioninformation--会员消费信息
(
 ConsumptionID int primary key not null,
 ID int foreign key references VIPCardinformation(VipCardID) not null,
 ShopsID int foreign key references Shopsinformation(ShopsID) not null,
)

create table VipCarinformation--会员车辆信息
(
  CarID int primary key not null,
  ID int foreign key references VIPCardinformation(VipCardID) not null,
  LicensePlateNumber nvarchar(20) not null,
)

create table VipActivityinformation--会员活动信息
(
  ActivityID nvarchar(10) primary key not null,
  ActivityName nvarchar(20) not null,
  StartTime nvarchar(50) not null,
  OvertTime nvarchar(50) not null,
)

create table VipReportLoseinformation--会员挂失信息
(
 ReportLoseID nvarchar(10) primary key not null,
 ReportLoseNum int foreign key references VIPCardinformation(VipCardID) not null,
 ReportLoseName nvarchar(10) foreign key references VIPinformation(VipID) not null,
)

insert into Mallinformation values
(1,'万达广场','龙岩','王健林','0599-666666'),
(2,'大悦城','龙岩','陈羿同','0599-888888'),
(3,'万象城','泉州','崔凯','0599-999999')

insert into VIPinformation values
('v001','亚索','男','2014-01-09','123456','居无定所'),
('v002','凯隐','男','2017-07-18','234567','均衡教派'),
('v003','盖伦','男','2010-04-27','345678','德玛西亚')

insert into VIPCardinformation values
(1,'亚索','123456','万达广场','2020-08-20','T01'),
(2,'凯隐','234567','大悦城','2020-06-23','T02'),
(3,'盖伦','345678','万象城','2020-08-10','T03')

insert into CardTypeinformation values
('T01','储蓄卡'),
('T02','IC卡'),
('T03','磁条卡')

insert into Shopsinformation values
(1,'D&R','万达广场1-4号'),
(2,'LOUIS VUITTON','大悦城4-3号'),
(3,'BVLGARI','万象城2-2号')