create database one
use one
go

create table tmpRank
(
 renId int primary key not null,
 keId int not null,
 Score int 
)

go
create trigger tr_TmpRankForInsert

on tmpRank
after insert
as
begin
	select * from inserted
	select * from deleted
end
go

insert into tmpRank (renId,keId,Score) values (1,11,99)
go

create trigger tr_TmpRankForDelete

on tmpRank
after delete
as
begin
	select * from inserted
	select * from deleted
end
go
update tmpRank set Score=9999 where keId=11 and renId=1

delete from tmpRank where keId=11 and renId=1
go

create trigger tr_TmpRankForUpdate
on tmpRank 
after update
as 
begin 
	select * from inserted
	select * from deleted
end
go

alter trigger tr_TmpRankForInsert
on tmpRank
instead of insert
as
begin
    set nocount on
	declare @score int ,@renId int,@keId int
	select @RenId=renId,@KeId=keId, @score=Score from inserted
	if @score>=0 and @score<=100
		begin
			insert into tmpRank (renId,keId,Score) values (@RenId,@KeId,@score)
		end
	else
		begin
			print '成绩不在0到100的范围内，请确认后重试'
		end

	set nocount off
end
go
insert into tmpRank (renId,keId,Score) values (1,11,99)

select * from tmpRank










