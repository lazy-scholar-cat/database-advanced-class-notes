use CPAC
go
select * from Area
GO
create function fn_A
(
@A nvarchar(80)
)
returns table
as
return
(
with CTE_A
as
(
	select a.Id,a.Code,a.AreaName,a.ParentCode from Area a where a.AreaName=@A
	union all
	select b.Id,b.Code,b.AreaName,b.ParentCode from Area b ,CTE_A c where b.ParentCode = c.Code
)
select * from CTE_A
)
go
select * from dbo.fn_A('����')