use master
go

create database VIPSystem
go

use VIPSystem
go

create table MarketInfo
--商场信息
--ID 商场名称 地址 商场注册法人 电话号码
(
	MarketID int primary key identity,
	MarketName nvarchar(20) not null,
	Sit nvarchar(50) not null,
	MarketLegalPerson nvarchar(20) not null,
	telephone nvarchar(20)
)

create table VIPInfo
--会员信息
--ID 姓名 性别 电话号码 地址
(
	VIPID int primary key identity, 
	Name nvarchar(20),
	sex nvarchar(20) not null,
	telephone nvarchar(20),
	Sit nvarchar(50) not null
)

create table VIPCardInfo
--会员卡信息
--会员卡编号 会员卡类型编号
(
	VIPCardID int primary key identity,
	VIPCardTypeID int not null
)

create table ShopInfo
--商铺信息
--商铺编号 商铺名称 商铺租赁人 商铺租赁日期 租赁到期时间
(
	ShopID int primary key identity,
	ShopName nvarchar(20) not null,
	Leaseholder nvarchar(20) not null,
	DateOfTheLease nvarchar(20) not null,
	DateDue nvarchar(20) not null
)

create table VIPGradeInfo
--会员卡信息
--会员卡ID 等级 升级所需经验 权限
(
	VIPID int primary key identity,
	Grade int not null,
	ToUpgrade int not null,
	VIPPower int not null
)

create table CommodityInfo
--商品信息CommodityInfo
--商品编号CommodityID 商品名称 商品类型编号 商品出产日期 商品保质期
(
	CommodityID int primary key identity,
	CommodityName nvarchar(20) not null,
	CommodityTypeID int not null,
	CommodityFromTheDate nvarchar(20) not null,
	CommodityExpirationDate nvarchar(20) not null
)

-------------插
-------------入
-------------数
-------------据

insert into MarketInfo values
--商场信息
--ID 商场名称 地址 商场注册法人 电话号码
('普通药水商场','闽西职业技术学院','蔡雪强','10086'),
('中级武器商场','瓦洛兰大陆','蔡雪弱','10081'),
('传说宠物商场','提瓦特大陆','强雪蔡','10082')

insert into VIPInfo values
--会员信息
--ID 姓名 性别 电话号码 地址
('黄杰烨','男','10010011','421'),
('段嗣凯','男','20020022','124'),
('刘建峰','男','30030033','244'),
('陈浩宇','男','40040044','444')

insert into VIPCardInfo values
--会员卡信息
--会员卡编号 会员卡类型编号
(1),
(2),
(3)

insert into ShopInfo values
--商铺信息
--商铺编号 商铺名称 商铺租赁人 商铺租赁日期 租赁到期时间
('氧气店','亚索','公元某年','西元元年'),
('血压店','永恩','公元某年','西元一年'),
('鸭血店','孤儿','公元某年','西元二年')

insert into VIPGradeInfo values
--会员卡信息
--会员卡ID 等级 升级所需经验 权限
(1,999999999,1),
(1,999999999,1),
(1,999999999,1)

insert into CommodityInfo values
--商品信息CommodityInfo
--商品编号CommodityID 商品名称 商品类型编号 商品出产日期 商品保质期
('打火机牌火柴',01,'2035','一年'),
('单轮飞蝶',01,'2021','一秒'),
('自主飞行黑板',01,'2044','一瞬间')


select * from MarketInfo
select * from VIPInfo
select * from VIPCardInfo
select * from ShopInfo
select * from VIPGradeInfo
select * from CommodityInfo
--use master
--drop database VIPSystem
--谨慎删库（不过删了也就删了）