# 2021-10-05 天气：晴 心情：就那样
# 存储过程（输出）

```sql
--课堂笔记（老师代码和自己理解）

--存储过程相对完整的语法
--通常中括号“[]”代表可选、而尖括号“<>”代表必填

create proc <存储过程名称>
[
	@XXXX 数据类型 [=默认值]
]
as
begin
	SQL语句
end

--存储过程的返回值
--语法
create proc <存储过程名称>
[
	@XXXX 数据类型 [=默认值] output
]
as
begin
	sql语句
end

go

--单个返回值
create proc proc_StudentInfo
@code	nvarchar(80) output
as
begin
	select @code  --输出“www.baidu.com”
	select @code=StudentCode from StudentInfo where Id=1
	select @code  --输出“01”
end

go

declare @anyCode nvarchar(80)='www.baidu.com'
exec proc_StudentInfo @anyCode output
select @anyCode	  --@anyCode 后面不加output 则输出“www.baidu.com”
				  --@anyCode 后面加上output 则输出“01”

理解：可以把这个看成java或C#中的方法传参。
而output，就像是编程语言的out、ref.(详情参考C#学习,5.13的笔记。)

补充：@anyCode 后面也要加output。

go

--修改存储过程语法
alter proc proc_StudentInfo
@code nvarchar(80) output
as
begin
	select @code
	select @code=StudentCode from StudentInfo where Id=1
end

go

--删除存储过程
--错误的删除命令
delete from proc_StudentInfo
go
--正确的删除存储过程的命令
drop proc proc_StudentInfo
go

--多个返回值
create proc proc_StudentInfo
@code nvarchar(80) output,
@name nvarchar(80) output
as
begin
	select @code,@name
	select @code=StudentCode,@name=StudentName from StudentInfo where Id=1
	select @code,@name
end

go
```

# 作业
## 题目
```sql
 如何执行一个标量函数
 --自定函数之标量函数 语法
create function <方法名称>
(--括号不能忽略
	@XXXX 数据类型 [=默认值], --内容可有可无
	@YYYY 数据类型 [=默认值]
)
returns int --表明返回会值是一个int的值
as
begin
	declare @result int
	set @result=1
	return @result
end
go

--实行代码
create function fn_GetInt
(
	@code nvarchar(80)
)
returns nvarchar (3)--表明返回会值是一个nvarchar的值，括号里如果不填数值默认为1个字符串
as
begin
	declare @result nvarchar(80)
	select @result=StudentName from StudentInfo
	where StudentCode=@code
	return @result
end
go

select dbo.fn_GetInt('01')

 下面这个对不对？
select fn_GetInt()
对，但不全对。因为缺少dbo这个前缀，所有会运行不了
```
https://www.cnblogs.com/viusuangio/p/6212072.html