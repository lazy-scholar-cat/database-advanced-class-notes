--CTE应用
;with CTE_A
as
(
	select ID,code,AreaName,parentcode from Area where AreaName='四川'
	union all
	select a.Id,a.Code,a.AreaName,a.parentcode from Area a,CTE_A c
	where a.ParentCode=c.Code
)select*from CTE_A
go

alter function fn_AreaInfoSelectByName(@name nvarchar(80),@level int=0)
returns table
as
return
(
	with CTE_A
	as
	(
		select id,code,areaName,parentCode,0 level from area where areaName=@name
		union all
		select a.id,a.code,a.areaName,a.parentCode,c.level+1 pp from area a,CTE_A c
		where a.parentcode=c.code
	)select*from CTE_A where level<=@level
	--如果level=@level会只查询出四川这一条信息
)
go

select*from dbo.fn_AreaInfoSelectByName('四川',0)

--触发器
语法:

create trigger <触发器名称>
on <数据表>
for--可以省略
<after | instead of><delete | insert | update>
as
begin
	sql语句
end
go

--实验代码
alter trigger tr_TmpRankForUpdate
on StudentCourseScore
after Update
as
begin
	select*from inserted --查询临时表
	select*from deleted --查询临时表
	declare @score int,@id int
	select @id=id,@score=score from inserted
	if(@score>=0 and @score<=100)
		begin
			update StudentCourseScore set score=@score where id=@id
		end
	else
		begin
			update StudentCourseScore set score=0 where id=@id
		end
end
go

select*from StudentCourseScore

update StudentCourseScore set Score=-2 where id=1