--	CTE递归及多语句表值函数
select*from Area --国省市县区数据表

create database ChinaInfo --新建国家省市县区信息库
go

--CTE递归
;with CTE_A
AS
(
	select Id 编号,AreaName 名称,ParentCode 所属城市编号 from Area
	union all
	select a.Id,a.AreaName,a.ParentCode from Area a,CTE_A b 
	where a.Id=b.所属城市编号
)select*from CTE_A

CTE递归
1.递归查询原理满足三个条件：a.初始条件，b.递归调用表达式，c.终止条件

--多语句表值函数语法
create function <函数名称>
(
	[
		@sss 数据类型 [=默认值],
		@xxx 数据类型 [=默认值]
	]
)
returns @res table (Id int,Name nvarchar(80))
as
begin
	return
end
go

create function fn_StudentInfoSelectByName(@name nvarchar(80))
returns @res table (Id int,Name nvarchar(80))
as
begin
	declare @x nvarchar(80)
	insert into @res (Id,Name) select id ,studentname from StudentInfo
	where StudentName=@name
	select @x=name from @res
	delete from @res
	insert into @res(Id,Name) select id,studentname from StudentInfo


	return
end
go
select*from dbo.fn_StudentInfoSelectByName('赵雷')