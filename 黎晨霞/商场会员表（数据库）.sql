drop database MallMembers --删库

use master
go 
create database MallMembers
go

use MallMembers
go

create table Users            --用户表
(
  Username nvarchar(50) not null unique,
  Password nvarchar(50) not null,
)

create table StoreInformation      --商场信息表
(
  StoId nvarchar(20) primary key,                                   --商场编号
  StoName nvarchar(20) not null,                                    --商场名称
  StoForShort nvarchar(20),                                         --商场简称
  StoSite nvarchar(80) not null,                                    --商场地址
  StoPrincipal nvarchar(20) not null,                               --商场负责人
  StoPrincipalPhone varchar(11) check(len(StoPrincipalPhone)=11)    --负责人电话
)
create table MemberMessage        --会员信息表
(
  MemId nvarchar(20) primary key,                                         --会员编号
  MemName nvarchar(20) not null,                                          --会员姓名
  MemSex char(2) DEFAULT('男')check(MemSex='男'or MemSex='女') not null,  --性别
  MemDateOfBirth date not null,                                           --出生年月
  MemPhone varchar(11) check(len(MemPhone)=11),                           --联系电话
  MemSite nvarchar(80) not null                                           --家庭住址
)
create table MembershipCardType    --会员卡类型信息      
(
  MetId nvarchar(20) primary key,                                         --会员卡类型编号
  MetName nvarchar(20) not null,                                          --类型名称
  MetText nvarchar(80),                                                   --类型介绍
)
create table MemberClass              --会员等级信息表
(
  MclId nvarchar(20) primary key,                                         --VIP等级编号
  MclNamr nvarchar(20),                                                   --等级名称
  MclGrowUp nvarchar(80)                                                  --等级成长
)
create table MembershipCard        --会员卡信息
(
  MecId nvarchar(20) primary key,                                         --会员卡编号
  MecDate date not null,                                                  --开卡日期
  MecSite nvarchar(80) not null,                                          --开卡地点
  MecBalance money not null,                                              --卡上余额
  MecMemId nvarchar(20) references MemberMessage(MemId),                  --会员编号  
  MecMetId nvarchar(20) references MembershipCardType(MetId),             --会员卡类型编号
  MecMclId nvarchar(20) references MemberClass (MclId),                   --会员等级编号
)
create table ShopInformation        --商铺信息
(
  ShoId nvarchar(20) primary key,                                         --商铺编号
  ShoName nvarchar(20) not null,                                          --商铺名称
  ShoSite nvarchar(80) not null,                                          --商铺地址
  ShoText nvarchar(80) not null,                                          --商铺介绍
  ShoPrincipal nvarchar(20) not null,                                     --商铺负责人
  ShoPrincipalPhone varchar(11) check(len(ShoPrincipalPhone)=11)          --负责人电话
)
create table MemberPointsInformation  --会员积分信息
(
  MpiId int primary key identity(1,1),                                    --会员积分信息编号
  MpiMemId nvarchar(20) references MemberMessage(MemId),                  --会员编号
  MpiMecId nvarchar(20) references MembershipCard(MecId),                 --会员卡编号
  MpiIntegral NVARCHAR(20)                                                --卡上积分 
)
create table ActivityInformation     --会员活动信息
(
  AciId int primary key identity(1,1),                                    --活动信息编号
  AciMemId nvarchar(20) references MemberMessage(MemId),                  --会员编号
  AciText nvarchar(80)                                                    --活动介绍
)
create table VehicleInformation      --会员车辆信息
(
  VeiId int primary key identity(1,1),                                    --会员车辆信息编号
  VeiMemId nvarchar(20) references MemberMessage(MemId),                  --会员编号
  VeiLicensePlateNumber nvarchar(20) not null,                            --会员车牌号
  VeiEntryTime datetime,                                                  --进入时间
  VeiDepartureTime datetime                                               --离开时间
)
create table MissingRecord           --会员卡挂失记录信息
(
  MisId int primary key identity(1,1),                                    --挂失信息编号
  MisMemId nvarchar(20) references MemberMessage(MemId),                  --会员编号
  MisMecId nvarchar(20) references MembershipCard(MecId),                 --挂失卡号
  MisDate  date ,                                                         --挂失时间    
  MisEliminate char(2) DEFAULT('是')check(MisEliminate='是'or MisEliminate='否') not null  --挂失是否消除
)
create table CreditsExchange        --会员积分兑换表
(
  CreId int primary key identity(1,1),                                    --积分兑换表编号
  CreMemId nvarchar(20) references MemberMessage(MemId),                  --会员编号 
  CreMecId nvarchar(20) references MembershipCard(MecId),                 --会员卡号
  CreTime int not null,                                                   --兑换次数
  CreShoId nvarchar(20) references ShopInformation(ShoId),                --兑换商铺编号 
  CreGoods nvarchar(80)                                                   --兑换物品
)

--表查询
select*from StoreInformation      --商场信息表
select*FROM MemberMessage         --会员信息表
select*from MembershipCardType    --会员卡类型信息
select*from MemberClass           --会员等级信息表
select*from MembershipCard        --会员卡信息
select*from ShopInformation       --商铺信息
select*from MemberPointsInformation  --会员积分信息
select*from ActivityInformation   --会员活动信息
select*from VehicleInformation    --会员车辆信息
select*from MissingRecord         --会员卡挂失记录信息
select*from CreditsExchange       --会员积分兑换表
select*from Users             --用户表

--数据插入
insert into StoreInformation      --商场信息表
values ('SM0001','万达广场','万达','龙岩市新罗区','张三','18725242258'),
       ('SM0002','宝龙城市广场','宝龙','福州市仓山区','李四','15225244597')

insert into MemberMessage         --会员信息表
values ('vip001','赵武','男','2001-01-12','13251645572','龙岩市新罗区'),
       ('vip002','王麻子','男','2001-08-08','15214569987','龙岩市新罗区'),
	   ('vip003','王狗蛋','男','2000-08-10','15484563354','福州市仓山区'),
       ('vip004','周雨','女','2000-09-20','12535497704','福州市仓山区')

insert into MembershipCardType    --会员卡类型信息
values ('TP0001','购物卡','只能用来购物的卡'),
       ('TP0002','电玩卡','只用于电玩城游玩的卡'),
	   ('TP0003','一卡通','商场里的万能通用卡，任何商场里的每个店都能用'),
       ('TP0004','吃货卡','吃货专用卡，用于各个商场的美食店')

insert into MemberClass           --会员等级信息表
values ('CL0001','青铜','消费1000元'),
       ('CL0002','白银','消费100000元'),
	   ('CL0003','黄金','消费10000000元'),
       ('CL0004','至尊黑卡','消费100000000元')

insert into MembershipCard        --会员卡信息
values ('CO0001','2019-05-20','万达','1000','vip001','TP0001','CL0003'),
       ('CO0002','2018-03-11','万达','500','vip002','TP0002','CL0001'),
	   ('CO0003','2020-11-11','宝龙','800','vip003','TP0004','CL0002'),
       ('CO0004','2019-12-12','宝龙','99999999999','vip004','TP0003','CL0004')

insert into ShopInformation       --商铺信息
values ('SH0001','大玩家电玩城','万达三楼305','没有介绍就是个电玩城','班尼特','15821512230'),
       ('SH0002','万民堂','万达四楼305','没有介绍就是个吃饭的地方','香菱','15425878222'),
	   ('SH0003','永辉超市','万达负一楼','没有介绍就是个超市','王五','15243698872'),
       ('SH0004','新华都','宝龙负一楼','没有介绍就是个超市','甲乙','15426358872'),
	   ('SH0005','小叫天','宝龙四楼406','没有介绍就是个吃饭的地方','丙丁','18725634562')

insert into MemberPointsInformation  --会员积分信息
values ('vip001','CO0001','111111'),
       ('vip002','CO0002','10000'),
	   ('vip003','CO0003','50000'),
       ('vip004','CO0004','99999999999999999')

insert into ActivityInformation   --会员活动信息
values ('vip001','中秋节大甩卖'),
       ('vip002','国庆促销'),
	   ('vip003','中秋节大甩卖'),
       ('vip004','七夕促销')

insert into VehicleInformation    --会员车辆信息
values ('vip001','闽A5425','2021-09-11 ','2021-09-11 '),
       ('vip002','闽A4566','2021-09-05 ','2021-09-05 '),
	   ('vip003','闽A3455','2021-08-11 ','2021-8-11 '),
       ('vip004','闽A3545','2021-07-11 ','2021-07-11 ')

insert into MissingRecord         --会员卡挂失记录信息
values ('vip003','CO0003','2021-09-12 ','否')

insert into CreditsExchange       --会员积分兑换表
values ('vip001','CO0001','1 ','SH0003','200元代金券'),
       ('vip002','CO0002','1 ','SH0001','300元电玩城畅玩券'),
       ('vip003','CO0003','1 ','SH0005','7.5折打折券'),
       ('vip004','CO0004','1 ','SH0002','一年免单券')


--登录

--用户登录，查看数据库中该用户是否存在，存在，则进入下一步，不存在，则进入注册阶段。

select * from UserTable where UserId='admin' and Password='123' 
--由于是后端设计，故where后面的条件是固定的。 

--当登录用户存在已被注销、禁用情况
  --1.先删除数据库中被注销、禁用的用户
    delete from UserTable where UserId='admin'
  --2.删除后重新注册用户
    insert into UserTable
	values('用户名','密码')

--用户登录时忘记密码，可通过用户名找回密码
    select*from UserTable where UserId='admin'

--注册

--注册分两种注册，一种是单个注册，一种是批量注册（看当时的业务条件需要哪个）

--单个注册
insert into UserTable 
values('admin','123')
--批量注册
insert into UserTable
values('user01','123'),
      ('user02','123'),
	  ('user03','123'),
      ('user04','123'),
	  ('user05','123'),
      ('user06','123')


--用户注册逻辑

--1.判断当前注册的用户名是否存在，如果存在则返回注册失败；反之，则注册成功
--可以使用unique语句来判断

	alter table UserTable add  constraint UQ_UserID unique(UserID)

--2.判断重复输入的密码是否一样，如果不一样，则重新输入；
--反之，注册成功，在数据表中添加一条数据
    --1.先判断密码是否相同
	 select*from UserTable where  Password='123'
	 --相同则，插入数据
	 insert into UserTable
	 values('用户名','123')
	 --不相同，则回到注册界面重新注册



--关于商场信息的使用场景

--1.在会员信息管理的时候，需要选择商场
select StoId,StoName,StoForShort from StoreInformation

--2.在会员卡类型管理时，需要选择商场
select StoId,StoName,StoForShort from StoreInformation

--3.商场信息本身的CRUD（create read update delete，补充：CRUD操作：create创建 read读取 update修改 delete删除）
    --添加
    insert into StoreInformation
    values ('SM0003','泰禾广场','泰禾','福州晋安区','隔壁老王','12542642245')
	--读取
	select*from StoreInformation
	--修改
	update StoreInformation set StoPrincipal='老王'
	--删除
	delete from StoreInformation where StoId='SM0001'

--关于会员信息的使用场景

--1.当有会员注册时，要登记会员信息
   insert into MemberMessage        
   values ('vip001','赵武','男','2001-01-12','13251645572','龙岩市新罗区')

--2.当有会员查询积分时，要使用会员信息
  select*from MemberPointsInformation where MpiMemId='vip001'

--3.当会员卡挂失时，要使用会员信息
  select*from MissingRecord where MisMemId='vip003'

--4.会员信息表本身的CRUD
    --添加
    insert into MemberMessage
    values ('vip001','赵武','男','2001-01-12','13251645572','龙岩市新罗区')
	--读取
	select*from MemberMessage
	--修改
	update MemberMessage set MemName='老王'
	--删除
	delete from MemberMessage where MemId='vip001'

--20210930笔记：

/*
-- 2. 判断密码和重复密码是否一致，是则可以注册，并在相应数据表中插入一条记录，否则返回失败信息
declare @username nvarchar(80) --前端传进来的用户名
declare @password nvarchar(80),@cofirmPassword nvarchar(80) --前端传进来的密码
declare @rowpassword nvarchar(80) --指通过用户名查询到记录后，这个代表用户记录中的密码
declare @tmpTable table (id int,Username nvarchar(80),Password nvarchar(80)) --定义了一个表，可有可无
declare @count int --变量
declare @res nvarchar(80) --接收结果的变量

select @count=count(*),@rowPassword=Password from Users where Username=@username --查询数据

if(@count>0) --当这个变量大于零时
	begin
	   set @res='当前用户名已被注册，请重试！'
	end
else
	begin
	   if(@password=@cofirmPassword) --两次输入的密码一致是
			begin
			    insert into Users (Username,Password) values(@username,@password) --将前端传进来的数据插入用户表中
				set @res='用户注册成功！'
			end
	   else
			begin
			  set @res='两次输入的密码不一致，请重试！'
			end
	end
*/


-- 更进一步，就是当前登录的用户可能已经被注销、禁用等情况，如何应对

-- 1.根据提供的用户名（这里假定用户名不能重复），查询出满足条件的记录

-- 2.判断查询到的记录的状态（如之前提过的IsActived、IsDeleted等等，也可以给用户增加额外的状态字段），给出已经被注销、已经被禁用、已经被删除等提示

-- 3.如果这条记录没有被注销、禁用、删除的情况，最后判断密码是不是和查询到的这条记录匹配，是则登录成功，否则提示“用户或者密码错误”

declare @username nvarchar(80),@password nvarchar(80),@rowPassword nvarchar(80) --前端传进来的用户名、密码和确认密码
declare @isDeleted bit ,@isActived bit --定义删除和禁用的字段
declare @count int --变量
declare @res nvarchar(80) --接收结果的变量

select @count=count(*),@isDeleted=1,@isActived=0,@rowPassword=Password from Users
where Username=@username group by password --查询数据

-- 如果根据提供的用户名，找到的记录数不为零，则表明用户名是对的，可以继续往下判断；否则直接提示用户名或密码错误
if(@count>0)
	begin
		if(@isDeleted=1)--如果删除标记为真，则表明当前用户已经被删除，作出提示；否则用户未被删除
			begin
				set @res='当前用户已被删除，请联系管理员！'
			end
		else
			begin
				if(@isActived=0)--如果当前用户未启用/激活，则提示信息用户未启用，登录失败；否则用户已经禁用
					begin
						set @res='当前用户已禁用，请联系管理员！'
					end
				else
					begin
						if(@password=@rowPassword)--如果传进来的密码和记录中获取的密码相同，则认为登陆成功，否则登陆失败。
							begin
								set @res='登陆成功'
							end
						else
							begin
								set @res='用户名或密码错误，请重试！'
							end
					end
			end
	end
else
	begin
		set @res='用户名或密码错误，请重试！'
	end
/*

*/

/*
--怎么样可以不让用户提交纯空格的用户名和密码
declare @username nvarchar(80),@password nvarchar(80),@cofirmPassword nvarchar(80) --前端传进来的用户名、密码和确认密码
declare @rowpassword nvarchar(80)
declare @count int
declare @res nvarchar(80)

select @count=count(*),@rowpassword=Password from Users where Username=@username 
group by Password
if(@count>0)
	begin
		if(CHARINDEX(' ',@username) > 0 or CHARINDEX(' ',@Password) > 0)
			begin
				set @res='用户名和密码，不允许有空格！请重新输入！'
			end
		else
			begin
				if(@password=@cofirmPassword) --两次输入的密码一致是
			begin
			    insert into Users (Username,Password) values(@username,@password) --将前端传进来的数据插入用户表中
				set @res='用户注册成功！'
			end
	   else
			begin
			  set @res='两次输入的密码不一致，请重试！'
			end
			end
		end
else
	begin
		set @res='用户名或密码错误，请重试！'
	end
*/