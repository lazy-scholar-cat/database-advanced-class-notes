--存储过程语法（输入）
/*

格式：

create proc 存储过程名称
as
begin

end
*/

--课上问题
--1.关于存储过程名称的命名规范（或要求）
--2.关于存储参数有没有上限

--课后作业
--预习存储过程的输出

--课上笔记（老师代码，复习）

select*from StudentInfo --查询学生信息表
go  --事务

--查询学生信息表（无参数的存储过程）
create proc proc_SelectStudentInfo
as
begin
   select*from StudentInfo  --我们要做的事情
end
go

exec proc_SelectStudentInfo --执行语句
go


--模糊查询（有参数的存储过程）
create proc proc_SelectStudentInfoWithPrameter
@name nvarchar(80)         --前端传来的数据
as
begin
	select*from StudentInfo
	where StudentName like @name  --模糊查询姓名与前端数据相同的同学
end
go

exec proc_SelectStudentInfoWithPrameter '%李%' --执行模糊查询，查询姓李的同学
go


--有多个参数的存储过程
create proc proc_SelectStudentInfoWithSomeParameters
@name nvarchar(80),
@code nvarchar(80)
as
begin
	select*from StudentInfo
	where StudentName like @name or StudentCode like @code --模糊查询姓名或者学号与前端数据相同的同学
end
go

exec proc_SelectStudentInfoWithSomeParameters '%李%','01'
go

--有默认值的存储过程
create proc proc_SelectStudentInfoWithAnyParameters
@code nvarchar(80)='01',
@name nvarchar(80)='%李%',
@birthday date='2020-10-02',
@sex char='m',--'m'是男,'f'是女
@classId int='1'
as
begin
	select*from StudentInfo
	where StudentCode like @code
	or StudentName like @name
	or Birthday <@birthday
	or sex =@sex
	or ClassId=@classId
end
go

exec proc_SelectStudentInfoWithAnyParameters
--补充点：1.如果是有默认值的存储过程，必须每项参数都要填上默认的参数值
--        2.问题：“为什么date（日期）可以用'2020-10-02'这样的字符串？”
--          回答：因为数据库后台会进行转换（简单来说，date（日期）和字符串之间是隐性转换。）
--注意：  在执行存储语句时，小心不要将执行语句一起执行。（容易报错，报错内容和错误示范如图。）