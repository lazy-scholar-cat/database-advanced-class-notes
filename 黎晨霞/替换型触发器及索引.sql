��������
1.inserted
2.deleted

--����ʱ
go
create trigger tr_StudentInfoForInsert
on studentInfo --�����ű����в���
after insert   --ִ��ʲô����
as
begin		   
	select*from inserted  
	select*from deleted
end
go

insert into StudentInfo (StudentCode,StudentName,ClassId,Birthday)
values(22,'������',2,'2001-01-22')
(inserted��Ϊ�����²�������ݣ�deleted��Ϊ��)

--����ʱ
go
create trigger tr_StudentInfoForUpdate
on studentInfo
after update
as
begin
	select*from inserted
	select*from deleted
end
go

update StudentInfo set ClassId=22 
where StudentCode=22 and StudentName='������'

(inserted�������Ǹ��º�������deleted�������Ǹ���ǰ�������)

--ɾ��ʱ
go
create trigger tr_StudentInfoForDelete
on studentInfo
after delete
as
begin
	select*from inserted
	select*from deleted
end
go

delete from StudentInfo where StudentCode=22 and StudentName='������'
(inserted��Ϊ�գ�deleted��������ɾ��ǰ�������)

�ܽ᣺
				insert				update					delete

inserted  �²���ļ�¼���		���º�ļ�¼���			  ��

deleted			  ��			����ǰ�ļ�¼���		ɾ��ǰ�ļ�¼���


������֮instead of

go
alter trigger tr_StudentCourseScore
on StudentCourseScore
instead of insert
as
begin
	set nocount on
	declare @score int ,@studentId int,@courseId int
	select @studentId=StudentId,@courseId=CourseId,@score=Score	 from inserted
	if @score>=0 and @score<=100
		begin
			insert into StudentCourseScore (StudentId,CourseId,Score)
			values(@studentId,@courseId,@score)
		end
	else
		begin
			print '�ɼ�����0��100�ķ�Χ�ڣ���ȷ�Ϻ����ԣ���'
		end

	set nocount off
end
go

insert into StudentCourseScore (StudentId,CourseId,Score)
values(22,222,20)
select*from StudentCourseScore
set nocount on

�ܽ᣺
instead of ���滻ִ�� insert update delete�е���䡣
���仰��˵����ɾ����instead of���ڵ�ʱ�򣬲�����������ִ�У�����ִ�е���ʵ�Ǳ��������Ǹ���������


create table UserInfo --����һ���û���
(
	Id int primary key identity(1,1),
	UserName nvarchar(20) not null,
	sex nchar(1) not null,
	birthday datetime not null,
)
go

-- �Զ��庯���в���ʹ��rand()��������������ͼ������������֡�
create view v_randNanem -- ����������ִ���2������Ϊ�����֣�����Ϊ������
as
select re=cast(rand()*10as int)
go

create view v_randSex -- �Ա� 0Ϊ��1ΪŮ
as
select re=cast(rand()*1000as int)%2
go

create view v_dateTime -- ���ʱ�䣨��120�꣩
as
select re=cast(checksum(newId())%(365*120) as datetime)+rand()
go

create view v_randFName -- ����
as
select re=cast(ceiling(rand()*(len('��������������������������������ֺθ���֣����л�ƺ��������������̲�����Ԭ�ڶ�����Ҷ��κ����Ŷ��򽪷�������¬��������½��Ҧ��������̷Τ����ʯ��������Ѧ���װ����κ¿���ʷë��������俵������Ǯʩţ�鹨��'))) as int)
go

create view v_randLName -- ����
as
select re=cast(ceiling(rand()*(len('����ά�������������󳿳�ʿ�Խ���������ʢ��衾��ڲ�����ŷ纽���������ɱ򸻺��������ʱ̩��������־��������±�ΰ�����㿡��ǿ��ƽ�����Ļ������������㺣ɽ�ʲ���������Ԫȫ��ʤѧ��ŷ�������˳���ӽ��β��ɿ��ǹ���ﰲ����ï�����г��Ⱦ�����׳��˼Ⱥ���İ�����ܹ����ƺ�ԣ������������������ܿ�ƺ������˿ɼ���Ӱ��֦˼�����Ӣ���������Ⱦ���������֥��Ƽ�����ҷ���ʴ����������������÷���������滷ѩ�ٰ���ϼ����ݺ�����𷲼Ѽ������Ҷ�������������ɺɯ������ٻ�������ӱ¶������������Ǻɵ���ü������ޱݼ���Է�ܰ�������԰��ӽ�������ع���ѱ�ˬ������ϣ����Ʈ�����'))) as int)
go

alter function func_UserInfoInsert
(
	@num int -- �û�������ٸ������ɶ��ٸ�
)
returns --����һ��������ʱ���ı�
@result table (
	id int primary key identity(1,1),
	UserName nvarchar(20),
	sex nchar(1),
	birthday datetime
)
as
begin
	declare @i int,@x int,@y int,@j int,@name nvarchar(12),@FName nvarchar(300),@LName nvarchar(300),@sex nchar(1),@date datetime
	set @i=0
	set @FName='��������������������������������ֺθ���֣����л�ƺ��������������̲�����Ԭ�ڶ�����Ҷ��κ����Ŷ��򽪷�������¬��������½��Ҧ��������̷Τ����ʯ��������Ѧ���װ����κ¿���ʷë��������俵������Ǯʩţ�鹨��'
	set @LName='����ά�������������󳿳�ʿ�Խ���������ʢ��衾��ڲ�����ŷ纽���������ɱ򸻺��������ʱ̩��������־��������±�ΰ�����㿡��ǿ��ƽ�����Ļ������������㺣ɽ�ʲ���������Ԫȫ��ʤѧ��ŷ�������˳���ӽ��β��ɿ��ǹ���ﰲ����ï�����г��Ⱦ�����׳��˼Ⱥ���İ�����ܹ����ƺ�ԣ������������������ܿ�ƺ������˿ɼ���Ӱ��֦˼�����Ӣ���������Ⱦ���������֥��Ƽ�����ҷ���ʴ����������������÷���������滷ѩ�ٰ���ϼ����ݺ�����𷲼Ѽ������Ҷ�������������ɺɯ������ٻ�������ӱ¶������������Ǻɵ���ü������ޱݼ���Է�ܰ�������԰��ӽ�������ع���ѱ�ˬ������ϣ����Ʈ�����'
	while @i<@num
		begin
			set @x=(select * from dbo. v_randNanem)
			if @x>2 -- ��������2������Ϊ������
				begin
					set @x=(select * from dbo.v_randFName)
					set @y=(select * from dbo.v_randLName)
					set @j=(select * from dbo.v_randLName)
					set @name=cast(substring(@FName,@x,1) as varchar)
					set @name=@name+cast(substring(@LName,@y,1) as varchar)
					set @name=@name+cast(substring(@LName,@j,1) as varchar)
				end
			else -- ��������Ϊ������
				begin
					set @x=(select * from dbo.v_randFName)
					set @y=(select * from dbo.v_randLName)
					set @name=cast(substring(@FName,@x,1) as varchar)
					set @name=@name+cast(substring(@LName,@y,1) as varchar)
				end
			set @x=(select * from dbo. v_randSex)
			if @x=0 -- �Ա� 0Ϊ��
				begin
					set @sex='��'
				end
			else -- 1ΪŮ
				begin
					set @sex='Ů'
				end
			set @date=(select * from dbo.v_dateTime)
			insert into @result values (@name,@sex,@date) 
			set @i=@i+1
		end
	return	
end

go

-- �ѷ��صı����ݲ����û���
insert into UserInfo(UserName,sex,birthday) select  UserName,sex,birthday from dbo.func_UserInfoInsert(10000000)

create index index_birthday
on UserInfo
(
	 UserName ASC
	
)

select * from UserInfo where sex='��' and UserName='�'