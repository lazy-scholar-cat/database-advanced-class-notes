--定义游标
declare cur_ForPerSonalInfo cursor
for
select*from PersonalInfo

--打开游标，准备使用
open cur_ForPerSonalInfo

--定义变量，用于接收每一行中的数据
declare @id int,@name nvarchar(80),@birthday date
--先获取一条，以使@@fetch_status状态初始化
fetch next from cur_ForPerSonalInfo
into @id,@name,@birthday

select @@FETCH_STATUS

--真正遍历记录的语句结构
while @@FETCH_STATUS =0
begin
	select @id,@name,@birthday --代表处理取出的每一行数据
	fetch next from cur_ForPerSonalInfo
	into @id,@name,@birthday
end

--关闭游标
close cur_ForPerSonalInfo
--释放游标
deallocate cur_ForPerSonalInfo


--事务

begin 
	begin tran
		insert into table1(id,col1,col2)
		values(1,'First row','First row');
		insert into table1(id,col1,col2)
		values(2,null,'Second row');
		insert into table1(id,col1,col2)
		values(3,'Third row','Third row');
	commit tran;
end
go
begin catch
	select
		ERROR_NUMBER()   as ErrorNumber,
		ERROR_SEVERITY() as ErrorSecerity,
		ERROR_STATE()    as ErrorState,
		ERROR_PROCEDURE()as ErrorProcedure,
		ERROR_LINE()     as ErrorLine,
		ERROR_MESSAGE()  as ErrorMessage
	raiserror ('Error in Transaction!',14,1)
rollback tran
-- 获得一个返回所有错误信息的记录和一个自定义的、指出已发生错误的信息。
-- DECLARE @er nvarchar(max)

-- SET @er = 'Error: '+ ERROR_MESSAGE();

-- RAISERROR(@er,14,1);

-- ROLLBACK TRAN

END CATCH;