--标量函数
create function <函数名称>
(
	@sss 数据类型 [=默认值],
	@ccc 数据类型 [=默认值]
)
returns 数据类型 --是returns，不是return
as
begin
	declare @result int =3
	return @result
end
go

--运行代码
create function fn_GetADecimal()
returns decimal(18,2)
as
begin
	return 18.2 -- 输出18.20
end
go
select dbo.fn_GetADecimal()
go

create function fn_GetAChar()
returns decimal(18,4)
as
begin
	return convert(decimal(18,4),'18.2')--输出18.2000，如果引号中是能转换成数据的。会自动转换。
end
go
select dbo.fn_GetAChar()
go
--表值函数的定义和使用
	--a.单语句表值函数的定义
	create function <函数名称>
	(
		[
		@sss 数据类型 [=默认值],
		@aaa 数据类型 [=默认值]
		]
	)
	returns table --返回的是表
	as
	return
	(
		select *from StudentInfo
	)
补充:可以省略begin……end
go

--运行代码1
create function fn_StudentInfoSelect
()
returns table
as
	return (select*from StudentInfo)
go
--运行代码2
create function fn_StudentInfoSelectById(@id int)
returns table
as
return 
(
	select*from StudentInfo
	where Id=@id
)
go
select*from dbo.fn_StudentInfoSelectById(2)

--公用表达式
create function fun_Get
(
	 @id int
)
returns table
as
   return
    (
		with CTE_A(姓名,课程)
			as
			(
			  select a.StudentName ,b.CourseName  from StudentInfo a
			  inner join CourseInfo b on a.id=b.Id where a.id=@id
			)select * from CTE_A 
	)
go
--调用
select * from dbo.fun_Get(2) --扔一个学生的id 查询学生姓名以及学习的课程